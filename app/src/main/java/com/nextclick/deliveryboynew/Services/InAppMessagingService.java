package com.nextclick.deliveryboynew.Services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nextclick.deliveryboynew.BuildConfig;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.activitys.DashboardActivity;
import com.nextclick.deliveryboynew.utils.PreferenceManager;

import java.util.Map;

public class InAppMessagingService extends FirebaseMessagingService {

    String order_id;
    public PreferenceManager preferenceManager;
    private static final String TAG = "MyFirebaseMsgService";

    int sounds[]={R.raw.notification_one,R.raw.notification_two,R.raw.notification_three,
            R.raw.notification_four,R.raw.notification_five,R.raw.notification_six,
            R.raw.notification_seveen};
    public static MediaPlayer mp;

    /**
     * Called when messagesp is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
    super.onMessageReceived(remoteMessage);
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());


            Log.i("DeliveryApp", "onMessageReceived");

            if (/* Check if data needs to be processed by long running job */ true) {

                scheduleJob();
                try{
                    JsonObject jsonObject = new JsonObject(); // com.google.gson.JsonObject
                    JsonParser jsonParser = new JsonParser(); // com.google.gson.JsonParser
                    Map<String, String> map = remoteMessage.getData();
                    String val;

                    for (String key : map.keySet()) {
                        val = map.get(key);
                        try {
                            jsonObject.add(key, jsonParser.parse(val));
                        } catch (Exception e) {
                            jsonObject.addProperty(key, val);
                        }
                    }
                    Log.d("json_object",jsonObject.toString());

                    try {

                        order_id = jsonObject.getAsJsonObject("data").getAsJsonObject("order").get("id").getAsString();
                    }
                    catch (Exception ex)
                    {
                        order_id="1234";
                        Log.i("DeliveryApp", "sendNotification ex "+ex.getMessage());
                    }

                    Log.i("DeliveryApp", "sendNotification order_id "+order_id);
                    sendNotification(jsonObject.getAsJsonObject("data").get("message").getAsString(),
                            jsonObject.getAsJsonObject("data").get("title").getAsString());

                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.d(TAG, "Message Notification getTitle: " + remoteMessage.getNotification().getTitle());

            scheduleJob();
            sendNotification(remoteMessage.getNotification().getBody(),
                    remoteMessage.getNotification().getTitle());

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
                .build();
        WorkManager.getInstance().beginWith(work).enqueue();
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody,String title) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("order_id",""+order_id);
        Log.i("DeliveryApp", "sendNotification order_id "+order_id);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_IMMUTABLE|PendingIntent.FLAG_UPDATE_CURRENT);

        preferenceManager=new PreferenceManager(getApplicationContext());
        try{
            int sound_position=preferenceManager.getInt("sound_position");
            //if (notification_code.equalsIgnoreCase("ODC")){
                mp = MediaPlayer.create(getApplicationContext(), sounds[sound_position]);
                mp.setLooping(true);
                mp.setVolume(20, 20);
                mp.start();

                try{
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mp != null) {
                                mp.stop();
                                mp=null;
                            }
                        }
                    }, 30000);
                }catch (Exception ignored){

                }

          //  }
        }
        catch (Exception e){
            System.out.println("aaaaaaaaa  catch noitification  "+e.getMessage());
            //if (notification_code.equalsIgnoreCase("ODC")){
                mp = MediaPlayer.create(getApplicationContext(), R.raw.notification);
                mp.setLooping(true);
                mp.setVolume(20, 20);
                mp.start();
                try{
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mp != null) {
                                mp.stop();
                                mp=null;
                            }
                        }
                    }, 30000);
                }catch (Exception e1){

                }

          //  }
        }

        String channelId = getString(R.string.default_notification_channel_id);
        final long[] VIBRATE_PATTERN    = {0, 500};
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        final Uri NOTIFICATION_SOUND_URI = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.alaram);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_nextclick_logo_balck_svg)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(NOTIFICATION_SOUND_URI)
                        .setVibrate(VIBRATE_PATTERN)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}
