package com.nextclick.deliveryboynew.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.nextclick.deliveryboynew.Services.InAppMessagingService;
import com.nextclick.deliveryboynew.Services.RestartService;


public class RestartServiceReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(RestartServiceReceiver.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, RestartService.class));
            context.startForegroundService(new Intent(context, InAppMessagingService.class));
        } else {
            context.startService(new Intent(context, RestartService.class));
            context.startService(new Intent(context, InAppMessagingService.class));
        }
  /*       context.startService(new Intent(context, RestartService.class));
         context.startService(new Intent(context, InAppMessagingService.class));*/

    }
}
