package com.nextclick.deliveryboynew.adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.fragments.BannersFragment;

public class BannersAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public BannersAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return BannersFragment.newInstance(myContext.getString(R.string.title_banner2), R.drawable.ic_banner_2);
            case 2:
                return BannersFragment.newInstance(myContext.getString(R.string.title_banner3), R.drawable.ic_banner_3);
            default:
                return BannersFragment.newInstance(myContext.getString(R.string.title_banner1), R.drawable.ic_banner_1);
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
