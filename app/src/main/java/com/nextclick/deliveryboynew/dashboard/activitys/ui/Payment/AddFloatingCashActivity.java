package com.nextclick.deliveryboynew.dashboard.activitys.ui.Payment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.deliveryboynew.R;

public class AddFloatingCashActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_floating_cash);


        ImageView back_image=findViewById(R.id.back_image);
        Button add_cash=findViewById(R.id.add_cash);
        back_image.setOnClickListener(this);
        add_cash.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        System.out.println("aaaaaaaa id " + v.getId());
        switch (v.getId()) {
            case R.id.back_image:
                finish();
                break;
            case R.id.add_cash:
                finish();
                break;
        }
    }
}