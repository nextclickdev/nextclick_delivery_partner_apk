package com.nextclick.deliveryboynew.dashboard.activitys.ui.Payment;

import static com.nextclick.deliveryboynew.Config.Config.WALLETHISTORY;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.UiMsgs.setEditTextErrorMethod;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PaymentsActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    private RecyclerView mList;
    private TextView start_date,tv_get,end_date,tv_earnings,tv_floating;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private PaymentHistoryAdapter adapter;
    String year,month,day,start_date_str,end_date_str;
    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="";
    private ArrayList<TransactionsPojo> transactionlist;
    private Context mContext;
    private PreferenceManager preferenceManager;

    public static final Integer PAYMENT_SUCCESS= 2;
    public static final Integer PAYMENT_FAIL= 3;

    String SelectedPaymentType=TYPE_CREDIT;
    public static final String TYPE_DEBIT = "DEBIT";
    public static final String TYPE_CREDIT = "CREDIT";
    LinearLayout relative_empty_cart;
    private String totalamount;
    private String notes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        mList = findViewById(R.id.recyclerview_payment_history);
        tv_earnings = findViewById(R.id.tv_earnings);
        tv_floating = findViewById(R.id.tv_floating);
        TextView tv_earnings_header = findViewById(R.id.tv_earnings_header);
        TextView tv_floating_header = findViewById(R.id.tv_floating_header);
        LinearLayout layout_earnings= findViewById(R.id.layout_earnings);
        LinearLayout  layout_floatings= findViewById(R.id.layout_floatings);
        ImageView back_image = findViewById(R.id.back_image);
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layout_earnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!=TYPE_CREDIT)
                {
                    SelectedPaymentType=TYPE_CREDIT;
                    layout_earnings.setBackgroundColor(0XFF660033);
                    tv_earnings_header.setTextColor(0XFFFFFFFF);
                    tv_earnings.setTextColor(0XFFFFFFFF);


                    layout_floatings.setBackgroundColor(0XFFCDCDCD);
                    tv_floating_header.setTextColor(0XFF000000);
                    tv_floating.setTextColor(0XFF000000);

                    getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
                }
            }
        });
        layout_floatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!=TYPE_DEBIT)
                {
                    SelectedPaymentType=TYPE_DEBIT;
                    layout_floatings.setBackgroundColor(0XFF660033);
                    tv_floating_header.setTextColor(0XFFFFFFFF);
                    tv_floating.setTextColor(0XFFFFFFFF);

                    layout_earnings.setBackgroundColor(0XFFCDCDCD);
                    tv_earnings_header.setTextColor(0XFF000000);
                    tv_earnings.setTextColor(0XFF000000);
                    getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
                }
            }
        });

        start_date = findViewById(R.id.start_date);
        tv_get = findViewById(R.id.tv_get);
        end_date = findViewById(R.id.end_date);
        relative_empty_cart= findViewById(R.id.relative_empty_cart);
        mContext=getApplicationContext();
        preferenceManager=new PreferenceManager(getApplicationContext());
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(getActivity(),AddFloatingCashActivity.class);
                //startActivity(intent);
                showDialog();

            }
        });

        getDate();
        transactionlist = new ArrayList<>();
        adapter = new PaymentHistoryAdapter(this, transactionlist);

        linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        // mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);




        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datepicker(0);
            }
        });
        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datepicker(1);
            }
        });
        tv_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
    }

    private void showDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.activity_add_floating_cash);
        Button applyButton = (Button) dialog.findViewById(R.id.add_cash);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        TextInputEditText tv_amount= (TextInputEditText) dialog.findViewById(R.id.tv_amount);
        TextInputEditText tv_note= (TextInputEditText) dialog.findViewById(R.id.tv_note);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // invokeDeliveryOrder(OrderPickupOtp);
                //if (isValid(tv_amount,tv_note)) {
                dialog.dismiss();
                invokeAddCashAPI(tv_amount.getText().toString(), tv_note.getText().toString());
                //}
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = displayWidth;
        dialog.getWindow().setAttributes(layoutParams);
    }

    private boolean isValid(TextInputEditText tv_amount, TextInputEditText tv_note) {
        boolean valid = true;
        if (tv_amount.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(tv_amount, getString(R.string.empty));
            valid = false;
        }  else if (tv_note.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(tv_note, getString(R.string.empty));
            valid = false;
        }
        return valid;
    }

    private void invokeAddCashAPI(String amount, String note) {

       /* Intent intent = new Intent(mContext, PaymentModesActivity.class);
        intent.putExtra("amount", amout);
        intent.putExtra("note", note);
        mContext.startActivity(intent);
        */

        totalamount =amount;
        notes = note;
         LoadingDialog.loadDialog(mContext);
        makePayment();
    }

    private void getData() {
        for(int i=0;i<5;i++) {
            PaymentObject paymentObject = new PaymentObject();
            paymentObject.setTransactionID("#12345"+i);
            if(i%2==0)
            {
                paymentObject.setTransactionType("Credit");
                paymentObject.setTransactionAmount("100/-");
                paymentObject.setTransactionDate("8th April 2021");
            }
            else
            {
                paymentObject.setTransactionType("Debit");
                paymentObject.setTransactionAmount("50/-");
                paymentObject.setTransactionDate("10th April 2021");
            }
            //  TransactionList.add(paymentObject);
        }
    }

    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str=year+"-"+month+"-"+day;
        end_date_str=year+"-"+month+"-"+day;

        start_date.setText(""+start_date_str);
        end_date.setText(""+end_date_str);

    }
    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(getApplicationContext(),new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    start_date_str=fromyear+"-"+frommonth+"-"+fromday;
                    start_date.setText(""+start_date_str);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    end_date_str=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                    //  end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+end_date_str);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }

    public void getWallet(String start_date_str, String end_date_str, String s, String s1){
        transactionlist.clear();

        /*
                {
"start_date" :"2021-02-02",
"end_date" :"2021-02-02",
"type" :"CREDIT",
"status": 1
}
         */

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        uploadMap.put("type", "CREDIT");
        uploadMap.put("status", ""+(SelectedPaymentType==TYPE_CREDIT?1:2));

        JSONObject json = new JSONObject(uploadMap);
        Log.d("VolleyResponse", "WALLETHISTORY request: " + json);
        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "WALLETHISTORY response: " + response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");


                                try{
                                    JSONArray transarray=dataobj.getJSONArray("wallet_transactions");
                                    for (int i=0;i<transarray.length();i++){
                                        JSONObject transobj=transarray.getJSONObject(i);
                                        TransactionsPojo transactionsPojo=new TransactionsPojo();
                                        transactionsPojo.setId(transobj.getString("id"));
                                        transactionsPojo.setTxn_id(transobj.getString("txn_id"));
                                        transactionsPojo.setAccount_user_id(transobj.getString("account_user_id"));
                                        transactionsPojo.setAmount(transobj.getString("amount"));
                                        transactionsPojo.setType(transobj.getString("type"));
                                        transactionsPojo.setBalance(transobj.getString("balance"));
                                        transactionsPojo.setEcom_order_id(transobj.getString("ecom_order_id"));
                                        transactionsPojo.setMessage(transobj.getString("message"));
                                        transactionsPojo.setCreated_at(transobj.getString("created_at"));
                                        transactionsPojo.setUpdated_at(transobj.getString("updated_at"));
                                        transactionsPojo.setStatus(transobj.getString("status"));
                                        transactionlist.add(transactionsPojo);
                                    }
                                    adapter.setrefresh(transactionlist);

                                    if(transactionlist.size()>0)
                                    {
                                        relative_empty_cart.setVisibility(View.GONE);
                                        mList.setVisibility(View.VISIBLE);
                                    }
                                    else
                                    {

                                        relative_empty_cart .setVisibility(View.VISIBLE);
                                        mList.setVisibility(View.GONE);
                                    }

                                }catch (JSONException e1){
                                    showNoDataFound();
                                }

                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("user");
                                    tv_earnings.setText("₹ "+paymentobj.getString("wallet"));
                                    tv_floating.setText("₹ "+paymentobj.getString("floating_wallet"));
                                }catch (JSONException e2){

                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));


                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void showNoDataFound() {
        relative_empty_cart.setVisibility(View.VISIBLE);
        mList.setVisibility(View.GONE);
    }


    public void sendPaymentstatus(String paymentid,String paymenttransactionid,
                                  String status,String mesage){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("payment_method_id", paymentid);
        uploadMap.put("payment_gw_txn_id", paymenttransactionid);
        uploadMap.put("amount", totalamount);
        uploadMap.put("status", status);
        uploadMap.put("message", mesage);

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaa json "+json.toString());

        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.PAYMENTSTATUS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            if (status){

                                Toast.makeText(mContext, "Payment Success", Toast.LENGTH_SHORT).show();
                                getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
                            }
                            else
                            {
                                //payment failed
                                Toast.makeText(mContext, "Payment Failed", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void makePayment() {
        final Checkout co = new Checkout();
        co.setImage(R.drawable.nextclick_icon_receipt);
        try {
            JSONObject orderRequest = new JSONObject();
            // orderRequest.put("amount", totatlAmount+"00"); // amount in the smallest currency unit
            orderRequest.put("amount", "100"); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image", R.drawable.nextclick_logo_white);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            Activity activity=this;
            co.open(activity, orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        }
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Toast.makeText(mContext, String.valueOf(R.string.payement_done), Toast.LENGTH_SHORT).show();
        System.out.println("aaaaaaaaaa  data sucess  "+s+paymentData.toString());
        System.out.println("aaaaaaaaaa sucess  "+paymentData.getOrderId()+" "+paymentData.getPaymentId()+" "+paymentData.getSignature()+" "+
                paymentData.getUserContact()+"  "+paymentData.getData().toString()+" "+paymentData.getUserEmail());
        sendPaymentstatus(PAYMENT_SUCCESS,paymentData.getPaymentId(),"");
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        System.out.println("aaaaaaaaaa  payment  error  "+s);
        try {
            JSONObject jsonObject1=new JSONObject(s);
            JSONObject errorobj=jsonObject1.getJSONObject("error");
            String description=errorobj.getString("description");
            sendPaymentstatus(PAYMENT_FAIL,"",description);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendPaymentstatus(Integer paymentStatus, String paymentID, String error) {
        LoadingDialog.dialog.dismiss();
        if (paymentStatus == PAYMENT_SUCCESS) {
            sendPaymentstatus("" + 2, paymentID, "" + PAYMENT_SUCCESS, "");
        } else {
            //  sendPaymentstatus(""+3,"",""+PAYMENT_FAIL,error);
            Toast.makeText(mContext, "Payment Failed with " + error, Toast.LENGTH_SHORT).show();
        }
    }
}