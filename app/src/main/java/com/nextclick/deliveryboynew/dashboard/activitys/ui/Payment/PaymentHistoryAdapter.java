package com.nextclick.deliveryboynew.dashboard.activitys.ui.Payment;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboynew.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {

    private Context context;
    private List<TransactionsPojo> list;

    public PaymentHistoryAdapter(FragmentActivity activity, ArrayList<TransactionsPojo> transactionlist) {
        this.context = activity;
        this.list = transactionlist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //        View v = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
        View v = LayoutInflater.from(context).inflate(R.layout.payment_history_detail, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TransactionsPojo payment = list.get(position);


        String[] spilt=payment.getCreated_at().split(" ");

        String date_after = formateDateFromstring("yyyy-MM-dd", "MMM, dd yyyy", spilt[0]);
        holder.tv_date.setText(payment.getCreated_at());//date_after


        holder.tv_id.setText(payment.getTxn_id());
        if(payment.getTrack_id()!=null && !payment.getTrack_id().equals("null"))
            holder.txt_order_id.setText("ORDER #"+payment.getTrack_id());
        else if(payment.getEcom_order_id()!=null && !payment.getEcom_order_id().equals("null"))
        holder.txt_order_id.setText("ORDER ID : "+payment.getEcom_order_id());
        else if(payment.getMessage()!=null && !payment.getMessage().equals("null"))
            holder.txt_order_id.setText("Note : "+payment.getMessage());

        holder.transaction_amount.setText(payment.getAmount());
        holder.transaction_type.setText(payment.getType());

        if (payment.getType().equalsIgnoreCase("CREDIT")){
            holder.transaction_amount.setTextColor(Color.parseColor("#70E86B"));
            holder.layout_background.setBackgroundColor(context.getResources().getColor(R.color.creditcolor));
        }else {
            holder.transaction_amount.setTextColor(Color.parseColor("#B22222"));
            holder.layout_background.setBackgroundColor(context.getResources().getColor(R.color.debitcolor));
        }
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {
            System.out.println("aaaaa exception  "+e.getMessage());
        }
        return outputDate;
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setrefresh(ArrayList<TransactionsPojo> transactionlist) {
        this.list=transactionlist;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_id,transaction_amount, transaction_type, tv_date,txt_order_id;
        private LinearLayout layout_background;

        public ViewHolder(View itemView) {
            super(itemView);



            tv_id = itemView.findViewById(R.id.tv_id);
            transaction_amount = itemView.findViewById(R.id.transaction_amount);
            transaction_type= itemView.findViewById(R.id.transaction_type);
            tv_date= itemView.findViewById(R.id.tv_date);
            txt_order_id=itemView.findViewById(R.id.txt_order_id);
            layout_background= itemView.findViewById(R.id.layout_background);
        }
    }
}

