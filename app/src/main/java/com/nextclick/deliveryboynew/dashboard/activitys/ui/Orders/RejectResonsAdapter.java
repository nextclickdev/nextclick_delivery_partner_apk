package com.nextclick.deliveryboynew.dashboard.activitys.ui.Orders;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.model.RejectOrdersRequets;

import java.util.List;

public class RejectResonsAdapter extends RecyclerView.Adapter<RejectResonsAdapter.ViewHolder> {

    private Context context;
    private List<RejectOrdersRequets> list;
    Dialog dialog;

    public RejectResonsAdapter(Context context,
                               List<RejectOrdersRequets> list, Dialog dialog) {
        this.context = context;
        this.list = list;
        this.dialog = dialog;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.rejectresonadapter, parent, false);
        //View v = LayoutInflater.from(context).inflate(R.layout.view_show_orders, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RejectOrdersRequets rejectOrdersRequets = list.get(position);

        holder.tv_name.setText(""+rejectOrdersRequets.getName());

        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OrderDetailsActivity)context).setrejectOrder(rejectOrdersRequets);
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        public ViewHolder(View itemView) {
            super(itemView);

            tv_name=itemView.findViewById(R.id.tv_name);

        }
    }
}
