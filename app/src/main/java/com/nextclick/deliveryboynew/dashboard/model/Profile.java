package com.nextclick.deliveryboynew.dashboard.model;

public class Profile {
    String id,unique_id,first_name,last_name,email,phone,wallet,image,security_deposited_amount;

    public String getSecurity_deposited_amount() {
        return security_deposited_amount;
    }

    public void setSecurity_deposited_amount(String security_deposited_amount) {
        this.security_deposited_amount = security_deposited_amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String aadharNumber,panNumber,rcNumber,drivingLicenseNumber;
    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }
    public String getAadharNumber() {
        return aadharNumber;
    }
    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }
    public String getPanNumber() {
        return panNumber;
    }
    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }
    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }
    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }
    public String getRcNumber() {
        return rcNumber;
    }


    String aadharNumberImage,panNumberImage,rcNumberImage,drivingLicenseImage;
    public void setAadharNumberImage(String aadharNumber) {
        this.aadharNumberImage = aadharNumber;
    }
    public String getAadharNumberImage() {
        return aadharNumberImage;
    }
    public void setPanNumberImage(String panNumber) {
        this.panNumberImage = panNumber;
    }
    public String getPanNumberImage() {
        return panNumberImage;
    }
    public void setRcNumberImage(String rcNumber) {
        this.rcNumberImage = rcNumber;
    }
    public String getRcNumberImage() {
        return rcNumberImage;
    }
    public void setDrivingLicenseImage(String vehicleNumber) {
        this.drivingLicenseImage = vehicleNumber;
    }
    public String getDrivingLicenseImage() {
        return drivingLicenseImage;
    }


    String aadharNumberStatus,panNumberStatus,rcNumberStatus,drivingLicenseNumberStatus;

    public String getAadharNumberStatus() {
        return aadharNumberStatus;
    }

    public void setAadharNumberStatus(String aadharNumberStatus) {
        this.aadharNumberStatus = aadharNumberStatus;
    }

    public String getPanNumberStatus() {
        return panNumberStatus;
    }

    public void setPanNumberStatus(String panNumberStatus) {
        this.panNumberStatus = panNumberStatus;
    }

    public String getRcNumberStatus() {
        return rcNumberStatus;
    }

    public void setRcNumberStatus(String rcNumberStatus) {
        this.rcNumberStatus = rcNumberStatus;
    }

    public String getDrivingLicenseNumberStatus() {
        return drivingLicenseNumberStatus;
    }

    public void setDrivingLicenseNumberStatus(String drivingLicenseNumberStatus) {
        this.drivingLicenseNumberStatus = drivingLicenseNumberStatus;
    }

    String aadharUploadMessage,panUploadMessage,rcUploadMessage,drivingLicenseUploadMessage;
    public void setAadharUploadMessage(String aadharNumber) {
        this.aadharUploadMessage = aadharNumber;
    }
    public String getAadharUploadMessage() {
        return aadharUploadMessage;
    }
    public void setPanUploadMessage(String panNumber) {
        this.panUploadMessage = panNumber;
    }
    public String getPanUploadMessage() {
        return panUploadMessage;
    }
    public void setDrivingLicenseUploadMessage(String drivingLicenseNumber) {
        this.drivingLicenseUploadMessage = drivingLicenseNumber;
    }
    public String getDrivingLicenseUploadMessage() {
        return drivingLicenseUploadMessage;
    }
    public void setRcUploadMessage(String rcNumber) {
        this.rcUploadMessage = rcNumber;
    }
    public String getRcUploadMessage() {
        return rcUploadMessage;
    }


    String bankPassbookStatus,cancelChequeStatus;

    public String getBankPassbookStatus() {
        return bankPassbookStatus;
    }

    public void setBankPassbookStatus(String bankPassbookStatus) {
        this.bankPassbookStatus = bankPassbookStatus;
    }

    public String getCancelChequeStatus() {
        return cancelChequeStatus;
    }

    public void setCancelChequeStatus(String cancelChequeStatus) {
        this.cancelChequeStatus = cancelChequeStatus;
    }

    String bankPassbookUploadMessage,cancelChequeUploadMessage;
    public void setBankPassbookUploadMessage(String bankPassbookUploadMessage) {
        this.bankPassbookUploadMessage = bankPassbookUploadMessage;
    }
    public String getBankPassbookUploadMessage() {
        return bankPassbookUploadMessage;
    }
    public void setCancelChequeUploadMessage(String cancelChequeUploadMessage) {
        this.cancelChequeUploadMessage = cancelChequeUploadMessage;
    }
    public String getCancelChequeUploadMessage() {
        return cancelChequeUploadMessage;
    }

    String bankPassbookImage,cancelChequeImage;
    public void setBankPassbookImage(String bankPassbookImage) {
        this.bankPassbookImage = bankPassbookImage;
    }
    public String getBankPassbookImage() {
        return bankPassbookImage;
    }
    public void setCancelChequeImage(String cancelChequeImage) {
        this.cancelChequeImage = cancelChequeImage;
    }
    public String getCancelChequeImage() {
        return cancelChequeImage;
    }


}

