package com.nextclick.deliveryboynew.dashboard.model;

import com.nextclick.deliveryboynew.model.OrderDetailsmodel;

public class DashboardObject {

    private boolean current_status;
    String today_earnings,today_floating_cash,today_deliveries,total_deliveries;
    OrderDetailsmodel deliveryOrder;

    public void setCurrentStatus(boolean current_status) {
        this.current_status=current_status;
    }

    public void setTodayEarnings(String today_earnings) {
        this.today_earnings=today_earnings;
    }

    public void setTodayFloatingCash(String today_floating_cash) {
        this.today_floating_cash=today_floating_cash;
    }

    public void setTodayDeliveris(String today_deliveries) {
        this.today_deliveries=today_deliveries;
    }

    public void setTotalDeliveris(String total_deliveries) {
        this.total_deliveries=total_deliveries;
    }

    public void setOnGoingOrder(OrderDetailsmodel deliveryOrder) {
        this.deliveryOrder=deliveryOrder;
    }

    public Boolean getCurrentStatus() {
        return  this.current_status;
    }

    public String getTodayEarnings() {
        return  this.today_earnings=today_earnings;
    }

    public String getTodayFloatingCash() {
        return this.today_floating_cash;
    }

    public String getTodayDeliveris() {
        return  this.today_deliveries;
    }

    public String getTotalDeliveris() {
       return this.total_deliveries;
    }

    public OrderDetailsmodel getOnGoingOrder() {
        return this.deliveryOrder;
    }
}
