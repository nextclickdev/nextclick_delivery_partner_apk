package com.nextclick.deliveryboynew.dashboard.activitys.ui.support;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.deliveryboynew.R;

public class SupportDetailActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_detail);


        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvMessage = findViewById(R.id.tvMessage);
        TextView tvTicketID= findViewById(R.id.tvTicketID);
        TextView tvStatus= findViewById(R.id.tvStatus);
        TextView  tv_created_at= findViewById(R.id.tvDate);

       Integer position=getIntent().getIntExtra("position",0);
        if (position==1){
            SupportModel supportModel= (SupportModel) getIntent().getSerializableExtra("supportmodel");

            tvTitle.setText(supportModel.getSubject());
            tvMessage.setText(supportModel.getMessage());
            tvTicketID.setText(supportModel.getId());
            tv_created_at.setText(supportModel.getCreated_at());
            if(position!=0)
            {
                tvStatus.setTextColor(0XFF35c534);
                tvStatus.setText("Closed");
            }
            else
            {
                tvStatus.setTextColor(0XFFff5251);
                tvStatus.setText("Open");
            }
        }

        ImageView back_image = findViewById(R.id.back_image);
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}