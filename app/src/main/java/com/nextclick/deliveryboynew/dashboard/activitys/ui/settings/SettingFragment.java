package com.nextclick.deliveryboynew.dashboard.activitys.ui.settings;

import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.newauthentication.UserSigninActivity;
import com.nextclick.deliveryboynew.utils.PreferenceManager;


public class SettingFragment extends Fragment {

    SettingviewModel settingviewModel;
    Switch switchactive;
    Button btn_logout;
    PreferenceManager preferenceManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        settingviewModel =
                new ViewModelProvider(this).get(SettingviewModel.class);
        View root = inflater.inflate(R.layout.fragment_setting, container, false);
        switchactive=root.findViewById(R.id.switchactive);
        btn_logout=root.findViewById(R.id.btn_logout);

        preferenceManager=new PreferenceManager(getContext());

        switchactive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferenceManager.putString(USER_TOKEN, null);
                Intent i= new Intent(getContext(), UserSigninActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });


        return root;
    }
}