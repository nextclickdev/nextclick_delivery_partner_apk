package com.nextclick.deliveryboynew.dashboard.activitys.ui.Map;

import com.google.android.gms.maps.model.PolylineOptions;

public interface MapParser {
    public void onPolylineOptionsUpdated(PolylineOptions lineOptions);
}