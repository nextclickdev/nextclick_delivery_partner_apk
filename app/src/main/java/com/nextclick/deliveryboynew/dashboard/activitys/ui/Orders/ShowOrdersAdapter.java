package com.nextclick.deliveryboynew.dashboard.activitys.ui.Orders;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.activitys.OrdersFragment;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Notifications.OrderDetail;
import com.nextclick.deliveryboynew.model.DeliveryOrder;
import com.nextclick.deliveryboynew.model.DeliveryOrderStatus;

import java.util.ArrayList;
import java.util.List;

public class ShowOrdersAdapter  extends RecyclerView.Adapter<ShowOrdersAdapter.ViewHolder> {

    private final OrdersFragment ordersFragment;
    private Context context;
    private List<DeliveryOrder> list;

    public ShowOrdersAdapter(OrdersFragment ordersFragment,Context context, List<DeliveryOrder> list) {
        this.ordersFragment =ordersFragment;
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.view_show_orders1, parent, false);
        //View v = LayoutInflater.from(context).inflate(R.layout.view_show_orders, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DeliveryOrder deliveryOrder = list.get(position);


        //holder.textTitle.setText(movie.getTitle());
        // holder.textRating.setText(String.valueOf(movie.getRating()));
        // holder.textYear.setText(String.valueOf(movie.getYear()));

        String text = "<font color=#9D9D9D>Srinivasa food hub, phone : </font> <font color=#141414>9966263344</font>";
        holder.seller_details.setText(Html.fromHtml(text));
        holder.vendor_details.setText(Html.fromHtml(text));

        holder.address_details.setText("House No : 3-161/A, Miyapur, Near manjeera pipeline road, Opposite to Chef Bakers, 500049.");
        holder.cust_address_details.setText("House No : 3-161/A, Miyapur, Near manjeera pipeline road, Opposite to Chef Bakers, 500049.");

        holder.tv_order_id.setText(deliveryOrder.getTrackID());
        holder.tv_order_earnings.setText(deliveryOrder.getDelivery_fee());

        DeliveryOrderStatus deliveryOrderStatus= deliveryOrder.getDeliveryOrderStatus();
        if(deliveryOrderStatus!=null)
        {
            holder.vendor_order_status.setText(deliveryOrderStatus.getStatus());
        }

        holder.order_status.setText(deliveryOrder.getStatusMessage());
        holder.tv_order_date.setText(deliveryOrder.getOrderDateInSimpleFormat());

        if(deliveryOrder.getStatus().equals("508"))
        holder.layout_track_click.setVisibility(View.GONE);

        try{
            if(deliveryOrder.getDelivery_job_status().equals("500")) {
                holder.layout_track_click.setVisibility(View.GONE);
            }
        }catch (NullPointerException e){

        }




        /*SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date now = new Date();
        String strDate = sdf.format(now);

        if(!deliveryOrder.getOrderDate().equals(strDate))
        {
            holder.layout_track_click.setVisibility(View.GONE);
        }
        else
        {
            holder.layout_track_click.setVisibility(View.VISIBLE);
        }*/

        holder.btn_reached_pickup_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ordersFragment.invokeAction(OrdersFragment.PICKUP_POINT,deliveryOrder);
            }
        });
        holder.btn_reached_delivery_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ordersFragment.invokeAction(OrdersFragment.DELIVERY_POINT,deliveryOrder);
            }
        });

        holder.layout_view_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("orderID", deliveryOrder.getID());
                intent.putExtra("jobID", deliveryOrder.getDeliveryJobID());
                intent.putExtra("product_id", deliveryOrder.getJobID());
                intent.putExtra("OrderPickupOtp", deliveryOrder.getOrderPickupOtp());
                intent.putExtra("OrderDeliveryOtp", deliveryOrder.getOrderDeliveryOtp());
                intent.putExtra("OrderStatus", deliveryOrder.getStatus());
                try{
                    intent.putExtra("deliveryboy_reject_status", deliveryOrder.getDelivery_job_status());
                }catch (NullPointerException e){

                }
                context.startActivity(intent);
                    /*if(layout_order_details.getVisibility()== View.VISIBLE)
                    {
                        layout_order_details.setVisibility(View.GONE);
                    }
                    else
                    {
                        layout_order_details.setVisibility(View.VISIBLE);
                    }*/
            }
        });


        if(position%2==0)
        {
            holder.layout_order_id.setBackgroundColor(0XFFC0F8BD);
            if(holder.layout_order_id1!=null)
                holder.layout_order_id1.setBackgroundColor(0XFFC0F8BD);
            holder.layout_order_status.setBackgroundColor(0XFFE3FAE1);
            holder.layout_vendor_order_status.setBackgroundColor(0XFFEEFCED);
        }
        else {

            holder.layout_order_id.setBackgroundColor(0XFFF8C3AE);
            if (holder.layout_order_id1 != null)
                holder.layout_order_id1.setBackgroundColor(0XFFF8C3AE);
            holder.layout_order_status.setBackgroundColor(0XFFFAD5C6);
            holder.layout_vendor_order_status.setBackgroundColor(0XFFFCE6DD);
        }



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView seller_details,vendor_details, address_details, cust_address_details,tv_order_id,tv_order_earnings,tv_order_date;
        public Button order_track_button,order_details_button;
        LinearLayout layout_order_id,layout_order_id1,layout_order_status,layout_vendor_order_status,layout_view_orders,layout_order_tracking,layout_track_click,layout_order_details;
        public ImageView img_track,img_down_arrow_clicked;
        Button  btn_reached_pickup_point,btn_reached_delivery_point;
        TextView vendor_order_status,order_status;

        RecyclerView recyclerview_order_details;
        private RecyclerView.Adapter adapter;
        private List<OrderDetail> OrderDetailList;

        public ViewHolder(View itemView) {
            super(itemView);

            seller_details = itemView.findViewById(R.id.seller_details);
            vendor_details = itemView.findViewById(R.id.vendor_details);
            address_details= itemView.findViewById(R.id.address_details);
            cust_address_details= itemView.findViewById(R.id.cust_address_details);

            vendor_order_status= itemView.findViewById(R.id.vendor_order_status);
            order_status= itemView.findViewById(R.id.order_status);

            order_track_button = itemView.findViewById(R.id.order_track_button);
            order_details_button = itemView.findViewById(R.id.order_details_button);
            img_track = itemView.findViewById(R.id.img_track);

            tv_order_id = itemView.findViewById(R.id.tv_order_id);

            tv_order_earnings= itemView.findViewById(R.id.tv_order_earnings);

            layout_order_id = itemView.findViewById(R.id.layout_order_id);
            layout_order_id1 = itemView.findViewById(R.id.layout_order_id1);
            tv_order_date = itemView.findViewById(R.id.tv_order_date);
            layout_order_status = itemView.findViewById(R.id.layout_order_status);
            layout_vendor_order_status = itemView.findViewById(R.id.layout_vendor_order_status);

            layout_order_details= itemView.findViewById(R.id.layout_order_details);
            layout_track_click= itemView.findViewById(R.id.layout_track_click);
            layout_order_tracking= itemView.findViewById(R.id.layout_order_tracking);
            layout_view_orders= itemView.findViewById(R.id.layout_view_orders);
            img_down_arrow_clicked= itemView.findViewById(R.id.img_down_arrow_clicked);

            btn_reached_pickup_point = itemView.findViewById(R.id.btn_reached_pickup_point);
            btn_reached_delivery_point = itemView.findViewById(R.id.btn_reached_delivery_point);


            /*layout_track_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(layout_order_tracking.getVisibility()== View.VISIBLE)
                    {
                        layout_order_tracking.setVisibility(View.GONE);
                    }
                    else
                    {
                        layout_order_tracking.setVisibility(View.VISIBLE);
                    }
                }
            });*/

            img_down_arrow_clicked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    layout_order_details.setVisibility(View.GONE);
                }
            });

            /*img_track.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   // final ProductModel productModel = data.get(position);
                    Intent intent = new Intent(context, OrderTrackingActivity.class);
                    intent.putExtra("product_id", 1234);//productModel.getId()
                    context.startActivity(intent);
                }
            });*/
            order_details_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // final ProductModel productModel = data.get(position);
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("product_id", 1234);//productModel.getId()
                    context.startActivity(intent);
                }
            });


            OrderDetailList = new ArrayList<>();

            for (int i = 0; i < 2; i++) {
                OrderDetail orderDetail = new OrderDetail();

                String discount ="Rs : 250 Discount" ;//"3.2 StrikeThrough Using SpannableString"
                SpannableString spannableString = new SpannableString(discount);
                spannableString.setSpan(new StrikethroughSpan(),5,7,0);
                orderDetail.setDiscount(spannableString.toString());

                OrderDetailList.add(orderDetail);
            }

         /*   recyclerview_order_details = itemView.findViewById(R.id.recyclerview_order_details);
            adapter = new ProductDetailAdapter(context, OrderDetailList);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerview_order_details.setHasFixedSize(true);
            recyclerview_order_details.setLayoutManager(linearLayoutManager);
            recyclerview_order_details.setAdapter(adapter);*/
        }
    }
}
