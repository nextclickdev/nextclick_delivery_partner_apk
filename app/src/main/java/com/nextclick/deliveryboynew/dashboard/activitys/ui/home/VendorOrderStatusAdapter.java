package com.nextclick.deliveryboynew.dashboard.activitys.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.model.VendorOrderStatus;

import java.util.List;

public class VendorOrderStatusAdapter extends RecyclerView.Adapter<VendorOrderStatusAdapter.ViewHolder> {

    private Context context;
    private List<VendorOrderStatus> list;

    public VendorOrderStatusAdapter(Context context, List<VendorOrderStatus> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.view_show_order_status, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VendorOrderStatus vendorOrderStatus = list.get(position);

        holder.tv_vendor_order_status_message.setText(vendorOrderStatus.getorderStatus());
        holder.img_vendor_order_status.setImageResource(vendorOrderStatus.getorderStatusResource());
        holder.vendor_order_status_layout.setBackgroundColor(vendorOrderStatus.getorderStatusBackground());

        holder.tv_vendor_order_status_message.setTextColor(vendorOrderStatus.getorderStatusForeground());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_vendor_order_status_message;
        public ImageView img_vendor_order_status;
        RelativeLayout vendor_order_status_layout;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_vendor_order_status_message = itemView.findViewById(R.id.tv_vendor_order_status_message);
            img_vendor_order_status = itemView.findViewById(R.id.img_vendor_order_status);
            vendor_order_status_layout = itemView.findViewById(R.id.vendor_order_status_layout);
        }
    }
}
