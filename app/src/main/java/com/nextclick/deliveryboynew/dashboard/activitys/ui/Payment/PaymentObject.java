package com.nextclick.deliveryboynew.dashboard.activitys.ui.Payment;

public class PaymentObject {

    public String transactionID;
    public String transactionDate;
    public String transactionType;
    public String transactionAmount;


    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String title) {
        this.transactionID = title;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String title) {
        this.transactionDate = title;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String title) {
        this.transactionType = title;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String title) {
        this.transactionAmount = title;
    }
}