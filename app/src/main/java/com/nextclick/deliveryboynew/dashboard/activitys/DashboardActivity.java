package com.nextclick.deliveryboynew.dashboard.activitys;

import static com.nextclick.deliveryboynew.Config.Config.ACCEPT_TERMSCONDITIONS;
import static com.nextclick.deliveryboynew.Config.Config.GET_TERMSCONDITIONS;
import static com.nextclick.deliveryboynew.Config.Config.REMOVETOKEN;
import static com.nextclick.deliveryboynew.Config.Config.USER_PROFILE;
import static com.nextclick.deliveryboynew.Config.Config.VALIDATE_TERMSCONDITIONS;
import static com.nextclick.deliveryboynew.Config.Config.WALLETHISTORY;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.FCM_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nextclick.crm.faq.FaqActivity;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.NotificationSoundsActivity;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.Services.ForegroundService;
import com.nextclick.deliveryboynew.apiServices.ApiService;
import com.nextclick.deliveryboynew.apiServices.RetrofitClient;
import com.nextclick.deliveryboynew.customer_support.CustomerSupportActivity;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Notifications.NotificationsFragment;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Notifications.OrderDetail;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Payment.PaymentFragment;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.home.HomeFragment;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.home.HomeViewModel;
import com.nextclick.deliveryboynew.dashboard.model.Profile;
import com.nextclick.deliveryboynew.fragments.ProfileNewFragment;
import com.nextclick.deliveryboynew.helpers.CustomDialog;
import com.nextclick.deliveryboynew.helpers.Language_Dialog;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.helpers.Logout_Dialog;
import com.nextclick.deliveryboynew.helpers.UiMsgs;
import com.nextclick.deliveryboynew.model.CommonResponse;
import com.nextclick.deliveryboynew.newauthentication.UserSigninActivity;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.RequestBody;

public class DashboardActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    private AppBarConfiguration mAppBarConfiguration;
    private ImageView img_profile, ivBackArrow, tv_del_imageView;
    private TextView tv_name, tv_email, tv_headername;
    Switch switchactive;
    NavigationView navigationView;
    LinearLayout layout_dashboard_header;
    private static final int REQUEST_PERMISSIONS = 100;
    PreferenceManager preferenceManager;
    boolean boolean_permission;
    private List<OrderDetail> OrderDetailList;

    Double latitude, longitude;
    TextView item_count;
    Geocoder geocoder;
    HomeViewModel homeViewModel;
    BottomNavigationView bottomNavigation;
    Toolbar toolbar_Dashboard, toolBar_otherLayout;
    DrawerLayout drawer;
    private Profile profileObject;
    TextView tv_del_name, tv_del_ID, tv_del_email;
    private static final int REQUEST_CHECK_SETTINGS = 101;
    private CustomDialog customDialog;
    private boolean checkterms = false;
    public static final int UPDATE_LOCATION = 999;
    private ProfileNewFragment profileNewFragment;
    String year, month, day, start_date_str, end_date_str, secutity_deposit_amount = "", wallet_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        init();
        checkLocationSettings();
        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().subscribeToTopic("news1").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d("TAG", "onComplete: ");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("TAG", "onComplete: ");
            }
        });

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("User navigated to Dashboard");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == UPDATE_LOCATION) {
            if (profileNewFragment != null)
                profileNewFragment.updateLocation(intent);
        }
    }

    protected void checkLocationSettings() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(mLocationSettingsRequest);
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                } catch (ApiException exception) {
                    if (exception.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {

                        // Cast to a resolvable exception.
                        ResolvableApiException resolvable = (ResolvableApiException) exception;
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        try {
                            resolvable.startResolutionForResult(
                                    DashboardActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private String orderId;

    private void init() {

        preferenceManager = new PreferenceManager(this);


        toolbar_Dashboard = findViewById(R.id.toolbar);
        toolBar_otherLayout = findViewById(R.id.toolBar_otherLayout);
        setSupportActionBar(toolbar_Dashboard);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        layout_dashboard_header = findViewById(R.id.layout_dashboard_header);
        tv_headername = findViewById(R.id.tv_headername);
        ivBackArrow = findViewById(R.id.ivBackArrow);

        customDialog = new CustomDialog(DashboardActivity.this);
        gettermsandconditions();
        item_count = findViewById(R.id.item_count);
        OrderDetailList = new ArrayList<>();
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToolbarTile(getString(R.string.menu_dashboard));
                showDashboardToolbar();
                navigationView.setCheckedItem(R.id.nav_dashboard);
                loadFragment(new HomeFragment(homeViewModel));
                bottomNavigation.getMenu().findItem(R.id.navigation_home).setChecked(true);

            }
        });

        getNotifications();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar_Dashboard, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);


                LinearLayout layout_profile = findViewById(R.id.layout_profile);
                tv_del_name = findViewById(R.id.tv_del_name);
                tv_del_ID = findViewById(R.id.tv_del_ID);
                tv_del_email = findViewById(R.id.tv_del_email);
                tv_del_imageView = findViewById(R.id.tv_del_imageView);

                if (profileObject != null && tv_del_name != null) {
                    setProfileData(profileObject);
                }
                layout_profile.setVisibility(View.VISIBLE);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        TextView leftThumbText = findViewById(R.id.leftThumbText);
        TextView rightThumbText = findViewById(R.id.rightThumbText);

        SwitchCompat swithNetworkStatus = findViewById(R.id.swithNetworkStatus);


        homeViewModel = new HomeViewModel(getApplicationContext());
        orderId = getIntent().getStringExtra("order_id");


        final Observer<View> viewOnClickListener = new Observer<View>() {
            @Override
            public void onChanged(@Nullable final View view) {

                switch (view.getId()) {
                    case R.id.layout_current_order_track:
                    case R.id.layout_today_del:
                    case R.id.layout_total_del:
                        showOtherToolbar();
                        setToolbarTile(getString(R.string.menu_orders));
                        tv_headername.setText(getString(R.string.menu_orders));
                        loadFragment(new OrdersFragment());
                        break;
                    case R.id.layout_earnings:
                        showOtherToolbar();
                        tv_headername.setText(getString(R.string.menu_payment));
                        setToolbarTile(getString(R.string.menu_payment));
                        loadFragment(new PaymentFragment(PaymentFragment.TYPE_EARNINGS, 0, profileObject.getSecurity_deposited_amount()));
                        bottomNavigation.getMenu().findItem(R.id.navigation_payment).setChecked(true);
                        break;
                    case R.id.layout_fCash:
                        showOtherToolbar();
                        tv_headername.setText(getString(R.string.menu_payment));
                        setToolbarTile(getString(R.string.menu_payment));
                        loadFragment(new PaymentFragment(PaymentFragment.TYPE_FLOATINGS, 0, profileObject.getSecurity_deposited_amount()));
                        bottomNavigation.getMenu().findItem(R.id.navigation_payment).setChecked(true);
                        break;
                }

            }
        };
        homeViewModel.getViewOnClickListener().observe(this, viewOnClickListener);

        final Observer<Boolean> nameObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable final Boolean status) {
                if (swithNetworkStatus != null)
                    swithNetworkStatus.setChecked(status);
            }
        };
        homeViewModel.getAvailableStatus().observe(this, nameObserver);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.nav_dashboard) {
                    setToolbarTile(getString(R.string.menu_dashboard));
                    loadFragment(new HomeFragment(homeViewModel));
                    showDashboardToolbar();
                } else {

                    if (id == R.id.nav_orders) {
                        showOtherToolbar();
                        setToolbarTile(getString(R.string.menu_orders));
                        tv_headername.setText(getString(R.string.menu_orders));
                        loadFragment(new OrdersFragment());
                    } else if (id == R.id.nav_payment) {
                        showOtherToolbar();
                        tv_headername.setText(getString(R.string.menu_payment));
                        setToolbarTile(getString(R.string.menu_payment));
                        loadFragment(new PaymentFragment());
                        bottomNavigation.getMenu().findItem(R.id.navigation_payment).setChecked(true);
                    } else if (id == R.id.nav_support) {
                        startActivity(new Intent(DashboardActivity.this, CustomerSupportActivity.class));
//                        showOtherToolbar();
//                        tv_headername.setText(getString(R.string.menu_support));
//                        setToolbarTile(getString(R.string.menu_support));
//                        loadFragment(new SupportFragment());
                    } else if (id == R.id.nav_faq) {
                        startActivity(new Intent(DashboardActivity.this, FaqActivity.class));
                    } else if (id == R.id.nav_logout) {
                        showOtherToolbar();
                        Logout_Dialog logout_dialog = new Logout_Dialog(DashboardActivity.this, 1);
                        logout_dialog.showDialog();


                    } else if (id == R.id.nav_ChangeNotificationSound) {
                        Intent in = new Intent(DashboardActivity.this, NotificationSoundsActivity.class);
                        startActivity(in);
                    }
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });


        bottomNavigation = findViewById(R.id.bottom_navigation);

        ImageView img_notifications = findViewById(R.id.img_notifications);
        img_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOtherToolbar();
                tv_headername.setText(getString(R.string.menu_notifications));
                setToolbarTile(getString(R.string.menu_notifications));
                loadFragment(new NotificationsFragment(swithNetworkStatus.isChecked()));
                bottomNavigation.getMenu().findItem(R.id.navigation_notifications).setChecked(true);
            }
        });


        swithNetworkStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    leftThumbText.setVisibility(View.VISIBLE);
                    rightThumbText.setVisibility(View.GONE);
                    if (homeViewModel != null)
                        homeViewModel.changeNetworkState(true);
                } else {
                    leftThumbText.setVisibility(View.GONE);
                    rightThumbText.setVisibility(View.VISIBLE);
                    if (homeViewModel != null)
                        homeViewModel.changeNetworkState(false);
                }
            }
        });


        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        setToolbarTile(getString(R.string.menu_dashboard));
                        showDashboardToolbar();
                        navigationView.setCheckedItem(R.id.nav_dashboard);
                        loadFragment(new HomeFragment(homeViewModel));
                        return true;
                    case R.id.navigation_payment:
                        //openFragment(SmsFragment.newInstance("", ""));
                        showOtherToolbar();
                        tv_headername.setText(getString(R.string.menu_payment));
                        setToolbarTile(getString(R.string.menu_payment));
                        loadFragment(new PaymentFragment());
                        return true;
                    case R.id.navigation_notifications:
                        showOtherToolbar();
                        tv_headername.setText(getString(R.string.menu_notifications));
                        setToolbarTile(getString(R.string.menu_notifications));
                        loadFragment(new NotificationsFragment(swithNetworkStatus.isChecked()));
                        return true;
                    case R.id.navigation_profile:
                        showOtherToolbar();
                        tv_headername.setText(getString(R.string.menu_profile));
                        setToolbarTile(getString(R.string.menu_profile));
                        profileNewFragment = new ProfileNewFragment();
                        loadFragment(profileNewFragment);
                        //loadFragment(new ProfileFragment());
                        return true;
                }
                return false;
            }
        });

        if (orderId == null || orderId.isEmpty()) {
            setToolbarTile(getString(R.string.menu_dashboard));
            loadFragment(new HomeFragment(homeViewModel));
            navigationView.setCheckedItem(R.id.nav_dashboard);
        } else {
            swithNetworkStatus.setChecked(true);
            showOtherToolbar();
            tv_headername.setText(getString(R.string.menu_notifications));
            setToolbarTile(getString(R.string.menu_notifications));
            loadFragment(new NotificationsFragment(swithNetworkStatus.isChecked()));
            bottomNavigation.getMenu().findItem(R.id.navigation_notifications).setChecked(true);
        }
        fetchUserDetails();
        getDate();
    }

    private void getNotifications() {
        LoadingDialog.loadDialog(this);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_NOTIFICATIONS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);

                    Log.d("VolleyResponse", "notifications response: " + response);


                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");

                    if (!status) {
                        item_count.setVisibility(View.GONE);
                    } else {
                        parseJsonData(jsonObject);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();

                } finally {
                    LoadingDialog.dialog.dismiss();

                    if (OrderDetailList == null || OrderDetailList.size() == 0) {
                        item_count.setVisibility(View.GONE);
                    } else {
                        item_count.setVisibility(View.VISIBLE);
                    }
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /**
     * Parse the notifications result
     *
     * @param jsonObject
     * @throws JSONException
     */
    private void parseJsonData(JSONObject jsonObject) throws JSONException {
        JSONArray dataArray = jsonObject.getJSONArray("data");
        if (dataArray != null && dataArray.length() > 0) {
            OrderDetailList = new ArrayList<>();
            for (int i = 0; i < dataArray.length(); i++) {
                OrderDetail orderDetail = new OrderDetail();

                JSONObject notficationJsonObject = dataArray.getJSONObject(i);
                orderDetail.setNotificationID(notficationJsonObject.getString("id"));
                orderDetail.setOrderNotificationStatus(notficationJsonObject.getString("status"));
                orderDetail.setOrderDate(notficationJsonObject.getString("created_at"));

                //test
                /*orderDetail.setOrderPreparationTime("2 Mints");
                orderDetail.setOrderEarnings("10000");*/
                orderDetail.setOrderAcceptedtimeRemaining(60);//60sec
                //


                try {
                    JSONObject orderJsonObject = notficationJsonObject.getJSONObject("order");
                    try {

                        orderDetail.setOrderStatusId(orderJsonObject.getString("order_status_id").toString());
                    } catch (Exception ex) {
                    }

                } catch (Exception ex) {

                }

                try {
                    JSONObject orderJsonObject = notficationJsonObject.getJSONObject("pickup_order");
                    try {

                        orderDetail.setOrderStatusId(orderJsonObject.getString("order_status_id").toString());
                    } catch (Exception ex) {
                    }

                } catch (Exception ex) {

                }


                // if (orderDetail.getTrackingID() != null && orderDetail.getTrackingID() != "null")
                if (orderDetail.getOrderStatusId()!=null&&orderDetail.getOrderStatusId().equalsIgnoreCase("11")) {
                    OrderDetailList.add(orderDetail);
                }

            }
            if (OrderDetailList.isEmpty()) {
                item_count.setVisibility(View.GONE);
            } else {
                item_count.setVisibility(View.VISIBLE);
                item_count.setText("" + (OrderDetailList.size()));
            }

        }
    }

    private void setProfileData(Profile profileObject) {
        if (tv_del_name != null) {
            tv_del_name.setText(profileObject.getFirst_name() + " " + profileObject.getLast_name());
            tv_del_ID.setText("ID : " + profileObject.getId());
            tv_del_email.setText(profileObject.getEmail());


            Glide.with(getApplicationContext())
                    .load(profileObject.getImage())
                    .placeholder(R.drawable.ui_profile_pic)//
                    .into(tv_del_imageView);//profileImage_edit
        }
    }


    boolean isBackArrowEnabled = false;

    private void showOtherToolbar() {
        isBackArrowEnabled = true;
        toolbar_Dashboard.setVisibility(View.GONE);
        toolBar_otherLayout.setVisibility(View.VISIBLE);
        layout_dashboard_header.setVisibility(View.GONE);
    }

    private void showDashboardToolbar() {
        isBackArrowEnabled = false;
        toolbar_Dashboard.setVisibility(View.VISIBLE);
        toolBar_otherLayout.setVisibility(View.GONE);
        layout_dashboard_header.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.clear();
        menu.add(0, R.id.action_change_language, Menu.NONE, getString(R.string.change_language)).setIcon(R.drawable.ic_logout).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {

            case R.id.action_change_language:
                Language_Dialog language_dialog = new Language_Dialog(DashboardActivity.this, DashboardActivity.this, true);
                language_dialog.showDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getValidateUser(String tcid, String title, String desc) {

        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("page_id", "2");
        termsmpa.put("tc_id", "" + tcid);
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VALIDATE_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {
                                //customDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa validate user " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (!status) {

                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = inflater.inflate(R.layout.activity_web, null);
                                    TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                    AlertDialog.Builder alert = new AlertDialog.Builder(DashboardActivity.this);
                                    alert.setIcon(R.mipmap.ic_launcher);
                                    alert.setTitle(title);
                                    // this is set the view from XML inside AlertDialog
                                    alert.setView(alertLayout);
                                    alert.setCancelable(false);
                                    tv_terms_id.setText(HtmlCompat.fromHtml(desc, HtmlCompat.FROM_HTML_MODE_COMPACT));

//                                    tv_terms_id.setText(desc);
                                    alert.setCancelable(true);
                                    alert.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            accepttermsandconditions(tcid);
                                        }
                                    })
                                           /* .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Action for 'NO' Button
                                                }
                                            })*/;
                                    AlertDialog dialog = alert.create();
                                    dialog.show();
                                } else {
                                    checkterms = true;
                                    System.out.println("aaaaaaaaa already accept");
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UiMsgs.showToast(DashboardActivity.this, getString(R.string.maintenance));
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UiMsgs.showToast(DashboardActivity.this, getString(R.string.oops));
                        System.out.println("aaaaaaa  111 " + error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void gettermsandconditions() {
        customDialog.show();
        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id", "2");
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_TERMSCONDITIONS + "?page_id=2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        customDialog.dismiss();
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaa getterms  " + jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try {
                                        JSONObject dataobject = jsonObject.getJSONObject("data");
                                        getValidateUser(dataobject.getString("id"),
                                                dataobject.getString("title"), dataobject.getString("desc"));
                                    } catch (Exception e) {
                                        JSONArray dataarray = jsonObject.getJSONArray("data");
                                        JSONObject dataobject = dataarray.getJSONObject(0);
                                        getValidateUser(dataobject.getString("id"),
                                                dataobject.getString("title"), dataobject.getString("desc"));
                                    }


                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UiMsgs.showToast(DashboardActivity.this, getString(R.string.maintenance));
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UiMsgs.showToast(DashboardActivity.this, getString(R.string.oops));
                        System.out.println("aaaaaaa  111 " + error.getMessage());

                        customDialog.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void accepttermsandconditions(String id) {

        JsonObject termsmpa = new JsonObject();
        termsmpa.addProperty("page_id", "2");
        termsmpa.addProperty("tc_id", "" + id);
        termsmpa.addProperty(APP_ID,APP_ID_VALUE);
//        final String data = new JSONObject(termsmpa).toString();
        RequestBody requestBody = RequestBody.create(
                okhttp3.MediaType.parse("application/json"),
                new Gson().toJson(termsmpa)
        );

        ApiService apiService = RetrofitClient.provideApi(this);
        Disposable disposable = apiService.acceptTermsAndConditions(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CommonResponse>() {
                    @Override
                    public void accept(CommonResponse it) throws Exception {
                        if (it.isStatus()) {
                           checkterms = true;
                        } else {
                            UiMsgs.showToast(DashboardActivity.this, getString(R.string.maintenance));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("TAG", "submitFormToServer: " + throwable.getLocalizedMessage());
                    }
                });


/*        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ACCEPT_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("terms_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    checkterms = true;
                                    System.out.println("aaaaaaaa terms conditions sucess ");
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UiMsgs.showToast(DashboardActivity.this, getString(R.string.maintenance));
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UiMsgs.showToast(DashboardActivity.this, getString(R.string.oops));
                        System.out.println("aaaaaaa  111 " + error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);*/
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        } else if (isBackArrowEnabled) {
            setToolbarTile(getString(R.string.menu_dashboard));
            showDashboardToolbar();
            navigationView.setCheckedItem(R.id.nav_dashboard);
            loadFragment(new HomeFragment(homeViewModel));
        } else
            super.onBackPressed();
    }

    public void loadFragment(Fragment fragment) {
        clearFragments();


        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.nav_host_fragment, fragment);
                    transaction.commitAllowingStateLoss();
                }

            }
        }, 100);
    }

    private void clearFragments() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof HomeFragment) {
                continue;
            } else if (fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
            }
        }
    }

    private void setToolbarTile(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();

    }

    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, USER_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    Log.d("VolleyResponse", "PROFILE_READ: " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    System.out.println("aaaaaa  responce  " + jsonObject);
                    profileObject = new Profile();
                    profileObject.setId(jsonObjectData.getString("id"));
                    //  profileObject.setUnique_id(jsonObjectData.getString("unique_id"));
                    profileObject.setFirst_name(jsonObjectData.getString("first_name"));
                    profileObject.setLast_name(jsonObjectData.getString("last_name"));
                    profileObject.setEmail(jsonObjectData.getString("email"));
                    profileObject.setPhone(jsonObjectData.getString("phone"));
                    // profileObject.setWallet(jsonObjectData.getString("wallet"));
                    profileObject.setImage(jsonObjectData.getString("profile_image"));
                    profileObject.setSecurity_deposited_amount(jsonObjectData.getString("security_deposited_amount"));
                    secutity_deposit_amount = jsonObjectData.getString("security_deposited_amount");
                    preferenceManager.putString("user_mobile", profileObject.getPhone());
                    preferenceManager.putString("user_email", profileObject.getEmail());

                    setProfileData(profileObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("fetch user error", error.toString());
            }
        }) {
           /* @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return super.getBody();
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String str = preferenceManager.getString(USER_TOKEN);
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token " + preferenceManager.getString(USER_TOKEN));
                //map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMDA4NCIsInVzZXJkZXRhaWwiOnsiaWQiOiIxMDAwODQiLCJpcF9hZGRyZXNzIjoiMjQwOTo0MDcwOjIxOWE6YTJjNDo6OGIwOmU4YTUiLCJ1c2VybmFtZSI6Imd1ampldGlzaHJhdmFuQGdtYWlsLmNvbSIsInVuaXF1ZV9pZCI6Ik5DVTAyMTEiLCJwYXNzd29yZCI6IiQyeSQwOCQ2b21iQXViMTFhTU9leFRCdUF2SDBPTVJsbFlTZ3FGa0JcL2luMzVQXC9USE94ZlwvUnFGNGhKLiIsInNhbHQiOm51bGwsImVtYWlsIjoiZ3VqamV0aXNocmF2YW5AZ21haWwuY29tIiwid2FsbGV0IjoiMC4wMCIsImFjdGl2YXRpb25fY29kZSI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX2NvZGUiOm51bGwsImZvcmdvdHRlbl9wYXNzd29yZF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjpudWxsLCJjcmVhdGVkX29uIjoiMTU5MDEyMTMxNSIsImxhc3RfbG9naW4iOm51bGwsImFjdGl2ZSI6IjEiLCJsaXN0X2lkIjoiMCIsImZpcnN0X25hbWUiOiJHdWpqZXRpIFNocmF2YW5rdW1hciIsImxhc3RfbmFtZSI6bnVsbCwiY29tcGFueSI6bnVsbCwicGhvbmUiOiIiLCJjcmVhdGVkX3VzZXJfaWQiOm51bGwsInVwZGF0ZWRfdXNlcl9pZCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMjAtMDUtMjIgMDQ6MjE6NTUiLCJ1cGRhdGVkX2F0IjpudWxsLCJkZWxldGVkX2F0IjpudWxsLCJzdGF0dXMiOiIxIn0sInRpbWUiOjE1OTAxMjEzNjB9.gm-lTQiaLcLLYu4KIpjMorFcayjO77IZFulCRlwYlTk");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public static final Integer PAYMENT_SUCCESS = 2;
    public static final Integer PAYMENT_FAIL = 3;

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        System.out.println("aaaaaaaaaa  data sucess  " + s + paymentData.toString());
        System.out.println("aaaaaaaaaa sucess  " + paymentData.getOrderId() + " " + paymentData.getPaymentId() + " " + paymentData.getSignature() + " " +
                paymentData.getUserContact() + "  " + paymentData.getData().toString() + " " + paymentData.getUserEmail());
        sendPaymentstatus(PAYMENT_SUCCESS, paymentData.getPaymentId(), "");
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        System.out.println("aaaaaaaaaa  payment  error  " + s);
        try {
            JSONObject jsonObject1 = new JSONObject(s);
            JSONObject errorobj = jsonObject1.getJSONObject("error");
            String description = errorobj.getString("description");
            sendPaymentstatus(PAYMENT_FAIL, "", description);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendPaymentstatus(Integer paymentStatus, String paymentID, String error) {
        FragmentManager fm = getSupportFragmentManager();
        PaymentFragment fragment = (PaymentFragment) fm.findFragmentById(R.id.nav_host_fragment);
        if (fragment != null)
            fragment.sendPaymentstatus(paymentStatus, paymentID, error);
    }

    private void stopForegroundService() {
        boolean IsForegroundServiceStarted = preferenceManager.getBoolean("IsForegroundServiceStarted");
        if (IsForegroundServiceStarted) {
            preferenceManager.putBoolean("IsForegroundServiceStarted", false);
            Intent serviceIntent = new Intent(this, ForegroundService.class);
            serviceIntent.putExtra(ForegroundService.KEY_FOREGROUND_SERVICE_ACTION_TYPE, ForegroundService.ACTION_UPDATE_FG_SERVICE_NOTIFICATION);
            this.stopService(serviceIntent);
        }
    }

    public void setLogout() {
        removeFcmToken(preferenceManager.getString(FCM_TOKEN));
    }

    private void removeFcmToken(final String msg) {
        customDialog.show();
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                REMOVETOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("fcm_res", response);
                    try {
                        customDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa response  removetoken " + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            PreferenceManager preferenceManager = new PreferenceManager(getApplicationContext());
                            preferenceManager.putString(USER_TOKEN, null);
                            preferenceManager.putString("user_mobile", null);
                            preferenceManager.putString("user_email", null);
                            if (MyMixPanel.isMixPanelSupport) {
                                MyMixPanel.logEvent("Delivery boy logged out from the application");
                                MyMixPanel.logOutCurrentUser();
                            }
                            Intent i = new Intent(getApplicationContext(), UserSigninActivity.class);
                            startActivity(i);
                            stopForegroundService();
                            finish();

                        } else {
                            Toast.makeText(DashboardActivity.this, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        customDialog.dismiss();
                        UiMsgs.showToast(DashboardActivity.this, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    customDialog.dismiss();
                    UiMsgs.showToast(DashboardActivity.this, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                UiMsgs.showToast(DashboardActivity.this, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void getDate() {
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year = "" + calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }
        if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str = year + "-" + month + "-" + day;
        end_date_str = year + "-" + month + "-" + day;
        getWallet(start_date_str, end_date_str);

    }

    public void getWallet(String start_date_str, String end_date_str) {

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        //uploadMap.put("type", "CREDIT");
        uploadMap.put("status", "" + 2);

        JSONObject json = new JSONObject(uploadMap);
        Log.d("VolleyResponse", "WALLETHISTORY request: " + json);
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "WALLETHISTORY response: " + response);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");

                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");

                                try {
                                    JSONObject paymentobj = dataobj.getJSONObject("user");
                                    wallet_amount = paymentobj.getString("wallet");
                                    System.out.println("aaaaaaaaaawallet amount  " + wallet_amount);
                                    try {
                                        if (secutity_deposit_amount.equalsIgnoreCase("null") || secutity_deposit_amount.isEmpty()) {
                                            fetchUserDetails();
                                        } else {
                                            if (Integer.parseInt(wallet_amount) == 0) {
                                                loadFragment(new PaymentFragment(PaymentFragment.TYPE_FLOATINGS, 1, secutity_deposit_amount));
                                                bottomNavigation.getMenu().findItem(R.id.navigation_payment).setChecked(true);
                                            }
                                        }

                                    } catch (NumberFormatException e) {
                                        if (secutity_deposit_amount.equalsIgnoreCase("null") || secutity_deposit_amount.isEmpty()) {
                                            fetchUserDetails();
                                        } else {
                                            //    loadFragment(new PaymentFragment(PaymentFragment.TYPE_FLOATINGS,1, secutity_deposit_amount));
                                            //    bottomNavigation.getMenu().findItem(R.id.navigation_payment).setChecked(true);
                                        }
                                    }

                                   /* System.out.println("aaaaaaaa paymentobj   "+paymentobj.toString());
                                    if (paymentobj.getString("floating_wallet").equalsIgnoreCase("0")||
                                            paymentobj.getString("floating_wallet").equalsIgnoreCase("null")){
                                        showOtherToolbar();
                                        tv_headername.setText(getString(R.string.menu_payment));
                                        setToolbarTile(getString(R.string.menu_payment));
                                    }*/
                                } catch (JSONException e2) {
                                    System.out.println("aaaaaaaaaa catch wallet" + e2.getMessage());
                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(DashboardActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(DashboardActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));


                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}