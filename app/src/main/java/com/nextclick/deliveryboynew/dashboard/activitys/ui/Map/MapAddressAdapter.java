package com.nextclick.deliveryboynew.dashboard.activitys.ui.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboynew.R;

import java.util.List;

public class MapAddressAdapter extends RecyclerView.Adapter<MapAddressAdapter.ViewHolder> {

    private final MapActivity mapActivity;
    private Context context;
    private List<VendorDetails> list;

    public MapAddressAdapter(MapActivity mapActivity, List<VendorDetails> list) {
        this.context = mapActivity;
        this.mapActivity =mapActivity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.map_address_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VendorDetails vendorDetail = list.get(position);
        holder.textTitle.setText(vendorDetail.getName());

        holder.layout_vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(context,VendorNavigation.class);
                intent.putExtra("vendorDetail", vendorDetail);
                context.startActivity(intent);*/
                mapActivity.drawVendorLocation(vendorDetail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textTitle;
        LinearLayout layout_vendor;

        public ViewHolder(View itemView) {
            super(itemView);

            textTitle = itemView.findViewById(R.id.tv_address);
            layout_vendor = itemView.findViewById(R.id.layout_vendor);


        }
    }
}
