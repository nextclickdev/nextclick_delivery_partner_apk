package com.nextclick.deliveryboynew.dashboard.activitys.ui.home;

import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.Services.ForegroundService;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Map.MapActivity;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Orders.OrderDetailsActivity;
import com.nextclick.deliveryboynew.dashboard.model.DashboardObject;
import com.nextclick.deliveryboynew.dashboard.model.LocationObject;
import com.nextclick.deliveryboynew.dashboard.model.VendorOrderStatus;
import com.nextclick.deliveryboynew.model.AssignedUserAddress;
import com.nextclick.deliveryboynew.model.DeliveryOrder;
import com.nextclick.deliveryboynew.model.DirectionObject;
import com.nextclick.deliveryboynew.model.OrderDetailsmodel;
import com.nextclick.deliveryboynew.model.OrderStatus;
import com.nextclick.deliveryboynew.utils.DirectionListener;
import com.nextclick.deliveryboynew.utils.LocationUtil.LocationDetailUtil;
import com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationListener;
import com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationUtil;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HomeFragment extends Fragment implements MyLocationListener, View.OnClickListener {

    private HomeViewModel homeViewModel;
    TextView tv_my_location;

    private RecyclerView recyclerView_orders;
    private LinearLayoutManager linearLayoutManager;
    private List<VendorOrderStatus> vendorOrderStatusList;
    private RecyclerView.Adapter adapter;
    LinearLayout layout_no_order,layout_current_order;
    Context mContext;
    PreferenceManager preferenceManager;
    private DashboardObject dashboardObject;
    TextView tv_current_order_id,tv_current_order_placed_date,tv_current_order_travel_distance,tv_current_order_preparation_time;
    TextView tv_current_order_today_earnings, tv_current_order_today_floating_cash, tv_current_order_today_deliveries, tv_current_order_total_deliveries;
    private Location currentLocation;

    public HomeFragment()
    {
        this.homeViewModel=new HomeViewModel(null);
    }

    public HomeFragment(HomeViewModel viewModel)
    {
        this.homeViewModel=viewModel;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
       // homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
       // homeViewModel.initializeGeoLocationLib();

        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        TextView tv_network_status =root.findViewById(R.id.tv_network_status);
        LinearLayout layout_network_status =root.findViewById(R.id.layout_network_status);
          layout_current_order=root.findViewById(R.id.layout_current_order);
        layout_no_order=root.findViewById(R.id.layout_no_order);
        LinearLayout layout_my_location=root.findViewById(R.id.layout_my_location);
        LinearLayout  layout_current_order_track=root.findViewById(R.id.layout_current_order_track);


        tv_current_order_id=root.findViewById(R.id.tv_current_order_id);
        tv_current_order_placed_date=root.findViewById(R.id.tv_current_order_placed_date);
        tv_current_order_travel_distance=root.findViewById(R.id.tv_current_order_travel_distance);
        tv_current_order_preparation_time=root.findViewById(R.id.tv_current_order_preparation_time);


        tv_current_order_today_earnings=root.findViewById(R.id.tv_current_order_today_earnings);
                tv_current_order_today_floating_cash=root.findViewById(R.id.tv_current_order_today_floating_cash);
                tv_current_order_today_deliveries=root.findViewById(R.id.tv_current_order_today_deliveries);
        tv_current_order_total_deliveries=root.findViewById(R.id.tv_current_order_total_deliveries);



        recyclerView_orders = root.findViewById(R.id.recyclerView_vendor_status_list);
        linearLayoutManager = new LinearLayoutManager(this.getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView_orders.setHasFixedSize(true);
        recyclerView_orders.setLayoutManager(linearLayoutManager);


       LinearLayout layout_today_del = root.findViewById(R.id.layout_today_del);
        LinearLayout layout_total_del = root.findViewById(R.id.layout_total_del);
        LinearLayout layout_earnings = root.findViewById(R.id.layout_earnings);
        LinearLayout layout_fCash = root.findViewById(R.id.layout_fCash);

        layout_today_del.setOnClickListener(this);
        layout_total_del.setOnClickListener(this);
        layout_earnings.setOnClickListener(this);
        layout_fCash.setOnClickListener(this);

        layout_current_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //previous flow
                //homeViewModel.setOnClickListener(v);

                //detail flow

                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra("orderID", dashboardObject.getOnGoingOrder().getId());
                intent.putExtra("jobID", dashboardObject.getOnGoingOrder().getDeliveryJobID());
                intent.putExtra("product_id", dashboardObject.getOnGoingOrder().getJobID());
                intent.putExtra("deliveryboy_reject_status", "empty");
                intent.putExtra("OrderStatus", dashboardObject.getOnGoingOrder().getStatus());
                intent.putExtra("checkecomorder", dashboardObject.getOnGoingOrder().isIsecomdetails());
                mContext.startActivity(intent);
            }
        });

        tv_my_location=root.findViewById(R.id.tv_my_location);

        LinearLayout showLocation=root.findViewById(R.id.showLocation);
        showLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MapActivity.class);
                mContext.startActivity(intent);

            }
        });
        final Observer<String> statusObserver = new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_SHORT).show();
            }
        };

        final Observer<Boolean> nameObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable final Boolean status) {
                if(status)
                {
                    if (MyMixPanel.isMixPanelSupport) {
                        MyMixPanel.logEvent("Delivery boy came to online");
                        MyMixPanel.logEvent("Delivery boy available now to accept the orders");
                    }
                    if(layout_network_status!=null) {
                        tv_network_status.setText(getString(R.string.online_status));
                        layout_network_status.setBackgroundColor(0XFF35c534);
                        layout_my_location.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    if (MyMixPanel.isMixPanelSupport) {
                        MyMixPanel.logEvent("Delivery boy went to offline");
                    }
                    if(layout_network_status!=null) {
                        tv_network_status.setText(getString(R.string.offline_status));
                        layout_network_status.setBackgroundColor(0XFFff5251);
                        //layout_current_order.setVisibility(View.GONE);
                        layout_my_location.setVisibility(View.GONE);
                    }
                }
            }
        };
        homeViewModel.getAvailableStatus().observe(getViewLifecycleOwner(), nameObserver);
        homeViewModel.getUpdatedStatus().observe(getViewLifecycleOwner(),statusObserver);

        //homeViewModel.initializeGeoLocationLib();
        final Observer<Location> locObserver = new Observer<Location>() {

            @Override
            public void onChanged(@Nullable final Location location) {

                currentLocation = location;
                List<Address> addresses = homeViewModel.address;
                if (addresses != null && addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    tv_my_location.setText(address);
                }

                if (dashboardObject != null && dashboardObject.getOnGoingOrder() != null)
                    setTravelDistance(dashboardObject.getOnGoingOrder().getVendorAddress().getLocation());

            }
        };
       // homeViewModel.getCurrentLocation().observe(getViewLifecycleOwner(), locObserver);

        //getDashboardDetails();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDashboardDetails();
        getCurrentLocation();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    private void getCurrentLocation() {
        MyLocationUtil.SetContext(mContext);
        MyLocationUtil.startLocationUpdates(this);
    }
    @Override
    public void onLocationReceived(Location location) {
        if (location != null) {
            updateLocation(location);
            if (homeViewModel != null)
                homeViewModel.UpdateCurrentLocation();
        }
    }

    @Override
    public void onLocationUpdated(Location location) {
        if (location != null) {
            updateLocation(location);
            if (homeViewModel != null)
                homeViewModel.UpdateCurrentLocation();
        }
    }

    @Override
    public void onLocationFailed(int code, String message) {

    }

    private void updateLocation(Location location) {
        currentLocation = location;
        homeViewModel.lastKnownLocation=location;
        List<Address> addresses = MyLocationUtil.getAddressFromLocation(location); //homeViewModel.address;
        if (addresses != null && addresses.size() > 0) {
            homeViewModel.address=addresses;
            String address = addresses.get(0).getAddressLine(0);
            tv_my_location.setText(address);
        }
        if (dashboardObject != null && dashboardObject.getOnGoingOrder() != null)
            setTravelDistance(dashboardObject.getOnGoingOrder().getVendorAddress().getLocation());
    }

    private void getVendorStatus(Integer status) {

        vendorOrderStatusList=new ArrayList<>();
        VendorOrderStatus status1= new VendorOrderStatus(status);
        status1.setorderStatusID(101);
        status1.setorderStatus("Accepted");
        vendorOrderStatusList.add(status1);

        status1= new VendorOrderStatus(status);
        status1.setorderStatusID(102);
        status1.setorderStatus("Preparing");
        vendorOrderStatusList.add(status1);

        status1= new VendorOrderStatus(status);
        status1.setorderStatusID(103);
        status1.setorderStatus("Out for Delivery");
        vendorOrderStatusList.add(status1);

        adapter = new VendorOrderStatusAdapter(this.getActivity(), vendorOrderStatusList);
        recyclerView_orders.setAdapter(adapter);

        layout_current_order.setVisibility(View.VISIBLE);
    }



    private void getDashboardDetails() {{
        //LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_DASHBOARD_DETAILS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);

                    Log.d("VolleyResponse", "GET_DASHBOARD_DETAILS response: " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status=jsonObject.getBoolean("status");

                    if(status) {

                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        if (dataObject != null) {

                            dashboardObject = new DashboardObject();

                            dashboardObject.setCurrentStatus(dataObject.getBoolean("current_status"));
                            dashboardObject.setTodayEarnings(dataObject.getString("today_earnings"));
                            dashboardObject.setTodayFloatingCash(dataObject.getString("today_floating_cash"));
                            dashboardObject.setTodayDeliveris(dataObject.getString("today_deliveries"));
                            dashboardObject.setTotalDeliveris(dataObject.getString("total_deliveries"));


                            if (dataObject.has("on_going_order")) {
                                try {
                                    JSONObject onGoingOrder = dataObject.getJSONObject("on_going_order");

                                    Integer orderFinalStatus = 0;
                                    try {
                                        JSONObject delivery_job = onGoingOrder.getJSONObject("delivery_job");
                                        DeliveryOrder deliveryOrder = new DeliveryOrder();
                                        deliveryOrder.setStatus(delivery_job.getString("status"));

                                        orderFinalStatus = Integer.parseInt(deliveryOrder.getStatus());



                                    } catch (JSONException e2) {

                                    }

                                    if (onGoingOrder.getString("is_ecom_order").equalsIgnoreCase("true")){
                                        OrderDetailsmodel deliveryOrder = new OrderDetailsmodel();
                                        deliveryOrder.setIsecomdetails(true);
                                        deliveryOrder.setTrack_id(onGoingOrder.getString("track_id"));
                                        deliveryOrder.setDelivery_fee(onGoingOrder.getString("delivery_fee"));
                                        deliveryOrder.setTotal(onGoingOrder.getString("total"));
                                        deliveryOrder.setUsed_wallet_amount(onGoingOrder.getString("used_wallet_amount"));
                                        deliveryOrder.setMessage(onGoingOrder.getString("message"));
                                        deliveryOrder.setPreparation_time(onGoingOrder.getString("preparation_time"));
                                        deliveryOrder.setShipping_address_id(onGoingOrder.getString("shipping_address_id"));
                                        deliveryOrder.setCreated_at(onGoingOrder.getString("created_at"));
                                        deliveryOrder.setDelivery_mode_id(onGoingOrder.getString("delivery_mode_id"));
                                        deliveryOrder.setCreated_user_id(onGoingOrder.getString("created_user_id"));
                                        deliveryOrder.setOrder_status_id(onGoingOrder.getString("order_status_id"));
                                        deliveryOrder.setPayment_id(onGoingOrder.getString("payment_id"));
                                        deliveryOrder.setId(onGoingOrder.getString("id"));
                                        JSONObject shippingobj = onGoingOrder.getJSONObject("vendor");
                                        AssignedUserAddress assignedUserAddress = new AssignedUserAddress();

                                        assignedUserAddress.setId(shippingobj.getString("id"));
                                        if (shippingobj.has("phone"))
                                            assignedUserAddress.setPhone(shippingobj.getString("phone"));
                                        else
                                            assignedUserAddress.setPhone("");

                                        if (shippingobj.has("email"))
                                            assignedUserAddress.setEmail(shippingobj.getString("email"));
                                        assignedUserAddress.setName(shippingobj.getString("name"));
                                        if (shippingobj.has("landmark"))
                                            assignedUserAddress.setLandmark(shippingobj.getString("landmark"));
                                        if (shippingobj.has("address"))
                                            assignedUserAddress.setAddress(shippingobj.getString("address"));
                                        assignedUserAddress.setLocation_id(shippingobj.getString("location_id"));
                                        if (shippingobj.has("vendor_user_id"))
                                            assignedUserAddress.setVendorUserID(shippingobj.getString("vendor_user_id"));
                                        if (shippingobj.has("unique_id"))
                                            assignedUserAddress.setUniqueID(shippingobj.getString("unique_id"));

                                        if (shippingobj.has("location")) {
                                            JSONObject locationObject = shippingobj.getJSONObject("location");

                                            LocationObject vendorLocation = new LocationObject();
                                            vendorLocation.setLocationID(locationObject.getString("id"));
                                            vendorLocation.setLatitude(locationObject.getString("latitude"));
                                            vendorLocation.setLongitude(locationObject.getString("longitude"));
                                            vendorLocation.setAddress(locationObject.getString("address"));

                                            if (assignedUserAddress.getAddress() == null || assignedUserAddress.getAddress().equals("null"))
                                                assignedUserAddress.setAddress(locationObject.getString("address"));

                                            assignedUserAddress.setLocation(vendorLocation);
                                        }

                                        JSONObject orderostatusobj = onGoingOrder.getJSONObject("order_status");
                                        OrderStatus orderStatus = new OrderStatus();
                                        orderStatus.setId(orderostatusobj.getString("id"));
                                        orderStatus.setDelivery_mode_id(orderostatusobj.getString("delivery_mode_id"));
                                        orderStatus.setSerial_number(orderostatusobj.getString("serial_number"));
                                        orderStatus.setStatus(orderostatusobj.getString("status"));
                                        deliveryOrder.setOrderStatus(orderStatus);

                                        deliveryOrder.setVendorAddress(assignedUserAddress);

                                        setTravelDistance(assignedUserAddress.getLocation());

                                        JSONObject deliveryJobObject = onGoingOrder.getJSONObject("delivery_job");

                                        // deliveryOrder.setDeliveryJobID(deliveryJobObject.getString("id"));//need to confirm with meher
                                        deliveryOrder.setJobID(deliveryJobObject.getString("job_id"));
                                        deliveryOrder.setStatus(deliveryJobObject.getString("status"));

                                    /*if (orderFinalStatus != 508) {
                                        dashboardObject.setOnGoingOrder(deliveryOrder);
                                        layout_current_order.setVisibility(View.VISIBLE);
                                    } else*/
                                        dashboardObject.setOnGoingOrder(deliveryOrder);
                                        layout_current_order.setVisibility(View.VISIBLE);
                                    }else{
                                        OrderDetailsmodel deliveryOrder = new OrderDetailsmodel();
                                        deliveryOrder.setIsecomdetails(false);
                                        deliveryOrder.setTrack_id(onGoingOrder.getString("track_id"));
                                        deliveryOrder.setDelivery_fee(onGoingOrder.getString("delivery_fee"));
                                      //  deliveryOrder.setTotal(onGoingOrder.getString("total"));
                                      //  deliveryOrder.setUsed_wallet_amount(onGoingOrder.getString("used_wallet_amount"));
                                      //  deliveryOrder.setMessage(onGoingOrder.getString("message"));
                                      //  deliveryOrder.setPreparation_time(onGoingOrder.getString("preparation_time"));
                                     //   deliveryOrder.setShipping_address_id(onGoingOrder.getString("shipping_address_id"));
                                        deliveryOrder.setCreated_at(onGoingOrder.getString("created_at"));
                                      //  deliveryOrder.setDelivery_mode_id(onGoingOrder.getString("delivery_mode_id"));
                                      //  deliveryOrder.setCreated_user_id(onGoingOrder.getString("created_user_id"));
                                      //  deliveryOrder.setOrder_status_id(onGoingOrder.getString("order_status_id"));
                                        deliveryOrder.setPayment_id(onGoingOrder.getString("payment_id"));
                                        deliveryOrder.setId(onGoingOrder.getString("id"));

                                        JSONObject shippingobj = onGoingOrder.getJSONObject("pickup_address");
                                        AssignedUserAddress assignedUserAddress = new AssignedUserAddress();

                                        assignedUserAddress.setId(shippingobj.getString("id"));
                                        if (shippingobj.has("phone"))
                                            assignedUserAddress.setPhone(shippingobj.getString("phone"));
                                        else
                                            assignedUserAddress.setPhone("");

                                        if (shippingobj.has("email"))
                                            assignedUserAddress.setEmail(shippingobj.getString("email"));
                                        assignedUserAddress.setName(shippingobj.getString("name"));
                                        if (shippingobj.has("landmark"))
                                            assignedUserAddress.setLandmark(shippingobj.getString("landmark"));
                                        if (shippingobj.has("address"))
                                            assignedUserAddress.setAddress(shippingobj.getString("address"));
                                        assignedUserAddress.setLocation_id(shippingobj.getString("location_id"));

                                        if (shippingobj.has("location")) {
                                            JSONObject locationObject = shippingobj.getJSONObject("location");

                                            LocationObject vendorLocation = new LocationObject();
                                            vendorLocation.setLocationID(locationObject.getString("id"));
                                            vendorLocation.setLatitude(locationObject.getString("latitude"));
                                            vendorLocation.setLongitude(locationObject.getString("longitude"));
                                            vendorLocation.setAddress(locationObject.getString("address"));

                                            if (assignedUserAddress.getAddress() == null || assignedUserAddress.getAddress().equals("null"))
                                                assignedUserAddress.setAddress(locationObject.getString("address"));

                                            assignedUserAddress.setLocation(vendorLocation);
                                        }

                                        JSONObject orderostatusobj = onGoingOrder.getJSONObject("order_status");
                                        OrderStatus orderStatus = new OrderStatus();
                                        orderStatus.setId(orderostatusobj.getString("id"));
                                        orderStatus.setDelivery_mode_id(orderostatusobj.getString("delivery_mode_id"));
                                        orderStatus.setSerial_number(orderostatusobj.getString("serial_number"));
                                        orderStatus.setStatus(orderostatusobj.getString("status"));
                                        deliveryOrder.setOrderStatus(orderStatus);

                                        deliveryOrder.setVendorAddress(assignedUserAddress);

                                        setTravelDistance(assignedUserAddress.getLocation());

                                        JSONObject deliveryJobObject = onGoingOrder.getJSONObject("delivery_job");

                                        // deliveryOrder.setDeliveryJobID(deliveryJobObject.getString("id"));//need to confirm with meher
                                        deliveryOrder.setJobID(deliveryJobObject.getString("job_id"));
                                        deliveryOrder.setStatus(deliveryJobObject.getString("status"));

                                    /*if (orderFinalStatus != 508) {
                                        dashboardObject.setOnGoingOrder(deliveryOrder);
                                        layout_current_order.setVisibility(View.VISIBLE);
                                    } else*/
                                        dashboardObject.setOnGoingOrder(deliveryOrder);
                                        layout_current_order.setVisibility(View.VISIBLE);
                                    }



                                } catch (Exception e) {
                                    layout_current_order.setVisibility(View.GONE);
                                    stopForegroundService();
                                }

                            }
                            else
                                stopForegroundService();

                            showDashboardDetails();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //LoadingDialog.dialog.dismiss();
                }
                finally {
                   // LoadingDialog.dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    }


    private void setTravelDistance(LocationObject locationObject) {
        if(!tv_current_order_travel_distance.getText().equals("--")  && locationObject==null || currentLocation ==null) {
            tv_current_order_travel_distance.setText("--");
            return;
        }

        DirectionListener shopDistanceListener = new DirectionListener() {
            @Override
            public void onDirectionResult(DirectionObject result) {
                if( tv_current_order_travel_distance != null) {
                    if (result != null) {
                        tv_current_order_travel_distance.setText(result.getTravelDistance());
                    }
                    else
                        tv_current_order_travel_distance.setText("--");
                }
            }
        };

        LocationDetailUtil.setCurrentLocation(currentLocation);

        LocationDetailUtil.getTravelDistanceFromCurrentLocation(mContext, shopDistanceListener,
                locationObject.getlongitude(),
                locationObject.getLatitude());
    }

    private void showDashboardDetails() {
        if (dashboardObject != null) {
            homeViewModel.changeNetworkState(dashboardObject.getCurrentStatus());

            tv_current_order_today_earnings.setText(dashboardObject.getTodayEarnings());
            tv_current_order_today_floating_cash.setText(dashboardObject.getTodayFloatingCash());
            tv_current_order_today_deliveries.setText(dashboardObject.getTodayDeliveris());
            tv_current_order_total_deliveries.setText(dashboardObject.getTotalDeliveris()   );


            if (dashboardObject.getOnGoingOrder() != null) {
                //layout_no_order.setVisibility(View.GONE);
                //layout_current_order.setVisibility(View.VISIBLE);//showing in vendor status to fill gap to get vendor status list

                tv_current_order_id.setText(dashboardObject.getOnGoingOrder().getTrack_id());
                tv_current_order_placed_date.setText(dashboardObject.getOnGoingOrder().getOrderDateInSimpleFormat());
                tv_current_order_preparation_time.setText(dashboardObject.getOnGoingOrder().getPreparation_time()+" mins");


//                startForegroundService(dashboardObject.getOnGoingOrder().getTrack_id(),dashboardObject.getOnGoingOrder().getOrderDateInSimpleFormat());

                Integer status = 101;
                OrderStatus orderStatus = dashboardObject.getOnGoingOrder().getOrderStatus();
                if (orderStatus != null) {
                    status = Integer.parseInt(orderStatus.getSerial_number());
                }
                getVendorStatus(status);
            } else {
                // layout_no_order.setVisibility(View.VISIBLE);
                layout_current_order.setVisibility(View.GONE);
            }
        } else {
            //layout_no_order.setVisibility(View.VISIBLE);
            layout_current_order.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.layout_today_del:
            case R.id.layout_total_del:
            case R.id.layout_earnings:
            case R.id.layout_fCash:
                homeViewModel.setOnClickListener(v);
                break;
        }
    }

    private void startForegroundService(String track_id, String orderDateInSimpleFormat) {
        boolean IsForegroundServiceStarted =false;// preferenceManager.getBoolean("IsForegroundServiceStarted");
        if(!IsForegroundServiceStarted) {
            preferenceManager.putBoolean("IsForegroundServiceStarted", true);

            Intent serviceIntent = new Intent(mContext, ForegroundService.class);
            serviceIntent.putExtra("inputTitle", "ORDER #"+track_id);
            serviceIntent.putExtra("inputBody", "Order Date:"+orderDateInSimpleFormat);
            serviceIntent.putExtra(ForegroundService.KEY_FOREGROUND_SERVICE_ACTION_TYPE, ForegroundService.ACTION_START_FG_SERVICE);
            ContextCompat.startForegroundService(mContext, serviceIntent);
        }
    }

    private void stopForegroundService() {
        boolean IsForegroundServiceStarted = preferenceManager.getBoolean("IsForegroundServiceStarted");
        if(IsForegroundServiceStarted)
        {
            preferenceManager.putBoolean("IsForegroundServiceStarted", false);
            Intent serviceIntent = new Intent(mContext, ForegroundService.class);
            serviceIntent.putExtra(ForegroundService.KEY_FOREGROUND_SERVICE_ACTION_TYPE, ForegroundService.ACTION_UPDATE_FG_SERVICE_NOTIFICATION);
            mContext.stopService(serviceIntent);

            MyLocationUtil.removeLocationUpdates(this);
        }
    }
}