package com.nextclick.deliveryboynew.dashboard.activitys.ui.Payment;

import static com.nextclick.deliveryboynew.Config.Config.WALLETHISTORY;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.UiMsgs.setEditTextErrorMethod;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.helpers.CustomDialog;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;
import com.nextclick.deliveryboynew.utils.payment.RazorPayHelper;
import com.razorpay.Checkout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PaymentFragment extends Fragment {

    private RecyclerView mList;
    private TextView start_date,tv_get,end_date,tv_earnings,tv_floating;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private PaymentHistoryAdapter adapter;
    String year,month,day,start_date_str,end_date_str;
    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="";
    private ArrayList<TransactionsPojo> transactionlist;
    private CustomDialog mCustomDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;

    String SelectedPaymentType= TYPE_FLOATINGS;
    public static final String TYPE_EARNINGS = "EARNINGS";
    public static final String TYPE_FLOATINGS = "FLOATINGS";
    LinearLayout relative_empty_cart;
    private String totalamount;
    private String notes,secirity_deposit_amount;
    private int whichposition;

    public PaymentFragment() {
        SelectedPaymentType=TYPE_EARNINGS;
    } public PaymentFragment(String typeEarnings,int whichposition,String secirity_deposit_amount) {
        SelectedPaymentType=typeEarnings;
        this.whichposition=whichposition;
        this.secirity_deposit_amount=secirity_deposit_amount;
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_payment, container, false);


        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to Payment activity");
        }

        mList = root.findViewById(R.id.recyclerview_payment_history);
        tv_earnings = root.findViewById(R.id.tv_earnings);
        tv_floating = root.findViewById(R.id.tv_floating);
       TextView tv_earnings_header = root.findViewById(R.id.tv_earnings_header);
        TextView tv_floating_header = root.findViewById(R.id.tv_floating_header);
        LinearLayout layout_earnings= root.findViewById(R.id.layout_earnings);
        LinearLayout  layout_floatings= root.findViewById(R.id.layout_floatings);

        layout_earnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(SelectedPaymentType!= TYPE_EARNINGS)
               {
                   SelectedPaymentType=TYPE_EARNINGS ;
                   layout_earnings.setBackgroundColor(0XFF660033);
                   tv_earnings_header.setTextColor(0XFFFFFFFF);
                   tv_earnings.setTextColor(0XFFFFFFFF);


                   layout_floatings.setBackgroundColor(0XFFCDCDCD);
                   tv_floating_header.setTextColor(0XFF000000);
                   tv_floating.setTextColor(0XFF000000);

                   getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
               }
            }
        });
        layout_floatings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!= TYPE_FLOATINGS)
                {
                    SelectedPaymentType= TYPE_FLOATINGS;
                    layout_floatings.setBackgroundColor(0XFF660033);
                    tv_floating_header.setTextColor(0XFFFFFFFF);
                    tv_floating.setTextColor(0XFFFFFFFF);

                    layout_earnings.setBackgroundColor(0XFFCDCDCD);
                    tv_earnings_header.setTextColor(0XFF000000);
                    tv_earnings.setTextColor(0XFF000000);

                    getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
                }
            }
        });

        start_date = root.findViewById(R.id.start_date);
        tv_get = root.findViewById(R.id.tv_get);
        end_date = root.findViewById(R.id.end_date);
        relative_empty_cart= root.findViewById(R.id.relative_empty_cart);
        mCustomDialog=new CustomDialog(getContext());

        mContext=getContext();
        preferenceManager=new PreferenceManager(getContext());
        if (whichposition==1){
            showDialog(1);

        }
        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(getActivity(),AddFloatingCashActivity.class);
                //startActivity(intent);
                showDialog(0);
                        
            }
        });

        getDate();
        transactionlist = new ArrayList<>();
        adapter = new PaymentHistoryAdapter(this.getActivity(), transactionlist);

        linearLayoutManager = new LinearLayoutManager(this.getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
       // mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);


        if(SelectedPaymentType==TYPE_FLOATINGS)
        {
            layout_floatings.setBackgroundColor(0XFF660033);
            tv_floating_header.setTextColor(0XFFFFFFFF);
            tv_floating.setTextColor(0XFFFFFFFF);

            layout_earnings.setBackgroundColor(0XFFCDCDCD);
            tv_earnings_header.setTextColor(0XFF000000);
            tv_earnings.setTextColor(0XFF000000);
            getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
        }
        else
            getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");

        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datepicker(0);
            }
        });
        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datepicker(1);
            }
        });
        tv_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");
            }
        });
        return root;
    }

    private void showDialog(int whichposition) {

        final Dialog dialog = new Dialog(this.getActivity());
        dialog.setContentView(R.layout.activity_add_floating_cash);
        Button applyButton = (Button) dialog.findViewById(R.id.add_cash);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        TextInputEditText tv_amount= (TextInputEditText) dialog.findViewById(R.id.tv_amount);
        TextInputEditText tv_note= (TextInputEditText) dialog.findViewById(R.id.tv_note);
        dialog.setCancelable(false);
        if (whichposition==1){
            tv_amount.setText(""+secirity_deposit_amount);
            tv_note.setText("Security Deposit Amount");
            tv_amount.setClickable(false);
            tv_amount.setEnabled(false);
        }
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // invokeDeliveryOrder(OrderPickupOtp);
                if (isValid(tv_amount,tv_note)) {
                    dialog.dismiss();
                    invokeAddCashAPI(tv_amount.getText().toString(), tv_note.getText().toString());
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whichposition==1){
                    Toast.makeText(mContext, getResources().getString(R.string.security_deposit_text), Toast.LENGTH_SHORT).show();
                }else{
                    dialog.dismiss();
                }

            }
        });

        //dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        DisplayMetrics displayMetrics = new DisplayMetrics();
       getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = displayWidth;
        dialog.getWindow().setAttributes(layoutParams);
    }

    private boolean isValid(TextInputEditText tv_amount, TextInputEditText tv_note) {
        boolean valid = true;
        if (tv_amount.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(tv_amount, getString(R.string.oops));
            valid = false;
        }  else if (tv_note.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(tv_note, getString(R.string.empty));
            valid = false;
        }
        return valid;
    }

    private void invokeAddCashAPI(String amount, String note) {

       /* Intent intent = new Intent(mContext, PaymentModesActivity.class);
        intent.putExtra("amount", amout);
        intent.putExtra("note", note);
        mContext.startActivity(intent);
        */

        totalamount =amount;
        notes = note;

        LoadingDialog.loadDialog(mContext);

        final Checkout co = new Checkout();
        co.setImage(R.drawable.nextclick_icon_receipt);
        try {
            JSONObject orderRequest = new JSONObject();
           //  orderRequest.put("amount", Utility.roundFloat(""+(100* Float.parseFloat(totalamount)),2)); // amount in the smallest currency unit
            orderRequest.put("amount", "100"); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image", R.drawable.nextclick_logo_white);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            co.open(getActivity(), orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        }
    }

    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str=year+"-"+month+"-"+day;
        end_date_str=year+"-"+month+"-"+day;

        start_date.setText(""+day+"/"+month+"/"+year);
        end_date.setText(""+day+"/"+month+"/"+year);

    }
    public void datepicker(final int i){
        final Calendar c;
         int mYear=0;
         int mMonth=0;
         int mDay=0;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }else {
            if (!fromyear.equalsIgnoreCase("")||!fromyear.isEmpty()||mYear==0){
                c = Calendar.getInstance();
                c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
            }else{
                Toast.makeText(mContext, "Please select startdate", Toast.LENGTH_SHORT).show();
            }

        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    start_date_str=fromyear+"-"+frommonth+"-"+fromday;
                    start_date.setText(""+fromday+"/"+frommonth+"/"+fromyear);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    end_date_str=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                    //  end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+today+"/"+tomonth+"/"+toyear);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }

    public void getWallet(String start_date_str, String end_date_str, String s, String s1){
        transactionlist.clear();

        /*
                {
"start_date" :"2021-02-02",
"end_date" :"2021-02-02",
"type" :"CREDIT",
"status": 1
}
         */

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        //uploadMap.put("type", "CREDIT");
        uploadMap.put("status", ""+(SelectedPaymentType!= TYPE_FLOATINGS ?1:2));

        JSONObject json = new JSONObject(uploadMap);
        Log.d("VolleyResponse", "WALLETHISTORY request: " + json);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "WALLETHISTORY response: " + response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");


                                try{
                                    JSONArray transarray=dataobj.getJSONArray("wallet_transactions");
                                    for (int i=0;i<transarray.length();i++){
                                        JSONObject transobj=transarray.getJSONObject(i);
                                        TransactionsPojo transactionsPojo=new TransactionsPojo();
                                        transactionsPojo.setId(transobj.getString("id"));
                                        transactionsPojo.setTxn_id(transobj.getString("txn_id"));
                                        transactionsPojo.setAccount_user_id(transobj.getString("account_user_id"));
                                        transactionsPojo.setAmount(transobj.getString("amount"));
                                        transactionsPojo.setType(transobj.getString("type"));
                                        transactionsPojo.setBalance(transobj.getString("balance"));
                                        transactionsPojo.setEcom_order_id(transobj.getString("ecom_order_id"));
                                        transactionsPojo.setMessage(transobj.getString("message"));
                                        transactionsPojo.setCreated_at(transobj.getString("created_at"));
                                        transactionsPojo.setUpdated_at(transobj.getString("updated_at"));
                                        transactionsPojo.setStatus(transobj.getString("status"));

                                        try {
                                            if(transobj.has("track_id"))
                                                transactionsPojo.setTrack_id(transobj.getString("track_id"));
                                            else if (transobj.has("order") && transobj.getJSONObject("order").has("track_id"))
                                                transactionsPojo.setTrack_id(transobj.getJSONObject("order").getString("track_id"));
                                        }
                                        catch (Exception ex){}

                                        transactionlist.add(transactionsPojo);
                                    }
                                    adapter.setrefresh(transactionlist);

                                    if(transactionlist.size()>0)
                                    {
                                        relative_empty_cart.setVisibility(View.GONE);
                                        mList.setVisibility(View.VISIBLE);
                                    }
                                    else
                                    {

                                        relative_empty_cart .setVisibility(View.VISIBLE);
                                        mList.setVisibility(View.GONE);
                                    }

                                }catch (JSONException e1){
                                            showNoDataFound();
                                }

                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("user");
                                    tv_earnings.setText("₹ "+paymentobj.getString("wallet"));
                                    tv_floating.setText("₹ "+paymentobj.getString("floating_wallet"));
                                }catch (JSONException e2){

                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));


                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void showNoDataFound() {
        relative_empty_cart.setVisibility(View.VISIBLE);
        mList.setVisibility(View.GONE);
    }

    public void sendPaymentstatus(String paymentid,String paymenttransactionid,
                                  String status,String mesage){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("transaction_id", paymenttransactionid);
        uploadMap.put("amount", totalamount);
        uploadMap.put("note", notes);

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaa json PAYMENTSTATUS "+json.toString());

        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.PAYMENTSTATUS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject PAYMENTSTATUS "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            if (status){

                                Toast.makeText(mContext, "Payment Success", Toast.LENGTH_SHORT).show();
                                getWallet(start_date_str,end_date_str,SelectedPaymentType,"1");

                            }
                            else
                            {
                                //payment failed
                                Toast.makeText(mContext, "Payment Failed", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }



    public void sendPaymentstatus(Integer paymentStatus, String paymentID, String error) {
        //if (paymentCompletedListener != null)
        //  paymentCompletedListener.onPaymentCompleted(paymentStatus, paymentID, error);
        LoadingDialog.dialog.dismiss();
        if(paymentStatus== RazorPayHelper.PAYMENT_SUCCESS)
        {

            sendPaymentstatus(""+2,paymentID,""+RazorPayHelper.PAYMENT_SUCCESS,"");
        }
        else
        {
            //  sendPaymentstatus(""+3,"",""+RazorPayHelper.PAYMENT_FAIL,error);
            Toast.makeText(mContext, getString(R.string.payment_failed)+" "+error, Toast.LENGTH_SHORT).show();
        }
    }
}