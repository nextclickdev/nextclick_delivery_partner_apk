package com.nextclick.deliveryboynew.dashboard.activitys.ui.Orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.model.OrderStatus;

import java.util.List;

public class SpinnerStatusList extends BaseAdapter {
    Context context;
    List<OrderStatus> orderStatusList;
    LayoutInflater inflter;

    public SpinnerStatusList(Context applicationContext,  List<OrderStatus> orderStatusList) {
        this.context = applicationContext;
        this.orderStatusList = orderStatusList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return orderStatusList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_item_status_type, null);
        RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.relative_layout_status);
        ImageView icon = (ImageView) view.findViewById(R.id.tv_support_icon);
        TextView tv_support_type = (TextView) view.findViewById(R.id.tv_support_type);
        TextView tv_support_type_selected = (TextView) view.findViewById(R.id.tv_support_type_selected);

        OrderStatus orderStatus =orderStatusList.get(i);
        tv_support_type.setText(orderStatus.getStatus());
        tv_support_type_selected.setText(orderStatus.getStatus());

        if(orderStatus.getStatus().equals("On its way"))
        {
            //layout.setBackgroundColor(0XFF660033);
            icon.setImageResource(R.drawable.ic_transfer_within_a_station_24px);
        }
        else if(orderStatus.getStatus().equals("Reject"))
        {
            layout.setBackgroundColor(0XFFE2E2E2);
            icon.setImageResource(R.drawable.ic_reject);
        }
        else
        {
            layout.setBackgroundColor(0XFFEBEBEB);
            icon.setImageResource(R.drawable.ic_accpeted);
        }

        return view;
    }
}

