package com.nextclick.deliveryboynew.dashboard.activitys.ui.Map;

import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.maps.SupportMapFragment;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.utils.LocationUtil.LocationDetailUtil;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapActivity extends AppCompatActivity implements MyLocationListener,OnMapReadyCallback, MapParser {

    private GoogleMap mMap;
    PreferenceManager preferenceManager;
    Context mContext;

    private RecyclerView recyclerview_address_details;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter adapter;
    TextView tv_no_data;
    Marker prevMarker =null;
    Polyline mPolyline;
    LatLng originPoint;
    MapRouteNavigationHelper mapRouteNavigationHelper;
    private Location myCurrentLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to see near by vendors list");
        }


        recyclerview_address_details = findViewById(R.id.recyclerview_address_details);
        tv_no_data= findViewById(R.id.tv_no_data);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(recyclerview_address_details.getContext(), linearLayoutManager.getOrientation());

        recyclerview_address_details.setHasFixedSize(true);
        recyclerview_address_details.setLayoutManager(linearLayoutManager);
        recyclerview_address_details.addItemDecoration(dividerItemDecoration);


        mContext=getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        // Obtain the SupportMapFragment and get notified
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

       // getNearestVendorList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNearestVendorList();
        registerLocationUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
      //  MyLocationUtil.removeLocationUpdates(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);

        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());

        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);

        // below line is use to draw our
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);

        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }



    private void getNearestVendorList() {
       // LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_NEAREST_VENDORS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("VolleyResponse", "GET_NEAREST_VENDORS response: " + response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray dataArray = jsonObject.getJSONArray("data");

                                List<VendorDetails> vendorDetails =new ArrayList<>();

                                if(dataArray.length()>0) {
                                    for (int i = 0; i < dataArray.length(); i++) {
                                       try{
                                           JSONObject jsonObjectData = dataArray.getJSONObject(i);
                                           VendorDetails vendorDetail = new VendorDetails();
                                           vendorDetail.setId(jsonObjectData.getString("id"));
                                           vendorDetail.setvendor_user_id(jsonObjectData.getString("vendor_user_id"));
                                           vendorDetail.setName(jsonObjectData.getString("name"));
                                           vendorDetail.setUnique_id(jsonObjectData.getString("unique_id"));
                                           vendorDetail.setLocation_id(jsonObjectData.getString("location_id"));
                                           vendorDetail.setLatitude(jsonObjectData.getDouble("latitude"));
                                           vendorDetail.setLongitude(jsonObjectData.getDouble("longitude"));
                                           vendorDetails.add(vendorDetail);
                                       }
                                       catch (Exception ex){
                                           ex.printStackTrace();
                                       }
                                    }
                                    addMarkers(vendorDetails);
                                }
                                else
                                {

                                    recyclerview_address_details.setVisibility(View.GONE);
                                    tv_no_data.setVisibility(View.VISIBLE);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                                recyclerview_address_details.setVisibility(View.GONE);
                                tv_no_data.setVisibility(View.VISIBLE);
                            }
                            finally {
                                //  LoadingDialog.dialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      // LoadingDialog.dialog.dismiss();
                        Log.d("VolleyResponse", "GET_NEAREST_VENDORS onErrorResponse: " + error);

                        recyclerview_address_details.setVisibility(View.GONE);
                        tv_no_data.setVisibility(View.VISIBLE);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void addMarkers(List<VendorDetails> locations)
    {
        adapter = new MapAddressAdapter(this, locations);
        recyclerview_address_details.setAdapter(adapter);

        recyclerview_address_details.setVisibility(View.VISIBLE);
        tv_no_data.setVisibility(View.GONE);

        for(int i=0;i<locations.size();i++) {
            VendorDetails  vendorDetail = locations.get(i);
            LatLng location =new LatLng(vendorDetail.getLatitude(), vendorDetail.getLongitude());
            mMap.addMarker(new MarkerOptions().position(location).title(vendorDetail.name)
                    // below line is use to add custom marker on our map.
                    .icon(BitmapFromVector(getApplicationContext(), R.drawable.ic_baseline_location_on_24)));

            moveToCurrentLocation(location);
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        }
    }

    private void moveToCurrentLocation(LatLng currentLocation)
    {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,15));
        // Zoom in, animating the camera.
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


    }

    public void drawVendorLocation(VendorDetails vendorDetail) {
        if (LocationDetailUtil.CurrentLocation != null) {
            myCurrentLocation=LocationDetailUtil.CurrentLocation;
            originPoint = new LatLng(vendorDetail.getLatitude(), vendorDetail.getLongitude());
            drawRoute(myCurrentLocation);
        }
    }

    private void registerLocationUpdates() {
     //   MyLocationUtil.SetContext(this);
      //  MyLocationUtil.startLocationUpdates(this);

        LocationDetailUtil.getCurrentLocation(this,this);

    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null) {
          myCurrentLocation=location;
        }
    }
    @Override
    public void onLocationUpdated(Location location) {
        if (location != null) {
            myCurrentLocation=location;
        }
    }

    @Override
    public void onLocationFailed(int code, String message) {

    }


    private void drawRoute(Location location) {
        LatLng myLatlng = new LatLng(location.getLatitude(), location.getLongitude());
        if (prevMarker != null)
            prevMarker.setVisible(false);
        prevMarker = addMarkerObject(myLatlng, "My Current Location",R.drawable.motor_pin);
        if (mapRouteNavigationHelper == null)
            mapRouteNavigationHelper = new MapRouteNavigationHelper(this, this);
        mapRouteNavigationHelper.drawRoute(myLatlng,originPoint);
    }
    private Marker addMarkerObject(LatLng latLng, String Address, int id) {

        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(Address)
                .icon(BitmapFromVector(getApplicationContext(), id)));


        CameraPosition position = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15.0f)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
       /*
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
        }*/
        return marker;
    }

    @Override
    public void onPolylineOptionsUpdated(PolylineOptions lineOptions) {
        if(mPolyline != null){
            mPolyline.remove();
        }
        mPolyline = mMap.addPolyline(lineOptions);
    }
}