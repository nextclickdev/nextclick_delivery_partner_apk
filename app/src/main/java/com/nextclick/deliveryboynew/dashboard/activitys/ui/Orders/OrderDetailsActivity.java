package com.nextclick.deliveryboynew.dashboard.activitys.ui.Orders;

import static com.nextclick.deliveryboynew.Config.Config.INVOKE_DELIVERY_ORDER;
import static com.nextclick.deliveryboynew.Config.Config.INVOKE_DELIVERY_PICKDROPORDER;
import static com.nextclick.deliveryboynew.Config.Config.PAYMENT_LINK;
import static com.nextclick.deliveryboynew.Config.Config.REJECT_ORDERS;
import static com.nextclick.deliveryboynew.Config.Config.RESONS_REJECTED;
import static com.nextclick.deliveryboynew.Config.Config.REVIEW_CREATION;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.FloatingWidgetService.checkservice;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.activitys.OrdersFragment;
import com.nextclick.deliveryboynew.dashboard.model.LocationObject;
import com.nextclick.deliveryboynew.dashboard.model.RejectOrdersRequets;
import com.nextclick.deliveryboynew.helpers.DialogsReson;
import com.nextclick.deliveryboynew.helpers.FloatingWidgetService;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.model.AssignedUserAddress;
import com.nextclick.deliveryboynew.model.CustomerModel;
import com.nextclick.deliveryboynew.model.DeleveryModesmodel;
import com.nextclick.deliveryboynew.model.DeliveryOrder;
import com.nextclick.deliveryboynew.model.DirectionObject;
import com.nextclick.deliveryboynew.model.ImagesModel;
import com.nextclick.deliveryboynew.model.ItemModel;
import com.nextclick.deliveryboynew.model.OrderDetailsmodel;
import com.nextclick.deliveryboynew.model.OrderStatus;
import com.nextclick.deliveryboynew.model.PaymentModel;
import com.nextclick.deliveryboynew.model.ProductDetailsModel;
import com.nextclick.deliveryboynew.model.SectionItemModel;
import com.nextclick.deliveryboynew.model.VarientModel;
import com.nextclick.deliveryboynew.model.customerDetailsModel;
import com.nextclick.deliveryboynew.utils.DirectionListener;
import com.nextclick.deliveryboynew.utils.LocationUtil.LocationDetailUtil;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.Utility;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OrderDetailsActivity extends AppCompatActivity implements LocationListener {

    RecyclerView recyclerview_order_details;
    private RecyclerView.Adapter adapter;
    private List<ProductDetailsModel> OrderDetailList;
    PreferenceManager preferenceManager;
    OrderDetailsmodel orderDetailsmodel;
    boolean checkecomorder;
    String orderID, OrderPickupOtp, jobID, deliveryboystatus, OrderStatus, deliveryboy_reject_status = "", delivery_boy_status = "", customermobile = "";
    String OrderDeliveryOtp, order_return_otp;//Hidden- actually user entered top
    TextView tv_order_job_id, tv_order_date_time, tv_order_my_status, tv_vendor_order_status, tv_reject;
    TextView tv_price_detail_id, tv_price_sub_total, tv_price_discount, tv_price_gst_amount;
    TextView tv_price_product_amount_total, tv_delivery_fee, tv_price_product_amount_total_include_del_fee, tv_tax_amount, tv_final_product_price, tv_product_quantity, order_receive_otp, tv_payment_method, tv_user_rating, tv_user_rating_feedback;
    private int REQUEST_CAMERA = 0, reject_type = 0;

    public static final String TRACKING_PICKUP = "PickUp";
    public static final String TRACKING_DELIVERY = "Delivery";
    //added on 16th april
    public TextView seller_details, vendor_details, address_details, cust_address_details, tv_order_id, tv_order_earnings,
            tv_delivery_distance, tv_pick_up_distance;
    Button btn_reached_pickup_point, btn_reached_delivery_point, btn_reached_delivery_point_to_vendor;
    LinearLayout pickup_layout_map, delivery_layout_map, layout_pickup_image, layout_delivery_image, order_receive_otp_layout, lay_pickup_address, rating_cart;
    ImageView img_pick_up, img_delivery;
    private String pickUpOrder64String, deliverOrder64String;
    private String trackingMode;
    Button btn_start_pickup, btn_end_delivery, btn_generate_payment_link, btn_return_delivery,img_phone;
    private boolean isCameraPermissionGranted, showCameraAction;
    private final int MY_CAMERA_PERMISSION_CODE = 100;
    private String amountTobeCollected;
    private static final int DRAW_OVER_OTHER_APP_PERMISSION = 123;
    private String trackID;
    double currentlatitude = 0.0, currentlongitude = 0.0, userlatitude, userlongitude, distance;
    private LocationManager locationManager;
    private int order_confirmation_time = 0;
    android.app.AlertDialog dialog4;
    LinearLayout order_cart, delivery_estimate;
    String pickupOTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        Context context = this;
        askForSystemOverlayPermission();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to order details activity");
        }

        LocationDetailUtil.getCurrentLocation(this, this);
        orderID = getIntent().getStringExtra("orderID");
        jobID = getIntent().getStringExtra("jobID");
        OrderPickupOtp = getIntent().getStringExtra("OrderPickupOtp");
        OrderDeliveryOtp = getIntent().getStringExtra("OrderDeliveryOtp");
        OrderStatus = getIntent().getStringExtra("OrderStatus");
        checkecomorder = getIntent().getBooleanExtra("checkecomorder", false);
        try {
            deliveryboy_reject_status = getIntent().getStringExtra("deliveryboy_reject_status");
        } catch (NullPointerException e) {
            deliveryboy_reject_status = "empty";
        }

        OrderDetailList = new ArrayList<>();
        tv_order_job_id = findViewById(R.id.tv_order_job_id);
        tv_order_date_time = findViewById(R.id.tv_order_date_time);
        tv_order_my_status = findViewById(R.id.tv_order_my_status);
        tv_vendor_order_status = findViewById(R.id.tv_vendor_order_status);
        tv_price_detail_id = findViewById(R.id.tv_price_detail_id);
        tv_price_sub_total = findViewById(R.id.tv_price_sub_total);
        tv_price_discount = findViewById(R.id.tv_price_discount);
        img_phone = findViewById(R.id.btn_call_customer);
        tv_price_gst_amount = findViewById(R.id.tv_price_gst_amount);
        tv_price_product_amount_total = findViewById(R.id.tv_price_product_amount_total);
        tv_delivery_fee = findViewById(R.id.tv_delivery_fee);
        tv_price_product_amount_total_include_del_fee = findViewById(R.id.tv_price_product_amount_total_include_del_fee);
        tv_tax_amount = findViewById(R.id.tv_tax_amount);
        tv_final_product_price = findViewById(R.id.tv_final_product_price);
        tv_payment_method = findViewById(R.id.tv_payment_method);
        tv_product_quantity = findViewById(R.id.tv_product_quantity);
        order_receive_otp = findViewById(R.id.order_receive_otp);
        order_receive_otp_layout = findViewById(R.id.order_receive_otp_layout);
        lay_pickup_address = findViewById(R.id.lay_pickup_address);
        tv_reject = findViewById(R.id.tv_reject);
        order_cart = findViewById(R.id.order_cart);
        delivery_estimate = findViewById(R.id.delivery_estimate);
        if (OrderPickupOtp != null && !OrderPickupOtp.isEmpty())
            order_receive_otp.setText(OrderPickupOtp);


        tv_reject.setVisibility(View.VISIBLE);
        layout_pickup_image = findViewById(R.id.layout_pickup_image);
        layout_delivery_image = findViewById(R.id.layout_delivery_image);
        img_pick_up = findViewById(R.id.img_pick_up);
        img_delivery = findViewById(R.id.img_delivery);

        if (checkecomorder) {
            order_cart.setVisibility(View.VISIBLE);
            delivery_estimate.setVisibility(View.VISIBLE);
        } else {
            order_cart.setVisibility(View.GONE);
            delivery_estimate.setVisibility(View.GONE);
        }

        getLocation();

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
                    101);
        }

        img_pick_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog(img_pick_up);
            }
        });
        img_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog(img_delivery);
            }
        });
        tv_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requesttypes();
            }
        });

        tv_order_id = findViewById(R.id.tv_order_id);

        ImageView back_image = findViewById(R.id.back_image);
        //  tv_order_id.setText(getIntent().getStringExtra("product_id"));


        preferenceManager = new PreferenceManager(this);
        //added on 16th april
        seller_details = findViewById(R.id.seller_details);
        vendor_details = findViewById(R.id.vendor_details);
        address_details = findViewById(R.id.address_details);
        cust_address_details = findViewById(R.id.cust_address_details);


        tv_pick_up_distance = findViewById(R.id.tv_pick_up_distance);
        tv_delivery_distance = findViewById(R.id.tv_delivery_distance);

        pickup_layout_map = findViewById(R.id.pickup_layout_map);
        delivery_layout_map = findViewById(R.id.delivery_layout_map);

        rating_cart = findViewById(R.id.rating_cart);
        rating_cart.setVisibility(View.GONE);
        tv_user_rating = findViewById(R.id.tv_user_rating);
        tv_user_rating_feedback = findViewById(R.id.tv_user_rating_feedback);

        btn_reached_pickup_point = findViewById(R.id.btn_reached_pickup_point);
        btn_reached_delivery_point = findViewById(R.id.btn_reached_delivery_point);
        btn_reached_delivery_point_to_vendor = findViewById(R.id.btn_reached_delivery_point_to_vendor);

        btn_start_pickup = findViewById(R.id.btn_start_pickup);
        btn_end_delivery = findViewById(R.id.btn_end_delivery);
        btn_return_delivery = findViewById(R.id.btn_return_delivery);
        //by default
        btn_start_pickup.setVisibility(View.GONE);
        btn_end_delivery.setVisibility(View.GONE);
        btn_return_delivery.setVisibility(View.GONE);

        btn_generate_payment_link = findViewById(R.id.btn_generate_payment_link);
        btn_generate_payment_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePaymentOnlineLink();
            }
        });

        changeViewByOrderStatus();
        LinearLayout track_shipping_address = findViewById(R.id.track_shipping_address);
        LinearLayout track_delivery_address = findViewById(R.id.track_delivery_address);
        track_shipping_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMapActivity(TRACKING_PICKUP, orderDetailsmodel.getVendorAddress());
            }
        });
        track_delivery_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMapActivity(TRACKING_DELIVERY, orderDetailsmodel.getShippingAddress());
            }
        });
        btn_reached_pickup_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeAction(OrdersFragment.PICKUP_POINT);
            }
        });
        btn_reached_delivery_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeAction(OrdersFragment.DELIVERY_POINT);
            }
        });
        btn_reached_delivery_point_to_vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reachedToDeliveryToVendor();
            }
        });

        recyclerview_order_details = findViewById(R.id.recyclerview_order_details);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview_order_details.setHasFixedSize(true);
        recyclerview_order_details.setLayoutManager(linearLayoutManager);

        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_start_pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkecomorder) {
                    if (orderDetailsmodel.getOrderStatus().getId().equalsIgnoreCase("12")) {
                        boolean checkproducts = true;
                        if (OrderDetailList != null && OrderDetailList.size() > 0) {
                            for (int i = 0; i < OrderDetailList.size(); i++) {
                                if (OrderDetailList.get(i).getProductCheckState() == false) {
                                    checkproducts = false;
                                    break;
                                }
                            }
                        }

                        if (checkproducts) {
                            invokePickedTheOrderAPI();
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.check_all_products), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        getOrderDetails();
                        Toast.makeText(context, "Please share OTP with " + orderDetailsmodel.getVendorAddress().getName(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    startPickup();
                }



               /* if (isAllProductsSelected())
                {
                   // startPickup();
                    if (orderDetailsmodel.getOrderStatus().getId().equalsIgnoreCase("12")){
                      //  Toast.makeText(context, "enter", Toast.LENGTH_SHORT).show();
                        invokePickedTheOrderAPI();
                    }else{
                        Toast.makeText(context, "Please share OTP with "+orderDetailsmodel.getVendorAddress().getName(), Toast.LENGTH_SHORT).show();
                      //
                    }

                }
                else {
                    Toast.makeText(getApplicationContext(), getString(R.string.check_all_products), Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        btn_end_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endDelivery(null, null);//OrderDeliveryOtp
            }
        });
        btn_return_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("aaaaaaaaaa returned  " + delivery_boy_status);
                if (delivery_boy_status.equalsIgnoreCase("602")) {
                    endretrndelivery(order_return_otp);
                } else {
                    Toast.makeText(context, "Please confirm reached to return vendor", Toast.LENGTH_SHORT).show();
                }


              /*  JSONArray jsonArray= new JSONArray();
                for (int k=0;k<OrderDetailList.size();k++){
                    JSONObject jsonObject1=new JSONObject();

                            try {
                                jsonObject1.put("item_id",OrderDetailList.get(k).getItem_id());
                                jsonObject1.put("vendor_product_variant_id",OrderDetailList.get(k).getVendor_product_variant_id());
                                jsonObject1.put("qty",OrderDetailList.get(k).getQty());
                                jsonObject1.put("return_available",OrderDetailList.get(k).getItemModel().getVarientModel().getReturn_available());
                                jsonObject1.put("return_id",OrderDetailList.get(k).getItemModel().getVarientModel().getReturn_id());
                                jsonArray.put(jsonObject1);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                Map<String, Object> uploadMap = new HashMap<>();
                uploadMap.put("order_id", orderID);
                uploadMap.put("delivery_job_id", jobID);
                JSONObject json = new JSONObject(uploadMap);
                try {
                    json.put("returned_products", jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("aaaaaaaa  return obj  "+json.toString());*/

            }
        });
    }

    boolean cameraOpened = false;

    @Override
    protected void onResume() {
        super.onResume();
        if (!cameraOpened) {
            getOrderDetails();

            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_PERMISSION_CODE);
            } else
                isCameraPermissionGranted = true;
        }
        if (checkservice) {
            try {
                FloatingWidgetService.stopservice();
                Intent intent = new Intent(this, FloatingWidgetService.class);
                stopService(intent);
            } catch (Exception e) {

            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_CAMERA_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isCameraPermissionGranted = true;
                    if (showCameraAction) {
                        showCameraAction = false;
                        capturePhoto(trackingMode);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.accept_camera_permission), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void requesttypes() {
        RequestQueue requestQueue = Volley.newRequestQueue(OrderDetailsActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, RESONS_REJECTED,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");
                            if (status && http_code == 200) {
                                JSONArray responsearray = jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + jsonObject.toString());
                                ArrayList<RejectOrdersRequets> rejectlist = new ArrayList<>();

                               /* userlatitude=17.4875418;
                                userlongitude=78.3953462;*/

                                if (currentlatitude == 0.0 || currentlongitude == 0.0) {
                                    getLocation();
                                }

                              /*  DirectionListener shopDistanceListener = new DirectionListener() {
                                    @Override
                                    public void onDirectionResult(DirectionObject result) {
                                            try {
                                                result.getTravelDistance();
                                                String[] separated = result.getTravelDistance().split(" ");
                                                distance=Double.parseDouble(separated[0])*100;

                                                System.out.println("aaaaaaaaaa preparationtime  " + result.getTravelDistance()+"   "+distance);
                                                for (int i=0;i<responsearray.length();i++){
                                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                                    if (jsonObject1.getString("id").equalsIgnoreCase("4")){
                                                        if (deliveryboystatus.equalsIgnoreCase("506")){
                                                            if (distance<20000){
                                                                RejectOrdersRequets rejectOrdersRequets=new RejectOrdersRequets();
                                                                rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                                rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                                rejectlist.add(rejectOrdersRequets);
                                                            }
                                                        }

                                                    }else{
                                                        if (jsonObject1.getString("id").equalsIgnoreCase("2")){
                                                            if (!orderDetailsmodel.getOrderStatus().getId().equalsIgnoreCase("11")){
                                                                RejectOrdersRequets rejectOrdersRequets=new RejectOrdersRequets();
                                                                rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                                rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                                rejectlist.add(rejectOrdersRequets);
                                                            }
                                                        }else{
                                                            RejectOrdersRequets rejectOrdersRequets=new RejectOrdersRequets();
                                                            rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                            rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                            rejectlist.add(rejectOrdersRequets);
                                                        }
                                                    }
                                                }
                                                DialogsReson dialogsReson=new DialogsReson(OrderDetailsActivity.this,rejectlist,
                                                        jsonObject.getString("message"));
                                                dialogsReson.showDialog();
                                            } catch (Exception ex) {
                                            }

                                    }
                                };*/
                                LatLng origin = new LatLng(Double.valueOf(currentlatitude), Double.valueOf(currentlongitude));
                                LatLng destination = new LatLng(Double.valueOf(userlatitude), Double.valueOf(userlongitude));

                                //  LocationDetailUtil.GetDirectionData(OrderDetailsActivity.this, origin, destination, shopDistanceListener);


                                double distance = distance(currentlatitude, currentlongitude, userlatitude, userlongitude);
                                System.out.println("aaaaaaaaaa latlng " + currentlatitude + "  " + currentlongitude + "  " + userlatitude + "  " + userlongitude);
                                System.out.println("aaaaaaaaaa distance " + distance);

                                try {
                                    for (int i = 0; i < responsearray.length(); i++) {
                                        JSONObject jsonObject1 = responsearray.getJSONObject(i);
                                        if (deliveryboystatus.equalsIgnoreCase("506")) {
                                            if (jsonObject1.getString("id").equalsIgnoreCase("4")) {
                                                if (distance < 200) {
                                                    RejectOrdersRequets rejectOrdersRequets = new RejectOrdersRequets();
                                                    rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                    rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                    rejectlist.add(rejectOrdersRequets);
                                                }
                                            }
                                            if (jsonObject1.getString("id").equalsIgnoreCase("2")) {

                                                RejectOrdersRequets rejectOrdersRequets = new RejectOrdersRequets();
                                                rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                rejectlist.add(rejectOrdersRequets);

                                            }


                                        } else {
                                            if (jsonObject1.getString("id").equalsIgnoreCase("2")) {
                                                if (!orderDetailsmodel.getOrderStatus().getId().equalsIgnoreCase("11")) {
                                                    RejectOrdersRequets rejectOrdersRequets = new RejectOrdersRequets();
                                                    rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                    rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                    rejectlist.add(rejectOrdersRequets);
                                                }
                                            } else {
                                                RejectOrdersRequets rejectOrdersRequets = new RejectOrdersRequets();
                                                rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                rejectlist.add(rejectOrdersRequets);
                                            }
                                        }
                                                    /*if (jsonObject1.getString("id").equalsIgnoreCase("4")){
                                                        if (deliveryboystatus.equalsIgnoreCase("506")){
                                                            if (distance<200){
                                                                RejectOrdersRequets rejectOrdersRequets=new RejectOrdersRequets();
                                                                rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                                rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                                rejectlist.add(rejectOrdersRequets);
                                                            }
                                                        }

                                                    }else{
                                                        if (jsonObject1.getString("id").equalsIgnoreCase("2")){
                                                            if (!orderDetailsmodel.getOrderStatus().getId().equalsIgnoreCase("11")){
                                                                RejectOrdersRequets rejectOrdersRequets=new RejectOrdersRequets();
                                                                rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                                rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                                rejectlist.add(rejectOrdersRequets);
                                                            }
                                                        }else{
                                                            RejectOrdersRequets rejectOrdersRequets=new RejectOrdersRequets();
                                                            rejectOrdersRequets.setId(jsonObject1.getString("id"));
                                                            rejectOrdersRequets.setName(jsonObject1.getString("reason"));
                                                            rejectlist.add(rejectOrdersRequets);
                                                        }
                                                    }*/
                                    }
                                    DialogsReson dialogsReson = new DialogsReson(OrderDetailsActivity.this, rejectlist,
                                            jsonObject.getString("message"));
                                    dialogsReson.showDialog();
                                } catch (Exception ex) {
                                }


                            }

                        } catch (JSONException e) {
                            Toast.makeText(OrderDetailsActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrderDetailsActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /**
     * It will show the view based on current order status
     */
    private void changeViewByOrderStatus() {
        try {

            if (deliveryboy_reject_status.equalsIgnoreCase("500")) {
                btn_reached_pickup_point.setVisibility(View.GONE);
                order_receive_otp_layout.setVisibility(View.GONE);
                btn_start_pickup.setVisibility(View.GONE);
                btn_end_delivery.setVisibility(View.GONE);
                btn_reached_pickup_point.setVisibility(View.GONE);
                btn_reached_delivery_point.setVisibility(View.GONE);
                lay_pickup_address.setVisibility(View.GONE);
                tv_reject.setVisibility(View.GONE);
                rating_cart.setVisibility(View.GONE);
                android.app.AlertDialog.Builder alertDialogBuilder = new
                        android.app.AlertDialog.Builder(OrderDetailsActivity.this, R.style.MyDialogTheme);
                alertDialogBuilder.setMessage("Order Rejected");
                alertDialogBuilder.setCancelable(true);
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.orange));
                    }
                });

                alertDialog.show();
            } else {
                if (Integer.parseInt(OrderStatus) >= 508) {
           /* btn_start_pickup.setEnabled(false);
            btn_start_pickup.setAlpha(0.5f);
            btn_end_delivery.setEnabled(false);
            btn_end_delivery.setAlpha(0.5f);*/

                    order_receive_otp_layout.setVisibility(View.GONE);
                    btn_start_pickup.setVisibility(View.GONE);
                    btn_end_delivery.setVisibility(View.GONE);
                    btn_reached_pickup_point.setVisibility(View.GONE);
                    btn_reached_delivery_point.setVisibility(View.GONE);
                    lay_pickup_address.setVisibility(View.VISIBLE);
                    tv_reject.setVisibility(View.GONE);


                    if (Integer.parseInt(OrderStatus) == 508)
                        rating_cart.setVisibility(View.VISIBLE);

                } else {
                    rating_cart.setVisibility(View.GONE);
                    btn_reached_delivery_point.setVisibility(View.GONE);
                    btn_reached_pickup_point.setVisibility(View.GONE);
                    if (Integer.parseInt(OrderStatus) >= 506) {
                        btn_reached_delivery_point.setEnabled(false);
                        btn_reached_delivery_point.setAlpha(0.5f);
                        btn_end_delivery.setVisibility(View.VISIBLE);//for now-ideally after image capture
                        lay_pickup_address.setVisibility(View.VISIBLE);

                    } else if (Integer.parseInt(OrderStatus) >= 505) {
                        btn_reached_delivery_point.setVisibility(View.VISIBLE);
                        lay_pickup_address.setVisibility(View.VISIBLE);
                    } else if (Integer.parseInt(OrderStatus) >= 504) {
                        btn_reached_pickup_point.setEnabled(false);
                        btn_reached_pickup_point.setAlpha(0.5f);
                        btn_start_pickup.setVisibility(View.VISIBLE);

                    } else {
                        btn_reached_pickup_point.setVisibility(View.VISIBLE);
                    }
                }
            }


        } catch (NumberFormatException e) {

        }
    }

    /**
     * It will check whether the all product items are selected
     *
     * @return true- when all product items are selected otherwise false
     */
    private boolean isAllProductsSelected() {
        if (OrderDetailList != null && OrderDetailList.size() > 0) {
            for (int i = 0; i < OrderDetailList.size(); i++) {
                if (OrderDetailList.get(i).getProductCheckState() == false)
                    return false;
            }
        }
        return true;
    }

    /**
     * It will show a dialog to show the otp to the vendor when picking up the order
     */
    private void startPickup() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.start_pickup_validation);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        EditText start_pickup_otp = (EditText) dialog.findViewById(R.id.tv_start_pickup_otp);
        start_pickup_otp.setText(OrderPickupOtp);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (start_pickup_otp.getText().toString().isEmpty() || start_pickup_otp.getText().toString().length() != 6) {
                    Toast.makeText(OrderDetailsActivity.this, "Please Enter Valid OTP", Toast.LENGTH_SHORT).show();
                } else {
                    pickupOTP = start_pickup_otp.getText().toString();
                    invokePickedTheOrderAPI();
                }

            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void invokePickedTheOrderAPI() {

        String url = "";
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("delivery_job_id", jobID);
        dataMap.put("order_id", orderID);

        if (checkecomorder) {
            url = Config.CHANGE_PICKED_ORDER;
        } else {
            url = Config.CHANGE_PICKED_PICKDROPORDER;
            dataMap.put("otp", pickupOTP);
        }
        String data = new JSONObject(dataMap).toString();
        Log.d("VolleyResponse", "CHANGE_PICKED_ORDER: request " + data);

        LoadingDialog.loadDialog(this);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("profile r response", response);
                            Log.d("VolleyResponse", "CHANGE_PICKED_ORDER: respnse " + response);

                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");

                            LoadingDialog.dialog.dismiss();
                            if (!status) {
                                Toast.makeText(getApplicationContext(), getString(R.string.pick_item_is_failed) + " " + response, Toast.LENGTH_SHORT).show();
                            } else {
                                if (pickUpOrder64String == null) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.capture_received_items), Toast.LENGTH_SHORT).show();
                                    capturePhoto(TRACKING_PICKUP);
                                } else {
                                    btn_start_pickup.setEnabled(true);
                                    btn_start_pickup.setAlpha(1f);
                                    invokeOrderConfirmationAPI();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.dialog.dismiss();
                        } finally {
                            LoadingDialog.dialog.dismiss();
                        }
                        //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /**
     * It will show to dialog to enter the otp from the user when he delivering the products to the user.s
     */
    private void endDelivery(String otp, String passcode) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.customer_otp_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        final EditText editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        final EditText editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
        final EditText editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        final EditText editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        final EditText editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        final EditText editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        TextView tv_lost_otp = (TextView) dialog.findViewById(R.id.tv_lost_otp);
        RelativeLayout layout_payment_status = (RelativeLayout) dialog.findViewById(R.id.layout_payment_status);
        ToggleButton tb_payment_status = (ToggleButton) dialog.findViewById(R.id.tb_payment_status);
        Button btn_verify_passcode = (Button) dialog.findViewById(R.id.btn_verify_passcode);
        final EditText et_passcode = (EditText) dialog.findViewById(R.id.et_passcode);
        TextView tv_error_passcode = (TextView) dialog.findViewById(R.id.tv_error_passcode);
        LinearLayout layout_otp = (LinearLayout) dialog.findViewById(R.id.layout_otp);
        LinearLayout layout_passcode = (LinearLayout) dialog.findViewById(R.id.layout_passcode);
        TextView tv_login_with_otp = (TextView) dialog.findViewById(R.id.tv_login_with_otp);
        final EditText et_collected_amount = (EditText) dialog.findViewById(R.id.et_collected_amount);

        TextView tv_header = (TextView) dialog.findViewById(R.id.tv_header);
        LinearLayout layout_payment_input = (LinearLayout) dialog.findViewById(R.id.layout_payment_input);

        Boolean isPaymentMandatory = false;

        et_collected_amount.setText("" + amountTobeCollected);
        et_collected_amount.setEnabled(false);
        et_collected_amount.setClickable(false);


        tv_lost_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_otp.setVisibility(View.GONE);
                layout_passcode.setVisibility(View.VISIBLE);
                tv_header.setText(getString(R.string.enter_passcode_here));
            }
        });
        tv_login_with_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_otp.setVisibility(View.VISIBLE);
                layout_passcode.setVisibility(View.GONE);

                tv_header.setText(getString(R.string.enter_otp_here));
            }
        });
        if (passcode != null && !passcode.isEmpty()) {
            tb_payment_status.setChecked(true);
            tv_error_passcode.setVisibility(View.VISIBLE);
            layout_otp.setVisibility(View.GONE);
            layout_passcode.setVisibility(View.VISIBLE);
            tv_error_passcode.setText("* " + getString(R.string.incorrect_passcode));
            layout_payment_input.setVisibility(View.VISIBLE);
            et_passcode.setText(passcode);
            et_collected_amount.setText(amountTobeCollected);
        }

        if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
            tb_payment_status.setChecked(true);
            tv_error.setVisibility(View.VISIBLE);
            tv_error.setText("* " + getString(R.string.incorrect_otp));
            layout_otp.setVisibility(View.VISIBLE);
            layout_payment_input.setVisibility(View.VISIBLE);
            layout_passcode.setVisibility(View.GONE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
            et_collected_amount.setText(amountTobeCollected);
        }

        if (amountTobeCollected != null && !amountTobeCollected.isEmpty()) {
            isPaymentMandatory = true;
            layout_payment_status.setVisibility(View.VISIBLE);
        } else {
            layout_payment_input.setVisibility(View.GONE);
        }

        tb_payment_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tv_error.setVisibility(View.GONE);
                    tv_error_passcode.setVisibility(View.GONE);
                    layout_payment_input.setVisibility(View.VISIBLE);
                    // et_collected_amount = (EditText) dialog.findViewById(R.id.et_collected_amount);
                } else
                    layout_payment_input.setVisibility(View.GONE);
            }
        });

       /* if (OrderDeliveryOtp != null && !OrderDeliveryOtp.isEmpty() && OrderDeliveryOtp.length() >= 6) {
            editTextone.setText(OrderDeliveryOtp.substring(0, 1));
            editTexttwo.setText(OrderDeliveryOtp.substring(1, 2));
            editTextthree.setText(OrderDeliveryOtp.substring(2, 3));
            editTextfour.setText(OrderDeliveryOtp.substring(3, 4));
            editTextFive.setText(OrderDeliveryOtp.substring(4, 5));
            editTextSix.setText(OrderDeliveryOtp.substring(5, 6));
        }*/

        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextone.length() == 1) {
                    editTextone.clearFocus();
                    editTexttwo.requestFocus();
                    editTexttwo.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTexttwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTexttwo.length() == 1) {
                    editTexttwo.clearFocus();
                    editTextthree.requestFocus();
                    editTextthree.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextthree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextthree.length() == 1) {
                    editTextthree.clearFocus();
                    editTextfour.requestFocus();
                    editTextfour.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextfour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextfour.length() == 1) {
                    editTextfour.clearFocus();
                    editTextFive.requestFocus();
                    editTextFive.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextFive.length() == 1) {
                    editTextFive.clearFocus();
                    editTextSix.requestFocus();
                    editTextSix.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Boolean finalIsPaymentMandatory = isPaymentMandatory;
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (finalIsPaymentMandatory == true) {
                    if (!tb_payment_status.isChecked()) {
                        tv_error.setVisibility(View.VISIBLE);
                        tv_error_passcode.setVisibility(View.VISIBLE);
                        String message = getString(R.string.enable_payment_check);
                        tv_error.setText(message);
                        tv_error_passcode.setText(message);
                        return;
                    } else {
                       /* double collectedAmount =0;
                        try {
                            collectedAmount = Double.parseDouble(et_collected_amount.getText().toString());
                        }
                        catch (Exception ex){}
                        if(collectedAmount == Double.parseDouble(amountTobeCollected))
                        {
                            tv_error.setVisibility(View.VISIBLE);
                            tv_error_passcode.setVisibility(View.VISIBLE);
                            String message = getString(R.string.collect_amount)+getString(R.string.Rs)+"."+amountTobeCollected;
                            tv_error.setText(message);
                            tv_error_passcode.setText(message);
                            return;
                        }*/
                    }
                }

                tv_error.setVisibility(View.GONE);
                tv_error_passcode.setVisibility(View.GONE);
                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    invokeDeliveryOrder(otp, null);
                } else {
                    tv_error.setVisibility(View.VISIBLE);
                    String message = getString(R.string.enter_6digit_otp);
                    tv_error.setText(message);
                }
            }
        });
        btn_verify_passcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passcode = et_passcode.getText().toString();

                if (finalIsPaymentMandatory == true) {
                    if (!tb_payment_status.isChecked()) {
                        tv_error.setVisibility(View.VISIBLE);
                        tv_error_passcode.setVisibility(View.VISIBLE);
                        String message = getString(R.string.enable_payment_check);
                        tv_error.setText(message);
                        tv_error_passcode.setText(message);
                        return;
                    } else {
                       /* double collectedAmount =0;
                        try {
                            collectedAmount = Double.parseDouble(et_collected_amount.getText().toString());
                        }
                        catch (Exception ex){}
                        System.out.println("aaaaaaaaa  collect amount  "+collectedAmount +"  "+amountTobeCollected);
                      //  if(collectedAmount < Double.parseDouble(amountTobeCollected))
                        if(collectedAmount == Double.parseDouble(amountTobeCollected))
                        {
                            tv_error.setVisibility(View.VISIBLE);
                            tv_error_passcode.setVisibility(View.VISIBLE);
                            String message = getString(R.string.collect_amount)+getString(R.string.Rs)+amountTobeCollected;
                            tv_error.setText(message);
                            tv_error_passcode.setText(message);
                            return;
                        }*/
                    }
                }

                tv_error.setVisibility(View.GONE);
                tv_error_passcode.setVisibility(View.GONE);
                if (passcode != null && !passcode.isEmpty()) {
                    dialog.dismiss();
                    invokeDeliveryOrder(null, passcode);//123456
                } else {
                    tv_error_passcode.setVisibility(View.VISIBLE);
                    tv_error_passcode.setText(getString(R.string.enter_passcode));
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }


    /**
     * It will get the order details from the server
     */
    public void getOrderDetails() {
        String url = "";
        System.out.println("aaaaaaaaaaaaa checkecomorder  " + checkecomorder);
        if (checkecomorder) {
            url = Config.ORDERDETAILS;
        } else {
            url = Config.PICKUPORDERDETAILS;
        }
        // OrderDetailList.clear();
        Context mContext = this;
        OrderDetailsActivity finalThis = this;
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("order_id", orderID);

        JSONObject json = new JSONObject(uploadMap);


        Log.d("VolleyResponse", "order_details request: " + json);
        System.out.println("aaaaaaaaa request" + json.toString());
        try {
            LoadingDialog.loadDialog(OrderDetailsActivity.this);
            LoadingDialog.dialog.dismiss();
        } catch (IllegalArgumentException e) {

        }

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "order_details: " + response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");

                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");
                                orderDetailsmodel = new OrderDetailsmodel();

                                try {
                                    order_confirmation_time = dataobj.getInt("order_confirmation_time");
                                    orderDetailsmodel.setOrder_confirmation_time(order_confirmation_time);
                                } catch (NullPointerException e) {

                                }

                                if (checkecomorder) {

                                    orderDetailsmodel.setTrack_id(dataobj.getString("track_id"));
                                    trackID = orderDetailsmodel.getTrack_id();
                                    orderDetailsmodel.setDelivery_fee(dataobj.getString("delivery_fee"));
                                    orderDetailsmodel.setTotal(dataobj.getString("total"));
                                    orderDetailsmodel.setUsed_wallet_amount(dataobj.getString("used_wallet_amount"));
                                    orderDetailsmodel.setMessage(dataobj.getString("message"));
                                    orderDetailsmodel.setPreparation_time(dataobj.getString("preparation_time"));
                                    orderDetailsmodel.setShipping_address_id(dataobj.getString("shipping_address_id"));
                                    orderDetailsmodel.setCreated_at(dataobj.getString("created_at"));
                                    orderDetailsmodel.setDelivery_mode_id(dataobj.getString("delivery_mode_id"));
                                    orderDetailsmodel.setCreated_user_id(dataobj.getString("created_user_id"));
                                    orderDetailsmodel.setOrder_status_id(dataobj.getString("order_status_id"));
                                    orderDetailsmodel.setPayment_id(dataobj.getString("payment_id"));
                                    orderDetailsmodel.setId(dataobj.getString("id"));


                                    try {

                                    /*if (Integer.parseInt(orderDetailsmodel.getStatus()) >= 4) {
                                        btn_reached_pickup_point.setEnabled(false);
                                        btn_reached_pickup_point.setAlpha(0.5f);
                                    }
                                     if (Integer.parseInt(orderDetailsmodel.getStatus()) >= 6) {
                                        btn_reached_delivery_point.setEnabled(false);
                                        btn_reached_delivery_point.setAlpha(0.5f);
                                    }*/
                                    } catch (Exception ex) {
                                    }


                                    if (dataobj.has("order_pickup_otp")) {
                                        OrderPickupOtp = dataobj.getString("order_pickup_otp");

                                        order_receive_otp.setText(OrderPickupOtp);
                                    }
                                    if (dataobj.has("order_delivery_otp"))
                                        OrderDeliveryOtp = dataobj.getString("order_delivery_otp");//hidden field

                                    if (dataobj.has("order_delivery_otp"))
                                        order_return_otp = dataobj.getString("order_return_otp");//hidden field

                                    timezone(dataobj.getString("created_at"));

                                    // tv_order_job_id.setText(dataobj.getString("track_id"));
                                    tv_order_id.setText("#" + dataobj.getString("track_id"));

                                    // tv_sub_total.setText(dataobj.getString("track_id"));
                                    tv_final_product_price.setText("\u20B9" + dataobj.getString("total"));
                                    tv_delivery_fee.setText(orderDetailsmodel.getDelivery_fee());//+"/-Rs"


                                    if (dataobj.getString("delivery_mode_id").equalsIgnoreCase("1")) {
                                        //  layout_delevery.setVisibility(View.GONE);
                                        //  layout_delevery_one.setVisibility(View.GONE);
                                    } else {
                                        //  layout_delevery.setVisibility(View.GONE);
                                        //  layout_delevery_one.setVisibility(View.GONE);
                                    }


                                    try {
                                        JSONObject shippingobj = dataobj.getJSONObject("shipping_address");
                                        AssignedUserAddress shippingAddr = getAssignedUserAddress(shippingobj, 1);
                                        orderDetailsmodel.setShippingAddress(shippingAddr);


                                        JSONObject vendorAddressObj = dataobj.getJSONObject("vendor");
                                        AssignedUserAddress vendorAddress = getAssignedUserAddress(vendorAddressObj, 0);
                                        orderDetailsmodel.setVendorAddress(vendorAddress);
                                        try {
                                            JSONObject customerDetails = dataobj.getJSONObject("customer");
                                            customerDetailsModel customer = new customerDetailsModel();
                                            customer.setPhone(customerDetails.getString("phone"));
                                            orderDetailsmodel.setCustomerDetailsModel(customer);
                                            img_phone.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                                    try {
                                                        intent.setData(Uri.parse("tel:" + String.valueOf(shippingobj.getString("phone"))));
                                                    } catch (JSONException e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                    startActivity(intent);

                                                }
                                            });
                                        } catch (Exception e) {

                                        }

                                        try {
                                            JSONObject orderostatusobj = dataobj.getJSONObject("order_status");
                                            OrderStatus orderStatus = new OrderStatus();
                                            orderStatus.setId(orderostatusobj.getString("id"));
                                            orderStatus.setDelivery_mode_id(orderostatusobj.getString("delivery_mode_id"));
                                            orderStatus.setSerial_number(orderostatusobj.getString("serial_number"));
                                            orderStatus.setStatus(orderostatusobj.getString("status"));
                                            orderDetailsmodel.setOrderStatus(orderStatus);

                                            tv_vendor_order_status.setText(orderStatus.getStatus());


                                            if (OrderStatus == null /*|| !OrderStatus.equals("508")*/)
                                                OrderStatus = orderStatus.getId();//from api
                                        } catch (JSONException e2) {

                                        }

                                        delivery_layout_map.setVisibility(View.VISIBLE);
                                        pickup_layout_map.setVisibility(View.VISIBLE);
                                        if (Integer.parseInt(OrderStatus) >= 508) {
                                            pickup_layout_map.setVisibility(View.GONE);
                                            delivery_layout_map.setVisibility(View.GONE);
                                            order_receive_otp_layout.setVisibility(View.GONE);
                                            lay_pickup_address.setVisibility(View.VISIBLE);
                                            tv_reject.setVisibility(View.GONE);

                                            if (Integer.parseInt(OrderStatus) == 508)
                                                rating_cart.setVisibility(View.VISIBLE);
                                        } else {


                                            if (Integer.parseInt(OrderStatus) >= 506) {
                                                //Reached to delivery point
                                                btn_reached_delivery_point.setEnabled(false);
                                                btn_reached_delivery_point.setAlpha(0.5f);
                                                btn_end_delivery.setVisibility(View.VISIBLE);//for now-ideally after image capture
                                                delivery_layout_map.setVisibility(View.GONE);
                                                pickup_layout_map.setVisibility(View.GONE);
                                                lay_pickup_address.setVisibility(View.VISIBLE);

                                                btn_reached_delivery_point.setVisibility(View.GONE);
                                            } else if (Integer.parseInt(OrderStatus) >= 505) {
                                                //Picked the order
                                                btn_reached_delivery_point.setVisibility(View.VISIBLE);
                                                pickup_layout_map.setVisibility(View.GONE);
                                                lay_pickup_address.setVisibility(View.VISIBLE);
                                                showCustomerLocation(vendorAddress.getLocation(), shippingAddr.getLocation(), tv_delivery_distance);
                                            } else if (Integer.parseInt(OrderStatus) >= 504) {
                                                //Reached to pickup point
                                                btn_reached_pickup_point.setEnabled(false);
                                                btn_reached_pickup_point.setAlpha(0.5f);
                                                btn_start_pickup.setVisibility(View.VISIBLE);
                                                btn_reached_pickup_point.setVisibility(View.GONE);
                                                showCustomerLocation(vendorAddress.getLocation(), shippingAddr.getLocation(), tv_delivery_distance);
                                            } else {
                                                setTravelDistance(vendorAddress.getLocation(), tv_pick_up_distance);
                                                showCustomerLocation(vendorAddress.getLocation(), shippingAddr.getLocation(), tv_delivery_distance);
                                            }
                                            //setTravelDistance(vendorAddress.getLocation(), tv_pick_up_distance);
                                            // showCustomerLocation(shippingAddr.getLocation(), tv_delivery_distance);
                                        }


                                        String text = "<font color=#9D9D9D>" + shippingAddr.getName() +
                                                "</font>";
                                    /*String text = "<font color=#9D9D9D>" + shippingAddr.getName() +
                                            "</font> <font color=#141414>" + shippingAddr.getPhone() + "</font>";*/
                                        vendor_details.setText(Html.fromHtml(text));
                                        address_details.setText(vendorAddress.getAddress());

                                        text = "<font color=#9D9D9D>" + vendorAddress.getName() +
                                                "</font>";
                                    /* text = "<font color=#9D9D9D>" + vendorAddress.getName() +
                                            "</font> <font color=#141414>" + vendorAddress.getPhone() + "</font>";*/
                                        seller_details.setText(Html.fromHtml(text));
                                        cust_address_details.setText(shippingAddr.getAddress());


                                    } catch (JSONException e1) {

                                    }

                                    try {
                                        JSONObject deleveryong = dataobj.getJSONObject("delivery_mode");
                                        DeleveryModesmodel deleveryModesmodel = new DeleveryModesmodel();
                                        deleveryModesmodel.setId(deleveryong.getString("id"));
                                        deleveryModesmodel.setName(deleveryong.getString("name"));
                                        deleveryModesmodel.setDesc(deleveryong.getString("desc"));
                                        orderDetailsmodel.setDeleveryModesmodel(deleveryModesmodel);
                                        // tv_deleverymode.setText(""+deleveryong.getString("name"));
                                    } catch (JSONException e2) {

                                    }

                                    try {
                                        JSONObject customerobj = dataobj.getJSONObject("customer");
                                        CustomerModel customerModel = new CustomerModel();
                                        customerModel.setId(customerobj.getString("id"));
                                        customerModel.setUnique_id(customerobj.getString("unique_id"));
                                        customerModel.setFirst_name(customerobj.getString("first_name"));
                                        customerModel.setPhone(customerobj.getString("phone"));
                                        customermobile = customerobj.getString("phone");
                                        orderDetailsmodel.setCustomerModel(customerModel);


                                    } catch (JSONException e2) {

                                    }


                                    try {
                                        JSONObject delivery_job = dataobj.getJSONObject("delivery_job");
                                        DeliveryOrder deliveryOrder = new DeliveryOrder();
                                        deliveryOrder.setStatus(delivery_job.getString("status"));
                                        deliveryOrder.setDeliveryJobID(delivery_job.getString("id"));
                                        jobID = deliveryOrder.getDeliveryJobID();
                                        deliveryboystatus = delivery_job.getString("status");
                                        tv_order_my_status.setText(deliveryOrder.getStatusMessage());


                                        if (delivery_job.has("rating")) {
                                            tv_user_rating.setText(Utility.getValidStringValue(delivery_job.getString("rating"), "-"));
                                        }
                                        if (delivery_job.has("feedback")) {
                                            tv_user_rating_feedback.setText(Utility.getValidStringValue(delivery_job.getString("feedback"), "-"));
                                        }


                                        if (delivery_job.has("delivery_boy")) {
                                            JSONObject delivery_boy = delivery_job.getJSONObject("delivery_boy");
                                            if (delivery_boy.has("delivery_boy_pickup_image")) {
                                                String delivery_boy_pickup_image = delivery_boy.getString("delivery_boy_pickup_image");

                                                Glide.with(OrderDetailsActivity.this)
                                                        .load(delivery_boy_pickup_image)
                                                        .placeholder(R.drawable.noimageone)
                                                        .into(img_pick_up);
                                                layout_pickup_image.setVisibility(View.VISIBLE);
                                                //showImageDialog(delivery_boy_pickup_image);
                                            }
                                            if (delivery_boy.has("delivery_boy_delivery_image")) {
                                                String delivery_boy_delivery_image = delivery_boy.getString("delivery_boy_delivery_image");
                                                Glide.with(OrderDetailsActivity.this)
                                                        .load(delivery_boy_delivery_image)
                                                        .placeholder(R.drawable.noimageone)
                                                        .into(img_delivery);
                                                layout_delivery_image.setVisibility(View.VISIBLE);
                                            }
                                            //600 Customer is unavailable,
                                            //601 Return accepted by admin
                                            //602 Went back to the vendor, 603 = Returned the order
                                            delivery_boy_status = delivery_job.getString("status");

                                            try {
                                                System.out.println("aaaaaaaaa 22 " + deliveryboystatus);
                                                if (deliveryboystatus.equalsIgnoreCase("506")) {
                                                    JSONArray jsonArray = delivery_job.getJSONArray("delivery_rejections");
                                                    JSONObject reject_deliveryobj = jsonArray.getJSONObject(0);
                                                    String rejectstatus = reject_deliveryobj.getString("status");
                                                    System.out.println("aaaaaaaaa rejectstatus " + rejectstatus);
                                                    if (rejectstatus.equalsIgnoreCase("3")) {
                                                        tv_reject.setVisibility(View.GONE);
                                                    }
                                                }
                                            } catch (Exception e) {
                                                System.out.println("aaaaaaaaa catch 11 " + e.getMessage());
                                            }

                                            if (delivery_boy_status.equalsIgnoreCase("600")) {
                                                JSONArray jsonArray = delivery_job.getJSONArray("delivery_rejections");
                                                JSONObject reject_deliveryobj = jsonArray.getJSONObject(0);
                                                String created_at = reject_deliveryobj.getString("created_at");
                                                String rejection_reason = reject_deliveryobj.getString("rejection_reason");
                                                userNotResponding(created_at, rejection_reason);
                                            /* pickup_layout_map.setVisibility(View.VISIBLE);
                                            btn_reached_delivery_point_to_vendor.setVisibility(View.VISIBLE);
                                            btn_reached_delivery_point.setVisibility(View.GONE);
                                            lay_pickup_address.setVisibility(View.GONE);
                                            btn_return_delivery.setVisibility(View.VISIBLE);*/

                                            } else if (delivery_boy_status.equalsIgnoreCase("601") || delivery_boy_status.equalsIgnoreCase("604")) {

                                                try {
                                                    if (dialog4 != null) {
                                                        dialog4.dismiss();
                                                    }
                                                } catch (NullPointerException e) {

                                                }
                                                pickup_layout_map.setVisibility(View.VISIBLE);

                                                btn_return_delivery.setVisibility(View.VISIBLE);
                                                btn_generate_payment_link.setVisibility(View.GONE);
                                                btn_reached_delivery_point.setVisibility(View.GONE);

                                                btn_reached_delivery_point_to_vendor.setVisibility(View.VISIBLE);

                                                lay_pickup_address.setVisibility(View.GONE);


                                            } else if (delivery_boy_status.equalsIgnoreCase("602")) {
                                                System.out.println("aaaaaaaa 602");
                                                btn_reached_delivery_point_to_vendor.setEnabled(false);
                                                btn_reached_delivery_point_to_vendor.setAlpha(0.5f);
                                                btn_return_delivery.setVisibility(View.VISIBLE);
                                                btn_generate_payment_link.setVisibility(View.GONE);
                                            } else if (delivery_boy_status.equalsIgnoreCase("603")) {
                                                System.out.println("aaaaaaaa 603");
                                                btn_return_delivery.setVisibility(View.GONE);
                                                btn_generate_payment_link.setVisibility(View.GONE);
                                                btn_reached_delivery_point_to_vendor.setVisibility(View.GONE);
                                            } else {
                                                System.out.println("aaaaaaaaaa else status  ");
                                                try {
                                                    if (dialog4 != null) {
                                                        dialog4.dismiss();
                                                    }
                                                } catch (NullPointerException e) {

                                                }
                                            }
                                        }

                                    } catch (JSONException e2) {

                                    }


                                    try {
                                        JSONObject paymentobj = dataobj.getJSONObject("payment");
                                        PaymentModel paymentModel = new PaymentModel();
                                        paymentModel.setId(paymentobj.getString("id"));
                                        paymentModel.setPayment_method_id(paymentobj.getString("payment_method_id"));
                                        paymentModel.setTxn_id(paymentobj.getString("txn_id"));
                                        paymentModel.setAmount(paymentobj.getString("amount"));
                                        paymentModel.setCreated_at(paymentobj.getString("created_at"));
                                        paymentModel.setMessage(paymentobj.getString("message"));
                                        paymentModel.setStatus(paymentobj.getString("status"));
                                        orderDetailsmodel.setPaymentModel(paymentModel);
                                        // payment_type.setText(paymentobj.getString("txn_id"));


                                        try {
                                            String paymentMethod = "";
                                            switch (paymentModel.getPayment_method_id()) {
                                                case "1":
                                                    paymentMethod = "Cash on Delivery";
                                                    // if(orderslist.get(position).getPaymentModel().getStatus().equals("2"))
                                                    //paymentMethod="Online Payment Link";
                                                    break;
                                                case "3":
                                                    paymentMethod = "Wallet Money";
                                                    break;
                                                case "4":
                                                    paymentMethod = "Online Payment Link";
                                                    break;
                                                default:
                                                    paymentMethod = "Online";
                                            }
                                            tv_payment_method.setText(paymentMethod);
                                        } catch (Exception ex) {

                                        }

                                        btn_generate_payment_link.setVisibility(View.GONE);
                                        amountTobeCollected = null;
                                        if (paymentModel.getPayment_method_id() != null && paymentModel.getPayment_method_id().equals("1")) {
                                            if (!paymentModel.getStatus().equals("2")) {//2- online payment
                                                amountTobeCollected = orderDetailsmodel.getTotal();
                                                if (Integer.parseInt(OrderStatus) >= 506) {

                                                    if (delivery_boy_status.equalsIgnoreCase("602") || delivery_boy_status.equalsIgnoreCase("603") || delivery_boy_status.equalsIgnoreCase("604")) {
                                                        btn_generate_payment_link.setVisibility(View.GONE);
                                                    } else {
                                                        btn_generate_payment_link.setVisibility(View.VISIBLE);
                                                    }

                                                    // -will do this once delivery boy reached to user

                                                }
                                                //else -- paid via online link
                                                // payment_type.setText("Online payment link");
                                            }
                                        }

                                    } catch (JSONException e2) {

                                    }

                                    if (OrderDetailList == null || OrderDetailList.size() == 0) {

                                        JSONArray productarray = dataobj.getJSONArray("ecom_order_details");
                                        for (int k = 0; k < productarray.length(); k++) {
                                            JSONObject productonj = productarray.getJSONObject(k);
                                            ProductDetailsModel productDetailsModel = new ProductDetailsModel();

                                            productDetailsModel.setId(productonj.getString("id"));
                                            productDetailsModel.setEcom_order_id(productonj.getString("ecom_order_id"));
                                            productDetailsModel.setItem_id(productonj.getString("item_id"));
                                            productDetailsModel.setVendor_product_variant_id(productonj.getString("vendor_product_variant_id"));
                                            productDetailsModel.setQty(productonj.getString("qty"));
                                            productDetailsModel.setPrice(productonj.getString("price"));
                                            productDetailsModel.setRate_of_discount(productonj.getString("rate_of_discount"));
                                            productDetailsModel.setSub_total(productonj.getString("sub_total"));
                                            productDetailsModel.setDiscount(productonj.getString("discount"));
                                            productDetailsModel.setTax(productonj.getString("tax"));
                                            productDetailsModel.setTotal(productonj.getString("total"));
                                            productDetailsModel.setCancellation_message(productonj.getString("cancellation_message"));
                                            productDetailsModel.setStatus(productonj.getString("status"));
                                            productDetailsModel.setPromotion_banner_id(productonj.getString("promotion_banner_id"));
                                            productDetailsModel.setOffer_product_id(productonj.getString("offer_product_id"));
                                            productDetailsModel.setOffer_product_variant_id(productonj.getString("offer_product_variant_id"));
                                            productDetailsModel.setOffer_product_qty(productonj.getString("offer_product_qty"));
                                            productDetailsModel.setPromotion_banner_discount(productonj.getString("promotion_banner_discount"));


                                            productDetailsModel.setOrderStatus(Integer.parseInt(OrderStatus));
                                  /*  if (Integer.parseInt(OrderStatus) >= 505){
                                        productDetailsModel.setProductCheckState(true);
                                    }else {
                                        productDetailsModel.setProductCheckState(false);
                                    }*/


                                            try {
                                                JSONObject itemobj = productonj.getJSONObject("item");
                                                ItemModel itemModel = new ItemModel();
                                                itemModel.setId(itemobj.getString("id"));
                                                itemModel.setName(itemobj.getString("name"));
                                                itemModel.setDesc(itemobj.getString("desc"));


                                                ArrayList<ImagesModel> imageslist = new ArrayList<>();
                                                try {
                                                    JSONArray jsonArray = itemobj.getJSONArray("item_images");
                                                    for (int M = 0; M < jsonArray.length(); M++) {
                                                        JSONObject jsonObject1 = jsonArray.getJSONObject(M);
                                                        ImagesModel imagesModel = new ImagesModel();
                                                        imagesModel.setId(jsonObject1.getString("id"));
                                                        imagesModel.setImage(jsonObject1.getString("image"));
                                                        imageslist.add(imagesModel);
                                                    }
                                                    itemModel.setImagelist(imageslist);
                                                } catch (Exception e) {

                                                }


                                                JSONObject varientobj = itemobj.getJSONObject("varinat");
                                                VarientModel varientModel = new VarientModel();
                                                varientModel.setId(varientobj.getString("id"));
                                                varientModel.setSku(varientobj.getString("sku"));
                                                varientModel.setPrice(varientobj.getString("price"));
                                                varientModel.setStock(varientobj.getString("stock"));
                                                varientModel.setDiscount(varientobj.getString("discount"));
                                                varientModel.setTax_id(varientobj.getString("tax_id"));
                                                varientModel.setStatus(varientobj.getString("status"));
                                                varientModel.setSection_item_id(varientobj.getString("section_item_id"));

                                                try {
                                                    varientModel.setReturn_id(varientobj.getString("return_id"));
                                                    varientModel.setReturn_available(varientobj.getString("return_available"));
                                                    varientModel.setReturn_id(varientobj.getJSONObject("return").getString("return_days"));
                                                } catch (Exception e) {

                                                }

                                                JSONObject sectionobj = varientobj.getJSONObject("section_item");
                                                SectionItemModel sectionItemModel = new SectionItemModel();
                                                sectionItemModel.setId(sectionobj.getString("id"));
                                                sectionItemModel.setName(sectionobj.getString("name"));
                                                sectionItemModel.setWeight(sectionobj.getString("weight"));

                                                varientModel.setSectionItemModel(sectionItemModel);

                                                itemModel.setVarientModel(varientModel);
                                                productDetailsModel.setItemModel(itemModel);

                                                if (!productonj.getString("status").equalsIgnoreCase("4")) {
                                                    OrderDetailList.add(productDetailsModel);
                                                }
                                            } catch (JSONException e2) {
                                                System.out.println("aaaaaaaaa catch e2 " + e2.getMessage());
                                            }
                                        }


                                        if (OrderDetailList.size() > 0) {
                                            System.out.println("aaaaaaaaaaaaa check " + OrderDetailList.size());
                                            if (adapter == null) {
                                                //no need to update every time
                                                adapter = new ProductDetailAdapter(getApplicationContext(), OrderDetailList, delivery_boy_status);
                                                recyclerview_order_details.setAdapter(adapter);
                                            }

                                            double subtotal = 0.0, discount = 0.0, rateofdiscount = 0.0, tax = 0.0;

                                            for (int i = 0; i < OrderDetailList.size(); i++) {
                                                if (delivery_boy_status.equalsIgnoreCase("601") || delivery_boy_status.equalsIgnoreCase("604")) {
                                                    try {
                                                        btn_generate_payment_link.setVisibility(View.GONE);
                                                        //  System.out.println("aaaaaaaaaaa try  ");
                                                        if (OrderDetailList.get(i).getItemModel().getVarientModel().getReturn_available().equalsIgnoreCase("1")) {
                                                            System.out.println("aaaaaaaaaaa try iff ");
                                                        } else {

                                                            subtotal = subtotal + Double.parseDouble(OrderDetailList.get(i).getSub_total());
                                                            discount = discount + Double.parseDouble(OrderDetailList.get(i).getDiscount());
                                                            rateofdiscount = rateofdiscount + Double.parseDouble(OrderDetailList.get(i).getRate_of_discount());
                                                            tax = tax + Double.parseDouble(OrderDetailList.get(i).getTax());
                                                            System.out.println("aaaaaaaaaaa try else " + subtotal + " " + discount);
                                                        }
                                                    } catch (NumberFormatException e) {

                                                        subtotal = subtotal + Double.parseDouble(OrderDetailList.get(i).getSub_total());
                                                        discount = discount + Double.parseDouble(OrderDetailList.get(i).getDiscount());
                                                        rateofdiscount = rateofdiscount + Double.parseDouble(OrderDetailList.get(i).getRate_of_discount());
                                                        tax = tax + Double.parseDouble(OrderDetailList.get(i).getTax());
                                                        System.out.println("aaaaaaaaaaa catch  " + subtotal + "  " + discount);
                                                    }

                                                } else {
                                                    subtotal = subtotal + Double.parseDouble(OrderDetailList.get(i).getSub_total());
                                                    discount = discount + Double.parseDouble(OrderDetailList.get(i).getDiscount());
                                                    rateofdiscount = rateofdiscount + Double.parseDouble(OrderDetailList.get(i).getRate_of_discount());
                                                    tax = tax + Double.parseDouble(OrderDetailList.get(i).getTax());

                                                }
                                            }
                                            if (discount == 0.0) {
                                                // tv_discount.setVisibility(View.GONE);
                                            }
                                            tv_price_sub_total.setText("\u20B9" + subtotal);
                                            tv_price_discount.setText("\u20B9" + discount);
                                            // tv_price_discount.setText("" + rateofdiscount + "% off( " + discount + " )");
                                            tv_tax_amount.setText("\u20B9" + tax);

                                            //price after discount
                                            tv_price_product_amount_total.setText("\u20B9" + (subtotal - discount));
                                            Double delFee = Utility.round(orderDetailsmodel.getDelivery_fee(), 2);
                                            tv_price_product_amount_total_include_del_fee.setText("\u20B9" + (delFee + subtotal - discount));
                                            // tv_final_product_price.setText("\u20B9" + (delFee+ tax + subtotal - discount));

                                            tv_delivery_fee.setText("\u20B9" + orderDetailsmodel.getDelivery_fee());// + "/-Rs"

                                            tv_product_quantity.setText(getString(R.string.price_details) + " (" + OrderDetailList.size() + getString(R.string.items)
                                                    + " )");
                                        }
                                    }
                                } else {

                                    orderDetailsmodel.setTrack_id(dataobj.getString("track_id"));
                                    trackID = orderDetailsmodel.getTrack_id();
                                    orderDetailsmodel.setDelivery_fee(dataobj.getString("delivery_fee"));
                                    //orderDetailsmodel.setTotal(dataobj.getString("total"));
                                    // orderDetailsmodel.setUsed_wallet_amount(dataobj.getString("used_wallet_amount"));
                                    // orderDetailsmodel.setMessage(dataobj.getString("message"));
                                    // orderDetailsmodel.setPreparation_time(dataobj.getString("preparation_time"));
                                    orderDetailsmodel.setShipping_address_id(dataobj.getString("pickup_address_id"));
                                    orderDetailsmodel.setCreated_at(dataobj.getString("created_at"));
                                    //orderDetailsmodel.setDelivery_mode_id(dataobj.getString("delivery_mode_id"));
                                    //orderDetailsmodel.setCreated_user_id(dataobj.getString("created_user_id"));
                                    orderDetailsmodel.setOrder_status_id(dataobj.getString("order_status_id"));
                                    orderDetailsmodel.setPayment_id(dataobj.getString("payment_id"));
                                    orderDetailsmodel.setId(dataobj.getString("id"));


                                    if (dataobj.has("order_pickup_otp")) {
                                        OrderPickupOtp = dataobj.getString("order_pickup_otp");

                                        order_receive_otp.setText(OrderPickupOtp);
                                    }
                                    if (dataobj.has("order_delivery_otp"))
                                        OrderDeliveryOtp = dataobj.getString("order_delivery_otp");//hidden field

                                    timezone(dataobj.getString("created_at"));


                                    tv_order_id.setText("#" + dataobj.getString("track_id"));


                                    tv_final_product_price.setText("\u20B9" + orderDetailsmodel.getDelivery_fee());
                                    tv_delivery_fee.setText(orderDetailsmodel.getDelivery_fee());//+"/-Rs"


                                    try {
                                        JSONObject shippingobj = dataobj.getJSONObject("delivery_address");
                                        AssignedUserAddress shippingAddr = getAssignedUserAddress(shippingobj, 1);
                                        orderDetailsmodel.setShippingAddress(shippingAddr);


                                        JSONObject vendorAddressObj = dataobj.getJSONObject("pickup_address");
                                        AssignedUserAddress vendorAddress = getAssignedUserAddress(vendorAddressObj, 0);
                                        orderDetailsmodel.setVendorAddress(vendorAddress);


                                        try {
                                            JSONObject orderostatusobj = dataobj.getJSONObject("order_status");
                                            OrderStatus orderStatus = new OrderStatus();
                                            orderStatus.setId(orderostatusobj.getString("id"));
                                            orderStatus.setDelivery_mode_id(orderostatusobj.getString("delivery_mode_id"));
                                            orderStatus.setSerial_number(orderostatusobj.getString("serial_number"));
                                            orderStatus.setStatus(orderostatusobj.getString("status"));
                                            orderDetailsmodel.setOrderStatus(orderStatus);


                                            tv_vendor_order_status.setText(orderStatus.getStatus());


                                            if (OrderStatus == null /*|| !OrderStatus.equals("508")*/)
                                                OrderStatus = orderStatus.getId();//from api
                                        } catch (JSONException e2) {

                                        }

                                        delivery_layout_map.setVisibility(View.VISIBLE);
                                        pickup_layout_map.setVisibility(View.VISIBLE);
                                        if (Integer.parseInt(OrderStatus) >= 508) {
                                            pickup_layout_map.setVisibility(View.GONE);
                                            delivery_layout_map.setVisibility(View.GONE);
                                            order_receive_otp_layout.setVisibility(View.GONE);
                                            lay_pickup_address.setVisibility(View.VISIBLE);
                                            tv_reject.setVisibility(View.GONE);

                                            if (Integer.parseInt(OrderStatus) == 508)
                                                rating_cart.setVisibility(View.VISIBLE);
                                        } else {


                                            if (Integer.parseInt(OrderStatus) >= 506) {
                                                //Reached to delivery point
                                                btn_reached_delivery_point.setEnabled(false);
                                                btn_reached_delivery_point.setAlpha(0.5f);
                                                btn_end_delivery.setVisibility(View.VISIBLE);//for now-ideally after image capture
                                                delivery_layout_map.setVisibility(View.GONE);
                                                pickup_layout_map.setVisibility(View.GONE);
                                                lay_pickup_address.setVisibility(View.VISIBLE);

                                                btn_reached_delivery_point.setVisibility(View.GONE);
                                            } else if (Integer.parseInt(OrderStatus) >= 505) {
                                                //Picked the order
                                                btn_reached_delivery_point.setVisibility(View.VISIBLE);
                                                pickup_layout_map.setVisibility(View.GONE);
                                                lay_pickup_address.setVisibility(View.VISIBLE);
                                                showCustomerLocation(vendorAddress.getLocation(), shippingAddr.getLocation(), tv_delivery_distance);
                                            } else if (Integer.parseInt(OrderStatus) >= 504) {
                                                //Reached to pickup point
                                                btn_reached_pickup_point.setEnabled(false);
                                                btn_reached_pickup_point.setAlpha(0.5f);
                                                btn_start_pickup.setVisibility(View.VISIBLE);
                                                btn_reached_pickup_point.setVisibility(View.GONE);
                                                showCustomerLocation(vendorAddress.getLocation(), shippingAddr.getLocation(), tv_delivery_distance);
                                            } else {
                                                setTravelDistance(vendorAddress.getLocation(), tv_pick_up_distance);
                                                showCustomerLocation(vendorAddress.getLocation(), shippingAddr.getLocation(), tv_delivery_distance);
                                            }
                                            //setTravelDistance(vendorAddress.getLocation(), tv_pick_up_distance);
                                            // showCustomerLocation(shippingAddr.getLocation(), tv_delivery_distance);
                                        }


                                        String text = "<font color=#9D9D9D>" + shippingAddr.getName() +
                                                "</font>";
                                    /*String text = "<font color=#9D9D9D>" + shippingAddr.getName() +
                                            "</font> <font color=#141414>" + shippingAddr.getPhone() + "</font>";*/
                                        vendor_details.setText(Html.fromHtml(text));
                                        address_details.setText(vendorAddress.getAddress());

                                        text = "<font color=#9D9D9D>" + vendorAddress.getName() +
                                                "</font>";
                                    /* text = "<font color=#9D9D9D>" + vendorAddress.getName() +
                                            "</font> <font color=#141414>" + vendorAddress.getPhone() + "</font>";*/
                                        seller_details.setText(Html.fromHtml(text));
                                        cust_address_details.setText(shippingAddr.getAddress());


                                    } catch (JSONException e1) {

                                    }

                                    try {
                                        JSONObject deleveryong = dataobj.getJSONObject("delivery_mode");
                                        DeleveryModesmodel deleveryModesmodel = new DeleveryModesmodel();
                                        deleveryModesmodel.setId(deleveryong.getString("id"));
                                        deleveryModesmodel.setName(deleveryong.getString("name"));
                                        deleveryModesmodel.setDesc(deleveryong.getString("desc"));
                                        orderDetailsmodel.setDeleveryModesmodel(deleveryModesmodel);
                                        // tv_deleverymode.setText(""+deleveryong.getString("name"));
                                    } catch (JSONException e2) {

                                    }

                               /* try {
                                    JSONObject customerobj = dataobj.getJSONObject("customer");
                                    CustomerModel customerModel = new CustomerModel();
                                    customerModel.setId(customerobj.getString("id"));
                                    customerModel.setUnique_id(customerobj.getString("unique_id"));
                                    customerModel.setFirst_name(customerobj.getString("first_name"));
                                    customerModel.setPhone(customerobj.getString("phone"));
                                    customermobile=customerobj.getString("phone");
                                    orderDetailsmodel.setCustomerModel(customerModel);


                                } catch (JSONException e2) {

                                }*/


                                    try {
                                        JSONObject delivery_job = dataobj.getJSONObject("delivery_job");
                                        DeliveryOrder deliveryOrder = new DeliveryOrder();
                                        deliveryOrder.setStatus(delivery_job.getString("status"));
                                        deliveryOrder.setDeliveryJobID(delivery_job.getString("id"));
                                        jobID = deliveryOrder.getDeliveryJobID();
                                        deliveryboystatus = delivery_job.getString("status");
                                        tv_order_my_status.setText(deliveryOrder.getStatusMessage());


                                        if (delivery_job.has("rating")) {
                                            tv_user_rating.setText(Utility.getValidStringValue(delivery_job.getString("rating"), "-"));
                                        }
                                        if (delivery_job.has("feedback")) {
                                            tv_user_rating_feedback.setText(Utility.getValidStringValue(delivery_job.getString("feedback"), "-"));
                                        }


                                        if (delivery_job.has("delivery_boy")) {
                                            JSONObject delivery_boy = delivery_job.getJSONObject("delivery_boy");
                                            if (delivery_boy.has("delivery_boy_pickup_image")) {
                                                String delivery_boy_pickup_image = delivery_boy.getString("delivery_boy_pickup_image");

                                                Glide.with(OrderDetailsActivity.this)
                                                        .load(delivery_boy_pickup_image)
                                                        .placeholder(R.drawable.noimageone)
                                                        .into(img_pick_up);
                                                layout_pickup_image.setVisibility(View.VISIBLE);
                                                //showImageDialog(delivery_boy_pickup_image);
                                            }
                                            if (delivery_boy.has("delivery_boy_delivery_image")) {
                                                String delivery_boy_delivery_image = delivery_boy.getString("delivery_boy_delivery_image");
                                                Glide.with(OrderDetailsActivity.this)
                                                        .load(delivery_boy_delivery_image)
                                                        .placeholder(R.drawable.noimageone)
                                                        .into(img_delivery);
                                                layout_delivery_image.setVisibility(View.VISIBLE);
                                            }
                                            //600 Customer is unavailable,
                                            //601 Return accepted by admin
                                            //602 Went back to the vendor, 603 = Returned the order
                                            delivery_boy_status = delivery_job.getString("status");

                                            try {
                                                System.out.println("aaaaaaaaa 22 " + deliveryboystatus);
                                                if (deliveryboystatus.equalsIgnoreCase("506")) {
                                                    JSONArray jsonArray = delivery_job.getJSONArray("delivery_rejections");
                                                    JSONObject reject_deliveryobj = jsonArray.getJSONObject(0);
                                                    String rejectstatus = reject_deliveryobj.getString("status");
                                                    System.out.println("aaaaaaaaa rejectstatus " + rejectstatus);
                                                    if (rejectstatus.equalsIgnoreCase("3")) {
                                                        tv_reject.setVisibility(View.GONE);
                                                    }
                                                }
                                            } catch (Exception e) {
                                                System.out.println("aaaaaaaaa catch 11 " + e.getMessage());
                                            }

                                            if (delivery_boy_status.equalsIgnoreCase("600")) {
                                                JSONArray jsonArray = delivery_job.getJSONArray("delivery_rejections");
                                                JSONObject reject_deliveryobj = jsonArray.getJSONObject(0);
                                                String created_at = reject_deliveryobj.getString("created_at");
                                                String rejection_reason = reject_deliveryobj.getString("rejection_reason");
                                                userNotResponding(created_at, rejection_reason);
                                            /* pickup_layout_map.setVisibility(View.VISIBLE);
                                            btn_reached_delivery_point_to_vendor.setVisibility(View.VISIBLE);
                                            btn_reached_delivery_point.setVisibility(View.GONE);
                                            lay_pickup_address.setVisibility(View.GONE);
                                            btn_return_delivery.setVisibility(View.VISIBLE);*/

                                            } else if (delivery_boy_status.equalsIgnoreCase("601") || delivery_boy_status.equalsIgnoreCase("604")) {

                                                try {
                                                    if (dialog4 != null) {
                                                        dialog4.dismiss();
                                                    }
                                                } catch (NullPointerException e) {

                                                }
                                                pickup_layout_map.setVisibility(View.VISIBLE);

                                                btn_return_delivery.setVisibility(View.VISIBLE);
                                                btn_generate_payment_link.setVisibility(View.GONE);
                                                btn_reached_delivery_point.setVisibility(View.GONE);

                                                btn_reached_delivery_point_to_vendor.setVisibility(View.VISIBLE);

                                                lay_pickup_address.setVisibility(View.GONE);


                                            } else if (delivery_boy_status.equalsIgnoreCase("602")) {
                                                System.out.println("aaaaaaaa 602");
                                                btn_reached_delivery_point_to_vendor.setEnabled(false);
                                                btn_reached_delivery_point_to_vendor.setAlpha(0.5f);
                                                btn_return_delivery.setVisibility(View.VISIBLE);
                                                btn_generate_payment_link.setVisibility(View.GONE);
                                            } else if (delivery_boy_status.equalsIgnoreCase("603")) {
                                                System.out.println("aaaaaaaa 603");
                                                btn_return_delivery.setVisibility(View.GONE);
                                                btn_generate_payment_link.setVisibility(View.GONE);
                                                btn_reached_delivery_point_to_vendor.setVisibility(View.GONE);
                                            } else {
                                                System.out.println("aaaaaaaaaa else status  ");
                                                try {
                                                    if (dialog4 != null) {
                                                        dialog4.dismiss();
                                                    }
                                                } catch (NullPointerException e) {

                                                }
                                            }
                                        }

                                    } catch (JSONException e2) {

                                    }


                                    try {
                                        JSONObject paymentobj = dataobj.getJSONObject("payment");
                                        PaymentModel paymentModel = new PaymentModel();
                                        paymentModel.setId(paymentobj.getString("id"));
                                        paymentModel.setPayment_method_id(paymentobj.getString("payment_method_id"));
                                        paymentModel.setTxn_id(paymentobj.getString("txn_id"));
                                        paymentModel.setAmount(paymentobj.getString("amount"));
                                        paymentModel.setCreated_at(paymentobj.getString("created_at"));
                                        paymentModel.setMessage(paymentobj.getString("message"));
                                        paymentModel.setStatus(paymentobj.getString("status"));
                                        orderDetailsmodel.setPaymentModel(paymentModel);
                                        // payment_type.setText(paymentobj.getString("txn_id"));


                                        try {
                                            String paymentMethod = "";
                                            switch (paymentModel.getPayment_method_id()) {
                                                case "1":
                                                    paymentMethod = "Cash on Delivery";
                                                    // if(orderslist.get(position).getPaymentModel().getStatus().equals("2"))
                                                    //paymentMethod="Online Payment Link";
                                                    break;
                                                case "3":
                                                    paymentMethod = "Wallet Money";
                                                    break;
                                                case "4":
                                                    paymentMethod = "Online Payment Link";
                                                    break;
                                                default:
                                                    paymentMethod = "Online";
                                            }
                                            tv_payment_method.setText(paymentMethod);
                                        } catch (Exception ex) {

                                        }

                                        btn_generate_payment_link.setVisibility(View.GONE);
                                        amountTobeCollected = null;
                                        if (paymentModel.getPayment_method_id() != null && paymentModel.getPayment_method_id().equals("1")) {
                                            if (!paymentModel.getStatus().equals("2")) {//2- online payment
                                                amountTobeCollected = orderDetailsmodel.getTotal();
                                                if (Integer.parseInt(OrderStatus) >= 506) {

                                                    if (delivery_boy_status.equalsIgnoreCase("602") || delivery_boy_status.equalsIgnoreCase("603") || delivery_boy_status.equalsIgnoreCase("604")) {
                                                        btn_generate_payment_link.setVisibility(View.GONE);
                                                    } else {
                                                        btn_generate_payment_link.setVisibility(View.VISIBLE);
                                                    }

                                                    // -will do this once delivery boy reached to user

                                                }
                                                //else -- paid via online link
                                                // payment_type.setText("Online payment link");
                                            }
                                        }

                                    } catch (JSONException e2) {

                                    }
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        } finally {
                            LoadingDialog.dialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                Toast.makeText(getApplicationContext(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaa token  " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    /**
     * It will send the order success information to the server when delivery guy hits on end delivery
     *
     * @param otp
     */
    public void invokeDeliveryOrder(String otp, String passcode) {

        String url = "";
        if (checkecomorder) {
            url = INVOKE_DELIVERY_ORDER;
        } else {
            url = INVOKE_DELIVERY_PICKDROPORDER;
        }
        LoadingDialog.loadDialog(this);

        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("order_id", orderID);
        if (otp != null && !otp.isEmpty()) {
            dataMap.put("otp", otp);
        }
        dataMap.put("delivery_job_id", jobID);
        if (passcode != null && !passcode.isEmpty()) {
            dataMap.put("passcode", passcode);
        } else
            dataMap.put("passcode", "");

        if (amountTobeCollected != null && !amountTobeCollected.isEmpty()) {
            dataMap.put("amount_collected", amountTobeCollected);
            //  dataMap.put("remarks",  "Cash collected");
        }

        String data = new JSONObject(dataMap).toString();
        Log.d("VolleyResponse", "INVOKE_DELIVERY_ORDER: request " + data);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    Log.d("VolleyResponse", "INVOKE_DELIVERY_ORDER: respnse " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");

                    LoadingDialog.dialog.dismiss();

                    if (!status) {
                        //  Toast.makeText(getApplicationContext(), "Your order has been failed with " + response, Toast.LENGTH_SHORT).show();
                        //  showErrorDialog("Incorrect otp");
                        //or
                        //show alert on same dialog
                        endDelivery(otp, passcode);
                    } else {
                        //  Toast.makeText(getApplicationContext(), "Your order has been delivered successfully.", Toast.LENGTH_SHORT).show();
                        // finish();
                        capturePhoto(TRACKING_DELIVERY);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                } finally {
                    LoadingDialog.dialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /**
     * It will inform the server when the deliver guy has reahed either on pick up or delivery point
     *
     * @param actionType
     */
    public void invokeAction(String actionType) {
       /*
       {
"delivery_job_id": 1,
"status": 4 // 4 = Reached to pickup point, 6 = reached to delivery point
}
        */

        int status = actionType == OrdersFragment.PICKUP_POINT ? 504 : 506;
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("delivery_job_id", jobID);
        dataMap.put("status", "" + status);

        String data = new JSONObject(dataMap).toString();
        Log.d("VolleyResponse", "CHANGE_DELIVERY_ORDER_STATUS: request " + data);

        LoadingDialog.loadDialog(this);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.CHANGE_DELIVERY_ORDER_STATUS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("profile r response", response);
                            Log.d("VolleyResponse", "CHANGE_DELIVERY_ORDER_STATUS: respnse " + response);

                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");

                            LoadingDialog.dialog.dismiss();
                            if (!status) {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_fail) + " " + response, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_success) + ".", Toast.LENGTH_SHORT).show();
                                if (actionType == OrdersFragment.PICKUP_POINT) {
                                    btn_reached_pickup_point.setEnabled(false);
                                    btn_reached_pickup_point.setAlpha(0.5f);

                                    btn_start_pickup.setVisibility(View.VISIBLE);
                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.logEvent("Delivery boy has reached to pick up point");
                                    }
                                } else {
                                    btn_reached_delivery_point.setEnabled(false);
                                    btn_reached_delivery_point.setAlpha(0.5f);

                                    btn_end_delivery.setVisibility(View.VISIBLE);

                                    if (amountTobeCollected != null && !amountTobeCollected.isEmpty()) {
                                        btn_generate_payment_link.setVisibility(View.VISIBLE);
                                    }


                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.logEvent("Delivery boy has reached to delivery point");
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.dialog.dismiss();
                        } finally {
                            LoadingDialog.dialog.dismiss();
                        }
                        //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /**
     * It open map view when deliver guy wants to locate vendor(pickup) location or user(delivery) location
     *
     * @param trackingPickup
     * @param address
     */
    private void openMapActivity(String trackingPickup, AssignedUserAddress address) {
        if (address.getLocation().getLatitude() != null && !address.getLocation().getLatitude().equals("null")) {
           /* VendorDetails vendorDetail = new VendorDetails();
            vendorDetail.setLocation_id(address.getLocation_id());
            vendorDetail.setName(address.getName());
            vendorDetail.setLatitude(Double.valueOf(address.getLocation().getLatitude()));
            vendorDetail.setLongitude(Double.valueOf(address.getLocation().getlongitude()));

            if (trackingPickup.equals(TRACKING_DELIVERY)) {
                Intent intent = new Intent(this, VendorNavigation.class);
                intent.putExtra("vendorDetail", vendorDetail);
                intent.putExtra("DeliveryType", TRACKING_DELIVERY);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, VendorNavigation.class);
                intent.putExtra("vendorDetail", vendorDetail);
                intent.putExtra("DeliveryType", TRACKING_PICKUP);
                startActivity(intent);
            }*/


            // Uri gmmIntentUri = Uri.parse("geo:"+address.getLocation().getLatitude()+","+address.getLocation().getlongitude());
            //Uri gmmIntentUri = Uri.parse("geo:"+"17.4875,78.3953");

            //mode sets the method of transportation. Mode is optional, and can be set to one of:
            //d for driving (default)
            //b for bicycling
            //l for two-wheeler
            //w for walking

            /*  Uri gmmIntentUri = Uri.parse("google.navigation:q="+address.getLocation().getLatitude()+
                    ","+address.getLocation().getlongitude()+"&q="+address.getLocation().getLatitude() +
                    ","+address.getLocation().getlongitude()+"&mode=l");*/

            System.out.println("aaaaaaaaaaaa  service click");


            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(OrderDetailsActivity.this)) {
                System.out.println("aaaaaaaaaaaa  service start 11");
                Intent intent = new Intent(OrderDetailsActivity.this, FloatingWidgetService.class);
                intent.putExtra("orderID", orderID);
                intent.putExtra("jobID", jobID);
                //  intent.putExtra("product_id", produ);
                //intent.putExtra("OrderPickupOtp", dashboardObject.getOnGoingOrder().getOrderPickupOtp());
                intent.putExtra("OrderStatus", OrderStatus);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startService(intent);
            } else {
                errorToast();
            }
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + address.getLocation().getLatitude() +
                    "," + address.getLocation().getlongitude() + "&mode=l");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }

    }

    /**
     * It will show a dialog when the delivery guy is successfully delivered the products to the user
     */
    private void invokeOrderSuccess() {
        //You order status has been accepted successfully.
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_order_accept_success);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        TextView tv_success = (TextView) dialog.findViewById(R.id.tv_success);
        tv_success.setText(getString(R.string.order_delivered));
        //tv_success.setText("Your order has been delivered successfully");
        //Your order has been delivered successfully.

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void timezone(String timezone) {
        String timeValue = "";
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Date past = format.parse("2016.02.05 AD at 23:59:30");
            Date past = format.parse(timezone);

            Date now = new Date();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if (seconds < 60) {
                timeValue = seconds + " " + getString(R.string.secongs_ago);
            } else if (minutes < 60) {
                timeValue = minutes + " " + getString(R.string.minutes_ago);
                System.out.println(minutes + " " + getString(R.string.minutes_ago));
            } else if (hours < 24) {
                timeValue = hours + " " + getString(R.string.hours_ago);
                System.out.println(hours + " " + getString(R.string.hours_ago));
            } else {
                timeValue = days + " " + getString(R.string.days_ago);
                System.out.println(days + " " + getString(R.string.days_ago));
            }
        } catch (Exception j) {
            System.out.println("aaaaa catch  " + j.getMessage());
            j.printStackTrace();
        }

        tv_order_date_time.setText(timeValue);
    }

    private AssignedUserAddress getAssignedUserAddress(JSONObject shippingobj, int chekuser) {

        AssignedUserAddress assignedUserAddress = new AssignedUserAddress();

        try {
            assignedUserAddress.setId(shippingobj.getString("id"));
            if (shippingobj.has("phone"))
                assignedUserAddress.setPhone(shippingobj.getString("phone"));
            else
                assignedUserAddress.setPhone("");

            if (shippingobj.has("email"))
                assignedUserAddress.setEmail(shippingobj.getString("email"));
            assignedUserAddress.setName(shippingobj.getString("name"));
            if (shippingobj.has("landmark"))
                assignedUserAddress.setLandmark(shippingobj.getString("landmark"));
            if (shippingobj.has("address"))
                assignedUserAddress.setAddress(shippingobj.getString("address"));
            assignedUserAddress.setLocation_id(shippingobj.getString("location_id"));
            if (shippingobj.has("vendor_user_id"))
                assignedUserAddress.setVendorUserID(shippingobj.getString("vendor_user_id"));
            if (shippingobj.has("unique_id"))
                assignedUserAddress.setUniqueID(shippingobj.getString("unique_id"));

            if (shippingobj.has("location")) {
                JSONObject locationObject = shippingobj.getJSONObject("location");

                LocationObject vendorLocation = new LocationObject();
                vendorLocation.setLocationID(locationObject.getString("id"));
                vendorLocation.setLatitude(locationObject.getString("latitude"));
                vendorLocation.setLongitude(locationObject.getString("longitude"));
                vendorLocation.setAddress(locationObject.getString("address"));

                try {
                    if (chekuser == 1) {
                        userlatitude = Double.parseDouble(locationObject.getString("latitude"));
                        userlongitude = Double.parseDouble(locationObject.getString("longitude"));
                        System.out.println("aaaaaaaaa userlatitude 111  " + userlatitude + "  " + userlongitude);
                    }

                } catch (NumberFormatException e) {

                }

                if (assignedUserAddress.getAddress() == null || assignedUserAddress.getAddress().equals("null"))
                    assignedUserAddress.setAddress(locationObject.getString("address"));

                assignedUserAddress.setLocation(vendorLocation);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return assignedUserAddress;
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void setTravelDistance(LocationObject locationObject, TextView view) {
        if (locationObject == null) {
            view.setText("--");
            return;
        }

        DirectionListener shopDistanceListener = new DirectionListener() {
            @Override
            public void onDirectionResult(DirectionObject result) {
                if (view != null) {
                    if (result != null) {
                        view.setText(result.getTravelDistance());
                    } else
                        view.setText("--");
                }
            }
        };
        LocationDetailUtil.getTravelDistanceFromCurrentLocation(this, shopDistanceListener,
                locationObject.getlongitude(),
                locationObject.getLatitude());
    }

    private void showCustomerLocation(LocationObject vendorLocation, LocationObject customerLocation, TextView view) {
        if (customerLocation == null) {
            view.setText("--");
            return;
        }

        DirectionListener shopDistanceListener = new DirectionListener() {
            @Override
            public void onDirectionResult(DirectionObject result) {
                if (view != null) {
                    if (result != null) {
                        view.setText(result.getTravelDistance());
                    } else
                        view.setText("--");
                }
            }
        };

        if (vendorLocation.getLatitude() != null && !vendorLocation.getLatitude().equals("null") &&
                customerLocation.getLatitude() != null && !customerLocation.getLatitude().equals("null")) {
            LatLng origin = new LatLng(Double.valueOf(vendorLocation.getLatitude()), Double.valueOf(vendorLocation.getlongitude()));
            LatLng destination = new LatLng(Double.valueOf(customerLocation.getLatitude()), Double.valueOf(customerLocation.getlongitude()));
            LocationDetailUtil.GetDirectionData(this, origin, destination, shopDistanceListener);
        }
    }

    /**
     * Capture the photo of products after accepting from the vendor and before delivering to the user
     *
     * @param trackingMode
     */
    private void capturePhoto(String trackingMode) {
        this.trackingMode = trackingMode;
        if (isCameraPermissionGranted) {
            cameraOpened = true;
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.accept_camera_permission), Toast.LENGTH_SHORT).show();
            showCameraAction = true;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //cameraOpened =false;
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        } else {
            String meessage = trackingMode == TRACKING_PICKUP ? getString(R.string.capture_recevied_products) : getString(R.string.capture_delivering_products);
            Toast.makeText(getApplicationContext(), meessage, Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
            /*
            if (trackingMode == TRACKING_PICKUP) {
                btn_start_pickup.setEnabled(false);
                btn_start_pickup.setAlpha(0.5f);
                btn_reached_delivery_point.setVisibility(View.VISIBLE);
            } else if (trackingMode == TRACKING_DELIVERY) {
                invokeOrderSuccess();
            }*/
        }

        if (requestCode == DRAW_OVER_OTHER_APP_PERMISSION) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    //Permission is not available. Display error text.
                    errorToast();
                    finish();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //setimage(whichimage,thumbnail);
        if (trackingMode == TRACKING_PICKUP) {
            pickUpOrder64String = convert(thumbnail);
            invokeOrderConfirmationAPI();
            Glide.with(this)
                    .load(thumbnail)
                    .placeholder(R.drawable.noimageone)
                    .into(img_pick_up);
            layout_pickup_image.setVisibility(View.VISIBLE);

            if (MyMixPanel.isMixPanelSupport) {
                MyMixPanel.logEvent("Delivery boy Captured the product pick up image");
            }
        } else {
            deliverOrder64String = convert(thumbnail);
            invokeOrderConfirmationAPI();

            Glide.with(this)
                    .load(thumbnail)
                    .placeholder(R.drawable.noimageone)
                    .into(img_delivery);
            layout_delivery_image.setVisibility(View.VISIBLE);

            if (MyMixPanel.isMixPanelSupport) {
                MyMixPanel.logEvent("Delivery boy Captured the product delivery image");
            }
        }

        /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;
        System.out.println("aaaaaaaaaa  size "+lengthbmp);*/
    }

    private void invokeOrderConfirmationAPI() {
        String URL = trackingMode == TRACKING_PICKUP ? Config.IMAGE_CONFIRMATION_PICKUP : Config.IMAGE_CONFIRMATION_DELIVERY;
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("delivery_job_id", jobID);
        if (trackingMode == TRACKING_PICKUP)
            dataMap.put("delivery_boy_pickup_image", "" + pickUpOrder64String);
        else
            dataMap.put("delivery_boy_delivery_image", "" + deliverOrder64String);


        String data = new JSONObject(dataMap).toString();
        Log.d("VolleyResponse", URL + ": request " + data);

        LoadingDialog.loadDialog(this);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        cameraOpened = false;
                        try {
                            Log.d("profile r response", response);
                            Log.d("VolleyResponse", URL + ": respnse " + response);

                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");

                            if (!status) {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_status_fail) + " " + response, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_success) + ".", Toast.LENGTH_SHORT).show();
                                if (trackingMode == TRACKING_PICKUP) {
                                    //now status 505
                                    btn_start_pickup.setVisibility(View.GONE);

                                    btn_reached_delivery_point.setVisibility(View.VISIBLE);
                                    btn_reached_pickup_point.setVisibility(View.GONE);
                                    lay_pickup_address.setVisibility(View.VISIBLE);

                                    tv_reject.setVisibility(View.VISIBLE);
                                    reject_type = 1;


                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.logEvent("Delivery boy has received the product from vendor");
                                    }
                                    showRatingDialog(0);
                                } else {

                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.logEvent("Delivery boy has successfully delivered the product to the end user");
                                    }
                                    //  showRatingDialog(1);
                                    invokeOrderSuccess();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_status_fail) + " " + e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
                cameraOpened = false;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void showRatingDialog(int whichposition) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.customer_rating_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        EditText et_feedback = (EditText) dialog.findViewById(R.id.et_feedback);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whichposition == 1) {
                    dialog.dismiss();
                    invokeOrderSuccess();
                } else {
                    String reviewStr = et_feedback.getText().toString().trim();
                    float rating = ratingBar.getRating();
                    if (reviewStr.length() > 2 && rating >= 1.0) {
                        dialog.dismiss();
                        if (orderDetailsmodel != null && orderDetailsmodel.getVendorAddress() != null) {
                            Map<String, String> maptoparse = new HashMap<>();
                            maptoparse.put("vendor_id", orderDetailsmodel.getVendorAddress().getId());
                            maptoparse.put("rating", rating + "".trim());
                            maptoparse.put("review", reviewStr);
                            JSONObject jsonObject = new JSONObject(maptoparse);
                            reviewSubmission(jsonObject);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please provide the data (Review & Rating)", Toast.LENGTH_SHORT).show();
                    }
                }
                // submitRating(ratingBar.getRating(), et_feedback.getText().toString());
                //finish();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (whichposition == 1) {
                    invokeOrderSuccess();
                }
                //finish();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }


    private void showErrorDialog(String errorMessage) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_error_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        TextView tv_success = (TextView) dialog.findViewById(R.id.tv_success);
        tv_success.setText(errorMessage);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void showImageDialog(ImageView img) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.image_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        TextView txt_upload_message = (TextView) dialog.findViewById(R.id.txt_upload_message);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        //closeButton.setVisibility(View.GONE);//looking not good
        ImageView img_preview = (ImageView) dialog.findViewById(R.id.img_preview);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Drawable drawable = img.getDrawable();
        Glide.with(this)
                .load(drawable)
                .placeholder(R.drawable.noimageone)
                .into(img_preview);


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void reviewSubmission(JSONObject dataObject) {
        final String dataStr = dataObject.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REVIEW_CREATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 201) {
                        //  UImsgs.showCustomToast(getApplicationContext(), "Submitted Successfully", SUCCESS);
                    } else {
                        // UImsgs.showCustomToast(context, "Sorry for Inconvenience \n Please submit your review again", WARNING);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //UImsgs.showCustomToast(context, "Sorry for Inconvenience \n Please submit your review again", WARNING);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // UImsgs.showCustomToast(context, "Sorry for Inconvenience \n Please submit your review again", WARNING);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AUTH_TOKEN, "Bearer " +preferenceManager.getString(USER_TOKEN));
                params.put("content-type", "application/json");
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }

    private void errorToast() {
        Toast.makeText(this, "Draw over other app permission not available. Can't start the application without the permission.", Toast.LENGTH_LONG).show();
    }

    public void setrejectOrder(RejectOrdersRequets rejectOrdersRequets) {
        LoadingDialog.loadDialog(this);
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("delivery_job_id", jobID);
        dataMap.put("order_id", orderID);
        dataMap.put("rejected_reason_id", rejectOrdersRequets.getId());

        String data = new JSONObject(dataMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(OrderDetailsActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REJECT_ORDERS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");
                            if (status && http_code == 200) {
                                Toast.makeText(OrderDetailsActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                System.out.println("aaaaaaaaaa   sucess " + jsonObject.toString());
                                finish();

                            }

                        } catch (JSONException e) {
                            Toast.makeText(OrderDetailsActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(OrderDetailsActivity.this, "Reject order failed with exception " + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void generatePaymentOnlineLink() {
        LoadingDialog.loadDialog(this);
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("order", trackID);//

        String data = new JSONObject(dataMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(OrderDetailsActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PAYMENT_LINK,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Integer status = jsonObject.getInt("status_code");//status_code : 200
                            if (status == 200) {
                                Toast.makeText(OrderDetailsActivity.this, "Payment link has successfully sent to customer email id.", Toast.LENGTH_SHORT).show();
                                System.out.println("aaaaaaaaaa   payment link generated " + jsonObject.toString());
                            } else
                                Toast.makeText(OrderDetailsActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            Toast.makeText(OrderDetailsActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(OrderDetailsActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);

            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            try {
                currentlatitude = location.getLatitude();
                currentlongitude = location.getLongitude();
            } catch (NullPointerException e) {

            }

            System.out.println("aaaaaaaaa getLastKnownLocation  " + currentlatitude + "  " + currentlongitude);

        } catch (SecurityException e) {
            e.printStackTrace();
            Log.d("Location Error", e.toString());

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            currentlatitude = location.getLatitude();
            currentlongitude = location.getLongitude();
            System.out.println("aaaaaaaaa location  " + currentlatitude + "  " + currentlongitude);
            // Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            // List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (Exception e) {

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void userNotResponding(String created_at, String rejection_reason) {

        try {
            if (dialog4 != null) {
                dialog4.dismiss();
            }
        } catch (NullPointerException e) {

        }


        LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertLayout1 = inflater1.inflate(R.layout.usernotresponding, null);

        final Button submit1 = alertLayout1.findViewById(R.id.submit);
        final Button cancel = alertLayout1.findViewById(R.id.cancel);
        final TextView tv_timer = alertLayout1.findViewById(R.id.tv_timer);
        final TextView tv_reason = alertLayout1.findViewById(R.id.tv_reason);
        final LinearLayout layout_call = alertLayout1.findViewById(R.id.layout_call);

        android.app.AlertDialog.Builder alert4 = new android.app.AlertDialog.Builder(OrderDetailsActivity.this);
        // alert4.setIcon(R.mipmap.ic_launcher);

        layout_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int permissionCheck = ContextCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(OrderDetailsActivity.this, new String[]{Manifest.permission.CALL_PHONE},
                            101);
                }

                System.out.println("aaaaaaaaa vendorphoneenumber  " + customermobile);
                if (!customermobile.isEmpty() || !customermobile.equalsIgnoreCase("")) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + customermobile));
                    if (ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        startActivity(callIntent);
                    }
                } else {
                    Toast.makeText(OrderDetailsActivity.this, "Mobile number not updated", Toast.LENGTH_SHORT).show();
                }

            }
        });
        // tv_reason.setText(""+rejection_reason);
        Date createdTime = Utility.getDateFromString(created_at);
        System.out.println("aaaaaaaaa date and time  " + Calendar.getInstance().getTime().toString() + " created " + createdTime.getTime());
        long remSeconds = (Calendar.getInstance().getTime().getTime() - createdTime.getTime());
        //  int time=Integer.parseInt(orderConfirmationtime)*60000;
        int time = (order_confirmation_time + 1) * 60000;
        long totaltime = time - (remSeconds);

        new CountDownTimer(totaltime, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                tv_timer.setText("Admin reaching to user Please wait " + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + "");

            }

            public void onFinish() {
                //  setAcceptreject();
                //  tv_status.setText("Approved");
                getOrderDetails();
            }
        }.start();

        alert4.setTitle("" + rejection_reason);
        // reason1.setHint(getString(R.string.reason));
        alert4.setView(alertLayout1);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert4.setCancelable(true);
        dialog4 = alert4.create();
        dialog4.dismiss();
        dialog4.show();

        submit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog4.dismiss();

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog4.dismiss();
            }
        });


    }

    //Reached to return deliveryboy to vrendor

    public void reachedToDeliveryToVendor() {

        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("delivery_job_id", jobID);

        String data = new JSONObject(dataMap).toString();
        System.out.println("aaaaaaaaa request " + data.toString());
        Log.d("VolleyResponse", "CHANGE_DELIVERY_ORDER_STATUS: request " + data);

        LoadingDialog.loadDialog(this);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.RETURN_REACHED,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("profile r response", response);
                            Log.d("VolleyResponse", "CHANGE_DELIVERY_ORDER_STATUS: respnse " + response);

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaa response  " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");

                            LoadingDialog.dialog.dismiss();
                            if (!status) {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_fail) + " " + response, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_success) + ".", Toast.LENGTH_SHORT).show();
                                btn_reached_delivery_point_to_vendor.setEnabled(false);
                                btn_reached_delivery_point_to_vendor.setAlpha(0.5f);
                                btn_end_delivery.setVisibility(View.GONE);
                                btn_return_delivery.setVisibility(View.VISIBLE);
                                if (MyMixPanel.isMixPanelSupport) {
                                    MyMixPanel.logEvent("Delivery boy has reached to delivery point");
                                }
                                getOrderDetails();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.dialog.dismiss();
                        } finally {
                            LoadingDialog.dialog.dismiss();
                        }
                        //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    public void endretrndelivery(String otp) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.customer_otp_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        final EditText editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        final EditText editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
        final EditText editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        final EditText editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        final EditText editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        final EditText editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        TextView tv_lost_otp = (TextView) dialog.findViewById(R.id.tv_lost_otp);
        RelativeLayout layout_payment_status = (RelativeLayout) dialog.findViewById(R.id.layout_payment_status);
        ToggleButton tb_payment_status = (ToggleButton) dialog.findViewById(R.id.tb_payment_status);
        Button btn_verify_passcode = (Button) dialog.findViewById(R.id.btn_verify_passcode);
        final EditText et_passcode = (EditText) dialog.findViewById(R.id.et_passcode);
        TextView tv_error_passcode = (TextView) dialog.findViewById(R.id.tv_error_passcode);
        LinearLayout layout_otp = (LinearLayout) dialog.findViewById(R.id.layout_otp);
        LinearLayout layout_passcode = (LinearLayout) dialog.findViewById(R.id.layout_passcode);
        TextView tv_login_with_otp = (TextView) dialog.findViewById(R.id.tv_login_with_otp);
        final EditText et_collected_amount = (EditText) dialog.findViewById(R.id.et_collected_amount);

        TextView tv_header = (TextView) dialog.findViewById(R.id.tv_header);
        LinearLayout layout_payment_input = (LinearLayout) dialog.findViewById(R.id.layout_payment_input);

        layout_payment_status.setVisibility(View.GONE);
        layout_payment_input.setVisibility(View.GONE);
        tv_lost_otp.setVisibility(View.GONE);

        Boolean isPaymentMandatory = false;

        et_collected_amount.setText("" + amountTobeCollected);
        et_collected_amount.setEnabled(false);
        et_collected_amount.setClickable(false);


        /*if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            tb_payment_status.setChecked(true);
            tv_error.setVisibility(View.VISIBLE);
            tv_error.setText("* "+getString(R.string.incorrect_otp));
            layout_otp.setVisibility(View.VISIBLE);
            layout_payment_input.setVisibility(View.VISIBLE);
            layout_passcode.setVisibility(View.GONE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
            et_collected_amount.setText(amountTobeCollected);
        }*/
        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextone.length() == 1) {
                    editTextone.clearFocus();
                    editTexttwo.requestFocus();
                    editTexttwo.setCursorVisible(true);
                } else {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTexttwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTexttwo.length() == 1) {
                    editTexttwo.clearFocus();
                    editTextthree.requestFocus();
                    editTextthree.setCursorVisible(true);
                } else {
                    editTexttwo.requestFocus();
                    editTexttwo.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextthree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextthree.length() == 1) {
                    editTextthree.clearFocus();
                    editTextfour.requestFocus();
                    editTextfour.setCursorVisible(true);
                } else {
                    editTextthree.requestFocus();
                    editTextthree.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextfour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextfour.length() == 1) {
                    editTextfour.clearFocus();
                    editTextFive.requestFocus();
                    editTextFive.setCursorVisible(true);
                } else {
                    editTextfour.requestFocus();
                    editTextfour.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextFive.length() == 1) {
                    editTextFive.clearFocus();
                    editTextSix.requestFocus();
                    editTextSix.setCursorVisible(true);
                } else {
                    editTextFive.requestFocus();
                    editTextFive.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    Map<String, Object> uploadMap = new HashMap<>();
                    uploadMap.put("order_id", orderID);
                    uploadMap.put("delivery_job_id", jobID);
                    uploadMap.put("order_return_otp", otp);
                    JSONObject json = new JSONObject(uploadMap);

                    returncomplete(json.toString());
                } else {
                    tv_error.setVisibility(View.VISIBLE);
                    String message = getString(R.string.enter_6digit_otp);
                    tv_error.setText(message);
                }
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    public void returncomplete(String data) {

       /* Map<String, String> dataMap = new HashMap<>();
        dataMap.put("delivery_job_id", jobID);

        String data = new JSONObject(dataMap).toString();*/
        System.out.println("aaaaaaaaa request " + data.toString());
        Log.d("VolleyResponse", "CHANGE_DELIVERY_ORDER_STATUS: request " + data);

        LoadingDialog.loadDialog(this);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.RETURN_ORDER,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("profile r response", response);
                            Log.d("VolleyResponse", "CHANGE_DELIVERY_ORDER_STATUS: respnse " + response);

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaa response  " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");

                            LoadingDialog.dialog.dismiss();
                            if (!status) {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_fail) + " Invalid OTP", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.delivery_order_success) + ".", Toast.LENGTH_SHORT).show();
                                if (MyMixPanel.isMixPanelSupport) {
                                    MyMixPanel.logEvent("Delivery boy has successfully returned the products to the vendor");
                                }
                                showRatingDialog(1);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.dialog.dismiss();
                        } finally {
                            LoadingDialog.dialog.dismiss();
                        }
                        //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa  token  " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}