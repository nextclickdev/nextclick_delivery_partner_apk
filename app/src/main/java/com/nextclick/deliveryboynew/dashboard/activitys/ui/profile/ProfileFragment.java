package com.nextclick.deliveryboynew.dashboard.activitys.ui.profile;

import static com.nextclick.deliveryboynew.Config.Config.PROFILE_READ;
import static com.nextclick.deliveryboynew.Config.Config.URL_States;
import static com.nextclick.deliveryboynew.Config.Config.VEHICLE_TYPES;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.UiMsgs.setEditTextErrorMethod;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.authentication.model.Constiuencies;
import com.nextclick.deliveryboynew.authentication.model.Districts;
import com.nextclick.deliveryboynew.authentication.model.DocField;
import com.nextclick.deliveryboynew.authentication.model.ImageUploadListener;
import com.nextclick.deliveryboynew.authentication.model.States;
import com.nextclick.deliveryboynew.authentication.model.VehicleType;
import com.nextclick.deliveryboynew.dashboard.model.Profile;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.Utility;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class ProfileFragment extends Fragment implements View.OnClickListener, LocationListener, ImageUploadListener {

    private TextView tv_edit,tv_userid,tv_heading,btnUpdate;


    private ImageView back_imageView,img_refresh,img_bankpassbook,img_canclecheque,
            img_cancel_cheque_status,img_bank_passbook_status,
            img_vehilce_front,img_vehilce_back,img_insurance_front;
    CircleImageView profileImage_edit;
    ImageView img_profile;
    TextInputEditText tv_address;

    ImageView img_reference_aadhar,img_front_aadhar,img_back_aadhar;
    ImageView img_reference_pan,img_front_pan,img_back_pan;
    ImageView img_reference_vechicalnumber,img_front_vechicalnumber,img_back_vechicalnumber;
    ImageView img_reference_rc,img_front_rc,img_back_rc;
    EditText et_adharnumber,et_pannumber,et_vechicalnumber,et_drlicencenumber,et_insurance_number,et_pincode;

    private TextInputEditText et_firstname,et_lastnamename,et_mobilenumber,et_emailid,et_address;

    private String adhar64="",pan64="",bankpas64="",canclecheque64="",rc64="",licence64="",profile64="";


    private boolean editview=false;
    private String userChoosenTask,base64image="";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Context mContext;
    PreferenceManager preferenceManager;
    Double lattitude, longitude;
    LocationManager locationManager;
    private Profile profileObject;


    List<Address> addresses;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private boolean isContinue = false;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LocationRequest locationRequest;
    Geocoder geocoder;
    private int whichimage;
    private RecyclerView recyclerview_docs;
    private ArrayList<DocField> listDocs;

    private Matcher matcher;
    private static final String PAN_PATTERN = "(([A-Za-z]{5})([0-9]{4})([a-zA-Z]))";

    private static final String AADHAR_PATTERN = "^[2-9]{1}[0-9]{11}$";//"[0-9]{12}";
    private Pattern patternAADHAR = Pattern.compile(AADHAR_PATTERN);
    private Pattern patternPAN = Pattern.compile(PAN_PATTERN);
    private static final String VECHICAL_PATTERN = "^[A-Z|a-z]{2}\\s?[0-9]{1,2}\\s?[A-Z|a-z]{0,3}\\s?[0-9]{4}$";
    private Pattern patternVECHICAL = Pattern.compile(VECHICAL_PATTERN);
    private static final String DRIVINGLICENCE_PATTERN = "(([A-Z]{2})([0-9]{14}))";
    private Pattern patternDRIVINGLICENCE = Pattern.compile(DRIVINGLICENCE_PATTERN);
    private ArrayList<String> vehicles;
    private final int MY_CAMERA_PERMISSION_CODE =100;
    private boolean isCameraPermissionGranted;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile_new, container, false);
        int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);
        }
        else
            isCameraPermissionGranted =true;

        init(root);
        enableEditFunctionality();
        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to Profile page");
        }
        return root;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_CAMERA_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isCameraPermissionGranted =true;
                }
                else
                {
                    Toast.makeText(getContext(), getString(R.string.accept_camera_permission), Toast.LENGTH_SHORT).show();
                }
            }
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals(getString(R.string.take_photo)))
                        cameraIntent();
                    else if(userChoosenTask.equals(getString(R.string.choose_from_library)))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;

        }
    }

    public void init(View view){


        mContext = getContext();
        preferenceManager = new PreferenceManager(mContext);
        preferenceManager.putBoolean(getString(R.string.refresh_profile), false);

        tv_edit=view.findViewById(R.id.tv_edit);
        back_imageView=view.findViewById(R.id.back_imageView);
        profileImage_edit=view.findViewById(R.id.profileImage_edit);
        img_profile=view.findViewById(R.id.img_profile);
        tv_userid=view.findViewById(R.id.tv_userid);
        et_firstname=view.findViewById(R.id.et_firstname);
        et_lastnamename=view.findViewById(R.id.et_lastnamename);
        et_mobilenumber=view.findViewById(R.id.et_mobilenumber);
        et_emailid=view.findViewById(R.id.et_emailid);
        img_refresh=view.findViewById(R.id.img_refresh);
        tv_address=view.findViewById(R.id.tv_address);
        et_address=view.findViewById(R.id.et_address);

        et_adharnumber=view.findViewById(R.id.et_adharnumber);
        et_pannumber=view.findViewById(R.id.et_pannumber);
        et_vechicalnumber=view.findViewById(R.id.et_vechicalnumber);
        et_drlicencenumber=view.findViewById(R.id.et_drlicencenumber);
        img_bankpassbook=view.findViewById(R.id.img_bankpassbook);
        img_canclecheque=view.findViewById(R.id.img_canclecheque);
        img_bankpassbook.setOnClickListener(this);
        img_canclecheque.setOnClickListener(this);

        tv_heading=view.findViewById(R.id.tv_heading);


        btnUpdate=view.findViewById(R.id.btnUpdate);


        img_cancel_cheque_status=view.findViewById(R.id.img_cancel_cheque_status);
        img_bank_passbook_status=view.findViewById(R.id.img_bank_passbook_status);
        img_reference_aadhar=view.findViewById(R.id.img_reference_aadhar);
        img_front_aadhar=view.findViewById(R.id.img_front_aadhar);
        img_back_aadhar=view.findViewById(R.id.img_back_aadhar);
        img_reference_pan=view.findViewById(R.id.img_reference_pan);
        img_front_pan=view.findViewById(R.id.img_front_pan);
        img_back_pan=view.findViewById(R.id.img_back_pan);
        img_reference_vechicalnumber=view.findViewById(R.id.img_reference_vechicalnumber);
        img_front_vechicalnumber=view.findViewById(R.id.img_front_vechicalnumber);
        img_back_vechicalnumber=view.findViewById(R.id.img_back_vechicalnumber);
        img_reference_rc=view.findViewById(R.id.img_reference_rc);
        img_front_rc=view.findViewById(R.id.img_front_rc);
        img_back_rc=view.findViewById(R.id.img_back_rc);

        img_reference_aadhar.setOnClickListener(this);
        img_front_aadhar.setOnClickListener(this);
        img_back_aadhar.setOnClickListener(this);
        img_reference_pan.setOnClickListener(this);
        img_front_pan.setOnClickListener(this);
        img_back_pan.setOnClickListener(this);
        img_reference_vechicalnumber.setOnClickListener(this);
        img_front_vechicalnumber.setOnClickListener(this);
        img_back_vechicalnumber.setOnClickListener(this);
        img_reference_rc.setOnClickListener(this);
        img_front_rc.setOnClickListener(this);
        img_back_rc.setOnClickListener(this);

        btnUpdate.setOnClickListener(this);


        recyclerview_docs = view.findViewById(R.id.recyclerview_docs);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview_docs.setHasFixedSize(true);
        recyclerview_docs.setLayoutManager(linearLayoutManager);
        setStaticFields();
        designAddressFields(view);
        img_vehilce_front= view.findViewById(R.id.img_vehilce_front);
        img_vehilce_back= view.findViewById(R.id.img_vehilce_back);
        img_insurance_front= view.findViewById(R.id.img_insurance_front);
        et_insurance_number= view.findViewById(R.id.et_insurance_number);
        et_pincode= view.findViewById(R.id.et_pincode);
        img_vehilce_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(300);
            }
        });
        img_vehilce_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(301);
            }
        });
        img_insurance_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(302);
            }
        });


        enable(et_firstname,0);
        enable(et_vechicalnumber,0);
        enable(et_lastnamename,0);
        enable(et_drlicencenumber,0);
        enable(et_mobilenumber,0);
        enable(et_emailid,0);
        enable(et_address,0);
        enable(et_adharnumber,0);
        enable(et_pannumber,0);
        fetchUserDetails();
        tv_edit.setOnClickListener(this);
        back_imageView.setOnClickListener(this);
        profileImage_edit.setOnClickListener(this);
        img_profile.setOnClickListener(this);
        img_refresh.setOnClickListener(this);

        geocoder = new Geocoder(mContext, Locale.getDefault());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        if (!isContinue) {
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaaaa  address "+address+" 1 "+addresses.get(0).getAddressLine(1));
                                tv_address.setText(address);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaaaa  address "+address+" 1 "+addresses.get(0).getAddressLine(1));
                                tv_address.setText(address);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        isContinue = false;

        getLocation();
    }

    @Override
    public void onClick(View v) {
        System.out.println("aaaaaaaa id "+v.getId());
        switch (v.getId()){
            case R.id.img_front_aadhar:
                selectImage(101);
                break;
            case R.id.img_back_aadhar:
                selectImage(102);
                break;
            case R.id.img_front_pan:
                selectImage(103);
                break;
            case R.id.img_back_pan:
                selectImage(104);
                break;
            case R.id.img_front_vechicalnumber:
                selectImage(105);
                break;
            case R.id.img_back_vechicalnumber:
                selectImage(106);
                break;
            case R.id.img_front_rc:
                selectImage(107);
                break;
            case R.id.img_back_rc:
                selectImage(108);
                break;
            case R.id.img_reference_aadhar:
                previewImage(R.drawable.aadhar_refernce);
                break;
            case R.id.img_reference_pan:
                previewImage(R.drawable.pan_reference);
                break;
            case R.id.img_reference_vechicalnumber:
                previewImage(R.drawable.licence_reference);
                break;
            case R.id.img_reference_rc:
                previewImage(R.drawable.vehicle_rc_reference);
                break;
            case R.id.tv_edit:
                System.out.println("aaaaaaaa  edit click");
               enableEditFunctionality();
                break;
            case R.id.back_imageView:
                if (editview==true){
                    editview=false;
                    tv_heading.setText("Profile View");
                    tv_edit.setVisibility(View.VISIBLE);
                    enable(et_firstname,0);
                    enable(et_vechicalnumber,0);
                    enable(et_lastnamename,0);
                    enable(et_drlicencenumber,0);
                    enable(et_mobilenumber,0);
                    enable(et_emailid,0);
                    enable(et_address,0);
                    enable(et_adharnumber,0);
                    enable(et_pannumber,0);
                }
                break;
            case R.id.profileImage_edit:
                selectImage(7);
                break;
            case R.id.img_profile:
                selectImage(7);
                break;


            case R.id.img_refresh:
                getLocation();
                break;

            case R.id.img_bankpassbook:
                showImageDialog(5);
                break;
            case R.id.img_canclecheque:
                showImageDialog(6);


                break;
            case R.id.btnUpdate:
                //Toast.makeText(mContext, "Profile Update API is yet to integrate.", Toast.LENGTH_SHORT).show();
                updateProfileData();
                break;
        }
    }

    private void updateProfileData() {
        if (isValidProfile()) {
            String url = "";
            String data = "";
            url = Config.PROFILE_UPDATE;

            Map<String, String> dataMap = new HashMap<>();
            dataMap.put("first_name", et_firstname.getText().toString());
            dataMap.put("last_name", et_lastnamename.getText().toString());
            dataMap.put("unique_id", tv_userid.getText().toString());
            dataMap.put("mobile", et_mobilenumber.getText().toString());
            dataMap.put("email", et_emailid.getText().toString());
           // dataMap.put("password", et_password.getText().toString());


            dataMap.put("pincode", et_pincode.getText().toString());
            dataMap.put("permanent_address", et_address.getText().toString());
            dataMap.put("vehicle_type_id", ""+selectedVehicle);
            dataMap.put("state", ""+stateid);
            dataMap.put("district", ""+districtid);
            dataMap.put("constituency", ""+constitueid);

            dataMap.put("aadhar_number", et_adharnumber.getText().toString());
            dataMap.put("pan_card_number",  et_pannumber.getText().toString());
            dataMap.put("driving_license_number",  et_drlicencenumber.getText().toString());
            dataMap.put("vehicle_number",  et_vechicalnumber.getText().toString());
            dataMap.put("latitude", ""+wayLatitude);
            dataMap.put("longitude", ""+wayLongitude);
            dataMap.put("geo_lcoation_address", ""+tv_address.getText().toString());
            dataMap.put("aadhar_card_image_front", ""+listDocs.get(0).getFrontImage());
            dataMap.put("aadhar_card_image_back", ""+listDocs.get(0).getBackImage());
            dataMap.put("pan_card_image_front", ""+listDocs.get(1).getFrontImage());
            dataMap.put("pan_card_image_back", ""+listDocs.get(1).getBackImage());
            dataMap.put("driving_license_image_front", ""+listDocs.get(2).getFrontImage());
            dataMap.put("driving_license_image_back", ""+listDocs.get(2).getBackImage());
            dataMap.put("rc_image_front", ""+listDocs.get(3).getFrontImage());
            dataMap.put("rc_image_back", ""+listDocs.get(3).getBackImage());
            dataMap.put("vehicle_image_front", ""+string_vehilce_front);
            dataMap.put("vehicle_image_back", ""+string_vehilce_back);
            dataMap.put("vehicle_insurance_image_front", ""+string_insurance_front);
            dataMap.put("vehicle_insurance_number", ""+et_insurance_number.getText().toString());

            dataMap.put("profile_image", ""+profile64);
            dataMap.put("bank_passbook_image", ""+bankpas64);
            dataMap.put("cancellation_cheque_image", ""+canclecheque64);

            data = new JSONObject(dataMap).toString();
            updateDetails(url, data);
        }
    }

    private boolean isValidProfile() {
        boolean valid = true;


        if (et_firstname.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_firstname, "Enter your first name");
            valid = false;
        } else if (et_lastnamename.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_lastnamename, "Enter your last name");
            valid = false;
        } else if (selectedVehicle.isEmpty()) {
            showErrorToast("Please select the vehicle tye");
            valid = false;
        } else if (et_address.getText() == null || et_address.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_address, "Enter Permanent Address");
            valid = false;
        } else if (constitueid == null || constitueid.isEmpty()) {
            showErrorToast("Please select the constituency");
            valid = false;
        } else if (et_pincode.getText() == null || et_pincode.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_pincode, "Enter Pincode");
            valid = false;
        }
        else if (!validateNewFields()) {
            valid = false;
        }
        /*else if (bankpas64.isEmpty()) {
            showErrorToast(getString(R.string.upload_passbook));
            valid = false;
        } else if (canclecheque64.isEmpty()) {
            showErrorToast(getString(R.string.upload_cancel_cheque));
            valid = false;
        }*/


        return valid;
    }

    private void showErrorToast(String message) {
        Toasty.error(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private Boolean validateNewFields() {
        boolean valid = true;


        if(!validateAadhaar(listDocs.get(0)))
        {
            valid=false;
        }
        else if(!validatePan(listDocs.get(1)))
        {
            valid=false;
        }
        else if(!validateLicense(listDocs.get(2)))
        {
            valid=false;
        }//dlicense
        else if(!validateRC(listDocs.get(3)))
        {
            valid=false;
        }//rc

        else if (string_vehilce_front== null ||  string_vehilce_front.isEmpty()) {
            showErrorToast("upload vehicle Front Image");
            valid=false;
        }
        else if (string_vehilce_back== null ||  string_vehilce_back.isEmpty()) {
            showErrorToast("upload vehicle Back Image");
            valid=false;
        }
        else if (et_insurance_number.getText()==null || et_insurance_number.getText().toString().isEmpty()){
            setEditTextErrorMethod(et_insurance_number,"Enter Vehicle Insurance number");
            valid=false;
        }
        /*if (!validateDRIVINGLICENCE(et_drlicencenumber.getText().toString())){
            errorMsg +="Enter Licene number,";
        }*/
        else if (string_insurance_front== null ||  string_insurance_front.isEmpty()) {
            showErrorToast("upload insurance Image");
            valid=false;
        }
        return valid;
    }


    private Boolean validateAadhaar(DocField docField) {
        boolean valid = true;
        if (et_adharnumber.getText() == null || et_adharnumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_adharnumber,"Enter Aadhaar card number");
            valid=false;

        } else if (!validatenAADHAR(et_adharnumber.getText().toString())) {
            setEditTextErrorMethod(et_adharnumber, getString(R.string.upload_valid_aadhaar));
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload Aadhaar Card Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload Aadhaar Card Back Image");
            valid=false;
        }
        return valid;
    }
    private Boolean validatePan(DocField docField) {
        boolean valid = true;
        if (et_pannumber.getText() == null || et_pannumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_pannumber,"Enter Pan card number");
            valid=false;
        } else if (!validatePAN(et_pannumber.getText().toString())) {
            setEditTextErrorMethod(et_pannumber,getString(R.string.upload_valid_pan));
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload Pan Card Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload Pan Card Back Image");
            valid=false;
        }
        return valid;
    }
    private Boolean validateLicense(DocField docField) {
        boolean valid = true;
        if (et_drlicencenumber.getText() == null || et_drlicencenumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_drlicencenumber, "Enter Driving License");
            valid=false;
        } else if (!validateDRIVINGLICENCE(et_drlicencenumber.getText().toString())) {
            setEditTextErrorMethod(et_drlicencenumber, "Enter Valid Driving License");
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload Driving Licence Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload Driving Licence Back Image");
            valid=false;
        }
        return valid;
    }
    private Boolean validateRC(DocField docField) {
        boolean valid = true;
        if (et_vechicalnumber.getText() == null || et_vechicalnumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_vechicalnumber, "Enter Vehicle Registration Number");
            valid=false;
        } else if (!validateVECHIClE(et_vechicalnumber.getText().toString())) {
            setEditTextErrorMethod(et_vechicalnumber, "Enter Valid Vehicle Registration Number");
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload RC Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload RC Back Image");
            valid=false;
        }
        return valid;
    }


    //

    private void updateDetails(String url, final String data) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.logEvent("Delivery boy updated the profile");
                                    }

                                    Toast.makeText(mContext,getString(R.string.update_success), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")) + "", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void showImageDialog(int i) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.image_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        TextView txt_upload_message = (TextView) dialog.findViewById(R.id.txt_upload_message);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        ImageView img_preview = (ImageView) dialog.findViewById(R.id.img_preview);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        int color=0XFFff5251;
        String url="";
        Drawable drawable= null;
        String status="0";
        String message="Invalid image";
        String errorMessage="";

        if(profileObject!=null) {
            /*if (i == 1) {
                url = profileObject.getAadharNumberImage();
                drawable= img_adhar.getDrawable();
                status= profileObject.getAadharNumberStatus();
                errorMessage= profileObject.getAadharUploadMessage();
            } else if (i == 2) {
                drawable= img_pancard.getDrawable();
                url = profileObject.getPanNumberImage();
                status= profileObject.getPanNumberStatus();
                errorMessage= profileObject.getPanUploadMessage();
            } else if (i == 3) {
                drawable= img_rc.getDrawable();
                url = profileObject.getRcNumberImage();
                status= profileObject.getRcNumberStatus();
                errorMessage= profileObject.getRcUploadMessage();
            } else if (i == 4) {
                drawable= img_drivinglicence.getDrawable();
                url = profileObject.getDrivingLicenseImage();
                status= profileObject.getDrivingLicenseNumberStatus();
                errorMessage= profileObject.getDrivingLicenseUploadMessage();
            } else*/ if (i == 5) {
                drawable= img_bankpassbook.getDrawable();
                url = profileObject.getBankPassbookImage();
                status= profileObject.getBankPassbookStatus();
                errorMessage= profileObject.getBankPassbookUploadMessage();
            } else if (i == 6) {
                drawable= img_canclecheque.getDrawable();
                url = profileObject.getCancelChequeImage();
                status= profileObject.getCancelChequeStatus();
                errorMessage= profileObject.getCancelChequeUploadMessage();
            }
        }

        if(status .equalsIgnoreCase("2"))
        {
            message = "Waiting for approval";
            color = 0XFF35c534;
        }
        else if(status.equalsIgnoreCase("1")) {
            message = "Uploaded Successfully";
            color = 0XFF35c534;
        }
        else {
            if (!TextUtils.isEmpty(errorMessage)) {
                message = errorMessage;

            }
        }
        img_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectImage(i);
            }
        });

        //dialogButton.setText(message);
        txt_upload_message.setText(message);
        txt_upload_message.setTextColor(color);
        dialogButton.setBackgroundColor(color);

        Glide.with(mContext)
                .load(drawable)//url
                .placeholder(R.drawable.noimageone)
                .into(img_preview);


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }
    private void selectImage(int whichimage) {
        this.whichimage=whichimage;
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(mContext);

                if (items[item].equals(getString(R.string.take_photo))) {
                    int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CAMERA},
                                MY_CAMERA_PERMISSION_CODE);
                    }
                    else{
                        isCameraPermissionGranted =true;
                         userChoosenTask =getString(R.string.take_photo);
                    if(result)
                        cameraIntent();
                    }




                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask =getString(R.string.choose_from_library);
                    if(result)
                        galleryIntent();

                } else if (items[item].equals(getString(R.string.cancel))){
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Bitmap bm_adhar,bm_pan,bm_drlicence,bm_bankpasbook,bm_cancelcheque,bm_rc,bm_profileimage;


    private void enableEditFunctionality() {
        editview=true;
        tv_edit.setVisibility(View.INVISIBLE);
        tv_heading.setText("Edit Profile");
        enable(et_firstname,1);
        enable(et_vechicalnumber,1);
        enable(et_lastnamename,1);
        enable(et_drlicencenumber,1);
        // enable(et_mobilenumber,1);
        //  enable(et_emailid,1);
        enable(et_address,1);
        enable(et_adharnumber,1);
        enable(et_pannumber,1);
    }

    public void enable(EditText editText,int position){
        if (position==0){
            editText.setClickable(false);
            editText.setEnabled(false);
        }else {
            editText.setClickable(true);
            editText.setEnabled(true);
        }

    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    /* @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        }*/
    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
        setimage(whichimage,bm);
    }
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //fixOrientation(thumbnail);

        // profileImage_edit.setImageBitmap(thumbnail);

        setimage(whichimage,thumbnail);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);


    }
    public void fixOrientation(Bitmap bitmap) {
        if (bitmap.getWidth() > bitmap.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(270);
            bitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            profileImage_edit.setImageBitmap(bitmap);
            img_profile.setImageBitmap(bitmap);
        }
    }

    public static Bitmap getRotateImage(String photoPath, Bitmap bitmap) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }

        return rotatedBitmap;

    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void fetchUserDetails() {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PROFILE_READ, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    Log.d("VolleyResponse", "PROFILE_READ: " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    System.out.println("aaaaaa  responce  "+jsonObject);
                    profileObject =new Profile();
                    profileObject.setId(jsonObjectData.getString("id"));
                    profileObject.setUnique_id(jsonObjectData.getString("unique_id"));
                    profileObject.setFirst_name(jsonObjectData.getString("first_name"));
                    profileObject.setLast_name(jsonObjectData.getString("last_name"));
                    profileObject.setEmail(jsonObjectData.getString("email"));
                    profileObject.setPhone(jsonObjectData.getString("phone"));
                    profileObject.setWallet(jsonObjectData.getString("wallet"));
                    profileObject.setImage(jsonObjectData.getString("image"));


                    JSONObject adhardoc = jsonObjectData.getJSONObject("aadhar");
                    JSONObject pandoc = jsonObjectData.getJSONObject("pan_card");
                    JSONObject rcdoc = jsonObjectData.getJSONObject("rc");
                    JSONObject liecencedoc = jsonObjectData.getJSONObject("driving_licence");
                    JSONObject passbookdoc = jsonObjectData.getJSONObject("pass_book");
                    JSONObject cancelcheckdoc = jsonObjectData.getJSONObject("cancel_cheque");



                    profileObject.setAadharNumberStatus(adhardoc.getString("status"));
                    profileObject.setPanNumberStatus(pandoc.getString("status"));
                    profileObject.setRcNumberStatus(rcdoc.getString("status"));
                    profileObject.setDrivingLicenseNumberStatus(liecencedoc.getString("status"));
                    profileObject.setBankPassbookStatus(passbookdoc.getString("status"));
                    profileObject.setCancelChequeStatus(cancelcheckdoc.getString("status"));

                    profileObject.setAadharUploadMessage(adhardoc.getString("message"));
                    profileObject.setPanUploadMessage(pandoc.getString("message"));
                    profileObject.setRcUploadMessage(rcdoc.getString("message"));
                    profileObject.setDrivingLicenseUploadMessage(liecencedoc.getString("message"));
                    profileObject.setBankPassbookUploadMessage(passbookdoc.getString("message"));
                    profileObject.setCancelChequeUploadMessage(cancelcheckdoc.getString("message"));

                    profileObject.setAadharNumber(jsonObjectData.getString("aadhar_number"));
                    profileObject.setPanNumber(jsonObjectData.getString("pan_card_number"));
                    profileObject.setRcNumber(jsonObjectData.getString("vehicle_number"));
                    profileObject.setDrivingLicenseNumber(jsonObjectData.getString("driving_license_number"));

                    listDocs.get(0).setFrontImage(jsonObjectData.getJSONObject("aadhar_front").getString("image"));
                    listDocs.get(0).setBackImage(jsonObjectData.getJSONObject("aadhar_back").getString("image"));
                    listDocs.get(1).setFrontImage(jsonObjectData.getJSONObject("pan_card_front").getString("image"));
                    listDocs.get(1).setBackImage(jsonObjectData.getJSONObject("pan_card_back").getString("image"));
                    listDocs.get(2).setFrontImage(jsonObjectData.getJSONObject("driving_licence_front").getString("image"));
                    listDocs.get(2).setBackImage(jsonObjectData.getJSONObject("driving_licence_back").getString("image"));
                    listDocs.get(3).setFrontImage(jsonObjectData.getJSONObject("rc_front").getString("image"));
                    listDocs.get(3).setBackImage(jsonObjectData.getJSONObject("rc_back").getString("image"));

                    if(jsonObjectData.has("vehicle_insurance_number"))
                        et_insurance_number.setText(jsonObjectData.getString("vehicle_insurance_number"));
                    if(jsonObjectData.has("permanent_address"))
                        et_address.setText(jsonObjectData.getString("permanent_address"));
                    if(jsonObjectData.has("vehicle_type_id")) {
                        selectedVehicle = jsonObjectData.getString("vehicle_type_id");
                        //vehicle_type_id
                        if (vehicleTypes != null && vehicleTypes.size() > 0) {
                            for (int i = 0; i < vehicleTypes.size(); i++) {
                                if (vehicleTypes.get(i).getId().equals(selectedVehicle)) {
                                    if (vehicles.size() > i + 1) {
                                        spinner_vehicle.setSelection(i + 1);
                                    }
                                    return;
                                }
                            }
                        }
                    }

                    refresh(listDocs);

                    if(jsonObjectData.has("pincode"))
                        et_pincode.setText(jsonObjectData.getString("pincode"));
                    if(jsonObjectData.has("state"))
                        stateid=jsonObjectData.getString("state");
                    if(jsonObjectData.has("district"))
                        districtid=jsonObjectData.getString("district");
                    if(jsonObjectData.has("constituency"))
                        constitueid=jsonObjectData.getString("constituency");

                    getStates();
                    getVehicleTypes();
                    if(jsonObjectData.has("vehicle_front")) {
                        string_vehilce_front=jsonObjectData.getJSONObject("vehicle_front").getString("image");
                        Glide.with(mContext)
                                .load(string_vehilce_front)
                                .placeholder(R.drawable.noimageone)
                                .into(img_vehilce_front);
                    }
                    if(jsonObjectData.has("vehicle_back")) {
                        string_vehilce_back=jsonObjectData.getJSONObject("vehicle_back").getString("image");
                        Glide.with(mContext)
                                .load(string_vehilce_back)
                                .placeholder(R.drawable.noimageone)
                                .into(img_vehilce_back);
                    }
                    if(jsonObjectData.has("vehicle_insurance_front_")) {
                        string_insurance_front=jsonObjectData.getJSONObject("vehicle_insurance_front_").getString("image");
                        Glide.with(mContext)
                                .load(string_insurance_front)
                                .placeholder(R.drawable.noimageone)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                .into(img_insurance_front);
                    }

                   /* profileObject.setAadharNumberImage(adhardoc.getString("image"));
                    profileObject.setPanNumberImage(pandoc.getString("image"));
                    profileObject.setRcNumberImage(rcdoc.getString("image"));
                    profileObject.setDrivingLicenseImage(liecencedoc.getString("image"));*/
                    profileObject.setBankPassbookImage(passbookdoc.getString("image"));
                    profileObject.setCancelChequeImage(cancelcheckdoc.getString("image"));


                    /*profileObject.setAadharNumberImage(jsonObjectData.getString("aadhar_card_image"));
                    profileObject.setPanNumberImage(jsonObjectData.getString("pan_card_image"));
                    profileObject.setRcNumberImage(jsonObjectData.getString("rc_image"));
                    profileObject.setDrivingLicenseImage(jsonObjectData.getString("driving_license_image"));*/




                    /*
                    //testing

                    profileObject.setAadharNumberImage("https://upload.wikimedia.org/wikipedia/commons/3/3f/A_sample_of_Aadhaar_card.jpg");
                    profileObject.setPanNumberImage("https://5.imimg.com/data5/TP/US/MU/SELLER-51778781/pan-card-500x500.jpg");
                    profileObject.setDrivingLicenseImage("https://uploads.carandbike.com/used/39961/large/mahindra-xuv500-rear-view.jpg");
                    profileObject.setRcNumberImage("https://i.pinimg.com/originals/f8/97/84/f89784e25a22ae76455edfa0c09f7a84.jpg");

                    //profileObject.setRcNumberImage("https://uploads.carandbike.com/used/39961/large/mahindra-xuv500-rear-view.jpg");
                    //profileObject.setDrivingLicenseImage("https://i.pinimg.com/originals/f8/97/84/f89784e25a22ae76455edfa0c09f7a84.jpg");


                    profileObject.setAadharNumber("858596963252");
                    profileObject.setPanNumber("AQDLBD1253");
                    profileObject.setRcNumber("1234578963");
                    profileObject.setDrivingLicenseNumber("TS12EJ0626");

                    profileObject.setAadharNumberStatus(1);
                    profileObject.setPanNumberStatus(1);
                    //testing done
                    */

                    tv_userid.setText(profileObject.getUnique_id());
                    et_firstname.setText(profileObject.getFirst_name());
                    et_lastnamename.setText(profileObject.getLast_name());
                    et_emailid.setText(profileObject.getEmail());
                    et_mobilenumber.setText(profileObject.getPhone());
                    et_adharnumber.setText(profileObject.getAadharNumber());
                    et_pannumber.setText(profileObject.getPanNumber());
                    et_vechicalnumber.setText(profileObject.getRcNumber());
                    et_drlicencenumber.setText(profileObject.getDrivingLicenseNumber());

                    String bankPassbookImage= profileObject.getBankPassbookImage();
                    String cancelChequeImage= profileObject.getCancelChequeImage();

                   /* SetStatusImage(profileObject.getAadharNumberStatus(),img_adhar_status);
                    SetStatusImage(profileObject.getPanNumberStatus(),img_pancard_status);
                    SetStatusImage(profileObject.getRcNumberStatus(),img_rc_status);
                    SetStatusImage(profileObject.getDrivingLicenseNumberStatus(),img_drivinglicence_status);
                    SetStatusImage(profileObject.getCancelChequeStatus(),img_cancel_cheque_status);
                    SetStatusImage(profileObject.getBankPassbookStatus(),img_bank_passbook_status);*/

                    Glide.with(mContext)
                            .load(profileObject.getImage())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.ui_profile_pic)//
                            .into(img_profile);//profileImage_edit


                    Glide.with(mContext)
                            .load(bankPassbookImage)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.noimageone)
                            .into(img_bankpassbook);
                    Glide.with(mContext)
                            .load(cancelChequeImage)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.noimageone)
                            .into(img_canclecheque);




                } catch (JSONException e) {
                    System.out.println("aaaaaaaa catch  "+e.getMessage());
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                }
                finally {
                    LoadingDialog.dialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
           /* @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return super.getBody();
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                //map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMDA4NCIsInVzZXJkZXRhaWwiOnsiaWQiOiIxMDAwODQiLCJpcF9hZGRyZXNzIjoiMjQwOTo0MDcwOjIxOWE6YTJjNDo6OGIwOmU4YTUiLCJ1c2VybmFtZSI6Imd1ampldGlzaHJhdmFuQGdtYWlsLmNvbSIsInVuaXF1ZV9pZCI6Ik5DVTAyMTEiLCJwYXNzd29yZCI6IiQyeSQwOCQ2b21iQXViMTFhTU9leFRCdUF2SDBPTVJsbFlTZ3FGa0JcL2luMzVQXC9USE94ZlwvUnFGNGhKLiIsInNhbHQiOm51bGwsImVtYWlsIjoiZ3VqamV0aXNocmF2YW5AZ21haWwuY29tIiwid2FsbGV0IjoiMC4wMCIsImFjdGl2YXRpb25fY29kZSI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX2NvZGUiOm51bGwsImZvcmdvdHRlbl9wYXNzd29yZF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjpudWxsLCJjcmVhdGVkX29uIjoiMTU5MDEyMTMxNSIsImxhc3RfbG9naW4iOm51bGwsImFjdGl2ZSI6IjEiLCJsaXN0X2lkIjoiMCIsImZpcnN0X25hbWUiOiJHdWpqZXRpIFNocmF2YW5rdW1hciIsImxhc3RfbmFtZSI6bnVsbCwiY29tcGFueSI6bnVsbCwicGhvbmUiOiIiLCJjcmVhdGVkX3VzZXJfaWQiOm51bGwsInVwZGF0ZWRfdXNlcl9pZCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMjAtMDUtMjIgMDQ6MjE6NTUiLCJ1cGRhdGVkX2F0IjpudWxsLCJkZWxldGVkX2F0IjpudWxsLCJzdGF0dXMiOiIxIn0sInRpbWUiOjE1OTAxMjEzNjB9.gm-lTQiaLcLLYu4KIpjMorFcayjO77IZFulCRlwYlTk");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void refresh(ArrayList<DocField> listDocs) {
        setImage(img_front_aadhar,listDocs.get(0).getFrontImage());
        setImage(img_back_aadhar,listDocs.get(0).getBackImage());
        setImage(img_front_pan,listDocs.get(1).getFrontImage());
        setImage(img_back_pan,listDocs.get(1).getBackImage());
        setImage(img_front_vechicalnumber,listDocs.get(2).getFrontImage());
        setImage(img_back_vechicalnumber,listDocs.get(2).getBackImage());
        setImage(img_front_rc,listDocs.get(3).getFrontImage());
        setImage(img_back_rc,listDocs.get(3).getBackImage());
    }

    private void setImage(ImageView imgWidget, String imgData) {
        if (imgData != null && !imgData.isEmpty()) {
            if (!imgData.startsWith("http")) {
                System.out.println("aaaaaaaaa imgdata iff "+imgData);
                try {
                    byte[] decodedString = Base64.decode(imgData, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                  //  imgWidget.setImageBitmap(decodedByte);
                    Glide.with(mContext)
                            .load(decodedByte)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.noimageone)//
                            .into(imgWidget);
                } catch (Exception ex) {

                }
            } else {
                System.out.println("aaaaaaaaa imgdata  "+imgData);
                Glide.with(mContext)
                        .load(imgData)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.noimageone)//
                        .into(imgWidget);
            }
        }
        else
        {
            Glide.with(mContext)
                    .load(R.drawable.noimageone)//
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imgWidget);
        }
    }

    private void SetStatusImage(Integer status, ImageView img) {
        if(status == 1)
        {
            //success
            img.setImageResource(R.drawable.ic_upload_success);
        }
        else
            img.setImageResource(R.drawable.ic_cancel_24px);
    }

    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

       /* try {
            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }*/

        if (isContinue) {
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        } else {

            mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
                if (location != null) {
                    wayLatitude = location.getLatitude();
                    wayLongitude = location.getLongitude();
                    List<Address> addresses  = null;
                    try {
                        addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                        String address = addresses.get(0).getAddressLine(0);
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String zip = addresses.get(0).getPostalCode();
                        String country = addresses.get(0).getCountryName();
                        System.out.println("aaaaaaa address "+address);
                        tv_address.setText(address);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                else {
                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });
        }
    }


    @Override
    public void onLocationChanged(@NonNull Location location) {
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            System.out.println("aaaaaaaaa  address 1 "+address);
            tv_address.setText(address);


        } catch (Exception e) {

        }
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    public boolean validatenAADHAR(String aadhar){
        matcher = patternAADHAR.matcher(aadhar);
        return matcher.matches();
    }
    public boolean validatePAN(String pan){
        matcher = patternPAN.matcher(pan);
        return matcher.matches();
    }
    public boolean validateVECHIClE(String vechicle){
        matcher = patternVECHICAL.matcher(vechicle);
        return matcher.matches();
    }
    public boolean validateDRIVINGLICENCE(String drivinglicence){
        matcher = patternDRIVINGLICENCE.matcher(drivinglicence);
        return matcher.matches();
    }


    private void setStaticFields() {
        listDocs =new ArrayList<>();

        DocField filed=new DocField();
        filed.setHint(getString(R.string.aadhar_details));
        filed.setImgTitle(getString(R.string.aadhar_images));
        filed.setDigits("0123456789");
        filed.setInputType(InputType.TYPE_CLASS_NUMBER);
        filed.setMaxLength(12);
        filed.setImageIndex(101);//front-101,back -102
        filed.setPreviewImage(R.drawable.aadhar_refernce);
        listDocs.add(filed);

        filed=new DocField();
        filed.setHint(getString(R.string.pan_details));
        filed.setImgTitle(getString(R.string.pan_images));
        filed.setDigits("0123456789QWERTYUIOPLKJHGFDSAZXZCVBNM");
        filed.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        filed.setMaxLength(10);
        filed.setImageIndex(103);
        filed.setPreviewImage(R.drawable.pan_reference);
        listDocs.add(filed);

        filed=new DocField();
        filed.setHint(getString(R.string.driving_license_details));
        filed.setImgTitle(getString(R.string.driving_license_images));
        filed.setDigits("0123456789QWERTYUIOPLKJHGFDSAZXZCVBNM");
        filed.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        filed.setMaxLength(16);
        filed.setImageIndex(105);
        filed.setPreviewImage(R.drawable.licence_reference);
        listDocs.add(filed);

        filed=new DocField();
        filed.setHint(getString(R.string.vehicle_details));
        filed.setImgTitle(getString(R.string.rc_images));
        filed.setDigits("0123456789QWERTYUIOPLKJHGFDSAZXZCVBNM");
        filed.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        filed.setMaxLength(10);
        filed.setImageIndex(107);
        filed.setPreviewImage(R.drawable.vehicle_rc_reference);
        listDocs.add(filed);

       // docDetailAdapter = new DocDetailAdapter(mContext, listDocs,this);
        //recyclerview_docs.setAdapter(docDetailAdapter);
    }



    ArrayList<VehicleType> vehicleTypes;
    private ArrayList<States> statelist;
    private ArrayList<Districts> districtlist;
    private ArrayList<Constiuencies> constiencieslist;
    private String stateid="",districtid="",constitueid="";//test
    Spinner spinner_state,spinner_distict,spinner_constitunesy,spinner_vehicle;
    String selectedVehicle="";
    private void designAddressFields(View view) {
        statelist = new ArrayList<States>();
        districtlist = new ArrayList<Districts>();
        constiencieslist = new ArrayList<Constiuencies>();
        spinner_state = view.findViewById(R.id.spinner_state);
        spinner_distict = view.findViewById(R.id.spinner_distict);
        spinner_constitunesy = view.findViewById(R.id.spinner_constitunesy);
        spinner_vehicle = view.findViewById(R.id.spinner_vehicle);
        spinner_vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statelist.size() == 0) {

                } else {
                    if (position != 0) {
                       selectedVehicle=vehicleTypes.get(position-1).getId();
                    } else {
                        selectedVehicle="";
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statelist.size() == 0) {
                    stateid="";
                } else {
                    if (position != 0) {
                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        stateid = statelist.get(position - 1).getId();
                        districtlist.clear();
                        getDistricts(statelist.get(position - 1).getId());
                        // getDistricts(statelist.get(position-1).getStateID());
                    } else {
                        stateid="";
                        districtid="";
                        constitueid="";
                        spinner_distict.setEnabled(false);
                        spinner_distict.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        ArrayList<String> selectlist = new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(mContext,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_distict.setAdapter(ad);
                        spinner_constitunesy.setAdapter(ad);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_distict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtlist.size() == 0) {
                    // Toast.makeText(mContext, "Please Select District", Toast.LENGTH_SHORT).show();
                    districtid="";
                } else {
                    if (position != 0) {

                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setClickable(true);
                        spinner_constitunesy.setEnabled(true);
                        districtid = districtlist.get(position - 1).getId();
                        constiencieslist.clear();
                        // getConstituencis(districtlist.get(position-1).getDistrictID());
                        getConstituencis(stateid, districtlist.get(position - 1).getId());
                    } else {
                        districtid="";
                        constitueid="";
                        spinner_constitunesy.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        ArrayList<String> selectlist = new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(mContext,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner_constitunesy.setAdapter(ad);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_constitunesy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtid.isEmpty()) {
                    //  Toast.makeText(mContext, "Please select District", Toast.LENGTH_SHORT).show();
                    constitueid="";
                } else {
                    try {
                        if (position != 0)
                            constitueid = constiencieslist.get(position - 1).getId();
                        else
                            constitueid="";
                    } catch (IndexOutOfBoundsException e) {
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ////getStates();
        //getVehicleTypes();
    }

    private void getVehicleTypes() {
        vehicleTypes=new ArrayList<>();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VEHICLE_TYPES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            System.out.println("VEHICLE_TYPES   sucess " + response.toString());
                            if (status && http_code==200){
                                Integer index =-1;
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    VehicleType vehicleType=new VehicleType();
                                    vehicleType.setId(jsonObject1.getString("id"));
                                    vehicleType.setName(jsonObject1.getString("name"));

                                    if(vehicleType.getId().equals(selectedVehicle))
                                    {
                                        index=i;
                                    }

                                    vehicleTypes.add(vehicleType);
                                }
                                vehicles=new ArrayList<String>();
                                vehicles.add("Select");
                                for (int k=0;k<vehicleTypes.size();k++){
                                    vehicles.add(vehicleTypes.get(k).getName());
                                }

                                ArrayAdapter ad = new ArrayAdapter(mContext,
                                        android.R.layout.simple_spinner_item, vehicles);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_vehicle.setAdapter(ad);

                                if(index !=-1) {
                                    spinner_vehicle.setSelection(index + 1);
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("VEHICLE_TYPES  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getStates() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    States states=new States();
                                    states.setId(jsonObject1.getString("id"));
                                    states.setName(jsonObject1.getString("name"));
                                    states.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    states.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    states.setCreated_at(jsonObject1.getString("created_at"));
                                    states.setUpdated_at(jsonObject1.getString("updated_at"));
                                    states.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    states.setStatus(jsonObject1.getString("status"));

                                    statelist.add(states);
                                }
                                ArrayList<String> statenames=new ArrayList<String>();
                                statenames.add("Select");
                                Integer selIndex=-1;
                                for (int k=0;k<statelist.size();k++){
                                    statenames.add(statelist.get(k).getName());
                                    if (statelist.get(k).getId().equals(stateid)) {
                                        selIndex=k+1;
                                    }
                                    System.out.println("aaaaaa state "+statelist.get(k).getId()+"  "+statelist.get(k).getName());
                                   /* if (isupdate==1){
                                        if (saveAddress.getState_id().equals(statelist.get(k).getId())){
                                            stateid=k;
                                        }
                                    }*/
                                }

                                ArrayAdapter ad = new ArrayAdapter(mContext,
                                        android.R.layout.simple_spinner_item, statenames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_state.setAdapter(ad);

                                try{
                                    if (selIndex != -1) {
                                        spinner_state.setSelection(selIndex);
                                        //getDistricts(stateid);
                                    }
                                }catch (NullPointerException e){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getDistricts(String stateID) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States+stateID,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("districts");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                districtlist.clear();
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);
                                    Districts districts=new Districts();
                                    districts.setId(jsonObject.getString("id"));
                                    districts.setName(jsonObject.getString("name"));
                                    districts.setState_id(jsonObject.getString("state_id"));
                                    districtlist.add(districts);
                                }
                                ArrayList<String> districtnames=new ArrayList<String>();
                                districtnames.add("Select");
                                System.out.println("aaaaaaa districtlist size "+districtlist.size());
                                Integer selIndex=-1;
                                for (int k=0;k<districtlist.size();k++){
                                    districtnames.add(districtlist.get(k).getName());
                                    if (districtlist.get(k).getId().equals(districtid)) {
                                        selIndex=k+1;
                                    }
                                    /*if (isupdate==1){
                                        if (saveAddress.getDistrict_id().equals(districtlist.get(k).getId())){
                                            distictid=k;
                                        }
                                    }*/
                                }
                                ArrayAdapter ad = new ArrayAdapter(mContext,
                                        android.R.layout.simple_spinner_item, districtnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_distict.setAdapter(ad);
                                try{
                                    if (selIndex!=-1) {
                                        spinner_distict.setSelection(selIndex);
                                        //getConstituencis(stateID,districtid);
                                    }
                                }catch (NullPointerException e){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getConstituencis(String stateID,String districtid) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States+stateID+"/"+districtid,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        System.out.println("aaaaaaa response const  "+response.toString());
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("constituenceis");

                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                constiencieslist.clear();
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);

                                    Constiuencies constiuencies=new Constiuencies();
                                    constiuencies.setId(jsonObject.getString("id"));
                                    constiuencies.setDistrict_id(jsonObject.getString("district_id"));
                                    constiuencies.setName(jsonObject.getString("name"));

                                    constiencieslist.add(constiuencies);
                                }
                                ArrayList<String> constnames=new ArrayList<String>();
                                constnames.add("Select");
                                int conid=0;
                                for (int k=0;k<constiencieslist.size();k++) {
                                    constnames.add(constiencieslist.get(k).getName());
                                    if (constiencieslist.get(k).getId().equals(constitueid)) {
                                        conid = k+1;
                                    }
                                }
                                ArrayAdapter ad = new ArrayAdapter(mContext,
                                        android.R.layout.simple_spinner_item, constnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_constitunesy.setAdapter(ad);
                                try{
                                    if (conid != -1) {
                                        spinner_constitunesy.setSelection(conid);
                                    }
                                }catch (NullPointerException e){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    //Images

    String string_vehilce_front,string_vehilce_back,string_insurance_front;
    public void setimage(int whichimage,Bitmap bitmap){
        String convString = convert(bitmap);
        if(whichimage> 100)
        {
            if(whichimage>=300)
            {
                switch (whichimage) {
                    case 300:
                        setImage(img_vehilce_front,convString);
                       // img_vehilce_front.setImageBitmap(bitmap);
                        string_vehilce_front = convString;
                        break;
                    case 301:
                        string_vehilce_back = convString;
                        setImage(img_vehilce_back,convString);
                       // img_vehilce_back.setImageBitmap(bitmap);
                        break;
                    case 302:
                        setImage(img_insurance_front,convString);
                       // img_insurance_front.setImageBitmap(bitmap);
                        string_insurance_front = convString;
                        break;
                }
            }
            else {
                switch (whichimage) {
                    case 101:
                        setImage(img_front_aadhar,convString);
                        listDocs.get(0).setFrontImage(convString);
                        break;//aadhar front
                    case 102:
                        setImage(img_back_aadhar,convString);
                        listDocs.get(0).setBackImage(convString);
                        break; //aadhar back
                    case 103:
                        setImage(img_front_pan,convString);
                        listDocs.get(1).setFrontImage(convString);
                        break;
                    case 104:
                        setImage(img_back_pan,convString);
                        listDocs.get(1).setBackImage(convString);
                        break;
                    case 105:
                        setImage(img_front_vechicalnumber,convString);
                        listDocs.get(2).setFrontImage(convString);
                        break;//dL
                    case 106:
                        setImage(img_back_vechicalnumber,convString);
                        listDocs.get(2).setBackImage(convString);
                        break;
                    case 107:
                        setImage(img_front_rc,convString);
                        listDocs.get(3).setFrontImage(convString);
                        break;//rc
                    case 108:
                        setImage(img_back_rc,convString);
                        listDocs.get(3).setBackImage(convString);
                        break;
                }
            }
        }
        else {

            switch (whichimage){
                case 5:
                    Glide.with(mContext)
                            .load(bitmap)
                            .placeholder(R.drawable.noimageone)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(img_bankpassbook);
                    //img_bankpassbook.setImageBitmap(bitmap);
                    bm_bankpasbook=bitmap;
                    bankpas64=convert(bitmap);
                    img_bank_passbook_status.setVisibility(View.GONE);
                    profileObject.setBankPassbookStatus(""+2);
                    break;
                case 6:

                    Glide.with(mContext)
                            .load(bitmap)
                            .placeholder(R.drawable.noimageone)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(img_canclecheque);


                    //img_canclecheque.setImageBitmap(bitmap);
                    bm_cancelcheque=bitmap;
                    canclecheque64=convert(bitmap);
                    img_cancel_cheque_status.setVisibility(View.GONE);
                    profileObject.setCancelChequeStatus(""+2);
                    break;
                case 7:
                    Glide.with(mContext)
                            .load(bitmap)
                            .placeholder(R.drawable.noimageone)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(img_profile);
                    //img_profile.setImageBitmap(bitmap);
                    bm_profileimage=bitmap;
                    profile64=convert(bitmap);
                    break;
            }
        }
    }

    @Override
    public void uploadImage(Integer type) {
        selectImage(type);
    }

    @Override
    public void previewImage(Integer image) {
        //image preivew
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.image_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        TextView txt_upload_message = (TextView) dialog.findViewById(R.id.txt_upload_message);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        closeButton.setVisibility(View.GONE);//looking not good
        ImageView img_preview = (ImageView) dialog.findViewById(R.id.img_preview);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Glide.with(this)
                .load(image)
                .placeholder(R.drawable.noimageone)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(img_preview);


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }
}
