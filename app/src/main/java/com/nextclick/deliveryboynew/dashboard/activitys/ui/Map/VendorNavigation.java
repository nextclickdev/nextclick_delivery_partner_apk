package com.nextclick.deliveryboynew.dashboard.activitys.ui.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Orders.OrderDetailsActivity;
import com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationUtil;
import com.nextclick.deliveryboynew.utils.Utility;

import java.util.ArrayList;

public class VendorNavigation extends AppCompatActivity implements OnMapReadyCallback, MyLocationListener, MapParser {

    private GoogleMap mMap;

    Polyline mPolyline;
    MapRouteNavigationHelper mapRouteNavigationHelper;
    VendorDetails vendorDetail;
    LatLng originPoint;
    Boolean isLocationUpdatesStarted=false;
    Marker prevMarker =null;

    TextView tv_arrival_Time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_navigation);

        Button btn_start_trip =findViewById(R.id.btn_start_trip);
        if(getIntent().getExtras() != null) {
            vendorDetail = (VendorDetails) getIntent().getSerializableExtra("vendorDetail");
            originPoint = new LatLng(vendorDetail.getLatitude(), vendorDetail.getLongitude());
            //originPoint= new LatLng(vendorDetail.getLongitude(), vendorDetail.getLatitude());

            String DeliveryType = getIntent().getStringExtra("DeliveryType");
            if(DeliveryType!=null && DeliveryType.equals(OrderDetailsActivity.TRACKING_DELIVERY))
            {
                btn_start_trip.setText("Start to Delivery Point");
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tv_arrival_Time=findViewById(R.id.tv_arrival_Time);
        btn_start_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTrip();
            }
        });
    }

    private void startTrip() {
        if(!isLocationUpdatesStarted) {
            isLocationUpdatesStarted = true;
            registerLocationUpdates();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        addMarkerObject(originPoint,vendorDetail.getName(),R.drawable.ic_baseline_location_on_24);
        getCurrentPosition();
    }

    private void addPolyLine(LatLng point1, LatLng point2) {

        PolylineOptions lineOptions = new PolylineOptions();
        ArrayList points = new ArrayList();
        points.add(point1);
        points.add(point2);

        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(Color.RED);
        lineOptions.geodesic(true);
        mMap.addPolyline(lineOptions);
    }


    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);
        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isLocationUpdatesStarted)
            MyLocationUtil.removeLocationUpdates((com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationListener) this);
        isLocationUpdatesStarted=false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mMap != null && !isLocationUpdatesStarted)
            registerLocationUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isLocationUpdatesStarted)
            MyLocationUtil.removeLocationUpdates((com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationListener) this);
        isLocationUpdatesStarted=false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isLocationUpdatesStarted)
            MyLocationUtil.removeLocationUpdates((com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationListener) this);
        isLocationUpdatesStarted=false;
    }

    private void registerLocationUpdates() {
        MyLocationUtil.SetContext(this);
        MyLocationUtil.startLocationUpdates((com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationListener) this);
    }

    private void getCurrentPosition() {
        MyLocationUtil.SetContext(this);
        MyLocationUtil.getCurrentPosition((com.nextclick.deliveryboynew.utils.LocationUtil.MyLocationListener) this);
    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null) {
            drawRoute(location);
        }
    }
    @Override
    public void onLocationUpdated(Location location) {
        if (location != null) {
            drawRoute(location);
        }
    }

    private void drawRoute(Location location) {
        LatLng myLatlng = new LatLng(location.getLatitude(), location.getLongitude());
        if (prevMarker != null)
            prevMarker.setVisible(false);
        prevMarker = addMarkerObject(myLatlng, "My Current Location",R.drawable.motor_pin);
        if (mapRouteNavigationHelper == null)
            mapRouteNavigationHelper = new MapRouteNavigationHelper(this, this);
        mapRouteNavigationHelper.drawRoute(myLatlng,originPoint);
    }
    @Override
    public void onLocationFailed(int code, String message) {

    }

    @Override
    public void onPolylineOptionsUpdated(PolylineOptions lineOptions) {
        tv_arrival_Time.setText(Utility.ArraivalTime);
        if(mPolyline != null){
            mPolyline.remove();
        }
        mPolyline = mMap.addPolyline(lineOptions);
    }

    private Marker addMarkerObject(LatLng latLng, String Address, int id) {

        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(Address)
                .icon(BitmapFromVector(getApplicationContext(), id)));


        CameraPosition position = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15.0f)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
       /*
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
        }*/
        return marker;
    }
}