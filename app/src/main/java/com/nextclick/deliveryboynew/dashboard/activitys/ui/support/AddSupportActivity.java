package com.nextclick.deliveryboynew.dashboard.activitys.ui.support;

import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.UiMsgs.setEditTextErrorMethod;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddSupportActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private Context mContext;
    private PreferenceManager preferenceManager;
    Spinner spinner_support_type;

    private List<SupportTypeObject> supportedFeedbackTypes;


    TextInputEditText editText_subject, et_message;
    private String selectedSuportType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_support);
        mContext = this;
        preferenceManager = new PreferenceManager(this);

        spinner_support_type = (Spinner) findViewById(R.id.spinner_support_type);
        spinner_support_type.setOnItemSelectedListener(this);

        ImageView back_image = findViewById(R.id.back_image);
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to add new support request");
        }

        //SpinnerSupportTypeAdapter adapter=new SpinnerSupportTypeAdapter(getActivity(), supportedFeedbackTypes);
        //spin.setAdapter(adapter);

        editText_subject = (TextInputEditText) findViewById(R.id.editText_subject);
        et_message = (TextInputEditText) findViewById(R.id.et_message);

        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    submitFeedback();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportTypes();
    }

    private void getSupportTypes() {
        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SUPPORT_QUERIES_TYPES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject SUBMIT_FEEDBACK " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                JSONArray dataArray = jsonObject.getJSONArray("data");
                                if (dataArray != null && dataArray.length() > 0) {

                                    supportedFeedbackTypes = new ArrayList<>();

                                    for (int i = 0; i < dataArray.length(); i++) {
                                        SupportTypeObject supportTypeObject = new SupportTypeObject();

                                        JSONObject supportTypeJsonObject = dataArray.getJSONObject(i);
                                        supportTypeObject.setID(supportTypeJsonObject.getString("id"));
                                        supportTypeObject.setTitle(supportTypeJsonObject.getString("title"));
                                        supportTypeObject.setDesc(supportTypeJsonObject.getString("desc"));
                                        supportTypeObject.setStatus(supportTypeJsonObject.getString("status"));

                                        supportedFeedbackTypes.add(supportTypeObject);
                                    }

                                    SpinnerSupportTypeAdapter adapter = new SpinnerSupportTypeAdapter(getApplicationContext(), supportedFeedbackTypes);
                                    spinner_support_type.setAdapter(adapter);
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private boolean isValid() {
        boolean valid = true;
        if (editText_subject.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(editText_subject, getString(R.string.empty));
            valid = false;
        } else if (et_message.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_message, getString(R.string.empty));
            valid = false;
        }
        return valid;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedSuportType = supportedFeedbackTypes.get(position).getID();
        //  Toast.makeText(getActivity(), supportedFeedbackTypes.get(position).getTitle(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void submitFeedback() {


        //general/api/support/support_queries/c
        /*
          {
    "request_type_id": "1",
    "mobile":"9823883224",
    "email":"andre@gmail.com",
    "subject":"hii thiks is bhagyeshwar",
    "message":"hello"
}
         */
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("request_type_id", selectedSuportType);
        dataMap.put("mobile", preferenceManager.getString("user_mobile"));
        dataMap.put("email", preferenceManager.getString("user_email"));
        dataMap.put("subject", editText_subject.getText().toString());
        dataMap.put("message", et_message.getText().toString());
        JSONObject json = new JSONObject(dataMap);
        System.out.println("aaaaaaa json SUBMIT_FEEDBACK " + json.toString());

        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SUBMIT_FEEDBACK,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject SUBMIT_FEEDBACK " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                invokeDialog(getString(R.string.support_created), true);

                                if (MyMixPanel.isMixPanelSupport) {
                                    MyMixPanel.logEvent("Delivery boy Successfully submitted the valuable feedback");
                                }
                            } else {
                                //payment failed
                                invokeDialog(getString(R.string.support_failed), false);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void invokeDialog(String message, Boolean isSuccess) {
        //You order status has been accepted successfully.
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_order_accept_success);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        TextView tv_success = (TextView) dialog.findViewById(R.id.tv_success);
        if (!isSuccess) {
            TextView tv_header = (TextView) dialog.findViewById(R.id.tv_success);
            tv_header.setText("Error");
        }
        tv_success.setText(message);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isSuccess) {
                    finish();
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isSuccess) {
                    finish();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}