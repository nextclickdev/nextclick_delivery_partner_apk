package com.nextclick.deliveryboynew.model;


import java.util.ArrayList;

public class ItemModel {

    String id,name,desc;
    ArrayList<ImagesModel> imagelist;
    VarientModel varientModel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<ImagesModel> getImagelist() {
        return imagelist;
    }

    public void setImagelist(ArrayList<ImagesModel> imagelist) {
        this.imagelist = imagelist;
    }

    public VarientModel getVarientModel() {
        return varientModel;
    }

    public void setVarientModel(VarientModel varientModel) {
        this.varientModel = varientModel;
    }
}
