package com.nextclick.deliveryboynew.model

/**
 * Created by Arun Vegyas on 25-06-2023.
 */
data class TicketModel(val status: Boolean,
			     val http_code: Int,
			     val message: String,
			     val data: List<Data>)
data class Data(
    val id: Int,
    val app_details_id: Int,
    val request_type: String,
    val title: String,
    val description: String,
    val severity: Int,
    val status: Int,
    val severity_text: String,
    val status_text: String,
    val created_user_id: Int,
    val updated_user_id: Int,
    val assigned_to: Int,
    val assigned_by: Int,
    val comment: String,
    val created_at: String,
    val updated_at: String,
    val assigned: Assigned
)

data class Assigned(
    val id: Int,
    val first_name: String,
    val last_name: String
)
