package com.nextclick.deliveryboynew.model;

import com.nextclick.deliveryboynew.dashboard.model.LocationObject;

public class AssignedUserAddress {

    String id,phone,email,name,landmark,address,location_id,vendor_user_id,unique_id;
    private LocationObject locationObject;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getVendorUserID() {
        return vendor_user_id;
    }

    public void setVendorUserID(String vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getUniqueID() {
        return unique_id;
    }

    public void setUniqueID(String unique_id) {
        this.location_id = location_id;
    }

    public void setLocation(LocationObject locationObject) {
        this.locationObject=locationObject;
    }
    public LocationObject getLocation()
    {
        return locationObject;
    }
}
