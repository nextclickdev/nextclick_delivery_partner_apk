package com.nextclick.deliveryboynew.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OrderDetailsmodel {

    String id,track_id,delivery_fee,total,used_wallet_amount,message,preparation_time,created_at,shipping_address_id,delivery_mode_id,
            created_user_id,order_status_id,payment_id,customer_phone_number;

    int order_confirmation_time;
    boolean isecomdetails;

    AssignedUserAddress shippingAddress;
    AssignedUserAddress vendorAddress;

    DeleveryModesmodel deleveryModesmodel;
    CustomerModel customerModel;
    OrderStatus orderStatus;
    PaymentModel paymentModel;


    Date orderCreatedDate;
    String orderDateInSimpleFormat;

    ArrayList<ProductDetailsModel> productlist;
    customerDetailsModel customerDetailsModel;

    public com.nextclick.deliveryboynew.model.customerDetailsModel getCustomerDetailsModel() {
        return customerDetailsModel;
    }

    public void setCustomerDetailsModel(com.nextclick.deliveryboynew.model.customerDetailsModel customerDetailsModel) {
        this.customerDetailsModel = customerDetailsModel;
    }

    public int getOrder_confirmation_time() {
        return order_confirmation_time;
    }

    public void setOrder_confirmation_time(int order_confirmation_time) {
        this.order_confirmation_time = order_confirmation_time;
    }

    public boolean isIsecomdetails() {
        return isecomdetails;
    }

    public void setIsecomdetails(boolean isecomdetails) {
        this.isecomdetails = isecomdetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(String delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUsed_wallet_amount() {
        return used_wallet_amount;
    }

    public void setUsed_wallet_amount(String used_wallet_amount) {
        this.used_wallet_amount = used_wallet_amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPreparation_time() {
        return preparation_time;
    }

    public void setPreparation_time(String preparation_time) {
        this.preparation_time = preparation_time;
    }

    public String getCreated_at() {
        return created_at;
    }
    public String getOrderDateInSimpleFormat() {
        if (orderDateInSimpleFormat == null && created_at!=null && !created_at.isEmpty()) {
            try {
                orderCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(created_at);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                orderDateInSimpleFormat = sdf.format(orderCreatedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return orderDateInSimpleFormat;
    }
    public Date getOrderCreatedDateValue() {
        if (orderCreatedDate == null && created_at != null && !created_at.isEmpty()) {
            try {
                orderCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(created_at);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                orderDateInSimpleFormat = sdf.format(orderCreatedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return orderCreatedDate;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getShipping_address_id() {
        return shipping_address_id;
    }

    public void setShipping_address_id(String shipping_address_id) {
        this.shipping_address_id = shipping_address_id;
    }

    public String getDelivery_mode_id() {
        return delivery_mode_id;
    }

    public void setDelivery_mode_id(String delivery_mode_id) {
        this.delivery_mode_id = delivery_mode_id;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getOrder_status_id() {
        return order_status_id;
    }

    public void setOrder_status_id(String order_status_id) {
        this.order_status_id = order_status_id;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public AssignedUserAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AssignedUserAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public AssignedUserAddress getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(AssignedUserAddress vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public DeleveryModesmodel getDeleveryModesmodel() {
        return deleveryModesmodel;
    }

    public void setDeleveryModesmodel(DeleveryModesmodel deleveryModesmodel) {
        this.deleveryModesmodel = deleveryModesmodel;
    }

    public CustomerModel getCustomerModel() {
        return customerModel;
    }

    public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public PaymentModel getPaymentModel() {
        return paymentModel;
    }

    public void setPaymentModel(PaymentModel paymentModel) {
        this.paymentModel = paymentModel;
    }

    public ArrayList<ProductDetailsModel> getProductlist() {
        return productlist;
    }

    public void setProductlist(ArrayList<ProductDetailsModel> productlist) {
        this.productlist = productlist;
    }



    String DeliveryJobID;
    public void setDeliveryJobID(String title) { this.DeliveryJobID = title; }
    public String getDeliveryJobID() { return DeliveryJobID; }
    String JobID;
    public void setJobID(String title) { this.JobID = title; }
    public String getJobID() { return JobID; }

    String Status;
    public void setStatus(String title) { this.Status = title; }
    public String getStatus() { return Status; }


}
