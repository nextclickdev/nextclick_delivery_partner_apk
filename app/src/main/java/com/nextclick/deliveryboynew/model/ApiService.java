package com.nextclick.deliveryboynew.model;

import com.nextclick.crm.models.responseModels.BrandsModel;
import com.nextclick.crm.models.responseModels.CategoryModel;
import com.nextclick.crm.models.responseModels.MenuModel;

import io.reactivex.rxjava3.core.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Arun Vegyas on 25-06-2023.
 */
public interface ApiService {
    @POST("general/api/support/customer_support")
    Observable<CommonResponse> customerFormToServer(@Body RequestBody param);

    @POST("vendor/api/ecom/shop_by_category/r/")
    Observable<CategoryModel> getCategories(@Body RequestBody param);

    @POST("vendor/api/ecom/shop_by_category/r/{cat_id}")
    Observable<MenuModel> getMenus(@Path(value = "cat_id",encoded = true) String cat_id);

    @GET("/general/api/master/categories/{cat_id}")
    Observable<BrandsModel> getBrands(@Path(value = "cat_id",encoded = true) String cat_id);


//
//    @GET("general/api/support/customer_support_detail")
//    Observable<>
}
