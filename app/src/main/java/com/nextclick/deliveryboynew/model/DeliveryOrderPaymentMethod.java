package com.nextclick.deliveryboynew.model;

public class DeliveryOrderPaymentMethod {

    String ID;
    public void setID(String ID) { this.ID = ID; }
    public String getID() { return ID; }
    String Name;
    public void setName(String Name) { this.Name = Name; }
    public String getName() { return Name; }
    String Description;
    public void setDescription(String Description) { this.Description = Description; }
    public String getDescription() { return Description; }

}
