package com.nextclick.deliveryboynew.payment;

import static android.view.Gravity.CENTER;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.payment.model.PaymentModes;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PaymentModesActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    private Context mContext;
    RecyclerView recycle_paymentmodes;
    PaymentModesAdapter paymentModesAdapter;
    ArrayList<PaymentModes> paymentModesArrayList;
    //private CustomDialog customDialog;
    private PreferenceManager preferenceManager;
    private String totalamount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_modes);

        paymentModesArrayList=new ArrayList<>();


     //   getSupportActionBar().hide();
        mContext=PaymentModesActivity.this;
        preferenceManager=new PreferenceManager(mContext);
        recycle_paymentmodes=findViewById(R.id.recycle_paymentmodes);

        recycle_paymentmodes.setLayoutManager(new GridLayoutManager(mContext,1));
        paymentModesAdapter=new PaymentModesAdapter(mContext,paymentModesArrayList);
        recycle_paymentmodes.setAdapter(paymentModesAdapter);
        totalamount=getIntent().getStringExtra("amount");//notes
        ImageView back_image = findViewById(R.id.back_image);
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getPaymentModes();
    }


    private void getPaymentModes() {
        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.PAYMENTMODES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    PaymentModes paymentModes=new PaymentModes();
                                    paymentModes.setId(jsonObject1.getString("id"));
                                    paymentModes.setName(jsonObject1.getString("name"));
                                    paymentModes.setDescription(jsonObject1.getString("description"));
                                    paymentModes.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    paymentModes.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    paymentModes.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    paymentModes.setCreated_at(jsonObject1.getString("created_at"));
                                    paymentModes.setUpdated_at(jsonObject1.getString("updated_at"));
                                    paymentModes.setStatus(jsonObject1.getString("status"));


                                    paymentModesArrayList.add(paymentModes);
                                }
                                paymentModesAdapter.setrefresh(paymentModesArrayList);

                            }

                        } catch (JSONException e) {
                            // Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                        finally {
                            LoadingDialog.dialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                // Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void setdeleverymodechanged(PaymentModes paymentModes) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(paymentModes.getName());
        alertDialogBuilder
                .setMessage("Do You Want To Procced")
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (paymentModes.getId().equalsIgnoreCase("1")){
                            dialog.cancel();
                            sendPaymentstatus(""+1,"",""+totalamount,""+1,"");
                        }else {
                            dialog.cancel();
                            razorPayPayment(""+totalamount);
                        }
                    }

                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void razorPayPayment(String totatlAmount) {
        final Checkout co = new Checkout();
        co.setImage(R.drawable.nextclick_icon_receipt);
        Activity activity = this;
        try {
            JSONObject orderRequest = new JSONObject();
            // orderRequest.put("amount", totatlAmount+"00"); // amount in the smallest currency unit
            orderRequest.put("amount", "100"); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image", R.drawable.nextclick_logo_white);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            co.open((Activity) activity, orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        }


    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
       // View v = getWindow().getDecorView().getRootView();
        Toast.makeText(mContext, String.valueOf(R.string.payement_done), Toast.LENGTH_SHORT).show();
       // Toast.makeText(v,R.string.payement_done, Toast.LENGTH_SHORT).show();
        System.out.println("aaaaaaaaaa  data sucess  "+s+paymentData.toString());
        System.out.println("aaaaaaaaaa sucess  "+paymentData.getOrderId()+" "+paymentData.getPaymentId()+" "+paymentData.getSignature()+" "+
                paymentData.getUserContact()+"  "+paymentData.getData().toString()+" "+paymentData.getUserEmail());
        sendPaymentstatus(""+2,paymentData.getPaymentId(),""+totalamount,""+2,"");
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        try{
            System.out.println("aaaaaaaaaa  payment  error  "+paymentData.toString()+"  "+s);
        }catch (NullPointerException e){

        }
        System.out.println("aaaaaaaaaa  payment  error  "+s);
        // UImsgs.showToast(mContext, String.valueOf("Error " + "  " + i + " " + s));
        try {
            JSONObject jsonObject1=new JSONObject(s);
            JSONObject errorobj=jsonObject1.getJSONObject("error");
            String description=errorobj.getString("description");
            sendPaymentstatus(""+3,"1",""+totalamount,""+3,description);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void succesDialog(String message){
        ImageView success_gif;
        Button success_ok_btn;
        TextView tv_ordermessage;
        final Dialog dialog = new Dialog(mContext);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.payment_success_layout);
        dialog.setCancelable(false);
        window.setGravity(CENTER);

        success_ok_btn = dialog.findViewById(R.id.success_ok_btn);
        success_gif = dialog.findViewById(R.id.success_gif);
        tv_ordermessage = dialog.findViewById(R.id.tv_ordermessage);

        tv_ordermessage.setText(""+message);
        Glide.with(mContext)
                .load(R.drawable.ordsucess)
                .into(success_gif);

        success_ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
              finish();
            }
        });


        window.setGravity(CENTER);
        dialog.show();
    }

    public void sendPaymentstatus(String paymentid,String paymenttransactionid,String amount,String status,String mesage){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("payment_method_id", paymentid);
        uploadMap.put("payment_gw_txn_id", paymenttransactionid);
        uploadMap.put("amount", totalamount);
        uploadMap.put("status", status);
        uploadMap.put("message", mesage);

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaa json "+json.toString());

        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.PAYMENTSTATUS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {


                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            if (status){
                                finish();
                            }
                            else
                            {
                                //payment failed
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}