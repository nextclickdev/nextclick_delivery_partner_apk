package com.nextclick.deliveryboynew;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.nextclick.deliveryboynew.adapters.BannersAdapter;
import com.nextclick.deliveryboynew.newauthentication.UserSigninActivity;

public class InfoActivity extends AppCompatActivity {

    Context mContext;
    Button mFinishBtn,mSkipBtn;
    ImageButton mNextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
       // getSupportActionBar().hide();
        mContext = getApplicationContext();

        BannersAdapter adapter = new BannersAdapter(this, getSupportFragmentManager(), 3);
        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        mNextBtn =findViewById(R.id.intro_btn_next);
        mFinishBtn =findViewById(R.id.intro_btn_finish);
        mSkipBtn =findViewById(R.id.intro_btn_skip);
        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, UserSigninActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, UserSigninActivity.class);
                startActivity(intent);
                finish();

               /* final CharSequence[] items = { "English", "Telugu",
                        "Hindi" };

                android.app.AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setTitle("Choose Language!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (item==0) {
                             setLocale("en");
                            recreate();

                        } else if (item==1) {
                            setLocale("te");
                            recreate();

                        } else if (item==2) {
                            setLocale("hi");
                            recreate();
                        }
                    }
                });
                builder.show();*/
            }
        });
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() < 2)
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                mNextBtn.setVisibility(position == 2 ? View.GONE : View.VISIBLE);
                mFinishBtn.setVisibility(position == 2 ? View.VISIBLE : View.GONE);

               /* if(position== 2)
                {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mContext, LoginActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    }, 700);
                }*/
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /*private void setLocale(String lang)
    {
        Locale locale=new Locale(lang);
        locale.setDefault(locale);

        Configuration config= new Configuration();
        config.setLocale(locale);

        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("My_Lang",lang);
        editor.apply();
    }

    //load lang form shared prefrences
    private void loadLocale()
    {
        SharedPreferences pref=getSharedPreferences("Settings",MODE_PRIVATE);
        String lang= pref.getString("My_Lang","");
        setLocale(lang);
    }*/
}