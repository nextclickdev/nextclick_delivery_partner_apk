package com.nextclick.deliveryboynew;

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FirebaseMessageReceiver : FirebaseMessagingService() {

    // Override onNewToken to get new token
    override fun onNewToken(token: String) {
	  super.onNewToken(token)
    }

    // Override onMessageReceived() method to extract the
    // title and
    // body from the message passed in FCM
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

	  if (remoteMessage.getNotification() != null) {

		showNotification(
		    remoteMessage.getNotification()!!.getTitle()!!,
		    remoteMessage.getNotification()!!.getBody()!!
		)
	  }
    }


    // Method to display the notifications
    fun showNotification(
	  title: String,
	  message: String
    ) {
	  // Pass the intent to switch to the MainActivity
	  val intent = Intent(this, MainActivity::class.java)
	  // Assign channel ID
	  val channel_id = "notification_channel"

	  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
	  // Pass the intent to PendingIntent to start the
	  // next Activity
	  val pendingIntent = PendingIntent.getActivity(
		this, 0, intent,
		PendingIntent.FLAG_ONE_SHOT
	  )

	  // Create a Builder object using NotificationCompat
	  // class. This will allow control over all the flags
	  val builder: NotificationCompat.Builder = NotificationCompat.Builder(
		this,
		channel_id
	  )
		.setSmallIcon(R.drawable.ic_launcher_background)
		.setAutoCancel(true)
		.setVibrate(
		    longArrayOf(
			  1000, 1000, 1000,
			  1000, 1000
		    )
		)
		.setOnlyAlertOnce(true)
		.setContentIntent(pendingIntent)


	  val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
	  // Check if the Android Version is greater than Oreo
	  if (Build.VERSION.SDK_INT
		>= Build.VERSION_CODES.O
	  ) {
		val notificationChannel = NotificationChannel(
		    channel_id, "web_app",
		    NotificationManager.IMPORTANCE_HIGH
		)
		notificationManager!!.createNotificationChannel(
		    notificationChannel
		)
	  }
	  notificationManager.notify(0, builder.build())
    }
}
