package com.nextclick.deliveryboynew.utils.LocationUtil;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MyLocationUtil {

    private static Geocoder myGeoCoder = null;

    public static Geocoder geoCoderInstance() {
        if(myGeoCoder == null){
            myGeoCoder = new Geocoder(MyLocationManager.mContext, Locale.getDefault());
        }
        return myGeoCoder;
    }
    private static void initGeoCoder(Context context)
    {
        if(myGeoCoder == null){
            myGeoCoder = new Geocoder(context, Locale.getDefault());
        }
    }

    public static void SetContext(Context context)
    {
        MyLocationManager.SetContext(context);
        initGeoCoder(context);
    }

    public static void getCurrentPosition(MyLocationListener listener) {
        MyLocationManager.getInstance().getCurrentPosition(listener);
    }
    public static void startLocationUpdates(MyLocationListener listener) {
        MyLocationManager.getInstance().startLocationUpdates(listener);
    }
    public static void removeLocationUpdates(MyLocationListener listener) {
        MyLocationManager.getInstance().removeLocationUpdates(listener);
    }

    public static List<Address> getAddressFromLocation(Location location) {
        List<Address> addresses = null;
        try {
            addresses = geoCoderInstance().getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    /*String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String zip = addresses.get(0).getPostalCode();
                    String country = addresses.get(0).getCountryName();*/

        } catch (IOException e) {
            e.printStackTrace();
        }
        return addresses;
    }
}
