package com.nextclick.deliveryboynew.utils.payment;

public interface PaymentCompletedListener {
    void onPaymentCompleted(Integer paymentStatus, String paymentID, String error);
}
