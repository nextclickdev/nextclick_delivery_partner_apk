package com.nextclick.deliveryboynew.utils;

import com.nextclick.deliveryboynew.model.DirectionObject;

public interface DirectionListener {
    void onDirectionResult(DirectionObject result);
}
