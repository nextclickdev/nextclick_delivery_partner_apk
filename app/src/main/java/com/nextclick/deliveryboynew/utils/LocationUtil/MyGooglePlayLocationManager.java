package com.nextclick.deliveryboynew.utils.LocationUtil;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyGooglePlayLocationManager extends MyLocationManager {

    private static String TAG = "MyGooglePlayLocationManager";

    MyLocationListener oneShotListener;
    MyLocationListener frequentUpdatesListener;
    boolean isOneShot = true;
    boolean isStale = false;
    FusedLocationProviderClient mFusedLocationProviderClient;

    public static boolean isGooglePlayServicesAvailable(Context context){
        Object status = null;
        try {
            Class googlePlayServicesUtil = Class.forName("com.google.android.gms.common.GooglePlayServicesUtil");
            Class[] argTypes = new Class[]{Context.class};
            Method method = googlePlayServicesUtil.getMethod("isGooglePlayServicesAvailable", argTypes);
            status = method.invoke(googlePlayServicesUtil, context);
        }catch(InvocationTargetException e){
            Log.e(TAG,"isGooglePlayServicesAvailable(): " + e.getTargetException().getMessage());
        }catch (Exception e) {
            Log.e(TAG,"GooglePlayServicesUtil Class Not Available. Checking for GoogleApiAvailability");
        }
        if(status == null) {
            try {
                Object googleApiAvailabilityObject;
                Class googleApiAvailabilityClass = Class.forName("com.google.android.gms.common.GoogleApiAvailability");
                Method getInstanceMethod = googleApiAvailabilityClass.getMethod("getInstance", null);
                googleApiAvailabilityObject = getInstanceMethod.invoke(googleApiAvailabilityClass, null);

                Class[] argTypes = new Class[]{Context.class};
                Method isGooglePlayServicesAvailableMethod = googleApiAvailabilityClass.getMethod("isGooglePlayServicesAvailable", argTypes);
                status = isGooglePlayServicesAvailableMethod.invoke(googleApiAvailabilityObject, context);
            } catch (Exception gae) {
                Log.e(TAG,"GoogleApiAvailability Class Not Available");
            }
        }
        if(status != null) {
            int result = ((Integer) status).intValue();
            Log.e(TAG,"isGooglePlayServicesAvailable(): (0 for SUCCESS) status = " + status);
            return result == ConnectionResult.SUCCESS ? true : false;
        }
        return false;
    }

    @Override
    public void getCurrentPosition(MyLocationListener listener) {
        this.oneShotListener=listener;
        isOneShot =true;

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
        checkLocationSettings();
        //requestLocationUpdates();
    }

    @Override
    public void startLocationUpdates(MyLocationListener listener) {
        this.frequentUpdatesListener=listener;
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
        isOneShot =false;
        isStale =false;
        checkLocationSettings();
        // requestLocationUpdates();
    }

    @Override
    public void removeLocationUpdates(MyLocationListener listener) {
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        frequentUpdatesListener =null;
        isStale= true;
    }

    protected void checkLocationSettings() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(mContext).checkLocationSettings(mLocationSettingsRequest);
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    Log.e(TAG,"Status code :"+ LocationSettingsStatusCodes.SUCCESS);
                    requestLocationUpdates();
                }catch(ApiException exception){
                    Log.e(TAG,"Status code :"+exception.getStatusCode());
                    if(exception.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED){

                        /*// Cast to a resolvable exception.
                        ResolvableApiException resolvable = (ResolvableApiException) exception;
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        resolvable.startResolutionForResult(
                                DashboardActivity.getActContext(),
                                REQUEST_CHECK_SETTINGS);*/

                        if(isOneShot){
                            executeErrorCallback(oneShotListener, MYLOC_ECODE_POSITION_UNAVAILABLE, MYLOC_EMSG_POSITION_UNAVAILABLE);
                            oneShotListener =null;
                        }else {
                            executeErrorCallback(frequentUpdatesListener, MYLOC_ECODE_POSITION_UNAVAILABLE, MYLOC_EMSG_POSITION_UNAVAILABLE);
                            removeLocationUpdates(frequentUpdatesListener);
                        }
                    }
                }
            }
        });
    }


    private void requestLocationUpdates() {
        Log.e(TAG, "onConnected(): Checking for last known location...");

        Task<Location> listener =mFusedLocationProviderClient.getLastLocation();
        listener.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onConnected(): Executing success callback with location=");
            }
        });

        listener.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                Log.e(TAG, "onConnected(): last known location = " + location);
                if (location != null) {
                    Log.e(TAG, "onConnected(): Executing success callback with location=" + location);
                    if (isOneShot) {
                        executeOnLocationReceived(oneShotListener, location);
                    } else {
                        executeOnLocationUpdated(frequentUpdatesListener, location);
                    }
                }
            }
        });

        LocationRequest locReq = LocationRequest.create();
        locReq.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (isOneShot) {
            locReq.setNumUpdates(1);
        } else {

            locReq.setInterval(2000);
            locReq.setFastestInterval(1000);
            locReq.setSmallestDisplacement(2);
        }
        mFusedLocationProviderClient.requestLocationUpdates(locReq, mLocationCallback, null);
    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location location = locationResult.getLastLocation();
            Log.e(TAG,"onLocationChanged() location="+location);
            if(isStale)
                return;
            if(isOneShot){
                executeOnLocationReceived(oneShotListener,location);
            }
            else
            {
                executeOnLocationUpdated(frequentUpdatesListener,location);
            }
        }
    };
}

