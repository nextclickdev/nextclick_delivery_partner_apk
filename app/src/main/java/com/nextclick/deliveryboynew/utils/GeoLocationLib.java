package com.nextclick.deliveryboynew.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

public class GeoLocationLib {

    private Context context;
    List<Address> addresses;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    Geocoder geocoder;
    private Location lastKnownLocation;
    Vector<LocationListener> listeners;

    public GeoLocationLib(Context context) {
        this.context = context;
        listeners = new Vector<>();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        geocoder = new Geocoder(context, Locale.getDefault());

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds
    }

    public void addListener(LocationListener listener)
    {
        if(!listeners.contains(listener))
        {
            listeners.add(listener);
            getLastKnowLocation();
        }
    }
    public void removeListener(LocationListener listener)
    {
        if(listeners.contains(listener))
        {
            listeners.remove(listener);
        }
    }

    public void getLastKnowLocation() {
        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {
                lastKnownLocation= location;
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    /*String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String zip = addresses.get(0).getPostalCode();
                    String country = addresses.get(0).getCountryName();*/
                    executeCallback(lastKnownLocation, addresses);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                executeCallback(lastKnownLocation, addresses);
            }
        });
    }

    private void executeCallback(Location lastKnownLocation, List<Address> address) {
        for (int i = 0; i < listeners.size(); i++) {
            LocationListener listener = listeners.get(i);
            listener.onLocationChanged(lastKnownLocation,address);
        }
    }
}
