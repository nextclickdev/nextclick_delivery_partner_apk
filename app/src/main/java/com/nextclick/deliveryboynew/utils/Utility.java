package com.nextclick.deliveryboynew.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.nextclick.deliveryboynew.R;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Utility {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static String ArraivalTime;
    static String defaultFormat = "yyyy-MM-dd HH:mm:ss";

    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            String[] storge_permissions = {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            };
            String[] storge_permissions_33 = {
                    Manifest.permission.READ_MEDIA_IMAGES,
                    Manifest.permission.READ_MEDIA_AUDIO,
                    Manifest.permission.READ_MEDIA_VIDEO
            };
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(context, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(context, Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context, storge_permissions_33, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                }
            }
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {


                    String[] p;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        p = storge_permissions_33;
                    } else {
                        p = storge_permissions;
                    }
                    ActivityCompat.requestPermissions((Activity) context, p, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static String UserIntent = "user";
    public static String DPIntent = "delivery_partner";
    public static String VendorIntent = "vendor";
    public static String ExecutiveIntent = "executive";

    public static Double round(String value, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(getDouble(value)));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    private static double getDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (Exception ex) {
        }
        return 0;
    }

    public static Double round(Double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    public static Date getDateFromString(String dateString) {
        return getDateFromString(dateString, defaultFormat);
    }

    public static Date getDateFromString(String dateString, String format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format).parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static void setupOtpInputs(Context mContext, EditText editTextone, EditText editTexttwo, EditText editTextthree,
                                      EditText editTextfour, EditText editTextFive, EditText editTextSix) {
        addTextChangedListener(editTextone, editTexttwo);
        addTextChangedListener(editTexttwo, editTextthree);
        addTextChangedListener(editTextthree, editTextfour);
        addTextChangedListener(editTextfour, editTextFive);
        addTextChangedListener(editTextFive, editTextSix);
        //    addTextChangedListener(editTextone,editTexttwo);

        editTextone.setOnKeyListener(new GenericKeyEvent(editTextone, null));
        editTexttwo.setOnKeyListener(new GenericKeyEvent(editTexttwo, editTextone));
        editTextthree.setOnKeyListener(new GenericKeyEvent(editTextthree, editTexttwo));
        editTextfour.setOnKeyListener(new GenericKeyEvent(editTextfour, editTextthree));
        editTextFive.setOnKeyListener(new GenericKeyEvent(editTextFive, editTextfour));
        editTextSix.setOnKeyListener(new GenericKeyEvent(editTextSix, editTextFive));
    }

    private static void addTextChangedListener(EditText editTextone, EditText editTexttwo) {
        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty())
                    editTexttwo.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static String getValidStringValue(String value, String defaultValue) {
        if (value != null && !value.equals("null"))
            return value;
        return defaultValue;
    }


    public static class GenericKeyEvent implements View.OnKeyListener {

        private final EditText currentView, previousView;

        public GenericKeyEvent(EditText currentView, EditText previousView) {
            this.currentView = currentView;
            this.previousView = previousView;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL &&
                    currentView.getId() != R.id.editTextone && currentView.getText().toString().isEmpty()) {
                //If current is empty then previous EditText's number will also be deleted
                if (previousView != null) {
                    //previousView.setText(null);
                    previousView.requestFocus();
                }
                return true;
            }
            return false;
        }
    }

    public static String roundFloat(String str, int places) {

        BigDecimal bigDecimal = new BigDecimal(str);
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        return bigDecimal.toString();
    }
}
