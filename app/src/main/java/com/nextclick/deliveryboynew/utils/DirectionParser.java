package com.nextclick.deliveryboynew.utils;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.nextclick.deliveryboynew.model.DirectionObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DirectionParser {

    private final boolean parseRouteInformation;

    public DirectionParser(boolean parseRouteInformation) {
        this.parseRouteInformation=parseRouteInformation;
    }

    /** Receives a DirectionParser and returns a list of lists containing latitude and longitude */
    public DirectionObject parse(JSONObject jObject) {

        DirectionObject directionObject = new DirectionObject();

        try {
            JSONArray jRoutes = jObject.getJSONArray("routes");
            for (int i = 0; i < jRoutes.length(); i++) {
                JSONArray jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                for (int j = 0; j < jLegs.length(); j++) {
                    try {
                        String distance = ((JSONObject) jLegs.get(j)).getJSONObject("distance").get("text").toString();
                        String duration = ((JSONObject) jLegs.get(j)).getJSONObject("duration").get("text").toString();

                        directionObject.setTravelDistance(distance);
                        directionObject.setDuration(duration);
                    } catch (Exception ex) {
                        Log.i("DeliveryApp", "exception in  DirectionParser" + ex.getMessage());
                    }
                }
            }

            if(parseRouteInformation) {
                //routes
                List<List<HashMap<String, String>>> routes = new ArrayList<>();
                JSONArray jLegs = null;
                JSONArray jSteps = null;
                jRoutes = jObject.getJSONArray("routes");


                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List path = new ArrayList<HashMap<String, String>>();

                    for (int j = 0; j < jLegs.length(); j++) {
                        try {
                            String distance = ((JSONObject) jLegs.get(j)).getJSONObject("distance").get("text").toString();
                            String duration = ((JSONObject) jLegs.get(j)).getJSONObject("duration").get("text").toString();
                            //   String arrival_time = ((JSONObject) jLegs.get(0)).getJSONObject("arrival_time").get("text").toString();
                            Utility.ArraivalTime = "You will arrive in " + duration + ", and distance remaining : " + distance;
                        } catch (Exception ex) {
                        }

                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                                hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

                directionObject.setRoutes(routes);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        return directionObject;
    }

    /**
     * Method to decode polyline points
     * Courtesy : jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     * */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }
}
