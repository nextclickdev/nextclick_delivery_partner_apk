package com.nextclick.deliveryboynew.utils.payment;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.nextclick.deliveryboynew.R;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONException;
import org.json.JSONObject;

public class RazorPayHelper implements PaymentResultWithDataListener {

    public static final Integer PAYMENT_SUCCESS= 2;
    public static final Integer PAYMENT_FAIL= 3;
    private static Context mContext;
    private static PaymentCompletedListener paymentCompletedListener;

    public static void makePayment(Activity activity,Context context,PaymentCompletedListener listener, String amount) {
        mContext = context;
        paymentCompletedListener =listener;
        final Checkout co = new Checkout();
        co.setImage(R.drawable.nextclick_icon_receipt);
        try {
            JSONObject orderRequest = new JSONObject();
            // orderRequest.put("amount", totatlAmount+"00"); // amount in the smallest currency unit
            orderRequest.put("amount", "100"); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image", R.drawable.nextclick_logo_white);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            co.open((Activity) activity, orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        }
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Toast.makeText(mContext, String.valueOf(R.string.payement_done), Toast.LENGTH_SHORT).show();
        System.out.println("aaaaaaaaaa  data sucess  "+s+paymentData.toString());
        System.out.println("aaaaaaaaaa sucess  "+paymentData.getOrderId()+" "+paymentData.getPaymentId()+" "+paymentData.getSignature()+" "+
                paymentData.getUserContact()+"  "+paymentData.getData().toString()+" "+paymentData.getUserEmail());
        sendPaymentstatus(PAYMENT_SUCCESS,paymentData.getPaymentId(),"");
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        System.out.println("aaaaaaaaaa  payment  error  "+s);
        try {
            JSONObject jsonObject1=new JSONObject(s);
            JSONObject errorobj=jsonObject1.getJSONObject("error");
            String description=errorobj.getString("description");
            sendPaymentstatus(PAYMENT_FAIL,"",description);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendPaymentstatus(Integer paymentStatus, String paymentID, String error) {
        //if (paymentCompletedListener != null)
          //  paymentCompletedListener.onPaymentCompleted(paymentStatus, paymentID, error);
    }
}
