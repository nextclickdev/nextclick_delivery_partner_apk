package com.nextclick.deliveryboynew.utils.LocationUtil;

import android.location.Location;

public interface MyLocationListener {
    public void onLocationReceived(Location location);//one time location
    public void onLocationUpdated(Location location);//peridioc updates
    public void onLocationFailed(int code, String message);
}
