package com.nextclick.deliveryboynew.newauthentication;

import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.View.GONE;
import static com.nextclick.deliveryboynew.Config.Config.FCM;
import static com.nextclick.deliveryboynew.Config.Config.FORGOT_PASSWORD;
import static com.nextclick.deliveryboynew.Config.Config.LOGIN;
import static com.nextclick.deliveryboynew.Config.Config.PROFILE_READ;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.INFO;
import static com.nextclick.deliveryboynew.Constants.Constants.SUCCESS;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.UiMsgs.setEditTextErrorMethod;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.Constants.Constants;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.authentication.IncomingSms;
import com.nextclick.deliveryboynew.dashboard.activitys.DashboardActivity;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.helpers.UiMsgs;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.Utility;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class UserSigninActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etUserId, etPassword;
    private TextView tvForgotPassword, tvRegister,btnLogin,tv_change_loginType;
    private String user_id_str, password_str;
    private Context mContext;
    TextInputLayout layout_password;
    PreferenceManager preferenceManager;
    private boolean skipFCM=false;

    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_signin);
        init();
        initializeListeners();

        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
            }
        }

    }
    private void init() {
        mContext = UserSigninActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        etUserId = findViewById(R.id.etUserId);
        etPassword = findViewById(R.id.etPassword);
        tvRegister = findViewById(R.id.tvRegister);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        btnLogin = findViewById(R.id.btnLogin);
        layout_password= findViewById(R.id.layout_password);
        tv_change_loginType= findViewById(R.id.tv_change_loginType);
       // FirebaseApp.initializeApp(UserSigninActivity.this);
       // FirebaseMessaging.getInstance().setAutoInitEnabled(true);
    }
    private void initializeListeners() {
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:

              /*  Intent intent=new Intent(UserSigninActivity.this, DashboardActivity.class);
                startActivity(intent);*/
                if (isValid()) {
                    if (!isOtpLogin)
                        login(password_str);
                    else
                        sendOTP(user_id_str);
                }
                break;
            case R.id.tvForgotPassword:
                Intent registerIntent = new Intent(this, ResetPasswordActivity.class);
                startActivity(registerIntent);
                /*LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.supporter_forgotpassword, null);

                final EditText mailtext = alertLayout.findViewById(R.id.forgot_mail);
                final TextView ok = alertLayout.findViewById(R.id.ok);

                AlertDialog.Builder alert = new AlertDialog.Builder(UserSigninActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("Forgot Password");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);
                final AlertDialog dialog = alert.create();
                dialog.show();
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String forgotmail = mailtext.getText().toString().trim();
                        if (forgotmail.length() == 0) {
                            mailtext.setError("Should Not Be Empty");
                            mailtext.requestFocus();
                        } else if (!isValidEmail(forgotmail)) {
                            mailtext.setError("Invalid");
                            mailtext.requestFocus();
                        } else {
                            forgotPassword(forgotmail, dialog,view,mailtext);
                        }

                    }
                });*/
                break;
            case R.id.tvRegister:
                goToRegister();
                break;
            default:
                break;
        }
    }


    public void forgotPassword(final String forgotMail, final AlertDialog adialog, View view, EditText mailtext) {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("identity", forgotMail);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();

        signInButtonForget(data,view,adialog,mailtext);
        // LoadingDialog.loadDialog(mContext);

    }
    private void signInButtonForget(String data, View view, AlertDialog adialog, EditText mailtext) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingDialog.dialog.dismiss();
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObjectResponce = new JSONObject(response);
                    System.out.println("aaaaaaa  respone forgot   "+jsonObjectResponce);
                    if (jsonObjectResponce.getBoolean("status")) {
                        adialog.dismiss();
                        String token = jsonObjectResponce.getString("token");
                        preferenceManager.putString(USER_TOKEN, token);
                        UiMsgs.showCustomToast(mContext, ""+ Html.fromHtml(jsonObjectResponce.getString("message")), SUCCESS);
                        UiMsgs.showSnackBar(view, String.valueOf(Html.fromHtml(jsonObjectResponce.getString("message").toString())));
                    } else if (!jsonObjectResponce.getBoolean("status")) {
                        mailtext.setError(""+jsonObjectResponce.getString("message"));
                        UiMsgs.showCustomToast(mContext, ""+Html.fromHtml(jsonObjectResponce.getString("message")), INFO);
                        UiMsgs.showSnackBar(view, jsonObjectResponce.getString("data"));
                    }
                } catch (JSONException e) {
                    adialog.dismiss();
                    UiMsgs.showCustomToast(mContext, "Server Under Maintenance ", INFO);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                adialog.dismiss();
                System.out.println("aaaaaaaa  error "+error.getMessage());
                Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void goToRegister() {
        Intent registerIntent = new Intent(this, VerifyAndCreateAccount.class);//UserRegistration
        startActivity(registerIntent);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(UserSigninActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Do you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean isValid() {
        boolean valid = true;

        user_id_str = etUserId.getText().toString().trim();
        password_str = etPassword.getText().toString().trim();
        int user_id_length = user_id_str.length(), password_length = password_str.length();

        if (user_id_str ==null || user_id_str.length()  == 0) {
            setEditTextErrorMethod(etUserId, getString(R.string.empty));
            valid = false;
        }  else if (user_id_str.length() < 10) {
            setEditTextErrorMethod(etUserId, getString(R.string.invalid_mobile));
            valid = false;
        }
        else if (user_id_str.startsWith("0") || (user_id_str.startsWith("1")) || user_id_str.startsWith("2")||
                user_id_str.startsWith("3")|| user_id_str.startsWith("4")||user_id_str.startsWith("5")){
            setEditTextErrorMethod(etUserId, getString(R.string.invalid_mobile));
            valid = false;
        }
        else if(!isOtpLogin) {
            if (password_str == null || password_str.length() == 0) {
                setEditTextErrorMethod(etPassword, getString(R.string.empty));
                valid = false;
            }
        }
        return valid;

    }

    private void login(String input) {

        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("identity", user_id_str);
        if (isOtpLogin)
            dataMap.put("otp", input);
        else
            dataMap.put("password", input);
        dataMap.put(APP_ID, APP_ID_VALUE);

        dataMap.put("intent", Utility.DPIntent);//Possible Intents: ["user", "delivery_partner", "vendor", "executive"]
        JSONObject json = new JSONObject(dataMap);


        LoadingDialog.loadDialog(mContext);
        final String data = json.toString();
        System.out.println("LOGIN  request "+data);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    System.out.println("LOGIN  response "+response);
                    try {
                        LoadingDialog.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            preferenceManager.putString("approval_status", null);
                            if(dataObject.has("approval_status")) {
                                String approval_status = dataObject.getString("approval_status");

                                final String[] token = {dataObject.getString("token")};
                                preferenceManager.putString(USER_TOKEN, token[0]);

                                if (approval_status.equals("1"))//1-active,2-in active, 3- pending
                                {
                                    preferenceManager.putString("approval_status", approval_status);
                                    Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();

                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.initializeMixPanel(getApplicationContext());
                                    }

                                    if(skipFCM) {

                                        startActivity(new Intent(UserSigninActivity.this, DashboardActivity.class));
                                        finish();

                                    }
                                    else {
                                        FirebaseApp.initializeApp(UserSigninActivity.this);
                                        try {

                                            FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                                                @Override
                                                public void onComplete(@NonNull Task<String> task) {
                                                    String token = task.getResult();
                                                    String msg = getString(R.string.fcm_token, token);
                                                    Log.d("TAG", token);
                                                    grantFCMPermission(token);
                                                }
                                            });
                                        } catch (Exception e) {
                                            try{

                                                String token1 = returnMeFCMtoken();
                                                String msg = getString(R.string.fcm_token, token[0]);
                                                Log.d("TAG", token1);
                                                grantFCMPermission(token1);
                                            }catch (Exception e1){
                                                e1.printStackTrace();
                                                UiMsgs.showToast(mContext, "Firebase exception " + e.getMessage());
                                            }
                                            e.printStackTrace();
                                            //UiMsgs.showToast(mContext, "Firebase exception " + e.getMessage());
                                        }
                                    }
                                }
                                else if (approval_status.equals("3"))
                                {
                                    Intent registerIntent = new Intent(mContext, WelcomeActivity.class);
                                    startActivity(registerIntent);
                                    //Toast.makeText(mContext, "Your account is waiting for admin approval, please contact the nextclick admin team.", Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    Intent intent = new Intent(mContext, WaitingActivity.class);
                                    intent.putExtra("approval_status",approval_status);
                                    startActivity(intent);
                                    //Toast.makeText(mContext, "Your status is currently in-active, please contact the nextclick admin team.", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else
                                Toast.makeText(mContext, "approval_status is not coming from the backend api", Toast.LENGTH_SHORT).show();

                        } else {
                            String messagae = jsonObject.getString("message");
                            if(messagae.equals("INCORRECT_PASSWORD"))
                            {
                                messagae="Incorrect password";
                            }
                            else if(messagae.equals("USER_NOT_EXISTS"))
                            {
                                messagae="You don't have the account please sign up";
                            }
                            else if(messagae.equals("INCORRECT_OTP"))
                            {
                                //messagae="Incorrect otp";
                                showVerifyOTPDialog(input, user_id_str, false);
                                return;
                            }
                            Toasty.error(mContext, messagae, Toast.LENGTH_SHORT).show();
                           // Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaa catch "+e.getMessage());
                        LoadingDialog.dialog.dismiss();
                        UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UiMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa catch "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    public static String returnMeFCMtoken() {
        final String[] token = {""};
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(task.isComplete()){
                    token[0] = task.getResult();
                    Log.e("AppConstants", "onComplete: new Token got: "+token[0] );

                }
            }
        });
        return token[0];
    }
    private void grantFCMPermission(final String msg) {
        LoadingDialog.loadDialog(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);

        Log.i("DeliveryApp", "grantFCMPermission FCM_TOKEN "+msg);

        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                FCM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("fcm_res", response);
                    try {
                        LoadingDialog.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            preferenceManager.putString(Constants.FCM_TOKEN, msg);
                            startActivity(new Intent(UserSigninActivity.this, DashboardActivity.class));
                            fetchUserDetails();
                            finish();
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        LoadingDialog.dialog.dismiss();
                        System.out.println("aaaaaaaaaa  catch  "+e.getMessage());
                        UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UiMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaaaaa  catch  "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " +preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PROFILE_READ, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    Log.d("VolleyResponse", "PROFILE_READ: " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    System.out.println("aaaaaa  responce  "+jsonObject);
                    String unique_id,firstName,lastName,email;
                    unique_id=jsonObjectData.getString("unique_id");
                    firstName=jsonObjectData.getString("first_name");
                    lastName=jsonObjectData.getString("last_name");
                    email=jsonObjectData.getString("email");

                    if (MyMixPanel.isMixPanelSupport) {
                        MyMixPanel.initializeMixPanel(getApplicationContext());
                        MyMixPanel.createUserID(unique_id);
                        MyMixPanel.sendUserLogin(firstName, lastName, email);
                        MyMixPanel.logEvent("User logged into NextClick Main application");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finally {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                //map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMDA4NCIsInVzZXJkZXRhaWwiOnsiaWQiOiIxMDAwODQiLCJpcF9hZGRyZXNzIjoiMjQwOTo0MDcwOjIxOWE6YTJjNDo6OGIwOmU4YTUiLCJ1c2VybmFtZSI6Imd1ampldGlzaHJhdmFuQGdtYWlsLmNvbSIsInVuaXF1ZV9pZCI6Ik5DVTAyMTEiLCJwYXNzd29yZCI6IiQyeSQwOCQ2b21iQXViMTFhTU9leFRCdUF2SDBPTVJsbFlTZ3FGa0JcL2luMzVQXC9USE94ZlwvUnFGNGhKLiIsInNhbHQiOm51bGwsImVtYWlsIjoiZ3VqamV0aXNocmF2YW5AZ21haWwuY29tIiwid2FsbGV0IjoiMC4wMCIsImFjdGl2YXRpb25fY29kZSI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX2NvZGUiOm51bGwsImZvcmdvdHRlbl9wYXNzd29yZF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjpudWxsLCJjcmVhdGVkX29uIjoiMTU5MDEyMTMxNSIsImxhc3RfbG9naW4iOm51bGwsImFjdGl2ZSI6IjEiLCJsaXN0X2lkIjoiMCIsImZpcnN0X25hbWUiOiJHdWpqZXRpIFNocmF2YW5rdW1hciIsImxhc3RfbmFtZSI6bnVsbCwiY29tcGFueSI6bnVsbCwicGhvbmUiOiIiLCJjcmVhdGVkX3VzZXJfaWQiOm51bGwsInVwZGF0ZWRfdXNlcl9pZCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMjAtMDUtMjIgMDQ6MjE6NTUiLCJ1cGRhdGVkX2F0IjpudWxsLCJkZWxldGVkX2F0IjpudWxsLCJzdGF0dXMiOiIxIn0sInRpbWUiOjE1OTAxMjEzNjB9.gm-lTQiaLcLLYu4KIpjMorFcayjO77IZFulCRlwYlTk");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    Boolean isOtpLogin =true;
    public void changeLoginType(View view)
    {
        if(tv_change_loginType.getText().toString().equals(getString(R.string.login_with_password)))
        {
            isOtpLogin =false;
            tv_change_loginType.setText(getString(R.string.login_with_otp));
            layout_password.setVisibility(View.VISIBLE);
        }
        else
        {
            isOtpLogin =true;
            tv_change_loginType.setText(getString(R.string.login_with_password));
            layout_password.setVisibility(GONE);
        }
    }


    private void sendOTP(String mobile) {


        LoadingDialog.loadDialog(mContext);

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+json.toString());
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            //showVerifyOTPDialog(jsonObject.getJSONObject("data").getString("otp"),mobile,true);
                            showVerifyOTPDialog(null,mobile,false);
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("message")).toString(), Toast.LENGTH_SHORT);
                        }
                    } catch (Exception e) {
                        Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } finally {
                        LoadingDialog.dialog.dismiss();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    Toast.makeText(mContext, "Server under maintenance", Toast.LENGTH_SHORT);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    EditText editTextone,editTexttwo,editTextthree, editTextfour,editTextFive,editTextSix;
    private void showVerifyOTPDialog(String otp,String mobilenumber,boolean isTest) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);


        LinearLayout layout_root= (LinearLayout) dialog.findViewById(R.id.layout_root);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
        layoutParams.width = displayMetrics.widthPixels-50;
        layout_root.setLayoutParams(layoutParams);

        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
        editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        TextView tv_timer_resend = (TextView) dialog.findViewById(R.id.tv_timer_resend);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_timer_resend.getText().toString().equals("Resend Otp"))
                    sendOTP(mobilenumber);
            }
        });
        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            if(!isTest)
                tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }

        Utility.setupOtpInputs(mContext,editTextone,editTexttwo,editTextthree,editTextfour,editTextFive,editTextSix);


        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    login(otp);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  "+message);
                getOtpFromMessage(message);
            }
        }
    };
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            String otp=matcher.group(0);
            if(otp!=null) {
                if(editTextone!=null) {
                    editTextone.setText("" + otp.charAt(0));
                    editTexttwo.setText("" + otp.charAt(1));
                    editTextthree.setText("" + otp.charAt(2));
                    editTextfour.setText("" + otp.charAt(3));
                    editTextFive.setText("" + otp.charAt(4));
                    editTextSix.setText("" + otp.charAt(5));
                }
            }
        }
    }


}