package com.nextclick.deliveryboynew.newauthentication;

import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.KeyEvent.KEYCODE_DEL;
import static com.nextclick.deliveryboynew.Config.Config.LOGIN;
import static com.nextclick.deliveryboynew.Config.Config.REGISTER_USER;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.deliveryboynew.Config.Config;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.authentication.IncomingSms;
import com.nextclick.deliveryboynew.helpers.CustomDialog;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.helpers.UiMsgs;
import com.nextclick.deliveryboynew.helpers.Validations;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserRegistration extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText etUserId, etPassword;
    private Context mContext;
    PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    private boolean isOtpVerified=false;
    private String verifiedmobile;
    Validations validations;
    ProgressDialog progressDialog;
    String  mobile_str, email_str,first_name_str,last_name_str, password_str,re_password_str;

    //variables
    ImageView signup_back_button;
    Button signup_next_button, signup_login_button;
    private TextView signup_title_text;
    ImageView img_profile;

    TextInputEditText tv_user_first_name,tv_user_last_name,tv_user_mail,tv_user_mobile,tv_user_password,tv_user_re_password;


    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private String userChoosenTask,base64image="";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private final int MY_CAMERA_PERMISSION_CODE =100;
    private boolean isCameraPermissionGranted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        init();
        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        mobile_str = getIntent().getStringExtra("mobile");
        if(mobile_str!=null && !mobile_str.isEmpty())
        {
            verifiedmobile=mobile_str;
            isOtpVerified=true;
        }

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
            }
        }
        int permissionCheck = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);
        }
        else
            isCameraPermissionGranted =true;
    }

    private void init() {
        mContext = UserRegistration.this;
        mCustomDialog = new CustomDialog(mContext);
        preferenceManager = new PreferenceManager(mContext);
        validations = new Validations();

        signup_back_button = findViewById(R.id.signup_back_button);
        signup_next_button = findViewById(R.id.signup_next_button);
        signup_login_button = findViewById(R.id.signup_login_button);
        signup_title_text = findViewById(R.id.signup_title_text);

        tv_user_first_name= findViewById(R.id.tv_user_first_name);
        tv_user_last_name= findViewById(R.id.tv_user_last_name);
        tv_user_mail= findViewById(R.id.tv_user_mail);
        tv_user_mobile= findViewById(R.id.tv_user_mobile);
        tv_user_password= findViewById(R.id.tv_user_password);
        tv_user_re_password= findViewById(R.id.tv_user_re_password);

        img_profile=findViewById(R.id.img_profile);

        img_profile.setOnClickListener(this);
        signup_back_button.setOnClickListener(this);
    }

    public void callNextSignupScreen(View view) {
        boolean isValid=validator();
        if (isValid) {
            if (isOtpVerified && mobile_str.equals(verifiedmobile)) {
                dataSender();
            } else {
                sendOtp(mobile_str);
            }
        }
        /*  callNextScreen();*/
    }
    private boolean validator() {
        boolean validity = true;
        first_name_str=tv_user_first_name.getText().toString();
        last_name_str=tv_user_last_name.getText().toString();
        email_str=tv_user_mail.getText().toString();
        //mobile_str=tv_user_mobile.getText().toString();
        password_str=tv_user_password.getText().toString();
        re_password_str=tv_user_re_password.getText().toString();
        if (profile64 ==null || profile64.isEmpty()) {
            Toast.makeText(mContext, "Please select Profile Picture", Toast.LENGTH_SHORT).show();
            validity = false;
        }
        else if (first_name_str.length() == 0) {
            tv_user_first_name.setError("Please Enter First Name");
            tv_user_first_name.requestFocus();
            validity = false;
        }
        else if (last_name_str.length() == 0) {
            tv_user_last_name.setError("Please Enter Last Name");
            tv_user_last_name.requestFocus();
            validity = false;
        }
        else if (validations.isBlank(email_str)) {
            tv_user_mail.setError("EMPTY");
            tv_user_mail.requestFocus();
            validity = false;
        }
        else if (validations.isValidEmail(email_str)) {
            tv_user_mail.setError(getString(R.string.enter_valid_mail_id));
            tv_user_mail.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() == 0) {
            tv_user_mobile.setError("Enter mobile number");
            tv_user_mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() < 10) {
            tv_user_mobile.setError(getString(R.string.enter_valid_mobile_number));
            tv_user_mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.startsWith("0") || (mobile_str.startsWith("1")) || mobile_str.startsWith("2")||
                mobile_str.startsWith("3")|| mobile_str.startsWith("4")||mobile_str.startsWith("5")){
            tv_user_mobile.setError(getString(R.string.enter_valid_mobile_number));
            tv_user_mobile.requestFocus();
            validity = false;
        }
        else  if (password_str.length() == 0) {
            tv_user_password.setError("Enter Password");
            tv_user_password.requestFocus();
            validity = false;
        }
        else if (password_str.length() < 6) {
            tv_user_password.setError("Minimum 6 Characters");
            tv_user_password.requestFocus();
            validity = false;
        } else  if (re_password_str.length() == 0) {
            tv_user_re_password.setError("Enter Confirm Password");
            tv_user_re_password.requestFocus();
            validity = false;
        }
        else if (re_password_str.length() < 6) {
            tv_user_re_password.setError("Minimum 6 Characters");
            tv_user_re_password.requestFocus();
            validity = false;
        }else if (!password_str.equalsIgnoreCase(re_password_str)){
            tv_user_re_password.setError(getString(R.string.invalid_password_mismatch));
            tv_user_re_password.requestFocus();
            validity = false;
        }

        return validity;
    }

    boolean skipAnim = false;
    private void callNextScreen() {
        Intent intent = new Intent(UserRegistration.this,WelcomeActivity.class);//SignUpKYCActivity, WelcomeActivity

        //Add transition
        Pair[] pairs = new Pair[4];
        pairs[0] = new Pair<View, String>(signup_back_button, "transition_back_arrow_btn");
        pairs[1] = new Pair<View, String>(signup_title_text, "transition_title_text");
        pairs[2] = new Pair<View, String>(signup_next_button, "transition_next_btn");
        pairs[3] = new Pair<View, String>(signup_login_button, "transition_login_btn");

        if (!skipAnim &&  Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(UserRegistration.this, pairs);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();//should not come back
    }

    public void callLoginActivity(View view) {
        //Intent intent = new Intent(UserRegistration.this, SigninActivity.class);
        //  startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signup_back_button:
                finish();
                break;
            case R.id.img_profile:
                selectImage();
                break;
        }
    }

    private void sendOtp(final String mobile) {
        //LoadingDialog.loadDialog(mContext);

        LoadingDialog.loadDialog(mContext);

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        fcmMap.put("passowrd", password_str);
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+json.toString());
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            showVerifyOTPDialog(null,false);//null
                            //showVerifyOTPDialog(jsonObject.getJSONObject("data").getString("otp"),true);//null
                        } else {
                            UiMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UiMsgs.showCustomToast(mContext, "Something went wrong", e.getMessage());
                        e.printStackTrace();
                    } finally {
                        LoadingDialog.dialog.dismiss();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UiMsgs.showCustomToast(mContext, "Server under maintenance", "");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void callVerifyOTPApi(String otp) {
        LoadingDialog.loadDialog(mContext);

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile_str);
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingDialog.dialog.dismiss();
                try {
                    Log.d("VolleyResponse", "VERIFY_OTP: respnse " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {
                        UiMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        showVerifyOTPDialog(otp,false);
                    } else {
                        isOtpVerified=true;
                        verifiedmobile=mobile_str;
                        dataSender();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void dataSender() {

        LoadingDialog.loadDialog(mContext);


        Map<String, Object> mainData = new HashMap<>();

        mainData.put("first_name", first_name_str);
        mainData.put("last_name", last_name_str);
        mainData.put("display_name", first_name_str);
        mainData.put("email", email_str);
        mainData.put("phone", mobile_str);
        mainData.put("password", password_str);
        mainData.put("profile_image", ""+profile64);
        mainData.put("primary_intent", Utility.DPIntent);//Possible Intents: ["user", "delivery_partner", "vendor", "executive"]

        JSONObject json = new JSONObject(mainData);
        registerUser(json.toString());
    }


    GenericTextWatcher watcher1,watcher2,watcher3,watcher4,watcher5,watcher6;
    EditText editTextone,editTexttwo,editTextthree, editTextfour,editTextFive,editTextSix;
    private void showVerifyOTPDialog(String otp, boolean isTesting) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        final EditText editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        final EditText editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
        final EditText editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        final EditText editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        final EditText editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        final EditText editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        TextView tv_timer_resend = (TextView) dialog.findViewById(R.id.tv_timer_resend);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp(mobile_str);
            }
        });

        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6) {
            if (!isTesting)
                tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }

        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextone.length() == 1) {
                    editTextone.clearFocus();
                    editTexttwo.requestFocus();
                    editTexttwo.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTexttwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTexttwo.length() == 1) {
                    editTexttwo.clearFocus();
                    editTextthree.requestFocus();
                    editTextthree.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextthree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextthree.length() == 1) {
                    editTextthree.clearFocus();
                    editTextfour.requestFocus();
                    editTextfour.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextfour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextfour.length() == 1) {
                    editTextfour.clearFocus();
                    editTextFive.requestFocus();
                    editTextFive.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextFive.length() == 1) {
                    editTextFive.clearFocus();
                    editTextSix.requestFocus();
                    editTextSix.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    callVerifyOTPApi(otp);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }


    public class GenericTextWatcher implements TextWatcher, View.OnKeyListener {
        private View view;
        String previousText = "";

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.editTextone:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextone.removeTextChangedListener(watcher1);
                            editTextone.setText(previousText);
                            editTextone.addTextChangedListener(watcher1);

                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(text);
                            editTexttwo.addTextChangedListener(watcher2);
                        }
                        requestFocusAndSelection(editTexttwo,1);
                    }
                    break;
                case R.id.editTexttwo:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(previousText);
                            editTexttwo.addTextChangedListener(watcher2);

                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(text);
                            editTextthree.addTextChangedListener(watcher3);

                        }
                        //  editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree,1);

                    } else if (text.length() == 0) {
                        //  editTextone.requestFocus();
                        requestFocusAndSelection(editTextone, 0);
                    }
                    break;
                case R.id.editTextthree:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(previousText);
                            editTextthree.addTextChangedListener(watcher3);

                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(text);
                            editTextfour.addTextChangedListener(watcher4);
                        }
                        //  editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour,1);
                    } else if (text.length() == 0) {
                        // editTexttwo.requestFocus();
                        requestFocusAndSelection(editTexttwo, 0);
                    }
                    break;
                case R.id.editTextfour:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(previousText);
                            editTextfour.addTextChangedListener(watcher4);

                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(text);
                            editTextFive.addTextChangedListener(watcher5);
                        }
                        //  editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,1);
                    } else if (text.length() == 0) {
                        // editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree, 0);
                    }
                    break;
                case R.id.editTextFive:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(previousText);
                            editTextFive.addTextChangedListener(watcher5);

                            editTextSix.removeTextChangedListener(watcher6);
                            editTextSix.setText(text);
                            editTextSix.addTextChangedListener(watcher6);
                        }
                        // editTextSix.requestFocus();
                        requestFocusAndSelection(editTextSix,1);
                    } else if (text.length() == 0) {
                        //editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour, 0);
                    }
                    break;
                case R.id.editTextSix:
                    if (text.length() == 0) {
                        // editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,0);
                    } else {
                        try {
                            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        } catch (Exception e) {
                        }
                    }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            if (arg0.length() > 0) {
                previousText = arg0.toString();
            }
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            previousText = "";
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTexttwo:
                        if (editTexttwo.getText().toString().trim().length() == 0)
                            editTextone.requestFocus();
                        break;
                    case R.id.editTextthree:
                        if (editTextthree.getText().toString().trim().length() == 0)
                            editTexttwo.requestFocus();
                        break;
                    case R.id.editTextfour:
                        if (editTextfour.getText().toString().trim().length() == 0)
                            editTextthree.requestFocus();
                        break;
                    case R.id.editTextFive:
                        if (editTextFive.getText().toString().trim().length() == 0)
                            editTextfour.requestFocus();
                        break;
                    case R.id.editTextSix:
                        if (editTextSix.getText().toString().trim().length() == 0)
                            editTextFive.requestFocus();
                        break;
                }

            }
            return false;
        }
    }

    private void requestFocusAndSelection(EditText et, int i) {
        et.requestFocus();
        if (et.getText().length() > 0)
            et.setSelection(1);
    }


    public void registerUser(final String json) {

        final String data = json;
        Log.v("requesr register ",data);
        System.out.println("aaaaaaaaaaa  request regeter  "+json.toString());

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaaa  response register "+jsonObject.toString());
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                            // String data = jsonObject.getString("data");
                            System.out.println("aaaaaaa responce "+jsonObject);
                            if (status) {
                                // UiMsgs.showCustomToast(mContext, "Account Successfully Created");
                                Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
                                //callNextScreen();
                                getUserToken();
                            } else {
                                try {
                                    if (message!=null && message.equals("DUPLICATE_IDENTITY")) {
                                        Toast.makeText(mContext, "It looks like you already have an account with NextClick, please proceed with the login", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception ex) {
                                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                }
                                Log.d("er msg", Html.fromHtml(message).toString());
                                LoadingDialog.dialog.dismiss();
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaaa  catchh "+e.getMessage());
                            UiMsgs.showToast(mContext, "Something went wrong");
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa  catchh "+error.getMessage());
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();
                UiMsgs.showToast(mContext, "Something went wrong");
                LoadingDialog.dialog.dismiss();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getUserToken() {
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("identity", mobile_str);
        dataMap.put("password", password_str);
        dataMap.put("intent", Utility.DPIntent);//Possible Intents: ["user", "delivery_partner", "vendor", "executive"]
        JSONObject json = new JSONObject(dataMap);
        login(json);
    }

    private void login(JSONObject json) {
        mCustomDialog.show();
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("login_res", response);
                    try {
                        mCustomDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            String token = dataObject.getString("token");
                            try {
                                preferenceManager.putString(USER_TOKEN, token);
                                preferenceManager.putString("approval_status", null);
                                callNextScreen();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();
                            callLoginActivity(null);
                        }
                    }catch (Exception e) {
                        mCustomDialog.dismiss();
                        e.printStackTrace();
                    }
                } else {
                    mCustomDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
                callLoginActivity(null);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  "+message);
                getOtpFromMessage(message);
            }
        }
    };
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            String otp=matcher.group(0);
            if(editTextone!=null) {
                editTextone.setText("" + otp.charAt(0));
                editTexttwo.setText("" + otp.charAt(1));
                editTextthree.setText("" + otp.charAt(2));
                editTextfour.setText("" + otp.charAt(3));
                editTextFive.setText("" + otp.charAt(4));
                editTextSix.setText("" + otp.charAt(5));
            }
        }
    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_CAMERA_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isCameraPermissionGranted = true;
                }
                else
                {
                    Toast.makeText(getApplicationContext(), getString(R.string.accept_camera_permission), Toast.LENGTH_SHORT).show();
                }
                break;
            }case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask!=null&&userChoosenTask.equals(getString(R.string.take_photo)))
                        cameraIntent();
                    else if(userChoosenTask.equals(getString(R.string.choose_from_library)))
                        galleryIntent();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();//code for deny
                }
                break;

        }
    }


    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(UserRegistration.this);

                if (items[item].equals(getString(R.string.take_photo))) {
                    userChoosenTask =getString(R.string.take_photo);
                    if(result)
                        cameraIntent();

                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask =getString(R.string.choose_from_library);
                    if(result)
                        galleryIntent();

                } else if (items[item].equals(getString(R.string.cancel))){
                    dialog.dismiss();
                }
            }
        }); // Set items without click listener

        AlertDialog dialog = builder.create();
        ListView listView = dialog.getListView(); // Get reference to the ListView

// Set item click listener for the ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean result = Utility.checkPermission(UserRegistration.this);

                if (items[position].equals(getString(R.string.take_photo))) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[position].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[position].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent() {
        if (isCameraPermissionGranted) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }
    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
        setimage(bm);
    }
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //fixOrientation(thumbnail);

        // profileImage_edit.setImageBitmap(thumbnail);

        setimage(thumbnail);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);


    }

    private String profile64="";
    public void setimage(Bitmap bitmap)
    {
        Glide.with(mContext)
                .load(bitmap)
                .placeholder(R.drawable.noimageone)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(img_profile);
        profile64=convert(bitmap);
    }
}