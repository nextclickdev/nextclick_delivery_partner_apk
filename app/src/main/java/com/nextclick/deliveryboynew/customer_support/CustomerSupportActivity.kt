package com.nextclick.deliveryboynew.customer_support

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.JsonParser

import com.nextclick.crm.activities.customer_support.CustomerSupportData
import com.nextclick.crm.activities.customer_support.CustomerSupportItem
import com.nextclick.deliveryboynew.Config.Config
import com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN
import com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN
import com.nextclick.deliveryboynew.R
import com.nextclick.deliveryboynew.utils.PreferenceManager


class CustomerSupportActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: CustomerSupportAdapter
    private lateinit var noData: TextView
    private lateinit var preferenceManager: PreferenceManager
    private var customerList = ArrayList<CustomerSupportItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
	  super.onCreate(savedInstanceState)
	  setContentView(R.layout.activity_customer_support)
	  supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.Iconblue)));
	  supportActionBar!!.title = "Customer Support"
	  supportActionBar!!.setDisplayHomeAsUpEnabled(true)
	  noData = findViewById(R.id.tv_nodata)
	  preferenceManager = PreferenceManager(this)
	  recyclerView = findViewById(R.id.rvCustomerSupport)
	  recyclerView.layoutManager = LinearLayoutManager(this)
	  recyclerView.setHasFixedSize(true)
	  adapter = CustomerSupportAdapter(customerList)
	  recyclerView.adapter = adapter
	  getCustomerSupportRequests()
    }

    override fun onResume() {
	  super.onResume()
	  getCustomerSupportRequests()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
	  return when (item.itemId) {
		android.R.id.home -> {
		    onBackPressed()
		    true
		}

		R.id.action_help -> {
			startActivity(Intent(this, ContactFormActivity::class.java))
		    true
		}

		else -> super.onOptionsItemSelected(item)
	  }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
	  // Inflate the menu; this adds items to the action bar if it is present.
	  menuInflater.inflate(R.menu.support_menu, menu)
	  return true
    }

    private fun getCustomerSupportRequests() {
	  customerList.clear()
	  val requestQueue = Volley.newRequestQueue(this)
	  val stringRequest: StringRequest = object : StringRequest(
		Method.GET, Config.CUSTOMER_SUPPORT_REQUESTS, Response.Listener<String> {
		    if (it != null) {
			  val parser = JsonParser()
			  val mJson = parser.parse(it)
			  val gson = Gson()
			  val faqModel: CustomerSupportData =
				gson.fromJson(mJson, CustomerSupportData::class.java)
			  customerList.addAll(faqModel.data)
			  if (customerList.isEmpty()) {
				noData.visibility = View.VISIBLE
				recyclerView.visibility = View.GONE
			  } else {
				noData.visibility = View.GONE
				recyclerView.visibility = View.VISIBLE
			  }
			  adapter.notifyDataSetChanged()
			  Log.d("TAG", "getCustomerSupportRequests: ")
		    }
		}, Response.ErrorListener {

		}
	  ) {
		override fun getHeaders(): MutableMap<String, String> {
		    val map: MutableMap<String, String> = HashMap()
		    map["Content-Type"] = "application/json"
		    map[AUTH_TOKEN] =
				"Bearer " +preferenceManager.getString(USER_TOKEN)
		    return map
		}
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }
}