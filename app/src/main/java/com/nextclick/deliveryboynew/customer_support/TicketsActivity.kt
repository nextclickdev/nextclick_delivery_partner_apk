package com.nextclick.deliveryboynew.customer_support

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.nextclick.deliveryboynew.Config.Config
import com.nextclick.deliveryboynew.Constants.Constants
import com.nextclick.deliveryboynew.R
import com.nextclick.deliveryboynew.model.TicketModel
import com.nextclick.deliveryboynew.utils.PreferenceManager


/**
 * Created by Arun Vegyas on 25-06-2023.
 */
class TicketsActivity : AppCompatActivity() {
    private lateinit var preferenceManager: PreferenceManager
    override fun onCreate(savedInstanceState: Bundle?) {
	  super.onCreate(savedInstanceState)
	  setContentView(R.layout.ticket_details)
		if (supportActionBar!=null){
			supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.Iconblue)))
		}
        supportActionBar!!.title = "Customer Support"
	  supportActionBar!!.setDisplayHomeAsUpEnabled(true)
	  preferenceManager = PreferenceManager(this)
        getCustomerSupportDetails()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
	  return when (item.itemId) {
		android.R.id.home -> {
		    onBackPressed()
		    true
		}

		else -> super.onOptionsItemSelected(item)
	  }
    }

    private fun getCustomerSupportDetails() {
	  val requestQueue = Volley.newRequestQueue(this)
	  val usl = Config.BASE_URL + "general/api/support/customer_support_detail?support_id=${
		intent.getIntExtra(
		    "id",
		    0
		)
	  }"
	  val stringRequest: StringRequest = @SuppressLint("CutPasteId")
	  object : StringRequest(
		Method.GET, usl, Response.Listener<String> {
		    if (it != null) {
			  val parser = JsonParser()
			  val mJson = parser.parse(it)
			  val gson = Gson()
			  val ticketModel: TicketModel =
				gson.fromJson(mJson, TicketModel::class.java)
			  if (ticketModel.status) {
				supportActionBar!!.title =
				    "#Order ID : ${ticketModel.data[0].id}"
				val data = ticketModel.data[0]
				val requestType = "Request Type: " + data.request_type
				val title = "Complaint: " + data.title
				val description = "Description: " + data.description
				val status = "Ticket Status: " + data.status_text
				val comment = "Comment: " + data.comment
				if (data.status_text == "Open") {
				    findViewById<TextView>(R.id.textAssignedTo).visibility = View.GONE
				} else {
				    if (data.assigned != null) {
					  val assignedTo =
						"Assigned To: " + data.assigned.first_name + data.assigned.last_name
					  findViewById<TextView>(R.id.textAssignedTo).text = assignedTo
					  findViewById<TextView>(R.id.textAssignedTo).visibility =
						View.GONE
				    }


				}
				// Set the text values to the respective TextViews
				if (data.comment != null) {
				    findViewById<TextView>(R.id.textComment).text = Html.fromHtml(comment).toString()
				    findViewById<TextView>(R.id.textComment).visibility = View.VISIBLE

				} else {
				    findViewById<TextView>(R.id.textComment).visibility = View.GONE
				}
				if (data.status_text.equals(
					  "open",
					  true
				    ) || data.status_text.equals("working", true)
				) {
				    findViewById<TextView>(R.id.textInfo).visibility = View.VISIBLE
				    findViewById<TextView>(R.id.textInfo).text =
					  "Our customer care/Admin team will get as soon as possible"
				} else {
				    findViewById<TextView>(R.id.textInfo).visibility = View.GONE
				}
				findViewById<TextView>(R.id.textRequestType).text = requestType
				findViewById<TextView>(R.id.textTitle).text = title
				findViewById<TextView>(R.id.textDescription).text = description
				findViewById<TextView>(R.id.textStatus).text = status
				Log.d("TAG", "getCustomerSupportRequests: ")
			  } else {
				Toast.makeText(this, ticketModel.message, Toast.LENGTH_LONG).show()
			  }

		    }
		}, Response.ErrorListener {

		}
	  ) {
		override fun getHeaders(): MutableMap<String, String> {
		    val map: MutableMap<String, String> = HashMap()
		    map["Content-Type"] = "application/json"
		    map[Constants.AUTH_TOKEN] =
				"Bearer " +preferenceManager.getString(Constants.USER_TOKEN)
		    return map
		}
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }
}