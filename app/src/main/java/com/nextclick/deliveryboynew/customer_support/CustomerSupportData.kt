package com.nextclick.crm.activities.customer_support

data class CustomerSupportData(   val status: Boolean,
					    val http_code: Int,
					    val message: String,
					    val data: List<CustomerSupportItem>)
data class CustomerSupportItem(
    val id: Int,
    val app_details_id: Int,
    val request_type: String,
    val title: String,
    val description: String,
    val severity: Int,
    val status: Int,
    val severity_text: String,
    val status_text: String,
    val created_user_id: Int,
    val updated_user_id: Int?,
    val created_at: String,
    val updated_at: String?
)







