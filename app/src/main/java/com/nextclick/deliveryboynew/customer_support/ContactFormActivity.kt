package com.nextclick.deliveryboynew.customer_support

import android.annotation.SuppressLint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.nextclick.deliveryboynew.Config.Config
import com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN
import com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN
import com.nextclick.deliveryboynew.R
import com.nextclick.deliveryboynew.apiServices.RetrofitClient
import com.nextclick.deliveryboynew.utils.PreferenceManager
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject


class ContactFormActivity : AppCompatActivity() {

    private lateinit var nameEditText: EditText
    private lateinit var mobileEditText: EditText
    private lateinit var emailEditText: EditText
    private lateinit var severityAutoCompleteTextView: Spinner
    private lateinit var issueTypeAutoCompleteTextView: Spinner
    private lateinit var titleEt: EditText
    private lateinit var descriptionEt: EditText
    private var severeityId: Int = 0
    private lateinit var preferenceManager: PreferenceManager
    private var token: String = ""
    private var issueTypeId: String = ""
	lateinit var disposable: Disposable

    @SuppressLint("AppCompatMethod")
    override fun onCreate(savedInstanceState: Bundle?) {
	  super.onCreate(savedInstanceState)
	  setContentView(R.layout.activity_contact_form)

	  // Initialize views
	  preferenceManager = PreferenceManager(this)
	  token = "Bearer " +preferenceManager.getString(USER_TOKEN)
	  supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.Iconblue)));

//	  nameEditText = findViewById(R.id.et_name)
//	  mobileEditText = findViewById(R.id.et_mobileno)
//	  emailEditText = findViewById(R.id.et_email)
	  titleEt = findViewById(R.id.et_title)
	  descriptionEt = findViewById(R.id.et_description)
	  supportActionBar!!.title = "Customer Support"
	  supportActionBar!!.setDisplayHomeAsUpEnabled(true)
	  severityAutoCompleteTextView = findViewById(R.id.severityAutoCompleteTextView)
	  issueTypeAutoCompleteTextView = findViewById(R.id.issueTypeAutoCompleteTextView)

	  // Populate severity dropdown
	  val severityOptions = arrayOf("Low", "Medium", "High", "Critical")
	  val severityAdapter = ArrayAdapter(this, R.layout.dropdown_item, severityOptions)
	  severityAutoCompleteTextView.adapter = severityAdapter

	  // Populate issue type dropdown
	  val issueTypeOptions = arrayOf(
		"Orders",
		"Payment settlement",
		"Documents",
		"Vehicle"
	  )
	  val issueTypeAdapter = ArrayAdapter(this, R.layout.dropdown_item, issueTypeOptions)
	  issueTypeAutoCompleteTextView.setAdapter(issueTypeAdapter)
	  severityAutoCompleteTextView.onItemSelectedListener =
		object : AdapterView.OnItemSelectedListener {
		    override fun onItemSelected(
			  parent: AdapterView<*>?,
			  view: View?,
			  position: Int,
			  id: Long
		    ) {
			  val selected = severityOptions[position]
			  severeityId = position
			  Log.d("TAG", "onItemSelected: $position")
		    }

		    override fun onNothingSelected(parent: AdapterView<*>?) {

		    }

		}
	  issueTypeAutoCompleteTextView.onItemSelectedListener =
		object : AdapterView.OnItemSelectedListener {
		    override fun onItemSelected(
			  parent: AdapterView<*>?,
			  view: View?,
			  position: Int,
			  id: Long
		    ) {
			  issueTypeId = issueTypeOptions[position]
		    }

		    override fun onNothingSelected(parent: AdapterView<*>?) {

		    }

		}
	  // Handle form submission
	  val submitButton = findViewById<Button>(R.id.submitButton)
	  submitButton.setOnClickListener {
		submitForm()
	  }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
	  return when (item.itemId) {
		android.R.id.home -> {
		    onBackPressed()
		    true
		}

		else -> super.onOptionsItemSelected(item)
	  }
    }

    private fun submitForm() {
	  var isValidForm = false
//	  val name = nameEditText.text.toString()
//	  val mobile = mobileEditText.text.toString()
//	  val email = emailEditText.text.toString()
	  val title = titleEt.text.toString()
	  val description = descriptionEt.text.toString()
	  if (title.isEmpty()) {
		isValidForm = false
		titleEt.error = "Please enter title"
	  } else if (description.isEmpty()) {
		isValidForm = false
		descriptionEt.error = "Please enter descritption"
	  } /*else if (mobile.isEmpty()) {
		mobileEditText.error = "Please enter your mobile number"
		isValidForm = false
	  } else if (email.isEmpty()) {
		emailEditText.error = "Please enter email Id"
		isValidForm = false
	  }*/ else if (issueTypeId.isEmpty()) {
		Toast.makeText(this, "Please select Issue Type", Toast.LENGTH_LONG).show()
	  } else {
		val stringObjectHashMap = JsonObject()
		stringObjectHashMap.addProperty("app_details_id", "4")
		stringObjectHashMap.addProperty("request_type", issueTypeId)
		stringObjectHashMap.addProperty("title", title)
		stringObjectHashMap.addProperty("description", description)
		stringObjectHashMap.addProperty("severity", severeityId)
		  val requestBody =
			  RequestBody.create("application/json".toMediaTypeOrNull(), Gson().toJson(stringObjectHashMap))
		submitFormToServer(requestBody)
	  }

	  // Perform form validation and submission
	  // ...
    }

    private fun submitFormToServer(json: RequestBody) {
		val apiService = RetrofitClient.provideApi(this)
		disposable = apiService.customerFormToServer(json)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe({
				if (it.isStatus) {
					Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
					onBackPressed()
				} else {
					Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
				}
			}, {
				Log.d("TAG", "submitFormToServer: ${it.localizedMessage}")
			})
    }
}
