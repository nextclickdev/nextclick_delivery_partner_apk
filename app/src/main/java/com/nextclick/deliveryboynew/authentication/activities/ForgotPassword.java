package com.nextclick.deliveryboynew.authentication.activities;

import static com.nextclick.deliveryboynew.Config.Config.FORGOT_PASSWORD;
import static com.nextclick.deliveryboynew.Constants.Constants.INFO;
import static com.nextclick.deliveryboynew.Constants.Constants.SUCCESS;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.Validations.isValidEmail;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.helpers.UiMsgs;
import com.nextclick.deliveryboynew.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPassword extends AppCompatActivity  implements View.OnClickListener {

    private EditText etUserId;
    private Context mContext;
    private TextView btnLogin;
    PreferenceManager preferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = ForgotPassword.this;
        etUserId = findViewById(R.id.etUserId);
        preferenceManager = new PreferenceManager(mContext);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    private void validate()
    {
        String mailId = etUserId.getText().toString().trim();
        if (mailId.length() == 0) {
            etUserId.setError(getString(R.string.email_id_should_exists));
            etUserId.requestFocus();
        } else if (!isValidEmail(mailId)) {
            etUserId.setError(getString(R.string.invalid_mail_id));
            etUserId.requestFocus();
        } else {
            forgotPassword(mailId);
        }
    }

    public void forgotPassword(String forgotMail) {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("identity", forgotMail);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();

        signInButtonForget(data);
    }

    private void signInButtonForget(String data) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingDialog.dialog.dismiss();
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObjectResponce = new JSONObject(response);
                    System.out.println("aaaaaaa  respone forgot   "+jsonObjectResponce);
                    if (jsonObjectResponce.getBoolean("status")) {
                        String token = jsonObjectResponce.getString("token");
                        preferenceManager.putString(USER_TOKEN, token);
                        UiMsgs.showCustomToast(mContext, ""+ Html.fromHtml(jsonObjectResponce.getString("message")), SUCCESS);
                       // UiMsgs.showSnackBar(view, String.valueOf(Html.fromHtml(jsonObjectResponce.getString("message").toString())));
                    } else if (!jsonObjectResponce.getBoolean("status")) {
                        etUserId.setError(""+jsonObjectResponce.getString("message"));
                        UiMsgs.showCustomToast(mContext, ""+Html.fromHtml(jsonObjectResponce.getString("message")), INFO);
                       // UiMsgs.showSnackBar(view, jsonObjectResponce.getString("data"));
                    }
                } catch (JSONException e) {
                    LoadingDialog.dialog.dismiss();
                    /*UiMsgs.showCustomToast(mContext, "Server Under Maintenance ", INFO);*/
                    UiMsgs.showCustomToast(mContext, "A Reset mail sent", INFO);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaaa  error "+error.getMessage());
                Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                validate();
                break;
        }
    }
}