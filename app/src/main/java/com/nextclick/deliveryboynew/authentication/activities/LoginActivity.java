package com.nextclick.deliveryboynew.authentication.activities;

import static com.nextclick.deliveryboynew.Config.Config.FCM;
import static com.nextclick.deliveryboynew.Config.Config.FORGOT_PASSWORD;
import static com.nextclick.deliveryboynew.Config.Config.LOGIN;
import static com.nextclick.deliveryboynew.Config.Config.PROFILE_READ;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID;
import static com.nextclick.deliveryboynew.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.deliveryboynew.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboynew.Constants.Constants.INFO;
import static com.nextclick.deliveryboynew.Constants.Constants.SUCCESS;
import static com.nextclick.deliveryboynew.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboynew.helpers.UiMsgs.setEditTextErrorMethod;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nextclick.deliveryboynew.Constants.Constants;
import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.activitys.DashboardActivity;
import com.nextclick.deliveryboynew.helpers.LoadingDialog;
import com.nextclick.deliveryboynew.helpers.UiMsgs;
import com.nextclick.deliveryboynew.utils.PreferenceManager;
import com.nextclick.deliveryboynew.utils.mixpanelutil.MyMixPanel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etUserId, etPassword;
    private TextView tvForgotPassword, tvRegister,btnLogin;
    private String user_id_str, password_str;
    private Context mContext;
    PreferenceManager preferenceManager;
    private boolean skipFCM=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_login);
        //getSupportActionBar().hide();
        init();
        initializeListeners();

    }
    private void init() {
        mContext = LoginActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        etUserId = findViewById(R.id.etUserId);
        etPassword = findViewById(R.id.etPassword);
        tvRegister = findViewById(R.id.tvRegister);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        btnLogin = findViewById(R.id.btnLogin);
    }
    private void initializeListeners() {
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                System.out.println("aaaaaaaa  "+etUserId.getText().toString());
              /*  Intent intent=new Intent(LoginActivity.this, DashboardActivity.class);
                startActivity(intent);*/
                if (isValid()) {
                    Map<String, String> dataMap = new HashMap<>();
                    dataMap.put("identity", user_id_str);
                    dataMap.put("password", password_str);
                    dataMap.put(APP_ID, APP_ID_VALUE);
                    JSONObject json = new JSONObject(dataMap);
                    login(json);
                }
                break;
            case R.id.tvForgotPassword:
                Intent registerIntent = new Intent(this, ForgotPassword.class);
                startActivity(registerIntent);
                /*LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.supporter_forgotpassword, null);

                final EditText mailtext = alertLayout.findViewById(R.id.forgot_mail);
                final TextView ok = alertLayout.findViewById(R.id.ok);

                AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("Forgot Password");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);
                final AlertDialog dialog = alert.create();
                dialog.show();
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String forgotmail = mailtext.getText().toString().trim();
                        if (forgotmail.length() == 0) {
                            mailtext.setError("Should Not Be Empty");
                            mailtext.requestFocus();
                        } else if (!isValidEmail(forgotmail)) {
                            mailtext.setError("Invalid");
                            mailtext.requestFocus();
                        } else {
                            forgotPassword(forgotmail, dialog,view,mailtext);
                        }

                    }
                });*/
                break;
            case R.id.tvRegister:
                goToRegister();
                break;
            default:
                break;
        }
    }


    public void forgotPassword(final String forgotMail, final AlertDialog adialog, View view, EditText mailtext) {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("identity", forgotMail);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();

        signInButtonForget(data,view,adialog,mailtext);
       // LoadingDialog.loadDialog(mContext);

    }
    private void signInButtonForget(String data, View view, AlertDialog adialog, EditText mailtext) {
        LoadingDialog.loadDialog(mContext);
                    RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            LoadingDialog.dialog.dismiss();
                            //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObjectResponce = new JSONObject(response);
                                System.out.println("aaaaaaa  respone forgot   "+jsonObjectResponce);
                                if (jsonObjectResponce.getBoolean("status")) {
                                    adialog.dismiss();
                                    String token = jsonObjectResponce.getString("token");
                                    preferenceManager.putString(USER_TOKEN, token);
                                    UiMsgs.showCustomToast(mContext, ""+Html.fromHtml(jsonObjectResponce.getString("message")), SUCCESS);
                                    UiMsgs.showSnackBar(view, String.valueOf(Html.fromHtml(jsonObjectResponce.getString("message").toString())));
                                } else if (!jsonObjectResponce.getBoolean("status")) {
                                    mailtext.setError(""+jsonObjectResponce.getString("message"));
                                    UiMsgs.showCustomToast(mContext, ""+Html.fromHtml(jsonObjectResponce.getString("message")), INFO);
                                    UiMsgs.showSnackBar(view, jsonObjectResponce.getString("data"));
                                }
                            } catch (JSONException e) {
                                adialog.dismiss();
                                UiMsgs.showCustomToast(mContext, "Server Under Maintenance ", INFO);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            LoadingDialog.dialog.dismiss();
                            adialog.dismiss();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                            Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();

                        }
                    }) {

                        @Override
                        public String getBodyContentType() {
                            return "application/json";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return data == null ? null : data.getBytes("utf-8");

                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);

    }
    private void goToRegister() {
        Intent registerIntent = new Intent(this, SignUpActivity.class);
        startActivity(registerIntent);
    }

    private boolean isValid() {
        boolean valid = true;

        user_id_str = etUserId.getText().toString().trim();
        password_str = etPassword.getText().toString().trim();
        int user_id_length = user_id_str.length(), password_length = password_str.length();
        if (user_id_length == 0) {
            setEditTextErrorMethod(etUserId,getString(R.string.empty));
            valid = false;
        } else if (user_id_length < 5) {
            setEditTextErrorMethod(etUserId, getString(R.string.invalid_user_id));
            valid = false;
        } else if (password_length == 0) {
            setEditTextErrorMethod(etPassword, getString(R.string.empty));
            valid = false;
        } else if (password_length < 4) {
            setEditTextErrorMethod(etPassword, getString(R.string.invalid_password));
            valid = false;
        }


        return valid;

    }

    private void login(JSONObject json) {
        LoadingDialog.loadDialog(mContext);
        final String data = json.toString();
        System.out.println("LOGIN  request "+data);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    System.out.println("LOGIN  response "+response);
                    try {
                        LoadingDialog.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            String token = dataObject.getString("token");
                            Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
                            preferenceManager.putString(USER_TOKEN, token);

                            if (MyMixPanel.isMixPanelSupport) {
                                MyMixPanel.initializeMixPanel(getApplicationContext());
                            }

                            if(skipFCM) {

                                startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                                finish();

                            }
                            else {
                                try {
                                    FirebaseApp.initializeApp(mContext);
                                    FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                                        @Override
                                        public void onComplete(@NonNull Task<String> task) {
                                            String token = task.getResult();
                                            String msg = getString(R.string.fcm_token, token);
                                            Log.d("TAG", token);
                                            grantFCMPermission(token);
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    UiMsgs.showToast(mContext, "Firebase exception " + e.getMessage());
                                }
                            }
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaa catch "+e.getMessage());
                        LoadingDialog.dialog.dismiss();
                        UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UiMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa catch "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void grantFCMPermission(final String msg) {
        LoadingDialog.loadDialog(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);

        Log.i("DeliveryApp", "grantFCMPermission FCM_TOKEN "+msg);

        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                FCM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("fcm_res", response);
                    try {
                        LoadingDialog.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            preferenceManager.putString(Constants.FCM_TOKEN, msg);
                            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                            fetchUserDetails();
                            finish();
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        LoadingDialog.dialog.dismiss();
                        System.out.println("aaaaaaaaaa  catch  "+e.getMessage());
                        UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UiMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaaaaa  catch  "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PROFILE_READ, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    Log.d("VolleyResponse", "PROFILE_READ: " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    System.out.println("aaaaaa  responce  "+jsonObject);
                    String unique_id,firstName,lastName,email;
                    unique_id=jsonObjectData.getString("unique_id");
                    firstName=jsonObjectData.getString("first_name");
                    lastName=jsonObjectData.getString("last_name");
                    email=jsonObjectData.getString("email");

                    if (MyMixPanel.isMixPanelSupport) {
                        MyMixPanel.initializeMixPanel(getApplicationContext());
                        MyMixPanel.createUserID(unique_id);
                        MyMixPanel.sendUserLogin(firstName, lastName, email);
                        MyMixPanel.logEvent("User logged into NextClick Main application");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finally {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                 map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                //map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMDA4NCIsInVzZXJkZXRhaWwiOnsiaWQiOiIxMDAwODQiLCJpcF9hZGRyZXNzIjoiMjQwOTo0MDcwOjIxOWE6YTJjNDo6OGIwOmU4YTUiLCJ1c2VybmFtZSI6Imd1ampldGlzaHJhdmFuQGdtYWlsLmNvbSIsInVuaXF1ZV9pZCI6Ik5DVTAyMTEiLCJwYXNzd29yZCI6IiQyeSQwOCQ2b21iQXViMTFhTU9leFRCdUF2SDBPTVJsbFlTZ3FGa0JcL2luMzVQXC9USE94ZlwvUnFGNGhKLiIsInNhbHQiOm51bGwsImVtYWlsIjoiZ3VqamV0aXNocmF2YW5AZ21haWwuY29tIiwid2FsbGV0IjoiMC4wMCIsImFjdGl2YXRpb25fY29kZSI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX2NvZGUiOm51bGwsImZvcmdvdHRlbl9wYXNzd29yZF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjpudWxsLCJjcmVhdGVkX29uIjoiMTU5MDEyMTMxNSIsImxhc3RfbG9naW4iOm51bGwsImFjdGl2ZSI6IjEiLCJsaXN0X2lkIjoiMCIsImZpcnN0X25hbWUiOiJHdWpqZXRpIFNocmF2YW5rdW1hciIsImxhc3RfbmFtZSI6bnVsbCwiY29tcGFueSI6bnVsbCwicGhvbmUiOiIiLCJjcmVhdGVkX3VzZXJfaWQiOm51bGwsInVwZGF0ZWRfdXNlcl9pZCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMjAtMDUtMjIgMDQ6MjE6NTUiLCJ1cGRhdGVkX2F0IjpudWxsLCJkZWxldGVkX2F0IjpudWxsLCJzdGF0dXMiOiIxIn0sInRpbWUiOjE1OTAxMjEzNjB9.gm-lTQiaLcLLYu4KIpjMorFcayjO77IZFulCRlwYlTk");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}