package com.nextclick.deliveryboynew.helpers;

import static android.content.Context.MODE_PRIVATE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import java.util.Locale;


public class Language_Dialog {
    private FragmentActivity fragmentActivity;
    private AppCompatActivity activity;
    private Context context;
    private AlertDialog.Builder builder;


    public Language_Dialog(Context ctx, AppCompatActivity activity, boolean isActivity) {
        this.context = ctx;
        this.activity = activity;
    }

    public Language_Dialog(FragmentActivity ctx, FragmentActivity activity) {
        this.context = ctx;
        this.fragmentActivity = activity;
    }

    public void showDialog() {
        final CharSequence[] items = {"English", "Telugu"};
        builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose Language!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    setLocale("en");
                    if (fragmentActivity != null)
                        fragmentActivity.recreate();
                    else if (activity != null)
                        restartCurrentActivity();

                } else if (item == 1) {
                    setLocale("te");
                    if (fragmentActivity != null)
                        fragmentActivity.recreate();
                    else if (activity != null)
                        restartCurrentActivity();
                }
            }

            private void restartCurrentActivity() {
                Intent intent = activity.getIntent();
                activity.finish();
                activity.startActivity(intent);
            }
        });
        builder.show();
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        locale.setDefault(locale);

        Configuration config = new Configuration();
        config.setLocale(locale);

        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = context.getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_Lang", lang);
        editor.apply();
    }

    //load lang form shared prefrences
    public void LoadLanguage() {
        SharedPreferences pref = context.getSharedPreferences("Settings", MODE_PRIVATE);
        if (pref != null && pref.contains("My_Lang")) {
            String lang = pref.getString("My_Lang", "");
            setLocale(lang);
        }
    }
}
