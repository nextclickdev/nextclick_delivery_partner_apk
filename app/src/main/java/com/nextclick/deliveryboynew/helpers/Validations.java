package com.nextclick.deliveryboynew.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.nextclick.deliveryboynew.utils.PreferenceManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validations {
    /**
     * Verify Blank Fields
     */
    public boolean isBlank(String string) {
        return string.trim().equals("");
    }

    /**
     * @param string
     * @return return true if this is null otherwise false
     */
    public boolean isBlank(EditText string) {
        return string.getText().toString().trim().equals("");
    }

    /**
     * Verify Full Name
     */
    public boolean isValidFullName(EditText fullName) {
        return Pattern.compile("^[\\p{L} .'-]+$", Pattern.CASE_INSENSITIVE).matcher(fullName.getText().toString().trim()).matches();
    }

    /**
     * Match Passwords
     */
    public boolean isMatching(EditText stringFirst, EditText stringSecond) {
        return stringFirst.getText().toString().trim().equals(stringSecond.getText().toString().trim());
    }

    /**
     * Email Validations
     */
    public boolean isValidEmail(String email) {
       /*String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
               + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
       return Pattern.compile(PATTERN_EMAIL, Pattern.CASE_INSENSITIVE).matcher(email.getText().toString().trim()).matches();*/
        String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +

                "\\@" +

                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +

                "(" +

                "\\." +

                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +

                ")+";
        Matcher matcher= Pattern.compile(validemail).matcher(email);
        return !matcher.matches();
    }
    /*
     * ^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]

     *
     * */

    public boolean isValidUrl(String url) {
       /*String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
               + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
       return Pattern.compile(PATTERN_EMAIL, Pattern.CASE_INSENSITIVE).matcher(email.getText().toString().trim()).matches();*/
        String validurl = "^((((https?|http?|ftps?|gopher|telnet|nntp)://)|(mailto:|news:))" +
                "(%[0-9A-Fa-f]{2}|[-()_.!~*';/?:@&=+$,A-Za-z0-9])+)" +
                "([).!';/?:,][[:blank:]])?$";
        Matcher matcher= Pattern.compile(validurl).matcher(url);
        return !matcher.matches();
    }


    public boolean isValidFb(String fb) {
       /*String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
               + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
       return Pattern.compile(PATTERN_EMAIL, Pattern.CASE_INSENSITIVE).matcher(email.getText().toString().trim()).matches();*/
        String validfb = "(?:http:\\/\\/)?(?:www.)?facebook.com\\/(?:(?:\\w)*#!\\/)?(?:pages\\/)?(?:[?\\w\\-]*\\/)?(?:profile.php\\?id=(\\d.*))?([\\w\\-]*)?";
        Matcher matcher= Pattern.compile(validfb).matcher(fb);
        return !matcher.matches();
    }



    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Verify City, State, Country
     */
    public boolean isValidLocation(EditText location) {
        return Pattern.compile("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)", Pattern.CASE_INSENSITIVE).matcher(location.getText().toString().trim()).matches();
    }

    /**
     * Check Postal Location
     */
    public boolean isValidPostalAddress(EditText postalAddress) {
        return Pattern.compile("^[a-zA-Z0-9_\\s\\.,]{1,}$", Pattern.CASE_INSENSITIVE).matcher(postalAddress.getText().toString().trim()).matches();
    }

    /**
     * Check Valid Date
     */
    public boolean isValidDate(EditText date) {
        return Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)", Pattern.CASE_INSENSITIVE).matcher(date.getText().toString().trim()).matches();
    }

    public boolean isValidDate(TextView date) {
        if (date.getText().toString().equalsIgnoreCase("YYYY/MM/DD")) {
            return true;
        } else {
        }
        return false/*Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)", Pattern.CASE_INSENSITIVE).matcher(date.getText().toString().trim()).matches()*/;
    }
//(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)

    /**
     * Check Valid NumberHelper
     */
    public boolean isValidNumber(EditText editText) {
        return Pattern.compile("((\\d{1,9})(((\\.)(\\d{0,2})){0,1}))", Pattern.CASE_INSENSITIVE).matcher(editText.getText().toString().trim()).matches();
    }

    public boolean isValidPassword(String pass) {

       // return Pattern.compile("^[\\p{L} .'-][0-10]+$").matcher(editText.getText().toString().trim()).matches();
        String validpass = "^[\\p{L} .'-][0-10]+$";
        Matcher matcher= Pattern.compile(validpass).matcher(pass);
        return !matcher.matches();


    }
    /**
     * Check Valid Phone NumberHelper
     */
    public boolean isValidPhoneNumber(EditText editText) {
        return Pattern.compile("\"^[7-9][0-9]{9}$\"", Pattern.CASE_INSENSITIVE).matcher(editText.getText().toString().trim()).matches();
    }

    /**
     * checck valid mobile no
     *
     * @param editText
     * @return
     */
    public boolean isValidPhone(String editText) {

//        return Pattern.compile("\\d{18}").matcher(editText.getText().toString().trim()).matches();

        String validemail = "\\d{10}";
        Matcher matcher= Pattern.compile(validemail).matcher(editText);
        return !matcher.matches();
    }

    public boolean isValidSelection(Spinner spinner) {
        try {
            if (spinner.getSelectedItemPosition()==0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;

    }


    public boolean isValidGST(EditText editText) {
        ///^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        //^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$

        return Pattern.compile("\"^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$\"", Pattern.CASE_INSENSITIVE).matcher(editText.getText().toString().trim()).matches();
    }


    public static boolean isValidURL(String url)
    {
        try {
            new URL(url).toURI();
            return true;
        }

        // If there was an Exception
        // while creating URL object
        catch (Exception e) {
            return false;
        }
    }

    public static Bitmap getFacebookProfilePicture(Context context){
        PreferenceManager preferenceManager = new PreferenceManager(context);
        URL imageURL = null;
        try {
            imageURL = new URL(preferenceManager.getString("fb_img_url"));
            Log.d("fbimgurl",imageURL.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
        return bitmap;


    }
}
