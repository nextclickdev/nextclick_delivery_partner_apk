package com.nextclick.deliveryboynew.helpers;

import static android.view.Gravity.CENTER;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nextclick.deliveryboynew.R;

public class LoadingDialog {

    public static Dialog dialog;
    
    public static void loadDialog(Context context){
        ImageView loader_gif;
        dialog = new Dialog(context);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gif_dialog);
        dialog.setCancelable(false);
        window.setGravity(CENTER);


        loader_gif = dialog.findViewById(R.id.loader_gif);
        Glide.with(context)
                .load(R.drawable.loader_gif)
                .into(loader_gif);
        window.setGravity(CENTER);
//        dialog.show();
    }
}
