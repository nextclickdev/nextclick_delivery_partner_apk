package com.nextclick.deliveryboynew.helpers;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nextclick.deliveryboynew.R;
import com.nextclick.deliveryboynew.dashboard.activitys.ui.Orders.OrderDetailsActivity;

public class FloatingWidgetService extends Service {


    private WindowManager mWindowManager;
    private View mFloatingWidget;
    public static ImageView collapsed_iv;
    public static RelativeLayout data_layout,relativeLayoutParent;
    public TextView no_tv,yes_tv;
    public static boolean checkservice=false;
    String orderid,job_id,orderstatus;
 
    public FloatingWidgetService() {

    }

    public static void stopservice() {
        data_layout.setVisibility(View.GONE);
        collapsed_iv.setVisibility(View.GONE);
        relativeLayoutParent.setVisibility(View.GONE);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        orderid=intent.getStringExtra("orderID");
        job_id=intent.getStringExtra("jobID");
        orderstatus=intent.getStringExtra("OrderStatus");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        checkservice=true;
        mFloatingWidget = LayoutInflater.from(this).inflate(R.layout.overlay_layout, null);
       /* final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER | Gravity.LEFT;
        params.x = 0;
        params.y = 100;*/



        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);

            params.gravity = Gravity.CENTER | Gravity.LEFT;
            params.x = 0;
            params.y = 100;
            mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            mWindowManager.addView(mFloatingWidget, params);

        } else {
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.CENTER | Gravity.LEFT;
            params.x = 0;
            params.y = 100;
            mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            mWindowManager.addView(mFloatingWidget, params);
        }

        //mWindowManager.addView(mFloatingWidget, params);

        collapsed_iv=mFloatingWidget.findViewById(R.id.collapsed_iv);
        data_layout=mFloatingWidget.findViewById(R.id.data_layout);
        no_tv=mFloatingWidget.findViewById(R.id.no_tv);
        yes_tv=mFloatingWidget.findViewById(R.id.yes_tv);
        relativeLayoutParent=mFloatingWidget.findViewById(R.id.relativeLayoutParent);

        data_layout.setVisibility(View.GONE);
        collapsed_iv.setVisibility(View.VISIBLE);

        System.out.println("aaaaaaa orderid "+orderid+"  jobid "+job_id+"  orderdtatus  "+orderstatus);
        collapsed_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 data_layout.setVisibility(View.VISIBLE);
                collapsed_iv.setVisibility(View.GONE);
                relativeLayoutParent.setVisibility(View.VISIBLE);
            }
        });

        yes_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               data_layout.setVisibility(View.GONE);
                collapsed_iv.setVisibility(View.GONE);
                relativeLayoutParent.setVisibility(View.GONE);

                try {
                    checkservice=true;
                    Intent i = new Intent(FloatingWidgetService.this, OrderDetailsActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("orderID", orderid);
                    i.putExtra("jobID", job_id);
                    i.putExtra("OrderStatus", orderstatus);

                    startActivity(i);
                    stopSelf();
                }catch (Exception e){
                    System.out.println("aaaaaaaaa   catch  "+e.getMessage());
                }
            }
        });
        no_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data_layout.setVisibility(View.GONE);
                collapsed_iv.setVisibility(View.VISIBLE);
            }
        });

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //if (mFloatingWidget != null) mWindowManager.removeView(mFloatingWidget);
    }

}
