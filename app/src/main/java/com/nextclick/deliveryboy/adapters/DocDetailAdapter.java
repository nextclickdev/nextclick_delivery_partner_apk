package com.nextclick.deliveryboy.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.authentication.activities.SignUpActivity;
import com.nextclick.deliveryboy.authentication.model.DocField;
import com.nextclick.deliveryboy.authentication.model.ImageUploadListener;
import com.nextclick.deliveryboy.helpers.UiMsgs;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class DocDetailAdapter extends RecyclerView.Adapter<DocDetailAdapter.ViewHolder> {

    private final ImageUploadListener imageUploadListener;
    private Context context;
    private List<DocField> list;
    private Integer errorIndex;
    private String message;

    public DocDetailAdapter(Context context, List<DocField> list,ImageUploadListener imageUploadListener) {
        this.context = context;
        this.list = list;
        this.imageUploadListener=imageUploadListener;
        errorIndex=-1;
        message="";
    }
    @Override
    public DocDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.inputlayout, parent, false);
        return new DocDetailAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DocDetailAdapter.ViewHolder holder, int position) {

        DocField field = list.get(position);


        holder.et_aadhar.setVisibility(View.GONE);
        holder.et_pannumber.setVisibility(View.GONE);
        holder.et_drlicencenumber.setVisibility(View.GONE);
        holder.et_vechicalnumber.setVisibility(View.GONE);
        switch (position)
        {
            case 0:
                holder.et_input_filed=holder.et_aadhar;
                holder.et_aadhar.setVisibility(View.VISIBLE);
                break;
            case 1:
                holder.et_input_filed=holder.et_pannumber;
                holder.et_pannumber.setVisibility(View.VISIBLE);
                break;
            case 2:
                holder.et_input_filed=holder.et_drlicencenumber;
                holder.et_drlicencenumber.setVisibility(View.VISIBLE);
                break;
            default:
                holder.et_input_filed=holder.et_vechicalnumber;
                holder.et_vechicalnumber.setVisibility(View.VISIBLE);
                break;
        }

        //if profile-disable ref_layout
        holder.tv_field.setText(field.getImgTitle());
        holder.et_input_filed.setHint(field.getHint());

        if(position==errorIndex) {
            UiMsgs.setEditTextErrorMethod(holder.et_input_filed, message);
        }

       /* if (!field.getDigits().isEmpty()) {
             //holder.et_input_filed.setKeyListener(DigitsKeyListener.getInstance(field.getDigits()));
        }
        else
            holder.et_input_filed.setKeyListener(null);
        holder.et_input_filed.setInputType(field.getInputType());
        if (field.getMaxLength() > 0) {
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(field.getMaxLength());
           // holder.et_input_filed.setFilters(fArray);
        } else {
            holder.et_input_filed.setFilters(null);
        }*/

        String text="";//field.getInputValue()!=null && !field.getInputValue().equals("null")?field.getInputValue():"";
        holder.et_input_filed.setText(text);

       setFrontImage(holder.img_front,field.getFrontImage());
        setFrontImage(holder.img_back,field.getBackImage());

        holder.img_reference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUploadListener.previewImage(field.getPreviewImage());
            }
        });
        holder.img_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUploadListener.uploadImage(field.getImageIndex());
            }
        });
        holder.img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUploadListener.uploadImage(field.getImageIndex()+1);
            }
        });
        holder.et_input_filed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                list.get(holder.getAdapterPosition()).setInputValue(holder.et_input_filed.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void setFrontImage(ImageView imgWidget, String imgData) {
        if (imgData != null && !imgData.isEmpty()) {
            if (!imgData.startsWith("http")) {
                try {
                    byte[] decodedString = Base64.decode(imgData, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    imgWidget.setImageBitmap(decodedByte);

                } catch (Exception ex) {

                }
            } else {
                Glide.with(context)
                        .load(imgData)
                        .placeholder(R.drawable.noimageone)//
                        .into(imgWidget);
            }
        }
        else
        {
            Glide.with(context)
                    .load(R.drawable.noimageone)//
                    .into(imgWidget);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void refresh(ArrayList<DocField> listDocs) {
        this.list=listDocs;
        notifyDataSetChanged();
    }

    public void setEditTextErrorMethod(Integer index, String message) {
        if(list.size()>index)
        {
            this.errorIndex=index;
            this.message=message;
            notifyDataSetChanged();
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextInputLayout tv_input_field;
        EditText et_input_filed;
        EditText et_aadhar,et_pannumber,et_vechicalnumber,et_drlicencenumber;
        TextView tv_field;
        LinearLayout ref_layout;
        ImageView img_reference,img_front,img_back;



        public ViewHolder(View itemView) {
            super(itemView);
            et_aadhar= itemView.findViewById(R.id.et_aadhar);
            et_pannumber= itemView.findViewById(R.id.et_pannumber);
            et_vechicalnumber= itemView.findViewById(R.id.et_vechicalnumber);
            et_drlicencenumber= itemView.findViewById(R.id.et_drlicencenumber);
            tv_field = itemView.findViewById(R.id.tv_field);
            img_reference = itemView.findViewById(R.id.img_reference);
            img_front = itemView.findViewById(R.id.img_front);
            img_back = itemView.findViewById(R.id.img_back);
            ref_layout=itemView.findViewById(R.id.ref_layout);
        }
    }
}

