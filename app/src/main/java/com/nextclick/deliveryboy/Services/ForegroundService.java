package com.nextclick.deliveryboy.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.maps.model.Dash;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.DashboardActivity;

public class ForegroundService  extends Service {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    public static final String KEY_FOREGROUND_SERVICE_ACTION_TYPE = "ForegroundServiceActionType";
    public static final int ACTION_START_FG_SERVICE=1;
    public static final int ACTION_STOP_FG_SERVICE=2;
    public static final int ACTION_UPDATE_FG_SERVICE_NOTIFICATION=3;

    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent!=null)
        {
            int fgServiceType = intent.getIntExtra(KEY_FOREGROUND_SERVICE_ACTION_TYPE,0);
            switch (fgServiceType)
            {
                case ACTION_START_FG_SERVICE:
                    String inputTitle = intent.getStringExtra("inputTitle");
                    String inputBody = intent.getStringExtra("inputBody");
                    createNotificationChannel();
                    Intent notificationIntent = new Intent(this, DashboardActivity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,
                            0, notificationIntent, 0);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                            .setContentTitle(inputTitle)
                            .setContentText(inputBody)
                            .setSmallIcon(R.drawable.nextclick_logo_white)
                            .setContentIntent(pendingIntent);


                    //button
                    Intent buttonActionIntent = new Intent(this,ForegroundServiceBroadcastReceiver.class);
                    PendingIntent actionItent = PendingIntent.getBroadcast(this,
                            1, buttonActionIntent, 0);
                   // builder.addAction(0,"Stop Updates",actionItent);

                    startForeground(1, builder.build());
                    break;
                case  ACTION_UPDATE_FG_SERVICE_NOTIFICATION:
                    break;
                case  ACTION_STOP_FG_SERVICE:
                    stopForeground(true);
                    break;
            }
        }


        return START_NOT_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
