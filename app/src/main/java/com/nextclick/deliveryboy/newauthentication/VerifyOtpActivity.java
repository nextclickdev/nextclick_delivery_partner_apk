package com.nextclick.deliveryboy.newauthentication;

import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static com.nextclick.deliveryboy.Constants.Constants.APP_ID;
import static com.nextclick.deliveryboy.Constants.Constants.APP_ID_VALUE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.deliveryboy.Config.Config;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.authentication.IncomingSms;
import com.nextclick.deliveryboy.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerifyOtpActivity extends AppCompatActivity {


    Button verify_btn;
    TextView mobile_no, resend_otp;
    ProgressBar progressBar;
    EditText verification_code;
    private ImageView signup_back_button;
    LinearLayout contentLayout;

    private String previousForm, mobile,password;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;


    ProgressDialog progressDialog;
    PreferenceManager preferenceManager;
    Context mContext;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        mContext = VerifyOtpActivity.this;
        preferenceManager = new PreferenceManager(mContext);

        previousForm = getIntent().getStringExtra("previousForm");
        mobile = getIntent().getStringExtra("mobile");
        mobile_no = findViewById(R.id.mobile_no);
        mobile_no.setText("(+91) " + mobile);

        verify_btn = findViewById(R.id.verify_btn);
        verification_code = findViewById(R.id.verification_code);
        progressBar = findViewById(R.id.progress_bar);
        resend_otp = findViewById(R.id.resend_otp);
        signup_back_button = findViewById(R.id.signup_back_button);
        resend_otp.setVisibility(View.GONE);//visible after otp received
        verify_btn.setVisibility(View.GONE);
        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        sendOTP(mobile);

    }


    public void goToBack(View view) {
        Intent intent=new Intent(VerifyOtpActivity.this, VerifyAndCreateAccount.class);
        intent.putExtra("mobile",mobile);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
            }
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
        //  LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public void resendOTP(View view) {
        sendOTP(mobile);
    }

    public void veriyOTP(View view) {
        if (verification_code.getText().toString() == null ||
                verification_code.getText().toString().length() == 0) {
            setTextFieldErrorMethod(verification_code, "EMPTY");
        } else if (verification_code.getText().toString().length() < 6) {
            setTextFieldErrorMethod(verification_code, "OTP length should be 6 digits");
        } else {
            verifyOTP(verification_code.getText().toString());
        }
    }

    private void setTextFieldErrorMethod(EditText field, String err_msg) {
        field.setError(err_msg);
        field.requestFocus();
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    private IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  " + message);
                getOtpFromMessage(message);
            }
        }
    };

    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp " + matcher.group(0));
            String otp = matcher.group(0);
            if (otp != null && otp.length() >= 6) {
                verification_code.setText(otp);
                verifyOTP(otp);
            }
        }
    }

    public void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public void showToast(Context context, String msg,int LENGTH_LONG) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    private void sendOTP(String mobile) {

        final ProgressDialog progressDialog = new ProgressDialog(VerifyOtpActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_icon_receipt);
        progressDialog.setMessage("Please wait while sending the otp.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        fcmMap.put("intent", "create_user");
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+json.toString());
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            if(jsonObject.getString("message").equals("USER_ALREADY_EXISTS"))
                            {
                                showAlert("User Already existed with the given mobile number, please proceed with the login",true);
                            }
                            else {
                                resend_otp.setVisibility(View.VISIBLE);
                                verify_btn.setVisibility(View.VISIBLE);
                                //for testing
                               // verification_code.setText(jsonObject.getJSONObject("data").getString("otp"));
                                //verifyOTP(jsonObject.getJSONObject("data").getString("otp"));
                            }
                        } else {
                            showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        showToast(mContext, "Something went wrong", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                } else {
                    progressDialog.dismiss();
                    showToast(mContext, "Server under maintenance", Toast.LENGTH_SHORT);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void verifyOTP(String otp) {
        final ProgressDialog progressDialog = new ProgressDialog(VerifyOtpActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_icon_receipt);
        progressDialog.setMessage("Please wait while validating the otp.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        progressDialog.show();

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    Log.d("VolleyResponse", "VERIFY_OTP: respnse " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {
                        //showAlert(jsonObject.getString("message"));
                        showAlert("Invalid OTP",false);
                    } else {
                        callNextScreen();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                showAlert("Invalid OTP",false);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void callNextScreen()
    {
        Intent intent = new Intent(VerifyOtpActivity.this, UserRegistration.class);//SignUpKYCActivity, WelcomeActivity
        intent.putExtra("mobile", mobile);
        startActivity(intent);
        finish();

        // VerifyOtpActivity.this.finish();
        //VerifyOtpActivity.this.getParent().finish();
    }
    public void showAlert(String message,final  boolean isBackNavigation) {
        AlertDialog.Builder builder = new AlertDialog.Builder(VerifyOtpActivity.this);
        builder.setTitle("Verify Phone");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(isBackNavigation)
                            goToBack(null);
                        else
                            dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}