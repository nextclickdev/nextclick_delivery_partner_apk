package com.nextclick.deliveryboy.newauthentication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.UiAutomation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nextclick.deliveryboy.Config.Config;
import com.nextclick.deliveryboy.FindAddressInMap;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.adapters.DocDetailAdapter;
import com.nextclick.deliveryboy.authentication.model.Constiuencies;
import com.nextclick.deliveryboy.authentication.model.Districts;
import com.nextclick.deliveryboy.authentication.model.DocField;
import com.nextclick.deliveryboy.authentication.model.ImageUploadListener;
import com.nextclick.deliveryboy.authentication.model.States;
import com.nextclick.deliveryboy.authentication.model.VehicleType;
import com.nextclick.deliveryboy.dashboard.activitys.DashboardActivity;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Orders.ProductDetailAdapter;
import com.nextclick.deliveryboy.helpers.LoadingDialog;
import com.nextclick.deliveryboy.helpers.UiMsgs;
import com.nextclick.deliveryboy.utils.AppConstants;
import com.nextclick.deliveryboy.utils.GpsUtils;
import com.nextclick.deliveryboy.utils.PreferenceManager;
import com.nextclick.deliveryboy.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import es.dmoral.toasty.Toasty;

import static com.nextclick.deliveryboy.Config.Config.GET_SHIFT_TYPES;
import static com.nextclick.deliveryboy.Config.Config.GET_TERMSCONDITIONS;
import static com.nextclick.deliveryboy.Config.Config.REGISTER;
import static com.nextclick.deliveryboy.Config.Config.URL_States;
import static com.nextclick.deliveryboy.Config.Config.VEHICLE_TYPES;
import static com.nextclick.deliveryboy.Constants.Constants.APP_ID;
import static com.nextclick.deliveryboy.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.deliveryboy.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.deliveryboy.Constants.Constants.USER_TOKEN;
import static com.nextclick.deliveryboy.helpers.UiMsgs.setEditTextErrorMethod;

public class DeliveryPatnerSignUp extends AppCompatActivity  implements View.OnClickListener, LocationListener, ImageUploadListener {

    TextInputLayout textInputLayout_mobile,textInputLayout_emailid;

    //TextInputEditText et_firstname,et_lastnamename,et_mobilenumber,et_emailid,et_address,et_password,et_confirmpassword;
    TextInputEditText et_address;
    EditText et_adharnumber,et_pannumber,et_vechicalnumber,et_drlicencenumber,et_insurance_number,et_pincode;
    TextView tv_address,btnregister;

    public static final int UPDATE_LOCATION = 999;
    ImageView img_reference_aadhar,img_front_aadhar,img_back_aadhar;
    ImageView img_reference_pan,img_front_pan,img_back_pan;
    ImageView img_reference_vechicalnumber,img_front_vechicalnumber,img_back_vechicalnumber;
    ImageView img_reference_rc,img_front_rc,img_back_rc;

    private ImageView tv_refresh,img_profile,img_bankpassbook,img_canclecheque,img_vehilce_front,img_vehilce_back,img_insurance_front;
    Geocoder geocoder;
    List<Address> addresses;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    String currentAddress;
    private LocationRequest locationRequest;
    private boolean isContinue = false;
    private boolean isGPS = false;
    private String userChoosenTask;
    private String base64image;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Bitmap bm_adhar,bm_pan,bm_drlicence,bm_bankpasbook,bm_cancelcheque,bm_rc,bm_profileimage;
    private String adhar64="",pan64="",bankpas64="",canclecheque64="",rc64="",licence64="",profile64="";
    private int whichimage; // 0- adhar,1- pancard,2 - passbook,3-caclecheque,4-rc,5 - drivinglicence,6=profile
    private Context mContext;
    LocationManager locationManager;
    private ImageView img_back;

    private Matcher matcher;
    private static final String PAN_PATTERN = "(([A-Za-z]{5})([0-9]{4})([a-zA-Z]))";
    private Pattern patternPAN = Pattern.compile(PAN_PATTERN);
    //google-vehicle regex-^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$
    private static final String VECHICAL_PATTERN = "^[A-Z|a-z]{2}\\s?[0-9]{1,2}\\s?[A-Z|a-z]{0,3}\\s?[0-9]{4}$";
    private Pattern patternVECHICAL = Pattern.compile(VECHICAL_PATTERN);
    private static final String DRIVINGLICENCE_PATTERN = "(([A-Z]{2})([0-9]{14}))";
    private Pattern patternDRIVINGLICENCE = Pattern.compile(DRIVINGLICENCE_PATTERN);
    private static final String MOBILE_PATTERN = "^[6-9]\\d{9}$";
    private Pattern patternMobile = Pattern.compile(MOBILE_PATTERN);
    private static final String AADHAR_PATTERN = "^[2-9]{1}[0-9]{11}$";//"[0-9]{12}";
    private Pattern patternAADHAR = Pattern.compile(AADHAR_PATTERN);
    private static final String EMAIL_PATTERN = "[a-z0-9.]+@[a-z]+\\.[a-z]{2,3}";
    private Pattern patternEMAILID = Pattern.compile(EMAIL_PATTERN);

    private CheckBox terms;
    private boolean isCameraPermissionGranted;
    private RecyclerView recyclerview_docs;
    private ArrayList<DocField> listDocs;
    PreferenceManager preferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_patner_sign_up);

        preferenceManager = new PreferenceManager(getApplicationContext());
        init();
        //createtestrecord();
        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                isGPS = isGPSEnable;
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_bankpassbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(2);
            }
        });
        img_canclecheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(3);
            }
        });
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(6);
            }
        });
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        if (!isContinue) {
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                if(addresses!=null && addresses.size()>0) {
                                    String address = addresses.get(0).getAddressLine(0);
                                    String city = addresses.get(0).getLocality();
                                    String state = addresses.get(0).getAdminArea();
                                    String zip = addresses.get(0).getPostalCode();
                                    String country = addresses.get(0).getCountryName();
                                    System.out.println("aaaaaaaaa  address " + address + " 1 " + addresses.get(0).getAddressLine(1));
                                    currentAddress = address;
                                    //tv_address.setText(address);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                           /* stringBuilder.append(wayLatitude);
                            stringBuilder.append("-");
                            stringBuilder.append(wayLongitude);
                            stringBuilder.append("\n\n");
*/
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaaaa  address "+address+" 1 "+addresses.get(0).getAddressLine(1));
                                currentAddress = address;
                                // tv_address.setText(address);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        isContinue = false;
        getLocation();
        getLocationChange();
        tv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson=new Gson();
                ArrayList<String> locArray=new ArrayList<>();
                locArray.add(currentAddress);
                locArray.add(""+wayLatitude);
                locArray.add(""+wayLongitude);
                String locationString = gson.toJson(locArray);
                Intent intent=new Intent(mContext, FindAddressInMap.class);
                intent.putExtra("location",locationString);
                startActivityForResult(intent, UPDATE_LOCATION);
               // getLocation();
            }
        });
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    if (terms.isChecked()){
                        //sendOtp(et_mobilenumber.getText().toString());
                        callRegisterAPI();
                    }else {
                        Toast.makeText(mContext, "Please accept terms and conditions", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gettermsandconditions();
            }
        });
    }

    private void createtestrecord() {

        InputStream is = getResources().openRawResource(R.raw.input);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }

            is.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

        String jsonString = writer.toString();
        register(jsonString.toString());
    }

    public void updateLocation(Intent intent)
    {
        ArrayList<String> locArray=new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = intent.getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if(locArray!=null && locArray.size()>=3) {
                currentAddress=locArray.get(0);
                tv_address.setText(locArray.get(0));
                wayLatitude = Double.parseDouble(locArray.get(1));
                wayLongitude = Double.parseDouble(locArray.get(2));
            }
        }catch (Exception ex) {
            getLocation();
        }
    }


    private final int MY_CAMERA_PERMISSION_CODE =100;
    @Override
    protected void onResume() {
        super.onResume();
        /*try {
            try {
                InputStreamReader inputReader = new InputStreamReader( getResources().openRawResource(R.raw.samle));
                BufferedReader bufReader = new BufferedReader(inputReader);
                String line="";
                String Result="";
                while((line = bufReader.readLine()) != null)
                    Result += line;
                register(Result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }catch (Exception ex){}*/
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);
        }
        else
            isCameraPermissionGranted =true;
    }



    private void gettermsandconditions() {

      /*  Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id", "1");
        final String data = new JSONObject(termsmpa).toString();*/

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_TERMSCONDITIONS+"?page_id=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaa gettransac  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    try{
                                        JSONObject dataobject = jsonObject.getJSONObject("data");
                                        LayoutInflater inflater = getLayoutInflater();
                                        View alertLayout = inflater.inflate(R.layout.activity_web, null);

                                        TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                        AlertDialog.Builder alert = new AlertDialog.Builder(DeliveryPatnerSignUp.this);
                                        alert.setIcon(R.mipmap.ic_launcher);
                                        alert.setTitle(dataobject.getString("title"));
                                        // this is set the view from XML inside AlertDialog
                                        alert.setView(alertLayout);
                                        alert.setCancelable(false);
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                        alert.setCancelable(true);
                                        alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                terms.setChecked(true);
                                                //   accepttermsandconditions(termsandConditions.getId());
                                            }
                                        })
                                            /*.setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Action for 'NO' Button
                                                    terms.setChecked(false);
                                                }
                                            })*/;
                                        AlertDialog dialog = alert.create();
                                        dialog.show();
                                    }catch (Exception e){
                                        JSONArray dataarray = jsonObject.getJSONArray("data");
                                        JSONObject dataobject=dataarray.getJSONObject(0);
                                        LayoutInflater inflater = getLayoutInflater();
                                        View alertLayout = inflater.inflate(R.layout.activity_web, null);

                                        TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                        AlertDialog.Builder alert = new AlertDialog.Builder(DeliveryPatnerSignUp.this);
                                        alert.setIcon(R.mipmap.ic_launcher);
                                        alert.setTitle(dataobject.getString("title"));
                                        // this is set the view from XML inside AlertDialog
                                        alert.setView(alertLayout);
                                        alert.setCancelable(false);
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                        alert.setCancelable(true);
                                        alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                terms.setChecked(true);
                                                //   accepttermsandconditions(termsandConditions.getId());
                                            }
                                        })
                                            /*.setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Action for 'NO' Button
                                                    terms.setChecked(false);
                                                }
                                            })*/;
                                        AlertDialog dialog = alert.create();
                                        dialog.show();
                                    }


                                }


                            } catch (Exception e) {

                                e.printStackTrace();
                                showErrorToast("Termsn and Conditions API failed with "+response);
                            }
                        } else {
                            UiMsgs.showToast(mContext, getString(R.string.maintenance));
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UiMsgs.showToast(mContext, getString(R.string.oops));
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

           /* @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void callRegisterAPI() {

        Map<String, Object> dataMap = new HashMap<>();
        /*dataMap.put("first_name", et_firstname.getText().toString());
        dataMap.put("last_name", et_lastnamename.getText().toString());
        dataMap.put("mobile", et_mobilenumber.getText().toString());
        dataMap.put("email", et_emailid.getText().toString());
        dataMap.put("password", et_password.getText().toString());*/

        dataMap.put("intent", Utility.DPIntent);
        Map<String, String> deliveryBoyAddress = new HashMap<>();//business_address
        deliveryBoyAddress.put("lat", ""+wayLatitude);
        deliveryBoyAddress.put("lng", ""+wayLongitude);
        deliveryBoyAddress.put("line1", et_address.getText().toString());
        deliveryBoyAddress.put("location", ""+tv_address.getText().toString());
        deliveryBoyAddress.put("zip_code", et_pincode.getText().toString());
        deliveryBoyAddress.put("state", ""+stateid);
        deliveryBoyAddress.put("district", ""+districtid);
        deliveryBoyAddress.put("constituency", ""+constitueid);
        dataMap.put("delivery_boy_address", deliveryBoyAddress);


        dataMap.put("vehicle_type_id", ""+selectedVehicle);
        dataMap.put("shift_id", ""+selectedShift);

        dataMap.put("aadhar", et_adharnumber.getText().toString());
        dataMap.put("pan",  et_pannumber.getText().toString());
        dataMap.put("driving_license",  et_drlicencenumber.getText().toString());
        dataMap.put("vehicle_number",  et_vechicalnumber.getText().toString());
        dataMap.put("vehicle_insurance", ""+et_insurance_number.getText().toString());


        dataMap.put("aadhar_card_image_front", ""+listDocs.get(0).getFrontImage());
        dataMap.put("aadhar_card_image_back", ""+listDocs.get(0).getBackImage());
        dataMap.put("pan_card_image_front", ""+listDocs.get(1).getFrontImage());
        dataMap.put("pan_card_image_back", ""+listDocs.get(1).getBackImage());
        dataMap.put("driving_license_image_front", ""+listDocs.get(2).getFrontImage());
        dataMap.put("driving_license_image_back", ""+listDocs.get(2).getBackImage());
        dataMap.put("rc_image_front", ""+listDocs.get(3).getFrontImage());
        dataMap.put("rc_image_back", ""+listDocs.get(3).getBackImage());

        dataMap.put("vehicle_image_front", ""+string_vehilce_front);
        dataMap.put("vehicle_image_back", ""+string_vehilce_back);
        dataMap.put("vehicle_insurance_image_front", ""+string_insurance_front);

        dataMap.put("profile_image", ""+profile64);
        dataMap.put("bank_passbook_image", ""+bankpas64);
        dataMap.put("cancellation_cheque_image", ""+canclecheque64);


        JSONObject json = new JSONObject(dataMap);
        System.out.println("register   json  "+json);
        register(json.toString());

        System.out.println("aaaaaaaaaa   location  "+wayLongitude+"  "+wayLatitude+"  "+tv_address.getText().toString());
    }

    public void init(){

        mContext=DeliveryPatnerSignUp.this;
        textInputLayout_mobile = findViewById(R.id.textInputLayout_mobile_no);
        textInputLayout_emailid = findViewById(R.id.textInputLayout_emailId);

        recyclerview_docs = findViewById(R.id.recyclerview_docs);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview_docs.setHasFixedSize(true);
        recyclerview_docs.setLayoutManager(linearLayoutManager);
        setStaticFields();
        designAddressFields();

        img_vehilce_front= findViewById(R.id.img_vehilce_front);
        img_vehilce_back= findViewById(R.id.img_vehilce_back);
        img_insurance_front= findViewById(R.id.img_insurance_front);
        et_insurance_number= findViewById(R.id.et_insurance_number);
        et_pincode= findViewById(R.id.et_pincode);
        img_vehilce_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(300);
            }
        });
        img_vehilce_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(301);
            }
        });
        img_insurance_front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(302);
            }
        });



       /* et_mobilenumber=findViewById(R.id.et_mobilenumber);
        et_firstname=findViewById(R.id.et_firstname);
        et_lastnamename=findViewById(R.id.et_lastnamename);
        et_emailid=findViewById(R.id.et_emailid);*/
        tv_refresh=findViewById(R.id.tv_refresh);
        tv_address=findViewById(R.id.tv_address);
        et_address=findViewById(R.id.et_address);
        terms=findViewById(R.id.terms);

        img_reference_aadhar=findViewById(R.id.img_reference_aadhar);
        img_front_aadhar=findViewById(R.id.img_front_aadhar);
        img_back_aadhar=findViewById(R.id.img_back_aadhar);
        img_reference_pan=findViewById(R.id.img_reference_pan);
        img_front_pan=findViewById(R.id.img_front_pan);
        img_back_pan=findViewById(R.id.img_back_pan);
        img_reference_vechicalnumber=findViewById(R.id.img_reference_vechicalnumber);
        img_front_vechicalnumber=findViewById(R.id.img_front_vechicalnumber);
        img_back_vechicalnumber=findViewById(R.id.img_back_vechicalnumber);
        img_reference_rc=findViewById(R.id.img_reference_rc);
        img_front_rc=findViewById(R.id.img_front_rc);
        img_back_rc=findViewById(R.id.img_back_rc);

        img_reference_aadhar.setOnClickListener(this);
        img_front_aadhar.setOnClickListener(this);
        img_back_aadhar.setOnClickListener(this);
        img_reference_pan.setOnClickListener(this);
        img_front_pan.setOnClickListener(this);
        img_back_pan.setOnClickListener(this);
        img_reference_vechicalnumber.setOnClickListener(this);
        img_front_vechicalnumber.setOnClickListener(this);
        img_back_vechicalnumber.setOnClickListener(this);
        img_reference_rc.setOnClickListener(this);
        img_front_rc.setOnClickListener(this);
        img_back_rc.setOnClickListener(this);


        img_bankpassbook=findViewById(R.id.img_bankpassbook);
        img_canclecheque=findViewById(R.id.img_canclecheque);

        btnregister=findViewById(R.id.btnregister);
       // et_password=findViewById(R.id.et_password);
        //et_confirmpassword=findViewById(R.id.et_confirmpassword);
        et_adharnumber=findViewById(R.id.et_adharnumber);
        et_pannumber=findViewById(R.id.et_pannumber);
        et_vechicalnumber=findViewById(R.id.et_vechicalnumber);
        et_drlicencenumber=findViewById(R.id.et_drlicencenumber);
        img_profile=findViewById(R.id.img_profile);
        img_back=findViewById(R.id.img_back);


        // et_pannumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_vechicalnumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_drlicencenumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        InputFilter[] filterArray = new InputFilter[2];
        filterArray[0] = new InputFilter.LengthFilter(10);
        filterArray[1] = new InputFilter.AllCaps();
        et_pannumber.setFilters(filterArray);
        et_vechicalnumber.setFilters(filterArray);

        InputFilter[] filterArray1 = new InputFilter[2];
        filterArray1[0] = new InputFilter.LengthFilter(16);
        filterArray1[1] = new InputFilter.AllCaps();
        et_drlicencenumber.setFilters(filterArray1);

        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i=start; i<end; i++) {
                    if (!Character.isLowerCase(source.charAt(i)) &&
                            !Character.isUpperCase(source.charAt(i))) {
                        return null;
                    }
                }
                return null;
            }
        };
        //et_firstname.setFilters(new InputFilter[]{filter});
        //et_lastnamename.setFilters(new InputFilter[]{filter});

        geocoder = new Geocoder(DeliveryPatnerSignUp.this, Locale.getDefault());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds


    }



    private void setStaticFields() {
        listDocs =new ArrayList<>();

        DocField filed=new DocField();
        filed.setHint(getString(R.string.aadhar_details));
        filed.setImgTitle(getString(R.string.aadhar_images));
        filed.setDigits("0123456789");
        filed.setInputType(InputType.TYPE_CLASS_NUMBER);
        filed.setMaxLength(12);
        filed.setImageIndex(101);//front-101,back -102
        filed.setPreviewImage(R.drawable.aadhar_refernce);
        listDocs.add(filed);

        filed=new DocField();
        filed.setHint(getString(R.string.pan_details));
        filed.setImgTitle(getString(R.string.pan_images));
        filed.setDigits("0123456789QWERTYUIOPLKJHGFDSAZXZCVBNM");
        filed.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        filed.setMaxLength(10);
        filed.setImageIndex(103);
        filed.setPreviewImage(R.drawable.pan_reference);
        listDocs.add(filed);

        filed=new DocField();
        filed.setHint(getString(R.string.driving_license_details));
        filed.setImgTitle(getString(R.string.driving_license_images));
        filed.setDigits("0123456789QWERTYUIOPLKJHGFDSAZXZCVBNM");
        filed.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        filed.setMaxLength(16);
        filed.setImageIndex(105);
        filed.setPreviewImage(R.drawable.licence_reference);
        listDocs.add(filed);

        filed=new DocField();
        filed.setHint(getString(R.string.vehicle_details));
        filed.setImgTitle(getString(R.string.rc_images));
        filed.setDigits("0123456789QWERTYUIOPLKJHGFDSAZXZCVBNM");
        filed.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        filed.setMaxLength(10);
        filed.setImageIndex(107);
        filed.setPreviewImage(R.drawable.vehicle_rc_reference);
        listDocs.add(filed);

        // docDetailAdapter = new DocDetailAdapter(getApplicationContext(), listDocs,this);
        //recyclerview_docs.setAdapter(docDetailAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_front_aadhar:
                selectImage(101);
                break;
            case R.id.img_back_aadhar:
                selectImage(102);
                break;
            case R.id.img_front_pan:
                selectImage(103);
                break;
            case R.id.img_back_pan:
                selectImage(104);
                break;
            case R.id.img_front_vechicalnumber:
                selectImage(105);
                break;
            case R.id.img_back_vechicalnumber:
                selectImage(106);
                break;
            case R.id.img_front_rc:
                selectImage(107);
                break;
            case R.id.img_back_rc:
                selectImage(108);
                break;
            case R.id.img_reference_aadhar:
                previewImage(R.drawable.aadhar_refernce);
                break;
            case R.id.img_reference_pan:
                previewImage(R.drawable.pan_reference);
                break;
            case R.id.img_reference_vechicalnumber:
                previewImage(R.drawable.licence_reference);
                break;
            case R.id.img_reference_rc:
                previewImage(R.drawable.vehicle_rc_reference);
                break;
        }
    }

    private boolean isValid() {
        boolean valid = true;

        /*int password_length = et_password.getText().toString().trim().length();
        int confirm_password_length = et_confirmpassword.getText().toString().trim().length();

        if (et_firstname.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_firstname, "Enter your first name");
            valid = false;
        } else if (et_lastnamename.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_lastnamename, "Enter your last name");
            valid = false;
        } else if (et_mobilenumber.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_mobilenumber, "Enter mobile number");
            valid = false;
        } else if (!validateMOBILENUMBER(et_mobilenumber.getText().toString())) {
            setEditTextErrorMethod(et_mobilenumber, getString(R.string.invalid_mobile_number));
            valid = false;
        } else if (!isValidPhoneNumber(et_mobilenumber.getText().toString().trim())) {
            setEditTextErrorMethod(et_mobilenumber, getString(R.string.enter_valid_mobile_number));
            valid = false;
        } else if (et_emailid.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_emailid, "email address");
            valid = false;
        } else if (!validateEMAILID(et_emailid.getText().toString().trim())) {
            setEditTextErrorMethod(et_emailid, getString(R.string.enter_valid_mail_id));
            valid = false;
        } else if (et_password.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_password, "Enter Password");
            valid = false;
        } else if (password_length < 6) {
            setEditTextErrorMethod(et_password, getString(R.string.invalid_password));
            valid = false;
        } else if (et_confirmpassword.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_confirmpassword, "Enter Confirm Password");
            valid = false;
        } else if (confirm_password_length < 6) {
            setEditTextErrorMethod(et_confirmpassword, getString(R.string.invalid_password));
            valid = false;
        } else if (!et_password.getText().toString().equals(et_confirmpassword.getText().toString())) {
            setEditTextErrorMethod(et_confirmpassword, getString(R.string.invalid_password_mismatch));
            valid = false;
        }
        else */if (selectedVehicle.isEmpty()) {
            showErrorToast("Please select the vehicle tye");
            valid = false;
        } else if (selectedShift.isEmpty()) {
            showErrorToast("Please select the Shift type");
            valid = false;
        } else if (et_address.getText() == null || et_address.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_address, "Enter Permanent Address");
            valid = false;
        } else if (constitueid == null || constitueid.isEmpty()) {
            showErrorToast("Please select the constituency");
            valid = false;
        } else if (et_pincode.getText() == null || et_pincode.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_pincode, "Enter Pincode");
            valid = false;
        }
        else if (!validateNewFields()) {
            valid = false;
        } else if (bankpas64.isEmpty()) {
            showErrorToast(getString(R.string.upload_passbook));
            valid = false;
        } else if (canclecheque64.isEmpty()) {
            showErrorToast(getString(R.string.upload_cancel_cheque));
            valid = false;
        }
        return valid;
    }

    private void showErrorDialog(String errorMessage) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_error_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        TextView tv_header= (TextView) dialog.findViewById(R.id.tv_header);
        TextView tv_success= (TextView) dialog.findViewById(R.id.tv_success);
        tv_success.setText(errorMessage);
        //tv_header.setText("Please do the following-");

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void showErrorToast(String message) {
        Toasty.error(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private Boolean validateNewFields() {
        boolean valid = true;


        if(!validateAadhaar(listDocs.get(0)))
        {
            valid=false;
        }
        else if(!validatePan(listDocs.get(1)))
        {
            valid=false;
        }
        else if(!validateLicense(listDocs.get(2)))
        {
            valid=false;
        }//dlicense
        else if(!validateRC(listDocs.get(3)))
        {
            valid=false;
        }//rc

        else if (string_vehilce_front== null ||  string_vehilce_front.isEmpty()) {
            showErrorToast("upload vehicle Front Image");
            valid=false;
        }
        else if (string_vehilce_back== null ||  string_vehilce_back.isEmpty()) {
            showErrorToast("upload vehicle Back Image");
            valid=false;
        }
        else if (et_insurance_number.getText()==null || et_insurance_number.getText().toString().isEmpty()){
            setEditTextErrorMethod(et_insurance_number,"Enter Vehicle Insurance number");
            valid=false;
        }
        /*if (!validateDRIVINGLICENCE(et_drlicencenumber.getText().toString())){
            errorMsg +="Enter Licene number,";
        }*/
        else if (string_insurance_front== null ||  string_insurance_front.isEmpty()) {
            showErrorToast("upload insurance Image");
            valid=false;
        }
        return valid;
    }


    private Boolean validateAadhaar(DocField docField) {
        boolean valid = true;
        if (et_adharnumber.getText() == null || et_adharnumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_adharnumber,"Enter Aadhaar card number");
            valid=false;

        } else if (!validatenAADHAR(et_adharnumber.getText().toString())) {
            setEditTextErrorMethod(et_adharnumber, getString(R.string.upload_valid_aadhaar));
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload Aadhaar Card Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload Aadhaar Card Back Image");
            valid=false;
        }
        return valid;
    }
    private Boolean validatePan(DocField docField) {
        boolean valid = true;
        if (et_pannumber.getText() == null || et_pannumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_pannumber,"Enter Pan card number");
            valid=false;
        } else if (!validatePAN(et_pannumber.getText().toString())) {
            setEditTextErrorMethod(et_pannumber,getString(R.string.upload_valid_pan));
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload Pan Card Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload Pan Card Back Image");
            valid=false;
        }
        return valid;
    }
    private Boolean validateLicense(DocField docField) {
        boolean valid = true;
        if (et_drlicencenumber.getText() == null || et_drlicencenumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_drlicencenumber, "Enter Driving License");
            valid=false;
        } else if (!validateDRIVINGLICENCE(et_drlicencenumber.getText().toString())) {
            setEditTextErrorMethod(et_drlicencenumber, "Enter Valid Driving License");
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload Driving Licence Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload Driving Licence Back Image");
            valid=false;
        }
        return valid;
    }
    private Boolean validateRC(DocField docField) {
        boolean valid = true;
        if (et_vechicalnumber.getText() == null || et_vechicalnumber.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_vechicalnumber, "Enter Vehicle Registration Number");
            valid=false;
        } else if (!validateVECHIClE(et_vechicalnumber.getText().toString())) {
            setEditTextErrorMethod(et_vechicalnumber, "Enter Valid Vehicle Registration Number");
            valid=false;
        }
        else if (docField.getFrontImage().isEmpty()) {
            showErrorToast("Upload RC Front Image");
            valid=false;
        }
        else if (docField.getBackImage().isEmpty()) {
            showErrorToast("Upload RC Back Image");
            valid=false;
        }
        return valid;
    }


    //

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(DeliveryPatnerSignUp.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DeliveryPatnerSignUp.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DeliveryPatnerSignUp.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_REQUEST);

        } else {
            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(DeliveryPatnerSignUp.this, location -> {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        List<Address> addresses  = null;
                        try {
                            addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                            if(addresses!=null && addresses.size()>0) {
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaa address " + address);
                                currentAddress = address;
                                //tv_address.setText(address);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                });
            }
        }
    }
    private void getLocation1() {
        if (ActivityCompat.checkSelfPermission(DeliveryPatnerSignUp.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DeliveryPatnerSignUp.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DeliveryPatnerSignUp.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_REQUEST);

        } else {
            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(DeliveryPatnerSignUp.this, location -> {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        List<Address> addresses  = null;
                        try {
                            addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                            if(addresses!=null && addresses.size()>0) {
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaa address " + address);
                                currentAddress = address;
                                // tv_address.setText(address);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                });
            }
        }
    }
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_CAMERA_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isCameraPermissionGranted =true;
                }
                else
                {
                    Toast.makeText(getApplicationContext(), getString(R.string.accept_camera_permission), Toast.LENGTH_SHORT).show();
                }
            }
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (isContinue) {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    } else {
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(DeliveryPatnerSignUp.this, location -> {
                            if (location != null) {
                                wayLatitude = location.getLatitude();
                                wayLongitude = location.getLongitude();
                                try {
                                    addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                    if(addresses!=null && addresses.size()>0) {
                                        String address = addresses.get(0).getAddressLine(0);
                                        String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String zip = addresses.get(0).getPostalCode();
                                        String country = addresses.get(0).getCountryName();
                                        System.out.println("aaaaaaa address " + address);
                                        currentAddress = address;
                                        //tv_address.setText(address);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        });
                    }
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;

            }case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals(getString(R.string.take_photo)))
                        cameraIntent();
                    else if(userChoosenTask.equals(getString(R.string.choose_from_library)))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;

        }
    }

    private void selectImage(int whichimage) {
        this.whichimage=whichimage;
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(DeliveryPatnerSignUp.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(DeliveryPatnerSignUp.this);

                if (items[item].equals(getString(R.string.take_photo))) {
                    userChoosenTask =getString(R.string.take_photo);
                    if(result)
                        cameraIntent();

                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask =getString(R.string.choose_from_library);
                    if(result)
                        galleryIntent();

                } else if (items[item].equals(getString(R.string.cancel))){
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        if(isCameraPermissionGranted) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
        else
        {
            Toast.makeText(getApplicationContext(), getString(R.string.accept_camera_permission), Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            // onCaptureImageResult(data);
            if (requestCode == AppConstants.GPS_REQUEST) {
                isGPS = true; // flag maintain before get location
            } if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
        else if(resultCode==UPDATE_LOCATION) {
            updateLocation(data);
        }
    }
    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
        setimage(whichimage,bm);

    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setimage(whichimage,thumbnail);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
    }

    private void register(String data) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingDialog.dialog.dismiss();
                if (response != null) {
                    System.out.println("aaaaa  response "+response.toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");
                            Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            showErrorToast( ""+Html.fromHtml(jsonObject.getString("message")));
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaa catch  "+e.getMessage());
                        UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("aaaaaaa catch Server under maintenance ");
                    UiMsgs.showToast(mContext, "Server under maintenance");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, preferenceManager.getString(USER_TOKEN));
                // map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocationChange() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            wayLatitude = lat1;
            wayLongitude = lang1;
            System.out.println("aaaaaaaaa  address 1 "+address);
            currentAddress = address;
            //tv_address.setText(address);


        } catch (Exception e) {

        }
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public boolean validatePAN(String pan){
        matcher = patternPAN.matcher(pan);
        return matcher.matches();
    }
    public boolean validateVECHIClE(String vechicle){
        matcher = patternVECHICAL.matcher(vechicle);
        return matcher.matches();
    }
    public boolean validateDRIVINGLICENCE(String drivinglicence){
        matcher = patternDRIVINGLICENCE.matcher(drivinglicence);
        return matcher.matches();
    }
    public boolean validateMOBILENUMBER(String mobile){
        matcher = patternMobile.matcher(mobile);
        return matcher.matches();
    }
    public boolean validatenAADHAR(String aadhar){
        matcher = patternAADHAR.matcher(aadhar);
        return matcher.matches();
    }
    public boolean validateEMAILID(String emailid){
        matcher = patternEMAILID.matcher(emailid);
        return matcher.matches();
    }



    /**
     * It will show to dialog to enter the otp received to his/her mobile
     */
    private void showVerifyOTPDialog(String otp) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        final EditText editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        final EditText editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
        final EditText editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        final EditText editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        final EditText editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        final EditText editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }

        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextone.length() == 1) {
                    editTextone.clearFocus();
                    editTexttwo.requestFocus();
                    editTexttwo.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTexttwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTexttwo.length() == 1) {
                    editTexttwo.clearFocus();
                    editTextthree.requestFocus();
                    editTextthree.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextthree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextthree.length() == 1) {
                    editTextthree.clearFocus();
                    editTextfour.requestFocus();
                    editTextfour.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextfour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextfour.length() == 1) {
                    editTextfour.clearFocus();
                    editTextFive.requestFocus();
                    editTextFive.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextFive.length() == 1) {
                    editTextFive.clearFocus();
                    editTextSix.requestFocus();
                    editTextSix.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    callVerifyOTPApi(otp);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void sendOtp(final String mobile) {
        LoadingDialog.loadDialog(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        //fcmMap.put("password", et_password.getText().toString());
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();

        System.out.println("sendOtp request "+data.toString());
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa response "+response.toString());
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            showVerifyOTPDialog(null);
                        } else {
                            UiMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UiMsgs.showCustomToast(mContext, "Something went wrong", e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    UiMsgs.showCustomToast(mContext, "Server under maintenance", "");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void callVerifyOTPApi(String otp) {
        LoadingDialog.loadDialog(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        //fcmMap.put("mobile", et_mobilenumber.getText().toString());
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingDialog.dialog.dismiss();

                try {
                    Log.d("VolleyResponse", "VERIFY_OTP: respnse " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {
                        UiMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        showVerifyOTPDialog(otp);
                    } else {
                        callRegisterAPI();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    ArrayList<VehicleType> vehicleTypes;
    ArrayList<VehicleType> shiftTypes;
    private ArrayList<States> statelist;
    private ArrayList<Districts> districtlist;
    private ArrayList<Constiuencies> constiencieslist;
    private String stateid="",districtid="",constitueid="";//test
    Spinner spinner_state,spinner_distict,spinner_constitunesy,spinner_vehicle,spinner_dp_shifts;
    String selectedVehicle="";
    String selectedShift="";
    private void designAddressFields() {
        statelist = new ArrayList<States>();
        districtlist = new ArrayList<Districts>();
        constiencieslist = new ArrayList<Constiuencies>();
        spinner_state = findViewById(R.id.spinner_state);
        spinner_distict = findViewById(R.id.spinner_distict);
        spinner_constitunesy = findViewById(R.id.spinner_constitunesy);
        spinner_vehicle = findViewById(R.id.spinner_vehicle);
        spinner_vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statelist.size() == 0) {

                } else {
                    if (position != 0) {
                        selectedVehicle=vehicleTypes.get(position-1).getId();
                    } else {
                        selectedVehicle="";
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_dp_shifts = findViewById(R.id.spinner_dp_shifts);
        spinner_dp_shifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (shiftTypes.size() == 0) {

                } else {
                    if (position != 0) {
                        selectedShift=shiftTypes.get(position-1).getId();
                    } else {
                        selectedShift="";
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statelist.size() == 0) {

                } else {
                    if (position != 0) {
                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        stateid = statelist.get(position - 1).getId();
                        districtlist.clear();
                        getDistricts(statelist.get(position - 1).getId());
                        // getDistricts(statelist.get(position-1).getStateID());
                    } else {
                        stateid="";
                        districtid="";
                        constitueid="";
                        spinner_distict.setEnabled(false);
                        spinner_distict.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        ArrayList<String> selectlist = new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(DeliveryPatnerSignUp.this,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_distict.setAdapter(ad);
                        spinner_constitunesy.setAdapter(ad);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_distict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtlist.size() == 0) {
                    // Toast.makeText(DeliveryPatnerSignUp.this, "Please Select District", Toast.LENGTH_SHORT).show();
                } else {
                    if (position != 0) {

                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setClickable(true);
                        spinner_constitunesy.setEnabled(true);
                        districtid = districtlist.get(position - 1).getId();
                        constiencieslist.clear();
                        // getConstituencis(districtlist.get(position-1).getDistrictID());
                        getConstituencis(stateid, districtlist.get(position - 1).getId());
                    } else {
                        districtid="";
                        constitueid="";
                        spinner_constitunesy.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        ArrayList<String> selectlist = new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(DeliveryPatnerSignUp.this,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner_constitunesy.setAdapter(ad);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_constitunesy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtid.isEmpty()) {
                    //  Toast.makeText(mContext, "Please select District", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        if (position != 0)
                            constitueid = constiencieslist.get(position - 1).getId();
                        else
                            constitueid="";
                    } catch (IndexOutOfBoundsException e) {
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        getStates();
        getVehicleTypes();
        getShiftTypes();
    }

    private void getVehicleTypes() {
        vehicleTypes=new ArrayList<>();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VEHICLE_TYPES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            System.out.println("VEHICLE_TYPES   sucess " + response.toString());
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    VehicleType vehicleType=new VehicleType();
                                    vehicleType.setId(jsonObject1.getString("id"));
                                    vehicleType.setName(jsonObject1.getString("name"));

                                    vehicleTypes.add(vehicleType);
                                }
                                ArrayList<String> vehicles=new ArrayList<String>();
                                vehicles.add("Select");
                                for (int k=0;k<vehicleTypes.size();k++){
                                    vehicles.add(vehicleTypes.get(k).getName());
                                }

                                ArrayAdapter ad = new ArrayAdapter(DeliveryPatnerSignUp.this,
                                        android.R.layout.simple_spinner_item, vehicles);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_vehicle.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(DeliveryPatnerSignUp.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DeliveryPatnerSignUp.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("VEHICLE_TYPES  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getStates() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    States states=new States();
                                    states.setId(jsonObject1.getString("id"));
                                    states.setName(jsonObject1.getString("name"));
                                    states.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    states.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    states.setCreated_at(jsonObject1.getString("created_at"));
                                    states.setUpdated_at(jsonObject1.getString("updated_at"));
                                    states.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    states.setStatus(jsonObject1.getString("status"));

                                    statelist.add(states);
                                }
                                ArrayList<String> statenames=new ArrayList<String>();
                                statenames.add("Select");
                                int stateid=0;
                                for (int k=0;k<statelist.size();k++){
                                    statenames.add(statelist.get(k).getName());
                                    System.out.println("aaaaaa state "+statelist.get(k).getId()+"  "+statelist.get(k).getName());
                                   /* if (isupdate==1){
                                        if (saveAddress.getState_id().equals(statelist.get(k).getId())){
                                            stateid=k;
                                        }
                                    }*/
                                }

                                ArrayAdapter ad = new ArrayAdapter(DeliveryPatnerSignUp.this,
                                        android.R.layout.simple_spinner_item, statenames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_state.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(DeliveryPatnerSignUp.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DeliveryPatnerSignUp.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getDistricts(String stateID) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States+stateID,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("districts");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);
                                    Districts districts=new Districts();
                                    districts.setId(jsonObject.getString("id"));
                                    districts.setName(jsonObject.getString("name"));
                                    districts.setState_id(jsonObject.getString("state_id"));
                                    districtlist.add(districts);
                                }
                                ArrayList<String> districtnames=new ArrayList<String>();
                                districtnames.add("Select");
                                int distictid=0;
                                System.out.println("aaaaaaa districtlist size "+districtlist.size());
                                for (int k=0;k<districtlist.size();k++){
                                    districtnames.add(districtlist.get(k).getName());
                                    /*if (isupdate==1){
                                        if (saveAddress.getDistrict_id().equals(districtlist.get(k).getId())){
                                            distictid=k;
                                        }
                                    }*/
                                }
                                ArrayAdapter ad = new ArrayAdapter(mContext,
                                        android.R.layout.simple_spinner_item, districtnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_distict.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(DeliveryPatnerSignUp.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getConstituencis(String stateID,String districtid) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States+stateID+"/"+districtid,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        System.out.println("aaaaaaa response const  "+response.toString());
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("constituenceis");

                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);

                                    Constiuencies constiuencies=new Constiuencies();
                                    constiuencies.setId(jsonObject.getString("id"));
                                    constiuencies.setDistrict_id(jsonObject.getString("district_id"));
                                    constiuencies.setName(jsonObject.getString("name"));

                                    constiencieslist.add(constiuencies);
                                }
                                ArrayList<String> constnames=new ArrayList<String>();
                                constnames.add("Select");
                                int conid=0;
                                for (int k=0;k<constiencieslist.size();k++){
                                    constnames.add(constiencieslist.get(k).getName());
                                    /*if (isupdate==1){
                                        if (saveAddress.getDistrict_id().equals(districtlist.get(k).getId())){
                                            conid=k;
                                        }
                                    }*/
                                }
                                ArrayAdapter ad = new ArrayAdapter(mContext,
                                        android.R.layout.simple_spinner_item, constnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_constitunesy.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    //Images

    String string_vehilce_front,string_vehilce_back,string_insurance_front;
    public void setimage(int whichimage,Bitmap bitmap) {
        String convString = convert(bitmap);
        if (whichimage > 100) {
            if (whichimage >= 300) {
                switch (whichimage) {
                    case 300:
                        img_vehilce_front.setImageBitmap(bitmap);
                        string_vehilce_front = convString;
                        break;
                    case 301:
                        img_vehilce_back.setImageBitmap(bitmap);
                        string_vehilce_back = convString;
                        break;
                    case 302:
                        img_insurance_front.setImageBitmap(bitmap);
                        string_insurance_front = convString;
                        break;
                }
            } else {
                switch (whichimage) {
                    case 101:
                        setImage(img_front_aadhar,convString);
                        listDocs.get(0).setFrontImage(convString);
                        break;//aadhar front
                    case 102:
                        setImage(img_back_aadhar,convString);
                        listDocs.get(0).setBackImage(convString);
                        break; //aadhar back
                    case 103:
                        setImage(img_front_pan,convString);
                        listDocs.get(1).setFrontImage(convString);
                        break;
                    case 104:
                        setImage(img_back_pan,convString);
                        listDocs.get(1).setBackImage(convString);
                        break;
                    case 105:
                        setImage(img_front_vechicalnumber,convString);
                        listDocs.get(2).setFrontImage(convString);
                        break;//dL
                    case 106:
                        setImage(img_back_vechicalnumber,convString);
                        listDocs.get(2).setBackImage(convString);
                        break;
                    case 107:
                        setImage(img_front_rc,convString);
                        listDocs.get(3).setFrontImage(convString);
                        break;//rc
                    case 108:
                        setImage(img_back_rc,convString);
                        listDocs.get(3).setBackImage(convString);
                        break;
                }
                // docDetailAdapter.refresh(listDocs);
            }
        }
        else
        {
            switch (whichimage){
                case 2:
                    img_bankpassbook.setImageBitmap(bitmap);
                    bm_bankpasbook=bitmap;
                    bankpas64=convert(bitmap);
                    break;
                case 3:
                    img_canclecheque.setImageBitmap(bitmap);
                    bm_cancelcheque=bitmap;
                    canclecheque64=convert(bitmap);
                    break;
                case 6:
                    img_profile.setImageBitmap(bitmap);
                    bm_profileimage=bitmap;
                    profile64=convert(bitmap);
                    break;
            }
        }
    }

    private void setImage(ImageView imgView, String convString) {
        try {
            byte[] decodedString = Base64.decode(convString, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            imgView.setImageBitmap(decodedByte);

        } catch (Exception ex) {

        }
    }

    @Override
    public void uploadImage(Integer type) {
        selectImage(type);
    }

    @Override
    public void previewImage(Integer image) {
        //image preivew
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.image_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        TextView txt_upload_message = (TextView) dialog.findViewById(R.id.txt_upload_message);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        closeButton.setVisibility(View.GONE);//looking not good
        ImageView img_preview = (ImageView) dialog.findViewById(R.id.img_preview);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Glide.with(this)
                .load(image)
                .placeholder(R.drawable.noimageone)
                .into(img_preview);


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void getShiftTypes() {
        shiftTypes = new ArrayList<>();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_SHIFT_TYPES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int http_code=jsonObject.getInt("status_code");
                            System.out.println("GET_SHIFT_TYPES   sucess " + response.toString());
                            if (http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    VehicleType vehicleType=new VehicleType();
                                    vehicleType.setId(jsonObject1.getString("id"));
                                    vehicleType.setName(jsonObject1.getString("name"));
                                    shiftTypes.add(vehicleType);
                                }
                                ArrayList shifts=new ArrayList<String>();
                                shifts.add("Select");
                                for (int k=0;k<shiftTypes.size();k++){
                                    shifts.add(shiftTypes.get(k).getName());
                                }

                                ArrayAdapter ad = new ArrayAdapter(mContext,
                                        android.R.layout.simple_spinner_item, shifts);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_dp_shifts.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("GET_SHIFT_TYPES  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}