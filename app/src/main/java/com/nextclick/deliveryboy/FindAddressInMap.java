package com.nextclick.deliveryboy;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nextclick.deliveryboy.Config.Config;
import com.nextclick.deliveryboy.dashboard.activitys.DashboardActivity;
import com.nextclick.deliveryboy.utils.Utility;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class FindAddressInMap extends AppCompatActivity implements OnMapReadyCallback, LocationListener,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener
{

    EditText shop_location, latitude_et, longitude_et;
    private Button get_location;
    private GoogleMap mMap;
    Double lattitude=0d, longitude=0d;
    private Location currentLocation;
    Context mContext;
    private boolean locationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1111;
    private String postalCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=this.getApplicationContext();
        setContentView(R.layout.activity_find_address_in_map);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 101);

        }
        shop_location = findViewById(R.id.shop_location);
        latitude_et = findViewById(R.id.latitude_et);
        longitude_et = findViewById(R.id.longitude_et);
        get_location = findViewById(R.id.get_location);
        ImageView img_current_location= findViewById(R.id.img_current_location);
        img_current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentLocation != null)
                    updateLocation(currentLocation);
                else
                    getLocation();
            }
        });
        searchLocation();
        ArrayList<String> locArray=new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = getIntent().getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if(locArray!=null && locArray.size()>=3)
            {
                lattitude = Double.parseDouble(locArray.get(1));
                longitude = Double.parseDouble(locArray.get(2));

                latitude_et.setText(locArray.get(1));
                longitude_et.setText(locArray.get(2));
                shop_location.setText(locArray.get(0));
                if(mMap!=null)
                {
                    setCurrentLocationonMap(true);
                }
            }
        }catch (Exception ex) {
            getLocation();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        get_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //submit
                if(longitude != 0 && lattitude != 0)
                {
                    Intent intent = new Intent();

                    Gson gson=new Gson();
                    ArrayList<String> locArray=new ArrayList<>();
                    locArray.add(shop_location.getText().toString());
                    locArray.add(latitude_et.getText().toString());
                    locArray.add(longitude_et.getText().toString());
                    locArray.add(postalCode);
                    String locationString = gson.toJson(locArray);
                    intent.putExtra("location",locationString);
                    setResult(DashboardActivity.UPDATE_LOCATION, intent);
                    finish();
                }
            }
        });




       /* // Construct a PlacesClient
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        placesClient = Places.createClient(this);

        // Construct a FusedLocationProviderClient.
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);*/


    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
            if(lattitude == null || longitude ==null)
             getLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                }
            }
        }

        if(lattitude == null || longitude ==null)
        getLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocationPermission();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (locationPermissionGranted) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setOnMyLocationButtonClickListener(this);
            mMap.setOnMyLocationClickListener(this);
            mMap.setOnMyLocationClickListener(onMyLocationClickListener);


            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.setMinZoomPreference(11);

            //  LatLng latLng= mMap.getCameraPosition().target;
            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    LatLng latLng= mMap.getCameraPosition().target;
                    updateLocation(latLng);
                }
            });

        } else {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            currentLocation = null;
            getLocationPermission();
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                updateLocation(point);
            }
        });
        if(longitude != 0 && lattitude != 0)
        {
            setCurrentLocationonMap(true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onStop() {
        super.onStop();
        if (locationManager != null)
            locationManager.unregisterGnssStatusCallback(mGnssStatusCallback);
    }

    LocationManager locationManager;
    GnssStatus.Callback mGnssStatusCallback;

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
            checkLocationSettings();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected void checkLocationSettings() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(mContext).checkLocationSettings(mLocationSettingsRequest);
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                }catch(ApiException exception){
                    if(exception.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED){
                        // Toasty.error(mContext, "Please turn on GPS to get location details.", Toast.LENGTH_SHORT).show();
                        ResolvableApiException resolvable = (ResolvableApiException) exception;
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        try {
                            resolvable.startResolutionForResult(
                                    getActcontext(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private Activity getActcontext() {
        return  this;
    }


    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        updateLocation(location);
    }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    Marker marker;
    private void setCurrentLocationonMap(boolean canZoom) {
        LatLng latLng = new LatLng(lattitude, longitude);
        if(marker!=null)
            marker.setVisible(false);
        marker =mMap.addMarker(new MarkerOptions().position(latLng).title(getString(R.string.location))
                .icon(BitmapFromVector(getApplicationContext(), R.drawable.ic_baseline_location_on_24))); //zoom to exact location point
        //if(canZoom)
        //ZoomToPoints(latLng);

        // Position the map's camera at the location of the marker.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));

       /* CameraPosition position = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/
    }
    private void ZoomToPoints(LatLng custLatLong) {
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(custLatLong);
            //builder.include(restLatLong);
            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                public void onCancel() {
                }

                public void onFinish() {
                    CameraUpdate zout = CameraUpdateFactory.zoomBy(-1.0f);//-3.0f
                    mMap.animateCamera(zout);
                }
            });
        } catch (Exception ex) {
            Log.e("failed in map", ex.getMessage());
        }
    }
    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);
        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private void updateLocation(Location location) {
        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            lattitude = Utility.round(location.getLatitude(),3);
            longitude = Utility.round(location.getLongitude(),3);
            latitude_et.setText(lattitude + "");
            longitude_et.setText(longitude + "");
            if(mMap!=null)
            {
                setCurrentLocationonMap(true);
            }
            postalCode=addresses.get(0).getPostalCode();
            String address = addresses.get(0).getAddressLine(0) + "";
            shop_location.setText(address);
        } catch (Exception e) {

        }
    }

    private void updateLocation(LatLng location) {
        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            lattitude = Utility.round(location.latitude,3);
            longitude = Utility.round(location.longitude,3);
            latitude_et.setText(lattitude + "");
            longitude_et.setText(longitude + "");

            if(mMap!=null)
            {
                setCurrentLocationonMap(false);
            }
            List<Address> addresses = geocoder.getFromLocation(lattitude, longitude, 1);
            postalCode=addresses.get(0).getPostalCode();
            String address = addresses.get(0).getAddressLine(0) + "";
            shop_location.setText(address);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        mMap.setMinZoomPreference(15);
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        //Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
        mMap.setMinZoomPreference(12);

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(new LatLng(location.getLatitude(),
                location.getLongitude()));

        circleOptions.radius(200);
        circleOptions.fillColor(Color.RED);
        circleOptions.strokeWidth(6);

        mMap.addCircle(circleOptions);
    }

    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {

                    mMap.setMinZoomPreference(12);

                    CircleOptions circleOptions = new CircleOptions();
                    circleOptions.center(new LatLng(location.getLatitude(),
                            location.getLongitude()));

                    circleOptions.radius(200);
                    circleOptions.fillColor(Color.RED);
                    circleOptions.strokeWidth(6);

                    mMap.addCircle(circleOptions);
                }
            };

    androidx.appcompat.widget.SearchView searchView;
    PlacesClient placesClient;
    public void searchLocation() {
        if(!Places.isInitialized()){
            Places.initialize(getApplicationContext(), Config.PLACES_API_KEY);
        }
        placesClient = Places.createClient(FindAddressInMap.this);

        final AutocompleteSupportFragment autocompleteSupportFragment =
                (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID,Place.Field.LAT_LNG,Place.Field.NAME));

        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                final LatLng latLng = place.getLatLng();
                Log.d("Lat",place.getLatLng()+"");
                Log.d("Name",place.getName()+"");
                Log.d("id",place.getId()+"");
                Log.d("address",place.getAddress()+"");

                lattitude = Utility.round(place.getLatLng().latitude,3);
                longitude = Utility.round(place.getLatLng().longitude,3);
                latitude_et.setText(lattitude + "");
                longitude_et.setText(longitude + "");
                if(mMap!=null)
                {
                    setCurrentLocationonMap(true);
                }
                // postalCode=place.getPlusCode();
                shop_location.setText(place.getAddress());

            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });


     /*   searchView = findViewById(R.id.idSearchView);
        searchView.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String location = searchView.getQuery().toString();
                List<Address> addressList = null;
                if (location != null || location.equals("")) {
                    Geocoder geocoder = new Geocoder(FindAddressInMap.this);
                    try {
                        addressList = geocoder.getFromLocationName(location, 1);
                        Address address = addressList.get(0);
                        lattitude = Utility.round(address.getLatitude(),3);
                        longitude = Utility.round(address.getLongitude(),3);
                        latitude_et.setText(lattitude + "");
                        longitude_et.setText(longitude + "");
                        if(mMap!=null)
                        {
                            setCurrentLocationonMap(true);
                        }
                        postalCode=addressList.get(0).getPostalCode();
                        shop_location.setText(addressList.get(0).getAddressLine(0) + "");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });*/
    }

}