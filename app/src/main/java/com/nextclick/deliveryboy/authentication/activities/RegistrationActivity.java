package com.nextclick.deliveryboy.authentication.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.graphics.BitmapCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.UiAutomation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.helpers.LoadingDialog;
import com.nextclick.deliveryboy.helpers.UiMsgs;
import com.nextclick.deliveryboy.utils.AppConstants;
import com.nextclick.deliveryboy.utils.GpsUtils;
import com.nextclick.deliveryboy.utils.Utility;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.nextclick.deliveryboy.Config.Config.REGISTER;
import static com.nextclick.deliveryboy.helpers.UiMsgs.setEditTextErrorMethod;

public class RegistrationActivity extends Activity implements View.OnClickListener, LocationListener {

    TextInputEditText et_firstname,et_lastnamename,et_mobilenumber,et_emailid,et_address,et_password,et_confirmpassword,
            et_adharnumber,et_pannumber,et_vechicalnumber,et_drlicencenumber;
    TextView tv_address,btnregister;

    private ImageView tv_refresh,img_adhar,img_pancard,img_bankpassbook,img_rc,img_canclecheque,img_drivinglicence,img_profile;
    Geocoder geocoder;
    List<Address> addresses;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LocationRequest locationRequest;
    private boolean isContinue = false;
    private boolean isGPS = false;
    private String userChoosenTask;
    private String base64image;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Bitmap bm_adhar,bm_pan,bm_drlicence,bm_bankpasbook,bm_cancelcheque,bm_rc,bm_profileimage;
    private String adhar64="",pan64="",bankpas64="",canclecheque64="",rc64="",licence64="",profile64="";
    private int whichimage; // 0- adhar,1- pancard,2 - passbook,3-caclecheque,4-rc,5 - drivinglicence,6=profile
    private Context mContext;
    LocationManager locationManager;
    private ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        init();
        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                isGPS = isGPSEnable;
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_adhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(0);
            }
        });
        img_pancard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(1);
            }
        });
        img_bankpassbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(2);
            }
        });
        img_canclecheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(3);
            }
        });
        img_rc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(4);
            }
        });
        img_drivinglicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(5);
            }
        });
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(6);
            }
        });
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        if (!isContinue) {
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaaaa  address "+address+" 1 "+addresses.get(0).getAddressLine(1));
                                tv_address.setText(address);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                           /* stringBuilder.append(wayLatitude);
                            stringBuilder.append("-");
                            stringBuilder.append(wayLongitude);
                            stringBuilder.append("\n\n");
*/
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaaaa  address "+address+" 1 "+addresses.get(0).getAddressLine(1));
                                tv_address.setText(address);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        isContinue = false;
        getLocation();
        getLocationChange();
        tv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    Map<String, String> dataMap = new HashMap<>();
                    dataMap.put("first_name", et_firstname.getText().toString());
                    dataMap.put("last_name", et_lastnamename.getText().toString());
                    dataMap.put("mobile", et_mobilenumber.getText().toString());
                    dataMap.put("email", et_emailid.getText().toString());
                    dataMap.put("password", et_password.getText().toString());
                    dataMap.put("vehicle_number", et_vechicalnumber.getText().toString());
                    dataMap.put("aadhar_number", et_adharnumber.getText().toString());
                    dataMap.put("pan_card_number", et_pannumber.getText().toString());
                    dataMap.put("driving_license_number", et_drlicencenumber.getText().toString());
                    dataMap.put("latitude", ""+wayLatitude);
                    dataMap.put("longitude", ""+wayLongitude);
                    dataMap.put("geo_lcoation_address", ""+tv_address.getText().toString());
                    dataMap.put("aadhar_card_image", ""+adhar64);
                    dataMap.put("pan_card_image", ""+pan64);
                    dataMap.put("bank_passbook_image", ""+bankpas64);
                    dataMap.put("cancellation_cheque_image", ""+canclecheque64);
                    dataMap.put("rc_image", ""+rc64);
                    dataMap.put("driving_license_image", ""+licence64);
                    dataMap.put("profile_image", ""+profile64);


                    JSONObject json = new JSONObject(dataMap);
                    register(json);
                    System.out.println("aaaaaaaaaa   json  "+json);
                    System.out.println("aaaaaaaaaa   location  "+wayLongitude+"  "+wayLatitude+"  "+tv_address.getText().toString());
                  //  Toast.makeText(RegistrationActivity.this, "valid", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void init(){
        et_mobilenumber=findViewById(R.id.et_mobilenumber);
        et_firstname=findViewById(R.id.et_firstname);
        et_lastnamename=findViewById(R.id.et_lastnamename);
        et_emailid=findViewById(R.id.et_emailid);
        tv_refresh=findViewById(R.id.tv_refresh);
        tv_address=findViewById(R.id.tv_address);
        et_address=findViewById(R.id.et_address);
        img_adhar=findViewById(R.id.img_adhar);
        img_pancard=findViewById(R.id.img_pancard);
        img_bankpassbook=findViewById(R.id.img_bankpassbook);
        img_canclecheque=findViewById(R.id.img_canclecheque);
        img_rc=findViewById(R.id.img_rc);
        btnregister=findViewById(R.id.btnregister);
        img_drivinglicence=findViewById(R.id.img_drivinglicence);
        et_password=findViewById(R.id.et_password);
        et_confirmpassword=findViewById(R.id.et_confirmpassword);
        et_adharnumber=findViewById(R.id.et_adharnumber);
        et_pannumber=findViewById(R.id.et_pannumber);
        et_vechicalnumber=findViewById(R.id.et_vechicalnumber);
        et_drlicencenumber=findViewById(R.id.et_drlicencenumber);
        img_profile=findViewById(R.id.img_profile);
        img_back=findViewById(R.id.img_back);


        // et_pannumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_vechicalnumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_drlicencenumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        InputFilter[] filterArray = new InputFilter[2];
        filterArray[0] = new InputFilter.LengthFilter(10);
        filterArray[1] = new InputFilter.AllCaps();
        et_pannumber.setFilters(filterArray);
        et_vechicalnumber.setFilters(filterArray);

        InputFilter[] filterArray1 = new InputFilter[2];
        filterArray1[0] = new InputFilter.LengthFilter(16);
        filterArray1[1] = new InputFilter.AllCaps();
        et_drlicencenumber.setFilters(filterArray1);

        geocoder = new Geocoder(RegistrationActivity.this, Locale.getDefault());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        mContext=RegistrationActivity.this;

       

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_adhar:

                break;
        }
    }

    private boolean isValid() {
        boolean valid = true;

       int password_length=et_password.getText().toString().trim().length();
       int confirm_password_length=et_confirmpassword.getText().toString().trim().length();

        if (et_firstname.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_firstname, getString(R.string.empty));
            valid = false;
        }  else if (et_mobilenumber.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_mobilenumber, getString(R.string.empty));
            valid = false;
        } else if (et_mobilenumber.getText().toString().trim().length() <10) {
            setEditTextErrorMethod(et_mobilenumber, getString(R.string.invalid_mobile));
            valid = false;
        }else if (!isValidPhoneNumber(et_mobilenumber.getText().toString().trim())){
            setEditTextErrorMethod(et_mobilenumber, getString(R.string.invalid_mobile_number));
            valid = false;
        }
        else if (et_emailid.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_emailid, getString(R.string.empty));
            valid = false;
        }else if (!isValidEmail(et_emailid.getText().toString().trim())){
            setEditTextErrorMethod(et_emailid, getString(R.string.invalid_mail_id));
            valid = false;
        }
        else if (et_password.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_password, getString(R.string.empty));
            valid = false;
        } else if (password_length < 6) {
            setEditTextErrorMethod(et_password, getString(R.string.invalid_password));
            valid = false;
        }else if (et_confirmpassword.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_confirmpassword, getString(R.string.empty));
            valid = false;
        } else if (confirm_password_length < 6) {
            setEditTextErrorMethod(et_confirmpassword, getString(R.string.invalid_confirm_password));
            valid = false;
        }else if (!et_password.getText().toString().equals(et_confirmpassword.getText().toString())){
            setEditTextErrorMethod(et_confirmpassword, getString(R.string.invalid_password_mismatch));
            valid = false;
        }else if (et_adharnumber.getText().toString().isEmpty()){
            setEditTextErrorMethod(et_adharnumber, getString(R.string.empty));
            valid = false;
        }else if (et_adharnumber.getText().toString().length()<12){
            setEditTextErrorMethod(et_adharnumber, getString(R.string.min_digits_required_12));
            valid = false;
        }
        else if (adhar64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_aadhar), Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (et_pannumber.getText().toString().isEmpty()){
            setEditTextErrorMethod(et_pannumber, getString(R.string.empty));
            valid = false;
        }else if (et_pannumber.getText().toString().length()<10){
            setEditTextErrorMethod(et_pannumber, getString(R.string.min_digits_required_10));
            valid = false;
        }
        else if (pan64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_pan), Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (bankpas64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_passbook), Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (canclecheque64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_cancel_cheque), Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (et_vechicalnumber.getText().toString().isEmpty()){
            setEditTextErrorMethod(et_vechicalnumber, getString(R.string.empty));
            valid = false;
        }else if (et_vechicalnumber.getText().toString().length()<10){
            setEditTextErrorMethod(et_vechicalnumber, getString(R.string.min_digits_required_10));
            valid = false;
        }
        else if (rc64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_RC), Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (et_drlicencenumber.getText().toString().isEmpty()){
            setEditTextErrorMethod(et_drlicencenumber, getString(R.string.empty));
            valid = false;
        }else if (et_drlicencenumber.getText().toString().length()<16){
            setEditTextErrorMethod(et_drlicencenumber, getString(R.string.min_digits_required_16));
            valid = false;
        }
        else if (licence64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_driving_license), Toast.LENGTH_SHORT).show();
            valid = false;
        }


        return valid;

    }
    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(RegistrationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(RegistrationActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegistrationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_REQUEST);

        } else {
            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(RegistrationActivity.this, location -> {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        List<Address> addresses  = null;
                        try {
                            addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                            String address = addresses.get(0).getAddressLine(0);
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String zip = addresses.get(0).getPostalCode();
                            String country = addresses.get(0).getCountryName();
                            System.out.println("aaaaaaa address "+address);
                            tv_address.setText(address);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                });
            }
        }
    }
    private void getLocation1() {
        if (ActivityCompat.checkSelfPermission(RegistrationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(RegistrationActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegistrationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_REQUEST);

        } else {
            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(RegistrationActivity.this, location -> {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        List<Address> addresses  = null;
                        try {
                            addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                            String address = addresses.get(0).getAddressLine(0);
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String zip = addresses.get(0).getPostalCode();
                            String country = addresses.get(0).getCountryName();
                            System.out.println("aaaaaaa address "+address);
                            tv_address.setText(address);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                });
            }
        }
    }
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (isContinue) {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    } else {
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(RegistrationActivity.this, location -> {
                            if (location != null) {
                                wayLatitude = location.getLatitude();
                                wayLongitude = location.getLongitude();
                                try {
                                    addresses = geocoder.getFromLocation(wayLatitude,wayLongitude, 1);
                                    String address = addresses.get(0).getAddressLine(0);
                                    String city = addresses.get(0).getLocality();
                                    String state = addresses.get(0).getAdminArea();
                                    String zip = addresses.get(0).getPostalCode();
                                    String country = addresses.get(0).getCountryName();
                                    System.out.println("aaaaaaa address "+address);
                                    tv_address.setText(address);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        });
                    }
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;

            }
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;

        }
    }

    private void selectImage(int whichimage) {
        this.whichimage=whichimage;
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(RegistrationActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           // onCaptureImageResult(data);
            if (requestCode == AppConstants.GPS_REQUEST) {
                isGPS = true; // flag maintain before get location
            } if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }
    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
        setimage(whichimage,bm);

    }
    public void setimage(int whichimage,Bitmap bitmap){
        switch (whichimage){
            case 0:
                img_adhar.setImageBitmap(bitmap);
                bm_adhar=bitmap;
                adhar64=convert(bitmap);
                Log.v("image",adhar64);
                break;
            case 1:
                img_pancard.setImageBitmap(bitmap);
                bm_pan=bitmap;
                pan64=convert(bitmap);
                break;
            case 2:
                img_bankpassbook.setImageBitmap(bitmap);
                bm_bankpasbook=bitmap;
                bankpas64=convert(bitmap);
                break;
            case 3:
                img_canclecheque.setImageBitmap(bitmap);
                bm_cancelcheque=bitmap;
                canclecheque64=convert(bitmap);
                break;
            case 4:
                img_rc.setImageBitmap(bitmap);
                bm_rc=bitmap;
                rc64=convert(bitmap);
                break;
            case 5:
                img_drivinglicence.setImageBitmap(bitmap);
                bm_drlicence=bitmap;
                licence64=convert(bitmap);
                break;
                case 6:
                img_profile.setImageBitmap(bitmap);
                bm_profileimage=bitmap;
                profile64=convert(bitmap);
                break;
        }

    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setimage(whichimage,thumbnail);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);


    }

    private void register(JSONObject json) {
        LoadingDialog.loadDialog(mContext);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    System.out.println("aaaaa  response "+response.toString());
                    try {
                        LoadingDialog.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");
                            Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaa catch  "+e.getMessage());
                        LoadingDialog.dialog.dismiss();
                        UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("aaaaaaa catch Server under maintenance ");
                    LoadingDialog.dialog.dismiss();
                    UiMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
               // map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocationChange() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            wayLatitude = lat1;
            wayLongitude = lang1;
            System.out.println("aaaaaaaaa  address 1 "+address);
            tv_address.setText(address);


        } catch (Exception e) {

        }
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}