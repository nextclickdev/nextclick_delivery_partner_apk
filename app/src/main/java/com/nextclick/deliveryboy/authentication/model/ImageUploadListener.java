package com.nextclick.deliveryboy.authentication.model;

public interface ImageUploadListener {
    public void uploadImage(Integer type);
    public void previewImage(Integer image);
}
