package com.nextclick.deliveryboy.authentication.model;

public class DocField {

    String hint,imgTitle,imgFront="",imgBack="",digits,inputValue="";
    Integer inputType,maxLength,imgIndex,refImage;
    public void setHint(String hint)
    {
        this.hint=hint;
    }
    public String getHint()
    {
        return  hint;
    }

    public void setImgTitle(String imgTitle)
    {
        this.imgTitle=imgTitle;
    }
    public String getImgTitle()
    {
        return  imgTitle;
    }

    public void setInputType(Integer inputType)
    {
        this.inputType=inputType;
    }
    public Integer getInputType()
    {
        return  inputType;
    }

    public void setMaxLength(Integer maxLength)
    {
        this.maxLength=maxLength;
    }
    public int getMaxLength() {
        return  maxLength;
    }

    public void setDigits(String digits)
    {
        this.digits=digits;
    }
    public String getDigits() {
        return digits;
    }

    public void setImageIndex(Integer i) {
        this.imgIndex=i;
    }
    public Integer getImageIndex() {
        return imgIndex;
    }

    public void setFrontImage(String imgFront) {
        this.imgFront=imgFront;
    }
    public String getFrontImage() {
        return imgFront;
    }

    public void setBackImage(String imgBack) {
        this.imgBack=imgBack;
    }
    public String getBackImage() {
        return imgBack;
    }

    public void setPreviewImage(Integer refImage) {
        this.refImage=refImage;
    }
    public Integer getPreviewImage() {
        return  refImage;
    }

    public void setInputValue(String inputValue)
    {
        this.inputValue=inputValue;
    }
    /*public String getInputValue() {
        return  inputValue;
    }*/
}
