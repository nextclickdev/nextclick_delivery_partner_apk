package com.nextclick.deliveryboy;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboy.adapters.NotificationsoundsAdapter;
import com.nextclick.deliveryboy.model.NotificationSoundsModel;
import com.nextclick.deliveryboy.utils.PreferenceManager;

import java.util.ArrayList;

public class NotificationSoundsActivity extends AppCompatActivity {

    RecyclerView recycle_sounds;
    private ImageView img_back;
    int sounds[]={R.raw.notification_one,R.raw.notification_two,R.raw.notification_three,
            R.raw.notification_four,R.raw.notification_five,R.raw.notification_six,
            R.raw.notification_seveen};
     NotificationsoundsAdapter notificationsoundsAdapter;
    private PreferenceManager preferenceManager;
    private Button btn_change;
    private int position=0;
    ArrayList<NotificationSoundsModel> listsounds;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_sounds);
        recycle_sounds=findViewById(R.id.recycle_sounds);
        btn_change=findViewById(R.id.btn_change);
        img_back=findViewById(R.id.img_back);

        preferenceManager=new PreferenceManager(NotificationSoundsActivity.this);
        listsounds=new ArrayList<>();
        for (int i=0;i<sounds.length;i++){
            NotificationSoundsModel notificationSoundsModel=new NotificationSoundsModel();
            if (preferenceManager.getInt("sound_position")==i){
                notificationSoundsModel.setIschecked(true);
            }else {
                notificationSoundsModel.setIschecked(false);
            }

            notificationSoundsModel.setSounds(sounds[i]);
            listsounds.add(notificationSoundsModel);
        }


        recycle_sounds.setLayoutManager(new GridLayoutManager(NotificationSoundsActivity.this,1));

        notificationsoundsAdapter=new NotificationsoundsAdapter(NotificationSoundsActivity.this,listsounds);
        recycle_sounds.setAdapter(notificationsoundsAdapter);

        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                preferenceManager.putInt("sound_position",position);
                Toast.makeText(NotificationSoundsActivity.this, "Sucessfully Changed", Toast.LENGTH_SHORT).show();

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void setSound(int position) {
        this.position=position;
        for (int i = 0; i < listsounds.size(); i++) {
            listsounds.get(i).setIschecked(false);
        }
        listsounds.get(position).setIschecked(true);

        notificationsoundsAdapter.setchange(listsounds);

       // notificationsoundsAdapter.notifyDataSetChanged();
       // preferenceManager.putInt("sound_position",position);

    }
}