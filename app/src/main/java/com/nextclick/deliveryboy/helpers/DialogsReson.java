package com.nextclick.deliveryboy.helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Orders.RejectResonsAdapter;
import com.nextclick.deliveryboy.dashboard.model.RejectOrdersRequets;

import java.util.ArrayList;

public class DialogsReson {
    private Context context;
    private Dialog dialog;
   private RecyclerView recycle_resons;
   private TextView tv_title;
   private ImageView img_cancel;
    public DialogsReson(Context ctx, ArrayList<RejectOrdersRequets> rejectlist, String message) {
        this.context = ctx;
        System.out.println("aaaaaaaaaaa dialog open "+rejectlist.size());
        dialog = new Dialog(context);
        //note_list=new ArrayList<>();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rejectdialog);
       // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        tv_title=dialog.findViewById(R.id.tv_title);
        recycle_resons=dialog.findViewById(R.id.recycle_resons);
        img_cancel=dialog.findViewById(R.id.img_cancel);
        recycle_resons.setLayoutManager(new GridLayoutManager(context,1));
        tv_title.setText(""+message);
        RejectResonsAdapter rejectResonsAdapter=new RejectResonsAdapter(ctx,rejectlist,dialog);
        recycle_resons.setAdapter(rejectResonsAdapter);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    public void showDialog() {
        dialog.show();
        dialog.setCancelable(true);
    }
    public void cancle(){
        dialog.dismiss();
    }
}
