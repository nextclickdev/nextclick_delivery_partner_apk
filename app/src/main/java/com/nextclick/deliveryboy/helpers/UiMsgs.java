package com.nextclick.deliveryboy.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import es.dmoral.toasty.Toasty;

import static com.nextclick.deliveryboy.Constants.Constants.ERROR;
import static com.nextclick.deliveryboy.Constants.Constants.INFO;
import static com.nextclick.deliveryboy.Constants.Constants.NORMAL;
import static com.nextclick.deliveryboy.Constants.Constants.SUCCESS;
import static com.nextclick.deliveryboy.Constants.Constants.WARNING;

public class UiMsgs {
    public static ProgressDialog progressDialog;
    private Context context;

    public UiMsgs(Context mContext) {
        this.context = mContext;
    }

    public static void setEditTextErrorMethod(EditText editText, String err_msg){
        editText.setError(err_msg);
        editText.requestFocus();
    }
    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showSnackBar(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        view = snackbar.getView();
        //TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        TextView tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void showCustomToast(Context context, String msg , String type){



        if(type.equalsIgnoreCase(SUCCESS)) {
            Toasty.success(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(ERROR)) {
            Toasty.error(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(WARNING)) {
            Toasty.warning(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(INFO)) {
            Toasty.info(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(NORMAL)) {
            Toasty.normal(context, msg,Toasty.LENGTH_LONG).show();
        }



    }

}
