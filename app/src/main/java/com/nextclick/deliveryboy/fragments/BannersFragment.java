package com.nextclick.deliveryboy.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.nextclick.deliveryboy.R;

public class BannersFragment  extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.banner_fragment, container, false);
        TextView tv = (TextView) v.findViewById(R.id.title);
        tv.setText(getArguments().getString("text"));
        tv.setVisibility(View.GONE);
        ImageView imageView = (ImageView) v.findViewById(R.id.image);
        imageView.setBackgroundResource(getArguments().getInt("img"));
        return v;
    }

    public static Fragment newInstance(String text, int image) {

        BannersFragment f = new BannersFragment();
        Bundle b = new Bundle();
        b.putString("text", text);
        b.putInt("img", image);
        f.setArguments(b);
        return f;
    }
}