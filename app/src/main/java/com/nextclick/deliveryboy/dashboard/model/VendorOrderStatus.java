package com.nextclick.deliveryboy.dashboard.model;

import com.nextclick.deliveryboy.R;

public class VendorOrderStatus {

    public Integer orderVendorStatusID = 103;
    public Integer orderStatusID;
    public String orderStatus;

    public VendorOrderStatus(Integer status)
    {
        orderVendorStatusID=status;
    }

    public Integer getorderStatusID() {
        return orderStatusID;
    }

    public void setorderStatusID(Integer title) {
        this.orderStatusID = title;
    }

    public String getorderStatus() {
        return orderStatus;
    }

    public void setorderStatus(String title) {
        this.orderStatus = title;
    }



    public Integer getorderStatusResource() {
        switch (orderStatusID)
        {
            case 101:
                if(orderVendorStatusID == 101)
                return R.drawable.ic_accpeted;
                else
                    return R.drawable.ic_accpeted;//initial process always accepted
            case 102:
                if(orderVendorStatusID == 102)
                    return R.drawable.ic_accpeted;
                else
                    return R.drawable.ic_reject;
            case 103:
                if(orderVendorStatusID == 103)
                    return R.drawable.ic__status_on_way_delivery;
                else
                    return R.drawable.ic_transfer_within_a_station_24px;
            default:
                return R.drawable.ic_accpeted;
        }
    }
    public Integer getorderStatusBackground() {
        switch (orderStatusID)
        {
            case 101:
                if(orderVendorStatusID == 101)
                    return  0XFFF26B35;
                else
                    return 0XFFE2E2E2;
            case 102:
                if(orderVendorStatusID == 102)
                    return  0XFFF26B35;
                else
                    return  0XFFEBEBEB;
            case 103:
                if(orderVendorStatusID == 103)
                    return  0XFFF26B35;
                else
                    return  0XFFE2E2E2;
            default:
                return  0XFFE2E2E2;
        }
    }
    public Integer getorderStatusForeground() {
        switch (orderStatusID)
        {
            case 101:

                if(orderVendorStatusID == 101)
                    return  0XFFFFFFFF;
                else
                    return 0XFF3F4D5F;
            case 102:

                if(orderVendorStatusID == 102)
                    return  0XFFFFFFFF;
                else
                    return 0XFF3F4D5F;
            case 103:
                if(orderVendorStatusID == 103)
                return  0XFFFFFFFF;
                else
                    return 0XFF3F4D5F;
            default:
                return  0XFF3F4D5F;
        }
    }

}
