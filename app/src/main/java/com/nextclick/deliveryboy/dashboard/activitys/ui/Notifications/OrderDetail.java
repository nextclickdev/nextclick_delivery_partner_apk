package com.nextclick.deliveryboy.dashboard.activitys.ui.Notifications;

import com.nextclick.deliveryboy.dashboard.model.LocationObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderDetail {
    public String orderID;
    String orderStatus;
    public String notificationID;
    public String orderDate;
    public String orderNotificationStatus;
    Integer orderAcceptedtimeRemaining;
    Date orderCreatedDate;

    public String orderDateInSimpleFormat;

    LocationObject vendorLocation,shippingLocation;

    public String orderEarnings;
    public String orderPreparationTime;
    public String shopDistance;
    public String deliveryDistance;
    public  String disountPrice;
    private int setDateInMonth;

    private boolean ispickdrop;

    public OrderDetail() {

    }

    public boolean isIspickdrop() {
        return ispickdrop;
    }

    public void setIspickdrop(boolean ispickdrop) {
        this.ispickdrop = ispickdrop;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getNotificationID() {
        return notificationID;
    }


    public String getOrderNotificationStatus() {
        return orderNotificationStatus;
    }

    public void setOrderNotificationStatus(String orderNotificationStatus) {
        this.orderNotificationStatus = orderNotificationStatus;
    }
    public Integer getOrderAcceptedtimeRemaining() {
        return orderAcceptedtimeRemaining;
    }

    public void setOrderAcceptedtimeRemaining(Integer orderAcceptedtimeRemaining) {
        this.orderAcceptedtimeRemaining = orderAcceptedtimeRemaining;
    }



    public String getShopDistance() {
        return shopDistance;
    }

    public void setShopDistance(String title) {
        this.shopDistance = title;
    }

    public String getDeliveryDistance() {
        return deliveryDistance;
    }

    public void setDeliveryDistance(String title) {
        this.deliveryDistance = title;
    }

    public void setDiscount(String title) {
        this.disountPrice = title;
    }

    public String getDiscount() {
        return disountPrice;
    }

    public void setDateInMonth(int setDayOfMonth) {
        this.setDateInMonth = setDayOfMonth;
    }
    public Integer getDateInMonth() {
        return setDateInMonth;
    }



    //Order object


    public void setOrderID(String title) {
        this.orderID = title;
    }

    public String getorderID() {
        return orderID;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }


    public void setOrderDate(String title) {
        this.orderDate = title;
    }


    public String getOrderDate() {
        return orderDate;
    }

    public String getOrderDateInSimpleFormat() {
        if (orderDateInSimpleFormat == null && orderDate!=null && !orderDate.isEmpty()) {
            try {
                orderCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(orderDate);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                orderDateInSimpleFormat = sdf.format(orderCreatedDate);
            } catch (ParseException e) {
                e.printStackTrace();
                orderDateInSimpleFormat = orderDate;
            }
        }
        return orderDateInSimpleFormat;
    }

    public Date getOrderCreatedDateValue() {
        if (orderCreatedDate == null && orderDate != null && !orderDate.isEmpty()) {
            try {
                orderCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(orderDate);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                orderDateInSimpleFormat = sdf.format(orderCreatedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return orderCreatedDate;
    }


    public String getOrderEarnings() {
        return orderEarnings;
    }

    public void setOrderEarnings(String title) {
        this.orderEarnings = title;
    }

    public String getOrderPreparationTime() {
        return orderPreparationTime;
    }

    public void setOrderPreparationTime(String title) {
        this.orderPreparationTime = title;
    }

    public LocationObject getVendorLocation() {
        return vendorLocation;
    }

    public void setVendorLocation(LocationObject vendorLocation) {
        this.vendorLocation = vendorLocation;
    }
    public LocationObject getShippingLocation() {
        return shippingLocation;
    }

    public void setShippingLocation(LocationObject shippingLocation) {
        this.shippingLocation = shippingLocation;
    }


    String TrackingID;

    public void setTrackingID(String title) {
        this.TrackingID = title;
    }

    public String getTrackingID() {
        return TrackingID;
    }

    String VendorUserID;

    public void setVendorUserID(String title) {
        this.VendorUserID = title;
    }

    public String getVendorUserID() {
        return VendorUserID;
    }
    String ShippingID;

    public void setShippingID(String title) {
        this.ShippingID = title;
    }

    public String getShippingID() {
        return ShippingID;
    }

    String OrderStatusId;

    public void setOrderStatusId(String title) {
        this.OrderStatusId = title;
    }

    public String getOrderStatusId() {
        return OrderStatusId;
    }

    String OrderCreatedDate;

    public void setOrderCreatedDate(String title) {
        this.OrderCreatedDate = title;
    }

    public String getOrderCreatedDate() {
        return OrderCreatedDate;
    }

}

