package com.nextclick.deliveryboy.dashboard.activitys.ui.Orders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.OrdersFragment;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Notifications.OrderDetail;
import com.nextclick.deliveryboy.dashboard.model.RejectOrdersRequets;
import com.nextclick.deliveryboy.model.DeliveryOrder;
import com.nextclick.deliveryboy.model.DeliveryOrderStatus;

import java.util.ArrayList;
import java.util.List;

public class RejectResonsAdapter extends RecyclerView.Adapter<RejectResonsAdapter.ViewHolder> {

    private Context context;
    private List<RejectOrdersRequets> list;
    Dialog dialog;

    public RejectResonsAdapter(Context context,
                               List<RejectOrdersRequets> list, Dialog dialog) {
        this.context = context;
        this.list = list;
        this.dialog = dialog;
    }

    @Override
    public RejectResonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.rejectresonadapter, parent, false);
        //View v = LayoutInflater.from(context).inflate(R.layout.view_show_orders, parent, false);
        return new RejectResonsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RejectResonsAdapter.ViewHolder holder, int position) {
        RejectOrdersRequets rejectOrdersRequets = list.get(position);

        holder.tv_name.setText(""+rejectOrdersRequets.getName());

        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OrderDetailsActivity)context).setrejectOrder(rejectOrdersRequets);
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        public ViewHolder(View itemView) {
            super(itemView);

            tv_name=itemView.findViewById(R.id.tv_name);

        }
    }
}
