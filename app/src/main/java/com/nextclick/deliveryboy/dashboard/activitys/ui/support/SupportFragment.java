package com.nextclick.deliveryboy.dashboard.activitys.ui.support;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.nextclick.deliveryboy.Config.Config;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.DashboardActivity;
import com.nextclick.deliveryboy.helpers.CustomDialog;
import com.nextclick.deliveryboy.helpers.LoadingDialog;
import com.nextclick.deliveryboy.helpers.UiMsgs;
import com.nextclick.deliveryboy.model.DeliveryOrder;
import com.nextclick.deliveryboy.utils.PreferenceManager;
import com.nextclick.deliveryboy.utils.mixpanelutil.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static com.nextclick.deliveryboy.Constants.Constants.USER_TOKEN;

public class SupportFragment extends Fragment {


    private ArrayList<SupportModel> supportlist;
    CustomDialog mCustomDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private RecyclerView recycle_support;
    private TextView tv_nodata;
    private SupportListAdapter supportListAdapter;
    private FloatingActionButton add_product_fab;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_support, container, false);
        recycle_support=root.findViewById(R.id.recycle_support);
        tv_nodata=root.findViewById(R.id.tv_nodata);
        add_product_fab=root.findViewById(R.id.fab);

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to Support activity");
        }

        mContext=getActivity();
        supportlist=new ArrayList<>();
        mCustomDialog=new CustomDialog(mContext);
        preferenceManager=new PreferenceManager(mContext);
        recycle_support.setLayoutManager(new GridLayoutManager(mContext,1));
        supportListAdapter=new SupportListAdapter(mContext,supportlist);
        recycle_support.setAdapter(supportListAdapter);


        add_product_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mContext,AddSupportActivity.class);
                startActivity(i);
            }
        });
        return  root;
    }


    @Override
    public void onResume() {
        super.onResume();
        getSupportList();
    }

    public void getSupportList(){
        supportlist.clear();

        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SUPPORT_QUERIES_LIST,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {

                        try {
                            if (mCustomDialog!=null)
                            {
                                if (mCustomDialog.isShowing())
                                {
                                    try{
                                        mCustomDialog.dismiss();
                                    }catch (IllegalArgumentException e){

                                    }

                                }
                            }
                            //  mCustomDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");

                                JSONArray jsonArray=jsonObject.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    SupportModel supportModel=new SupportModel();
                                    supportModel.setId(jsonObject1.getString("id"));
                                    supportModel.setToken_no(jsonObject1.getString("token_no"));
                                    supportModel.setApp_details_id(jsonObject1.getString("app_details_id"));
                                    supportModel.setRequest_type_id(jsonObject1.getString("request_type_id"));
                                    supportModel.setMobile(jsonObject1.getString("mobile"));
                                    supportModel.setEmail(jsonObject1.getString("email"));
                                    supportModel.setSubject(jsonObject1.getString("subject"));
                                    supportModel.setMessage(jsonObject1.getString("message"));
                                    supportModel.setQuery_owner_id(jsonObject1.getString("query_owner_id"));
                                    supportModel.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    supportModel.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    supportModel.setCreated_at(jsonObject1.getString("created_at"));
                                    supportModel.setUpdated_at(jsonObject1.getString("updated_at"));
                                    supportModel.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    supportModel.setStaus(jsonObject1.getString("staus"));
                                    supportModel.setApp_id(jsonObject1.getString("app_id"));
                                    supportModel.setApp_name(jsonObject1.getString("app_name"));
                                    supportModel.setTitle(jsonObject1.getString("title"));

                                    supportlist.add(supportModel);
                                }

                                if (supportlist.size()!=0){
                                    tv_nodata.setVisibility(View.GONE);
                                    recycle_support.setVisibility(View.VISIBLE);
                                    supportListAdapter.setrefresh(supportlist);
                                }else {
                                    tv_nodata.setVisibility(View.VISIBLE);
                                    recycle_support.setVisibility(View.GONE);
                                }
                            }

                        } catch (JSONException e) {
                            if (mCustomDialog!=null)
                            {
                                if (mCustomDialog.isShowing())
                                {
                                    try{
                                        mCustomDialog.dismiss();
                                    }catch (IllegalArgumentException e1){

                                    }
                                }
                            }

                            tv_nodata.setVisibility(View.VISIBLE);
                            recycle_support.setVisibility(View.GONE);
                            //   mCustomDialog.dismiss();
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mCustomDialog!=null)
                {
                    if (mCustomDialog.isShowing())
                    {
                        try{
                            mCustomDialog.dismiss();
                        }catch (IllegalArgumentException e){

                        }
                    }
                }
                tv_nodata.setVisibility(View.VISIBLE);
                recycle_support.setVisibility(View.GONE);
                //  mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN",preferenceManager.getString(USER_TOKEN));
                // map.put("APP_ID",APP_ID_VALUE);

                return map;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
