package com.nextclick.deliveryboy.dashboard.activitys.ui.Notifications;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.model.DirectionObject;
import com.nextclick.deliveryboy.utils.DirectionListener;
import com.nextclick.deliveryboy.utils.LocationUtil.LocationDetailUtil;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    private final NotificationsFragment notificationsFragment;
    private Context context;
    private List<OrderDetail> list;
    

    public OrdersAdapter(NotificationsFragment notificationsFragment, Context context, List<OrderDetail> list) {
        this.context = context;
        this.list = list;
        this.notificationsFragment=notificationsFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
        return new ViewHolder(v);
    }

    int timeRemaining = 0;

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderDetail orderDetail = list.get(position);

        holder.orderID.setText(orderDetail.getTrackingID());
        holder.orderDate.setText(String.valueOf(orderDetail.getOrderDateInSimpleFormat()));
        holder.orderEarnings.setText(String.valueOf(orderDetail.getOrderEarnings()));
        holder.orderPreparationTime.setText(String.valueOf(orderDetail.getOrderPreparationTime()));

        holder.btn_order_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationsFragment.setAcceptNotification(orderDetail);
            }
        });

        /*String text = String.format(Locale.getDefault(), "Time Remaining %02d min: %02d sec",
                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);
        holder.orderAcceptedtimeRemaining.setText(text);*/

        if (holder.timer != null) {
            holder.timer.cancel();
        }
        holder.btn_order_reject.setVisibility(View.GONE);

        if(orderDetail.getOrderCreatedDateValue()!=null) {


            long remSeconds = 60;//(Calendar.getInstance().getTime().getTime()-orderDetail.getOrderCreatedDate().getTime())/1000;

            if (orderDetail.getOrderAcceptedtimeRemaining() > 0 && remSeconds <= 60) {
                holder.btn_order_accept.setVisibility(View.VISIBLE);
                long millisecInFuture = 1000 * remSeconds;//1000 * 60 //10000;

                holder.timer = new CountDownTimer(millisecInFuture, 100) {
                    @Override
                    public void onTick(long ms) {
                        if (Math.round((float) ms / 1000.0f) != timeRemaining) {
                            timeRemaining = Math.round((float) ms / 1000.0f);
                            holder.orderAcceptedtimeRemaining.setText("" + timeRemaining);
                        }
                    }

                    @Override
                    public void onFinish() {
                        holder.orderAcceptedtimeRemaining.setText("0");
                        orderDetail.setOrderAcceptedtimeRemaining(0);
                        holder.btn_order_accept.setVisibility(View.GONE);
                    }
                }.start();
            } else {
                holder.orderAcceptedtimeRemaining.setText("0");
                holder.btn_order_accept.setVisibility(View.GONE);
            }
        }else {
            holder.orderAcceptedtimeRemaining.setText("0");
            holder.btn_order_accept.setVisibility(View.GONE);
        }

        /*if (orderDetail.getOrderAcceptedtimeRemaining() > 0) {
            holder.btn_order_accept.setVisibility(View.VISIBLE);
            timeRemaining = 0;
            long millisecInFuture =1000*orderDetail.getOrderAcceptedtimeRemaining(); //10000;
            new CountDownTimer(10000, 100) {
                public void onTick(long ms) {
                    if (Math.round((float) ms / 1000.0f) != timeRemaining) {
                        timeRemaining = Math.round((float) ms / 1000.0f);
                        holder.orderAcceptedtimeRemaining.setText("" + timeRemaining);
                    }
                    Log.i("test", "ms=" + ms + " till finished=" + timeRemaining);
                }

                public void onFinish() {
                    holder.orderAcceptedtimeRemaining.setText("0");
                    orderDetail.setOrderAcceptedtimeRemaining(0);
                    holder.btn_order_accept.setVisibility(View.GONE);
                }
            }.start();
        }
        else
            holder.btn_order_accept.setVisibility(View.GONE);*/


       /*timeRemaining = 100;
       new CountDownTimer(10000, 1000){
            public void onTick(long millisUntilFinished){
                holder.orderAcceptedtimeRemaining.setText(String.valueOf(timeRemaining));
                timeRemaining--;
            }
            public  void onFinish(){

                holder.orderAcceptedtimeRemaining.setText("Time OUT!!");
            }
        }.start();*/

        try{
            if(orderDetail.getVendorLocation().getTravelDistance()==null) {

                DirectionListener shopDistanceListener = new DirectionListener() {
                    @Override
                    public void onDirectionResult(DirectionObject result) {
                        if (result != null) {
                            orderDetail.getVendorLocation().setTravelDistance(result.getTravelDistance());
                            holder.shopDistance.setText(result.getTravelDistance());

                            Log.i("DirectionListener", "shopDistanceListener at "+position+", and the result is "+result.getTravelDistance());
                        }
                    }
                };
                LocationDetailUtil.getTravelDistanceFromCurrentLocation(context, shopDistanceListener, orderDetail.getVendorLocation().getlongitude(),
                        orderDetail.getVendorLocation().getLatitude());
            }
            else
            {
                holder.shopDistance.setText(orderDetail.getVendorLocation().getTravelDistance());
            }
            if(orderDetail.getShippingLocation().getTravelDistance()==null) {
                DirectionListener deliveryDistanceListener = new DirectionListener() {
                    @Override
                    public void onDirectionResult(DirectionObject result) {
                        if (result != null) {
                            orderDetail.getShippingLocation().setTravelDistance(result.getTravelDistance());
                            holder.deliveryDistance.setText(result.getTravelDistance());
                            Log.i("DirectionListener", "deliveryDistance at "+position+", and the result is "+result.getTravelDistance());
                        }
                    }
                };

                LocationDetailUtil.getTravelDistanceFromCurrentLocation(context, deliveryDistanceListener, orderDetail.getShippingLocation().getlongitude(),
                        orderDetail.getShippingLocation().getLatitude());
            }
            else
            {
                holder.deliveryDistance.setText(orderDetail.getShippingLocation().getTravelDistance());
            }

        }catch (Exception e){

        }


        //timer
        //holder.orderAcceptedtimeRemaining.setText(String.valueOf(orderDetail.getOrderAcceptedtimeRemaining()));

        // holder.shopDistance.setText(orderDetail.getTitle());
        //  holder.deliveryDistance.setText(String.valueOf(orderDetail.getRating()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView orderID, orderDate, orderEarnings,shopDistance, deliveryDistance, orderPreparationTime,orderAcceptedtimeRemaining;

        Button btn_order_accept,btn_order_reject;

        CountDownTimer timer;

        public ViewHolder(View itemView) {
            super(itemView);

            orderID = itemView.findViewById(R.id.tv_order_id);
            orderDate = itemView.findViewById(R.id.tv_order_created_date);
            orderEarnings = itemView.findViewById(R.id.tv_order_earnings);
            shopDistance = itemView.findViewById(R.id.tv_shop_distance);
            deliveryDistance = itemView.findViewById(R.id.tv_delivery_distance);
            orderPreparationTime = itemView.findViewById(R.id.tv_current_order_preparation_time);
            orderAcceptedtimeRemaining = itemView.findViewById(R.id.tv_current_order_time_remaining);

            btn_order_accept= itemView.findViewById(R.id.btn_order_accept);
            btn_order_reject= itemView.findViewById(R.id.btn_order_reject);


        }
    }
}
