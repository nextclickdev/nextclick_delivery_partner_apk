package com.nextclick.deliveryboy.dashboard.activitys.ui.Notifications;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.deliveryboy.Config.Config;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.DashboardActivity;
import com.nextclick.deliveryboy.dashboard.model.LocationObject;
import com.nextclick.deliveryboy.helpers.LoadingDialog;
import com.nextclick.deliveryboy.utils.LocationUtil.LocationDetailUtil;
import com.nextclick.deliveryboy.utils.PreferenceManager;
import com.nextclick.deliveryboy.utils.mixpanelutil.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.deliveryboy.Constants.Constants.MIXPANEL_INTEGRATED;
import static com.nextclick.deliveryboy.Constants.Constants.USER_TOKEN;

public class NotificationsFragment extends Fragment {


    private RecyclerView recyclerview_notifications;
    PreferenceManager preferenceManager;
    Context mContext;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<OrderDetail> OrderDetailList;
    private RecyclerView.Adapter adapter;
    LinearLayout layout_no_order;

    boolean networkStatus;

    public NotificationsFragment(boolean checked)
    {
        networkStatus= checked;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notification, container, false);

        recyclerview_notifications = root.findViewById(R.id.recyclerview_notifications);
        layout_no_order = root.findViewById(R.id.layout_no_order);
        OrderDetailList = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(this.getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(recyclerview_notifications.getContext(), linearLayoutManager.getOrientation());

        recyclerview_notifications.setHasFixedSize(true);
        recyclerview_notifications.setLayoutManager(linearLayoutManager);
        recyclerview_notifications.addItemDecoration(dividerItemDecoration);

        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to Notification activity");
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocationDetailUtil.getCurrentLocation(mContext,getViewLifecycleOwner());
        getNotifications();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * get notification from the server
     */
    private void getNotifications()  {
        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_NOTIFICATIONS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);

                    Log.d("VolleyResponse", "notifications response: " + response);


                    JSONObject jsonObject = new JSONObject(response);
                    boolean status=jsonObject.getBoolean("status");

                    if (!status) {
                        recyclerview_notifications.setVisibility(View.GONE);
                        layout_no_order.setVisibility(View.VISIBLE);
                    }
                    else {
                        parseJsonData(jsonObject);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();

                }
                finally {
                    LoadingDialog.dialog.dismiss();

                    if (OrderDetailList == null || OrderDetailList.size() == 0) {
                        recyclerview_notifications.setVisibility(View.GONE);
                        layout_no_order.setVisibility(View.VISIBLE);
                    }
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /**
     * Parse the notifications result
     * @param jsonObject
     * @throws JSONException
     */
    private void parseJsonData(JSONObject jsonObject) throws JSONException {
        JSONArray dataArray = jsonObject.getJSONArray("data");
        if (dataArray !=null && dataArray.length() > 0) {
            OrderDetailList = new ArrayList<>();
            for (int i = 0; i < dataArray.length(); i++) {
                OrderDetail orderDetail = new OrderDetail();

                JSONObject notficationJsonObject = dataArray.getJSONObject(i);
                orderDetail.setNotificationID(notficationJsonObject.getString("id"));
                orderDetail.setOrderNotificationStatus(notficationJsonObject.getString("status"));
                orderDetail.setOrderDate(notficationJsonObject.getString("created_at"));

                //test
                /*orderDetail.setOrderPreparationTime("2 Mints");
                orderDetail.setOrderEarnings("10000");*/
                orderDetail.setOrderAcceptedtimeRemaining(60);//60sec
                //

                try{
                    JSONObject orderJsonObject = notficationJsonObject.getJSONObject("order");
                    try {

                        orderDetail.setOrderID(orderJsonObject.getString("id"));
                        orderDetail.setOrderEarnings(orderJsonObject.getString("delivery_fee"));
                        orderDetail.setOrderPreparationTime(orderJsonObject.getString("preparation_time"));


                        orderDetail.setTrackingID(orderJsonObject.getString("track_id"));
                        orderDetail.setVendorUserID(orderJsonObject.getString("vendor_user_id"));
                        orderDetail.setShippingID(orderJsonObject.getString("shipping_address_id"));
                        orderDetail.setOrderStatusId(orderJsonObject.getString("order_status_id"));
                        orderDetail.setOrderCreatedDate(orderJsonObject.getString("created_at"));
                    } catch (Exception ex) {
                    }

                    orderDetail.setIspickdrop(false);
                    JSONObject vendorLocationObject = orderJsonObject.getJSONObject("vendor_location");
                    JSONObject shippingLocationObject = orderJsonObject.getJSONObject("shipping_location");
                    LocationObject vendorLocation = new LocationObject();
                    vendorLocation.setLocationID(vendorLocationObject.getString("id"));
                    vendorLocation.setLatitude(vendorLocationObject.getString("latitude"));
                    vendorLocation.setLongitude(vendorLocationObject.getString("longitude"));
                    vendorLocation.setAddress(vendorLocationObject.getString("address"));
                    orderDetail.setVendorLocation(vendorLocation);

                    LocationObject shippingLocation = new LocationObject();
                    shippingLocation.setLocationID(shippingLocationObject.getString("id"));
                    shippingLocation.setLatitude(shippingLocationObject.getString("latitude"));
                    shippingLocation.setLongitude(shippingLocationObject.getString("longitude"));
                    shippingLocation.setAddress(shippingLocationObject.getString("address"));
                    orderDetail.setShippingLocation(shippingLocation);


                }catch (Exception ex){

                }

                try{
                    JSONObject orderJsonObject = notficationJsonObject.getJSONObject("pickup_order");
                    try {
                        orderDetail.setOrderID(orderJsonObject.getString("id"));
                        orderDetail.setOrderEarnings(orderJsonObject.getString("delivery_fee"));
                       // orderDetail.setOrderPreparationTime(orderJsonObject.getString("preparation_time"));

                        orderDetail.setTrackingID(orderJsonObject.getString("track_id"));
                       // orderDetail.setVendorUserID(orderJsonObject.getString("vendor_user_id"));
                       // orderDetail.setShippingID(orderJsonObject.getString("shipping_address_id"));
                        orderDetail.setOrderStatusId(orderJsonObject.getString("order_status_id"));
                        orderDetail.setOrderCreatedDate(orderJsonObject.getString("created_at"));
                    } catch (Exception ex) {
                    }

                    orderDetail.setIspickdrop(true);

                    JSONObject vendorLocationObject = orderJsonObject.getJSONObject("pickup_location");
                    JSONObject shippingLocationObject = orderJsonObject.getJSONObject("delivery_location");

                    LocationObject vendorLocation = new LocationObject();
                    vendorLocation.setLocationID(vendorLocationObject.getString("id"));
                    vendorLocation.setLatitude(vendorLocationObject.getString("latitude"));
                    vendorLocation.setLongitude(vendorLocationObject.getString("longitude"));
                    vendorLocation.setAddress(vendorLocationObject.getString("address"));
                    orderDetail.setVendorLocation(vendorLocation);

                    LocationObject shippingLocation = new LocationObject();
                    shippingLocation.setLocationID(shippingLocationObject.getString("id"));
                    shippingLocation.setLatitude(shippingLocationObject.getString("latitude"));
                    shippingLocation.setLongitude(shippingLocationObject.getString("longitude"));
                    shippingLocation.setAddress(shippingLocationObject.getString("address"));
                    orderDetail.setShippingLocation(shippingLocation);


                }catch (Exception ex){

                }




               // if (orderDetail.getTrackingID() != null && orderDetail.getTrackingID() != "null")
                    OrderDetailList.add(orderDetail);
            }
            recyclerview_notifications.setVisibility(View.VISIBLE);
            layout_no_order.setVisibility(View.GONE);
            adapter = new OrdersAdapter(this , mContext, OrderDetailList);
            recyclerview_notifications.setAdapter(adapter);
        }
    }

    /**
     * It will notify to the server regarding of new order acceptance by the deliver boy
     * @param orderDetail
     */
    public void setAcceptNotification(OrderDetail orderDetail) {

        String url="";
        if (orderDetail.isIspickdrop()){
            url=Config.ACCEPT_NOTIFICATION + "/" + orderDetail.getorderID();
        }else{
            url=Config.SET_NOTIFICATION_ACCEPT_NEW + "/" + orderDetail.getorderID();
        }
        LoadingDialog.loadDialog(mContext);

        Log.d("VolleyResponse", "notifications/accept URL: " + Config.SET_NOTIFICATION_ACCEPT_NEW + "/" + orderDetail.getorderID());

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("VolleyResponse", "notifications/accept response: " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = false;
                    if (jsonObject.has("status"))
                        status = jsonObject.getBoolean("status");
                    else if (jsonObject.has("status_code"))
                        status =  jsonObject.getInt("status_code")==200?true:false;//status_code : 200

                    LoadingDialog.dialog.dismiss();
                    if (!status) {
                        String errorMsg=jsonObject.getString("message");
                        if(errorMsg.equals("JOB_ALREADY_ALLOCATED"))
                            errorMsg=getString(R.string.JOB_ALREADY_ALLOCATED); 
                        else if(errorMsg.equals("VEHICLE_TYPE_MISMATCH"))
                            errorMsg=getString(R.string.VEHICLE_TYPE_MISMATCH);
                        else if(errorMsg.equals("OTHER_JOB_INPROGRESS"))
                            errorMsg=getString(R.string.OTHER_JOB_INPROGRESS);
                        showErrorDialog(errorMsg);
                    } else {
                        if (MyMixPanel.isMixPanelSupport) {
                            MyMixPanel.logEvent("Delivery boy accepted the new order");
                        }
                        showOrderAcceptanceDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                } finally {
                    LoadingDialog.dialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return null;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    public void setAcceptNotification_old_end_point(OrderDetail orderDetail) {

        LoadingDialog.loadDialog(mContext);
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("notification_id", orderDetail.getNotificationID());
        dataMap.put("order_id", orderDetail.getorderID());
        dataMap.put("job_type",  "1"); // 1 = Delivery, 2 = Return

        String data = new JSONObject(dataMap).toString();


        Log.d("VolleyResponse", "notifications/accept reqyest: " + data);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SET_NOTIFICATION_ACCEPT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("VolleyResponse", "notifications/accept response: " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status=jsonObject.getBoolean("status");

                    LoadingDialog.dialog.dismiss();
                    if (!status) {
                        //Toast.makeText(mContext, "Order acceptanace is failed :  "+response, Toast.LENGTH_SHORT).show();
                        showErrorDialog(jsonObject.getString("message"));
                    }
                    else {
                        // Toast.makeText(mContext, "You order status has been accepted successfully.", Toast.LENGTH_SHORT).show();
                        // getNotifications();

                        if (MyMixPanel.isMixPanelSupport) {
                            MyMixPanel.logEvent("Delivery boy accepted the new order");
                        }

                        showOrderAcceptanceDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                }
                finally {
                    LoadingDialog.dialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    /**
     * It will show dialog to the deliver boy on his acceptance of new order
     */
    private void showOrderAcceptanceDialog() {
        //You order status has been accepted successfully.
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_order_accept_success);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(mContext, DashboardActivity.class));
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                startActivity(new Intent(mContext, DashboardActivity.class));
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    private void showErrorDialog(String errorMessage) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_error_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        TextView tv_success= (TextView) dialog.findViewById(R.id.tv_success);
        tv_success.setText(errorMessage);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }
}

