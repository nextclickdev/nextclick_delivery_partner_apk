package com.nextclick.deliveryboy.dashboard.activitys.ui.home;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.deliveryboy.Config.Config;
import com.nextclick.deliveryboy.helpers.LoadingDialog;
import com.nextclick.deliveryboy.helpers.UiMsgs;
import com.nextclick.deliveryboy.utils.GeoLocationLib;
import com.nextclick.deliveryboy.utils.LocationListener;
import com.nextclick.deliveryboy.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.deliveryboy.Config.Config.REGISTER;
import static com.nextclick.deliveryboy.Constants.Constants.USER_TOKEN;

public class HomeViewModel extends ViewModel implements LocationListener {

    Context mContext;
    private MutableLiveData<Boolean> mAvailableStatus;
    GeoLocationLib geoLocationLib;
    PreferenceManager _preferenceManager;

    private MutableLiveData<Location> onLocationChanged;
    private MutableLiveData<View> viewOnClickListener;

    private PreferenceManager getPreferenceInstance()
    {
        if (_preferenceManager == null)
            _preferenceManager = new PreferenceManager(mContext);
        return _preferenceManager;
    }

    public HomeViewModel(Context mContext) {
        this.mContext=mContext;
        mAvailableStatus = new MutableLiveData<>();
        onLocationChanged = new MutableLiveData<>();
        viewOnClickListener = new MutableLiveData<>();
        mAvailableStatus.setValue(false);
    }

    public LiveData<Boolean> getAvailableStatus() {
        return mAvailableStatus;
    }
    public LiveData<Location> getCurrentLocation() {
        return onLocationChanged;
    }
    public LiveData<View> getViewOnClickListener() {
        return viewOnClickListener;
    }

    public void changeNetworkState(boolean availbleStatus) {
        mAvailableStatus.setValue(availbleStatus);
        updateAvailabilityStatus(availbleStatus);
    }

    public void initializeGeoLocationLib()
    {
        if(geoLocationLib==null) {
            geoLocationLib = new GeoLocationLib(mContext);
            geoLocationLib.addListener(this);
        }
    }
    public void removeGeoLocationLib() {
        if (geoLocationLib != null) {
            geoLocationLib.removeListener(this);
        }
    }

    Location lastKnownLocation;
    List<Address> address;
    @Override
    public void onLocationChanged(Location lastKnownLocation, List<Address> address) {
        this.lastKnownLocation=lastKnownLocation;
        this.address=address;
        onLocationChanged.setValue(lastKnownLocation);
    }

    private void updateAvailabilityStatus(boolean availbleStatus) {
        Map<String, String> dataMap = new HashMap<>();
        if(availbleStatus) {
            //online
            dataMap.put("login_status", "1");
            if(lastKnownLocation!=null) {
                dataMap.put("latitude", "" + lastKnownLocation.getLatitude());
                dataMap.put("longitude", "" + lastKnownLocation.getLongitude());
            }
            if(address!=null) {
                dataMap.put("address", address.get(0).getSubLocality());
                //dataMap.put("address", address.get(0).getLocality());
            }
        }
        else {
            //offline
            dataMap.put("login_status", "2");
        }
        JSONObject json = new JSONObject(dataMap);
        final String data = json.toString();
        System.out.println("MANAGE_LOGIN_SESSION  response "+data.toString());

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.MANAGE_LOGIN_SESSION,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    System.out.println("MANAGE_LOGIN_SESSION  response "+response.toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");
                            UpdateCurrentLocation();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaa catch  "+e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("aaaaaaa catch Server under maintenance ");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", getPreferenceInstance().getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void UpdateCurrentLocation() {
        if (mAvailableStatus.getValue()) {
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put("login_status", "1");
            if (lastKnownLocation != null) {
                dataMap.put("latitude", "" + lastKnownLocation.getLatitude());
                dataMap.put("longitude", "" + lastKnownLocation.getLongitude());
            }
            if (address != null) {
                dataMap.put("address", address.get(0).getSubLocality());
                //dataMap.put("address", address.get(0).getLocality());
            }


            JSONObject json = new JSONObject(dataMap);
            final String data = json.toString();
            System.out.println("UPDATE_CURRENT_LOCATION  request " + data.toString());

            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_CURRENT_LOCATION,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response != null) {
                                System.out.println("UPDATE_CURRENT_LOCATION  response " + response.toString());
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    System.out.println("aaaaaaaaa jsonobject " + jsonObject.toString());
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        String message = jsonObject.getString("message");
                                    }
                                } catch (Exception e) {
                                    System.out.println("aaaaaaa catch  " + e.getMessage());
                                    e.printStackTrace();
                                }
                            } else {
                                System.out.println("aaaaaaa catch Server under maintenance ");
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("aaaaaaa error  " + error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", getPreferenceInstance().getString(USER_TOKEN));
                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
    }

    public void setOnClickListener(View onClickListener) {
        viewOnClickListener.setValue(onClickListener);
    }
}