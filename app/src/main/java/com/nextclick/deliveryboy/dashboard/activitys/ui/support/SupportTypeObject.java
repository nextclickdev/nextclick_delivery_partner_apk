package com.nextclick.deliveryboy.dashboard.activitys.ui.support;

import com.nextclick.deliveryboy.model.DeliveryOrderPayment;
import com.nextclick.deliveryboy.model.DeliveryOrderStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SupportTypeObject {

    String Status;
    public void setStatus(String title) { this.Status = title; }
    public String getStatus() { return Status; }


    String ID;
    public void setID(String title) { this.ID = title; }
    public String getID() { return ID; }

    String title;
    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return title; }

    String desc;
    public void setDesc(String title) { this.desc = title; }
    public String getDesc() { return desc; }
}
