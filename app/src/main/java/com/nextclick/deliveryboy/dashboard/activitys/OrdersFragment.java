package com.nextclick.deliveryboy.dashboard.activitys;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nextclick.deliveryboy.Config.Config;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Orders.ShowOrdersAdapter;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Orders.SpinnerStatusList;
import com.nextclick.deliveryboy.dashboard.activitys.ui.support.SpinnerSupportTypeAdapter;
import com.nextclick.deliveryboy.helpers.LoadingDialog;
import com.nextclick.deliveryboy.model.DeliveryOrder;
import com.nextclick.deliveryboy.model.DeliveryOrderPayment;
import com.nextclick.deliveryboy.model.DeliveryOrderPaymentMethod;
import com.nextclick.deliveryboy.model.DeliveryOrderStatus;
import com.nextclick.deliveryboy.model.OrderStatus;
import com.nextclick.deliveryboy.utils.PreferenceManager;
import com.nextclick.deliveryboy.utils.mixpanelutil.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.deliveryboy.Constants.Constants.USER_TOKEN;

public class OrdersFragment  extends Fragment  implements
        AdapterView.OnItemSelectedListener, View.OnClickListener {


    public static final String PICKUP_POINT = "Reached to Pickup Point";
    public static final String DELIVERY_POINT = "Reached to Delivery Point";
    //String[] statusList = { "Received", "Accept", "Reject", "Reached to pick up point", "On its way", "Reached to delivey point", "Delivered"};

    private RecyclerView recyclerView_orders;
    Context mContext;
    PreferenceManager preferenceManager;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<DeliveryOrder> deliveryOrderList;
    private List<OrderStatus> orderStatusList;
    private RecyclerView.Adapter adapter;
    LinearLayout layout_order_filter;
   // LinearLayout date_layout;
    //DatePicker datePicker;
    private TextView start_date, end_date;
    LinearLayout layout_empty_cart;
    Button btn_start_date,btn_end_date;
    private OrderStatus selectedStatus;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_orders, container, false);
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        recyclerView_orders = root.findViewById(R.id.recyclerview_orders);

        layout_empty_cart= root.findViewById(R.id.layout_empty_cart);
        linearLayoutManager = new LinearLayoutManager(this.getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(recyclerView_orders.getContext(),
                linearLayoutManager.getOrientation());

        recyclerView_orders.setHasFixedSize(true);
        recyclerView_orders.setLayoutManager(linearLayoutManager);
        // recyclerView_orders.addItemDecoration(dividerItemDecoration);

        deliveryOrderList = new ArrayList<>();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Delivery boy navigated to order history activity");
        }

        /*Spinner spin = (Spinner) root.findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(this);

        orderStatusList= new ArrayList<>();
        getStatusList();

        SpinnerStatusList aa = new SpinnerStatusList(getActivity(), orderStatusList);
        //aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);*/

        start_date = root.findViewById(R.id.start_date);
        end_date = root.findViewById(R.id.end_date);
        Button apply_button = root.findViewById(R.id.apply_button);
        apply_button.setOnClickListener(this);
        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);
        getDate();
      //  getData();


      //  adapter = new ShowOrdersAdapter(this.getActivity(), ShowOrderList);
       // recyclerView_orders.setAdapter(adapter);

        layout_order_filter = root.findViewById(R.id.layout_order_filter);
        FloatingActionButton fab = root.findViewById(R.id.filter_action);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layout_order_filter.getVisibility() == View.GONE)
                    layout_order_filter.setVisibility(View.VISIBLE);
                else
                    layout_order_filter.setVisibility(View.GONE);
            }
        });

        /*Button btn_date_done = root.findViewById(R.id.btn_date_done);
        btn_start_date = root.findViewById(R.id.btn_start_date);
        btn_end_date = root.findViewById(R.id.btn_end_date);
        date_layout= root.findViewById(R.id.date_layout);
        datePicker= root.findViewById(R.id.datePicker);
        btn_date_done.setOnClickListener(this);
        btn_start_date.setOnClickListener(this);
        btn_end_date.setOnClickListener(this);*/

        return root;
    }

    private void getStatusList() {
//{ "Received", "Accept", "Reject", "Reached to pick up point", "On its way", "Reached to delivey point", "Delivered"};
        OrderStatus status =new OrderStatus();
        status.setId("");
        status.setStatus("Received");
        orderStatusList.add(status);

        status =new OrderStatus();
        status.setId("");
        status.setStatus("Accept");
        orderStatusList.add(status);

        status =new OrderStatus();
        status.setId("");
        status.setStatus("Reject");
        orderStatusList.add(status);

        status =new OrderStatus();
        status.setId("504");
        status.setStatus("Reached to pickup point");
        orderStatusList.add(status);

        status =new OrderStatus();
        status.setId("");
        status.setStatus("On its way");
        orderStatusList.add(status);

        status =new OrderStatus();
        status.setId("505");
        status.setStatus("Picked the order");
        orderStatusList.add(status);

        status =new OrderStatus();
        status.setId("506");
        status.setStatus("Reached to delivery point");
        orderStatusList.add(status);

        status =new OrderStatus();
        status.setId("508");
        status.setStatus("Delivered");
        orderStatusList.add(status);
    }

    @Override
    public void onResume() {
        super.onResume();
        getOrdersList();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        view.findViewById(R.id.relative_layout_status_selected).setVisibility(View.VISIBLE);
        view.findViewById(R.id.relative_layout_status).setVisibility(View.GONE);

        selectedStatus = orderStatusList.get(position);
        //Toast.makeText(getActivity(), statusList[position] , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    boolean isSartDateType = false;

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.start_date:
                isSartDateType = true;
                datepicker(0);
                break;
            case R.id.end_date:
                if ((fromyear.equals("")) || (frommonth.equals("")) || (fromday.equals(""))){
                    Toast.makeText(getActivity(), getString(R.string.select_startdate), Toast.LENGTH_SHORT).show();
                }else {
                    isSartDateType = false;
                    datepicker(1);
                }

                break;
            case R.id.apply_button:
                //layout_order_filter.setVisibility(View.GONE);
                recyclerView_orders.setAdapter(null);
                deliveryOrderList.clear();
                getOrdersList();
                /*if(deliveryOrderList.size()>0) {
                    adapter = new ShowOrdersAdapter(this,this.getActivity(), deliveryOrderList);
                    recyclerView_orders.setAdapter(adapter);
                    recyclerView_orders.setVisibility(View.VISIBLE);
                    txt_no_data_found.setVisibility(View.GONE);
                }
                else
                {
                    recyclerView_orders.setVisibility(View.GONE);
                    txt_no_data_found.setVisibility(View.VISIBLE);
                }*/
                break;
           /* case R.id.reset_button:

                //layout_order_filter.setVisibility(View.GONE);
                deliveryOrderList.clear();
                getDate();
                selectedStatus =null;
                getOrdersList();
                adapter = new ShowOrdersAdapter(this,this.getActivity(), deliveryOrderList);
                recyclerView_orders.setAdapter(adapter);
                layout_order_filter.setVisibility(View.GONE);
                txt_no_data_found.setVisibility(View.GONE);
                recyclerView_orders.setVisibility(View.VISIBLE);
                break;*/
            case R.id.btn_date_done:
               /* date_layout.setVisibility(View.GONE);
                if(isSartDateType)
                {
                    setDate(0);
                    Toast.makeText(getActivity(), "selected start date is "+datePicker.getDayOfMonth(), Toast.LENGTH_LONG).show();
                }
                else
                {
                    setDate(1);
                }*/
                break;
        }
    }



    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="",startdate="",enddate="";

    private void datepicker(int i) {

        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            c.clear();
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(this.getActivity(),
                new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    startdate=fromyear+"-"+frommonth+"-"+fromday;
                    start_date.setText(fromday+"-"+frommonth+"-"+fromyear);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    enddate=toyear+"-"+tomonth+"-"+today;
                    //end_date.setText(""+enddate);
                    end_date.setText(today+"-"+tomonth+"-"+toyear);
                }
            }
        }, mYear, mMonth, mDay);

        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            c1.clear();
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());

            c2.clear();
            c1.clear();
        }
        datePickerDialog.show();

    }

   /* private void setDate(int i) {
        int year= datePicker.getYear();
        int monthOfYear= datePicker.getMonth();
        int dayOfMonth= datePicker.getDayOfMonth();
        if (i==0){
            if ((monthOfYear+1) <= 9) {
                frommonth = 0 + "" + (monthOfYear+1);
            } else {
                frommonth = "" + (monthOfYear+1);
            }  if (dayOfMonth <= 9) {
                fromday = 0 + "" + dayOfMonth;
            } else {
                fromday = "" + dayOfMonth;
            }
            fromyear=""+year;
            startdate=fromyear+"-"+frommonth+"-"+fromday;
            btn_start_date.setText(""+startdate);
        }else {
            if ((monthOfYear+1) <= 9) {
                tomonth = 0 + "" + (monthOfYear+1);
            } else {
                tomonth = "" + (monthOfYear+1);
            }  if (dayOfMonth <= 9) {
                today = 0 + "" + dayOfMonth;
            } else {
                today = "" + dayOfMonth;
            }
            toyear=""+year;
            enddate=toyear+"-"+tomonth+"-"+today;
            btn_end_date.setText(""+enddate);
        }
    }*/
   public void getDate(){
       String year,month,day;
       Calendar calander = Calendar.getInstance();
       int mday = calander.get(Calendar.DAY_OF_MONTH);
       int cMonth = calander.get(Calendar.MONTH) + 1;
       year =""+ calander.get(Calendar.YEAR);
       if ((cMonth) <= 9) {
           month = 0 + "" + (cMonth);
       } else {
           month = "" + (cMonth);
       }  if (mday <= 9) {
           day = 0 + "" + mday;
       } else {
           day = "" + mday;
       }
       start_date.setText(day+"-"+month+"-"+year);
       end_date.setText(day+"-"+month+"-"+year);
       startdate=year+"-"+month+"-"+day;
       enddate=year+"-"+month+"-"+day;
   }


    private void getOrdersList() {
        LoadingDialog.loadDialog(mContext);
        deliveryOrderList.clear();

        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("start_date", startdate);
        dataMap.put("end_date", enddate);
        if (selectedStatus != null)
            dataMap.put("delivery_boy_status", selectedStatus.getId());
        else
            dataMap.put("delivery_boy_status", "");
        String data = new JSONObject(dataMap).toString();
        Log.d("VolleyResponse", "GET_ORDERS_LIST request: " + data);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_ORDERS_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);

                    Log.d("VolleyResponse", "delivery_orders response: " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status=jsonObject.getBoolean("status");

                    if (!status) {
                        setVisibility();
                    }
                    else {
                        parseJsonData(jsonObject);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                    setVisibility();
                }
                finally {
                    LoadingDialog.dialog.dismiss();
                    setVisibility();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void parseJsonData(JSONObject jsonObject) throws JSONException {
        JSONArray dataArray = jsonObject.getJSONArray("data");
        if (dataArray !=null && dataArray.length() > 0) {
            deliveryOrderList = new ArrayList<>();

            for (int i = 0; i < dataArray.length(); i++) {
                DeliveryOrder deliveryOrder = new DeliveryOrder();

                JSONObject notficationJsonObject = dataArray.getJSONObject(i);


                if (notficationJsonObject.has("delivery_fee"))
                    deliveryOrder.setDelivery_fee(notficationJsonObject.getString("delivery_fee"));

                deliveryOrder.setDeliveryJobID(notficationJsonObject.getString("delivery_job_id"));
                deliveryOrder.setDelivery_job_status(notficationJsonObject.getString("delivery_job_status"));
                deliveryOrder.setJobID(notficationJsonObject.getString("job_id"));
                deliveryOrder.setStatus(notficationJsonObject.getString("status"));
                deliveryOrder.setID(notficationJsonObject.getString("id"));
                deliveryOrder.setTrackID(notficationJsonObject.getString("track_id"));
                if (notficationJsonObject.has("order_pickup_otp"))
                deliveryOrder.setOrderPickupOtp(notficationJsonObject.getString("order_pickup_otp"));

                if (notficationJsonObject.has("order_delivery_otp"))
                deliveryOrder.setOrderDeliveryOtp(notficationJsonObject.getString("order_delivery_otp"));//hidden field

                deliveryOrder.setPreparationTime(notficationJsonObject.getString("preparation_time"));
                deliveryOrder.setShippingAddressID(notficationJsonObject.getString("shipping_address_id"));
                deliveryOrder.setPaymentID(notficationJsonObject.getString("payment_id"));
                deliveryOrder.setTotal(notficationJsonObject.getString("total"));
                deliveryOrder.setMessage(notficationJsonObject.getString("message"));
                deliveryOrder.setCreatedUserID(notficationJsonObject.getString("created_user_id"));
                deliveryOrder.setVendorUserID(notficationJsonObject.getString("vendor_user_id"));
                deliveryOrder.setCreatedAt(notficationJsonObject.getString("created_at"));
                deliveryOrder.setUpdatedAt(notficationJsonObject.getString("updated_at"));
                deliveryOrder.setOrderStatusID(notficationJsonObject.getString("order_status_id"));

                JSONObject paymentJsonObject = notficationJsonObject.getJSONObject("payment");
                JSONObject orderStatusJsonObject = notficationJsonObject.getJSONObject("order_status");


                DeliveryOrderPayment deliveryOrderPayment =new DeliveryOrderPayment();
                deliveryOrderPayment.setID(paymentJsonObject.getString("id"));
                deliveryOrderPayment.setTransactionID(paymentJsonObject.getString("txn_id"));
                deliveryOrderPayment.setAmount(paymentJsonObject.getString("amount"));
                deliveryOrderPayment.setCreatedAt(paymentJsonObject.getString("created_at"));
                deliveryOrderPayment.setMessage(paymentJsonObject.getString("message"));
                deliveryOrderPayment.setStatus(paymentJsonObject.getString("status"));
                deliveryOrderPayment.setPaymentMethodID(paymentJsonObject.getString("payment_method_id"));


                JSONObject paymentMethodJsonObject = paymentJsonObject.getJSONObject("payment_method");
                DeliveryOrderPaymentMethod deliveryOrderPaymentMethod =new DeliveryOrderPaymentMethod();
                deliveryOrderPaymentMethod.setID(paymentMethodJsonObject.getString("id"));
                deliveryOrderPaymentMethod.setName(paymentMethodJsonObject.getString("name"));
                deliveryOrderPaymentMethod.setDescription(paymentMethodJsonObject.getString("description"));
                deliveryOrderPayment.setDeliveryOrderPaymentMethod(deliveryOrderPaymentMethod);
                deliveryOrder.setDeliveryOrderPayment(deliveryOrderPayment);



                DeliveryOrderStatus deliveryOrderStatus =new DeliveryOrderStatus();
                deliveryOrderStatus.setID(orderStatusJsonObject.getString("id"));
                deliveryOrderStatus.setDeliveryModeID(orderStatusJsonObject.getString("delivery_mode_id"));
                deliveryOrderStatus.setStatus(orderStatusJsonObject.getString("status"));
                deliveryOrderStatus.setSerialNumber(orderStatusJsonObject.getString("serial_number"));

                deliveryOrder.setDeliveryOrderStatus(deliveryOrderStatus);

                deliveryOrderList.add(deliveryOrder);
            }
        }
        setVisibility();
    }

    private void setVisibility() {

       if(deliveryOrderList !=null && deliveryOrderList.size()>0) {
           recyclerView_orders.setAdapter(null);
           recyclerView_orders.setVisibility(View.VISIBLE);
           layout_empty_cart.setVisibility(View.GONE);
           adapter = new ShowOrdersAdapter(this,this.getActivity(), deliveryOrderList);
           recyclerView_orders.setAdapter(adapter);
       }
       else
       {
           recyclerView_orders.setVisibility(View.GONE);
           layout_empty_cart.setVisibility(View.VISIBLE);
       }
    }

    public void invokeAction(String actionType,DeliveryOrder deliveryOrder) {
       /*
       {
"delivery_job_id": 1,
"status": 4 // 4 = Reached to pickup point, 6 = reached to delivery point
}
        */

        int status =actionType ==PICKUP_POINT?4:6;
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("delivery_job_id", deliveryOrder.getDeliveryJobID());
        dataMap.put("status", ""+status);

        String data = new JSONObject(dataMap).toString();

        Log.d("VolleyResponse", "delivery_orders/change_status request: " + data);
        LoadingDialog.loadDialog(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.CHANGE_DELIVERY_ORDER_STATUS,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);

                    Log.d("VolleyResponse", "delivery_orders/change_status response: " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status=jsonObject.getBoolean("status");

                    LoadingDialog.dialog.dismiss();
                    if (!status) {
                        Toast.makeText(mContext, getString(R.string.delivery_order_status_fail)+" "+response, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(mContext, getString(R.string.delivery_order_status_success) ,Toast.LENGTH_SHORT).show();
                        getOrdersList();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                }
                finally {
                    LoadingDialog.dialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
