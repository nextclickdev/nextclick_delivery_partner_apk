package com.nextclick.deliveryboy.dashboard.activitys.ui.settings;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SettingviewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public SettingviewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Settings fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
