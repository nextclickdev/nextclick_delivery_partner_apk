package com.nextclick.deliveryboy.dashboard.activitys.ui.Orders;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.OrdersFragment;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Notifications.OrderDetail;
import com.nextclick.deliveryboy.model.ProductDetailsModel;

import java.util.List;

import java.util.List;

public class ProductDetailAdapter extends RecyclerView.Adapter<ProductDetailAdapter.ViewHolder> {

    private Context context;
    private List<ProductDetailsModel> list;
    OrderDetailsActivity orderDetailsActivity;
    String delivery_boy_status;

    public ProductDetailAdapter(Context context, List<ProductDetailsModel> list,String delivery_boy_status) {
        this.context = context;
        this.list = list;
        this.orderDetailsActivity=orderDetailsActivity;
        this.delivery_boy_status=delivery_boy_status;
    }

    @Override
    public ProductDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //        View v = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
        View v = LayoutInflater.from(context).inflate(R.layout.list_product_detail, parent, false);
        return new ProductDetailAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductDetailAdapter.ViewHolder holder, int position) {
       // ProductDetailsModel productDetailsModel = list.get(position);

       // holder.txt_Discount.setText(productDetailsModel.getDiscount());

        ProductDetailsModel orderItems = list.get(position);

       // orderItems.setProductCheckState(true);//for now teting
        holder.chk_product_state.setChecked(orderItems.getProductCheckState());
        if(orderItems.getOrderStatus()>= 508)
        {
            holder.chk_product_state.setVisibility(View.GONE);
        }
        else {
            holder.chk_product_state.setVisibility(View.VISIBLE);
            if (orderItems.getOrderStatus() >= 505) {
                holder.chk_product_state.setEnabled(false);
                holder.chk_product_state.setChecked(true);
            }
        }
        holder.chk_product_state.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                orderItems.setProductCheckState(isChecked);
            }
        });

        if (delivery_boy_status.equalsIgnoreCase("601")||delivery_boy_status.equalsIgnoreCase("602")||delivery_boy_status.equalsIgnoreCase("604")){
            holder.chk_product_state.setVisibility(View.VISIBLE);
            try{
                if (list.get(position).getItemModel().getVarientModel().getReturn_available().equalsIgnoreCase("1")){
                    holder.order_details_list.setBackgroundColor(context.getResources().getColor(R.color.gray));
                    holder.chk_product_state.setVisibility(View.GONE);

                }else{
                    holder.chk_product_state.setChecked(true);
                    holder.chk_product_state.setEnabled(false);
                }
            }catch (Exception e){

            }
        }

        holder.tv_pname.setText(orderItems.getItemModel().getName());
        holder.tv_quantity.setText(orderItems.getQty());
        try{
            holder.tv_pdescription.setText(orderItems.getItemModel().getVarientModel().getSectionItemModel().getName()+"");
        }catch (NullPointerException e){

        }



        try{
            String promotionid=orderItems.getPromotion_banner_id();
            if (!promotionid.equalsIgnoreCase("")|| !promotionid.isEmpty()){

                if (promotionid.equalsIgnoreCase("1")){
                    }else if (promotionid.equalsIgnoreCase("2")){
                      }
                else if (promotionid.equalsIgnoreCase("3")){
                    int offer_producy_qty=Integer.parseInt(orderItems.getQty());
                    holder.tv_quantity.setText("Quantity : "+(offer_producy_qty+offer_producy_qty)+"");
                }else if (promotionid.equalsIgnoreCase("4")){

                }else if (promotionid.equalsIgnoreCase("5")){

                }else if (promotionid.equalsIgnoreCase("6")){

                }
                else {

                }
            }else {

            }
        }catch (NullPointerException | NumberFormatException e){

        }
        //holder.tv_subtotal.setText("Sub Total : "+orderItems.getSub_total());

        if (orderItems.getDiscount().equalsIgnoreCase("0")) {
            holder.txt_Discount.setVisibility(View.GONE);
        }else {
            holder.txt_Discount.setVisibility(View.VISIBLE);
           // holder.txt_Discount.setText("Rs : "+orderItems.getDiscount()+" Discount");
            holder.txt_Discount.setText("Discount Rs : "+orderItems.getDiscount());
        }

        //holder.tv_tax.setText("Tax : "+orderItems.getTax());
        holder.tv_totalprice.setText("Rs : "+orderItems.getSub_total());
        //holder.tv_totalprice.setText("Rs : "+orderItems.getTotal());

        try{
            if(orderItems.getItemModel().getImagelist()!=null && orderItems.getItemModel().getImagelist().size()>0) {
                Glide.with(context)
                        .load(orderItems.getItemModel().getImagelist().get(0).getImage())
                        .placeholder(R.drawable.ic_reject)//pizza
                        .into(holder.img_product);
            }
        }catch (Exception e){

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_pname,tv_pdescription,txt_Discount,tv_quantity,tv_totalprice;//tv_subtotal
        ImageView img_product;
        CheckBox chk_product_state;
        LinearLayout order_details_list;


        public ViewHolder(View itemView) {
            super(itemView);
            txt_Discount= itemView.findViewById(R.id.txt_discount);

            img_product = itemView.findViewById(R.id.img_product);
            tv_pname = itemView.findViewById(R.id.tv_pname);
            tv_pdescription = itemView.findViewById(R.id.tv_pdescription);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
           // tv_subtotal = itemView.findViewById(R.id.tv_subtotal);
            //tv_tax = itemView.findViewById(R.id.tv_tax);
            tv_totalprice = itemView.findViewById(R.id.tv_totalprice);
            chk_product_state = itemView.findViewById(R.id.chk_product_state);
            order_details_list = itemView.findViewById(R.id.order_details_list);


            //added on 16th april



        }
    }
}
