package com.nextclick.deliveryboy.utils;

import com.nextclick.deliveryboy.model.DirectionObject;

public interface DirectionListener {
    void onDirectionResult(DirectionObject result);
}
