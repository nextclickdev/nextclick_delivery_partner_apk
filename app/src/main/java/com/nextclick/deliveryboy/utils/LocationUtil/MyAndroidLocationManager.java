package com.nextclick.deliveryboy.utils.LocationUtil;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.LocationServices;

import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

public class MyAndroidLocationManager extends MyLocationManager {

    private static String TAG = "MyAndroidLocationManager";
    private LocationManager locationManager = null;
    private CustomLocationListener networkWatchListener = null;
    private CustomLocationListener gpsWatchListener = null;
    MyLocationListener oneShotListener;
    MyLocationListener frequentUpdatesListener;
    boolean isOneShot = true;
    boolean isStale = false;

    void init() {
        if (locationManager == null) {
            Context context = mContext;
            if (context != null)
                locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }
    }
    private CustomLocationListener getWatchClientListener(String provider){
        if(provider != null){
            if(provider.equals(LocationManager.NETWORK_PROVIDER)){
                if(networkWatchListener == null)
                    networkWatchListener = new CustomLocationListener(false);
                return networkWatchListener;
            }
            else{
                if(gpsWatchListener == null)
                    gpsWatchListener = new CustomLocationListener(false);
                return gpsWatchListener;
            }
        }
        return null;
    }
    private String getProvider(boolean enableHighAccuracy, boolean useBestProvider){
        Log.e(TAG,"getProvider(START) {");
        String provider = null;
        init();
        Log.e(TAG,"getProvider(): enableHighAccuracy = [ "+enableHighAccuracy+" ]   useBestProvider= [ "+useBestProvider+" ]");
        if(locationManager != null){
            if(useBestProvider){
                Log.e(TAG,"getProvider(): useBestProvider is true,  so querying for bestProvoder from System ");
                provider = locationManager.getBestProvider(new Criteria(), true);
                Log.e(TAG,"getProvider():  useBestProvider is true. bestProvoder from System ="+provider);
                if(provider == null){
                    provider = enableHighAccuracy ? LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER;
                    Log.e(TAG,"getProvider(): useBestProvider is true.  bestProvoder picked ="+provider);
                }
            }
            else {
                if(enableHighAccuracy){
                    //Check if GPS provider is enabled or not
                    Log.e(TAG,"getProvider(): enableHighAccuracy is 'true', so checking if GPS is enabled in System settings... ");
                    if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                        Log.e(TAG,"getProvider(): with enableHighAccuracy as 'true & 'GPS is in enabled state, Applicable provider is 'GPS' ");
                        provider = LocationManager.GPS_PROVIDER;
                    } //If GPS provider is disabled, fall back to 'network' provider
                    else if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                        Log.e(TAG,"getProvider(): 'GPS is not in enabled state, next applicable provider is 'network' ");
                        provider = LocationManager.NETWORK_PROVIDER;
                    }
                    else{
                        Log.e(TAG,"getProvider(): None of the providers(gps/network) are enabled in System settings ");
                    }
                }
                else {
                    Log.e(TAG,"getProvider(): enableHighAccuracy is 'false', so checking if 'network' is enabled in System settings... ");
                    if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                        Log.e(TAG,"getProvider(): with enableHighAccuracy as 'false' & 'network' provider is in enabled state, Applicable provider is 'network' ");
                        provider = LocationManager.NETWORK_PROVIDER;
                    }
                }
                Log.e(TAG,"getProvider(): After checking the system settings as per user options, provider is = "+provider);
            }
        }

        Log.e(TAG,"getProvider(END) } provider= "+provider);
        return provider;
    }

    @Override
    public void getCurrentPosition(MyLocationListener listener) {
        this.oneShotListener = listener;
        executeOnLocationReceived(oneShotListener, getLatestKnownLocation());
        isOneShot = true;
    }

    @Override
    public void startLocationUpdates(MyLocationListener listener) {
        this.frequentUpdatesListener = listener;
        isOneShot = false;
        isStale = false;
        executeOnLocationUpdated(frequentUpdatesListener, getLatestKnownLocation());

        String provider = getProvider(true,false);
        locationManager.requestLocationUpdates(provider, 1000, 1000, getWatchClientListener(provider));
    }

    @Override
    public void removeLocationUpdates(MyLocationListener listener) {

    }

    private Location getLatestKnownLocation() {
        Location bestResult = null;
        long bestTime = Long.MIN_VALUE;
        Log.e(TAG, "getLatestKnownLocation() START {");
        // Iterate through all the providers on the system, keeping
        // note of the most recent result.
        init();
        if (locationManager != null) {
            List<String> matchingProviders = locationManager.getAllProviders();
            for (String provider : matchingProviders) {
                Location location = locationManager.getLastKnownLocation(provider);
                Log.e(TAG, "getLatestKnownLocation(): provider=" + provider + "     location=" + location);
                if (location != null) {
                    long time = location.getTime();
                    if (time > bestTime) {
                        bestResult = location;
                        bestTime = time;
                    }
                }
            }
        }
        Log.e(TAG, "getLatestKnownLocation() best location(interms of time) = " + bestResult);
        Log.e(TAG, "getLatestKnownLocation() END }");
        return bestResult;
    }


    class CustomLocationListener implements LocationListener {

        private boolean isOneShot = false;

        public CustomLocationListener(boolean isOneShot) {
            this.isOneShot = isOneShot;
        }

        public void onLocationChanged(Location location) {
            Log.e(TAG, "HTML5-KonyLocationManager :: " + " ########## Kony Location :[ " + location + " ]##########");
            Log.e(TAG, "KonyLocationListener.onLocationChanged() : Location received from Android location manager location=" + location);
            if (isOneShot) {
                if (oneShotListener != null && location != null) {
                    executeOnLocationReceived(oneShotListener, location);
                }
            } else {
                if (frequentUpdatesListener != null && location != null) {
                    executeOnLocationUpdated(frequentUpdatesListener, location);
                }
            }
        }

        public void onProviderDisabled(String provider) {
            Log.e(TAG, "KonyLocationListener.onProviderDisabled() provider=" + provider);
            if (locationManager != null) {

            }
        }

        public void onProviderEnabled(String provider) {
            Log.e(TAG, "KonyLocationListener.onProviderEnabled() provider=" + provider);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "KonyLocationListener.onStatusChanged() provider=" + provider + "  status=" + status);
            if (status == LocationProvider.OUT_OF_SERVICE || status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
                if (isOneShot) {
                    executeErrorCallback(oneShotListener, MYLOC_ECODE_POSITION_UNAVAILABLE, MYLOC_EMSG_POSITION_UNAVAILABLE);
                    oneShotListener =null;
                }
            } else {
                executeErrorCallback(frequentUpdatesListener, MYLOC_ECODE_POSITION_UNAVAILABLE, MYLOC_EMSG_POSITION_UNAVAILABLE);
                removeLocationUpdates(frequentUpdatesListener);
            }
        }
    }

}

