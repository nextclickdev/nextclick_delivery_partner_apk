package com.nextclick.deliveryboy.utils.mixpanelutil;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Geocoder;
import android.util.Base64;
import android.util.Log;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.nextclick.deliveryboy.utils.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.Random;

import static com.nextclick.deliveryboy.Constants.Constants.USER_TOKEN;

public class MyMixPanel {


    public static boolean isMixPanelSupport =true;


    private static final String MIXPANEL_API_TOKEN ="864231871a545f50612f4f3e5786bcb4";// "1c14307787baa150e09de6172d7d5937";
    private static MixpanelAPI mMixpanel;
    private static Context context;
    static boolean isInit =false;

    public static MixpanelAPI getInstance() {
        if (mMixpanel == null) {
            // Initialize the Mixpanel library for tracking and push notifications.
            mMixpanel = MixpanelAPI.getInstance(context, MIXPANEL_API_TOKEN);
        }
        return mMixpanel;
    }

    public static void initializeMixPanel(Context context) {

        if(MyMixPanel.context==null)
            MyMixPanel.context = context;
        isInit =true;
    }

    public static void createUserID(String UserID) {

        if (isInit && UserID != null) {

            // We also identify the current user with a distinct ID, and
            // register ourselves for push notifications from Mixpanel.

            getInstance().identify(UserID); //this is the distinct_id value that
            // will be sent with events. If you choose not to set this,
            // the SDK will generate one for you

            getInstance().getPeople().identify(UserID); //this is the distinct_id
            // that will be used for people analytics. You must set this explicitly in order
            // to dispatch people data.
        }
    }


    public static void logEvent(String eventName){
        if(!isInit)
            return;
        getInstance().track(eventName, null);
    }

    public static void sendUserLogin(String firstName,String lastName ,String email ){
        if(!isInit)
            return;
        final MixpanelAPI.People people = getInstance().getPeople();
        people.set("$first_name", firstName);
        people.set("$last_name", lastName);
        people.set("$email", email);
        people.set("$UserType", "DeliveryBoy");//Vendor/User

        // We also want to keep track of how many times the user
        // has updated their info.
        people.increment("Update Count", 1L);

        try {
            final JSONObject domainProperty = new JSONObject();
            domainProperty.put("user domain", domainFromEmailAddress(email));
            mMixpanel.registerSuperProperties(domainProperty);
        } catch (final JSONException e) {
            Log.e("MixpanelAPI","Cannot write user email address domain as a super property");
        }
    }


    public static void logOutCurrentUser() {

        if(!isInit)
            return;
        getInstance().reset();
    }

    private static String domainFromEmailAddress(String email) {
        String ret = "";
        final int atSymbolIndex = email.indexOf('@');
        if ((atSymbolIndex > -1) && (email.length() > atSymbolIndex)) {
            ret = email.substring(atSymbolIndex + 1);
        }
        return ret;
    }
}
