package com.nextclick.deliveryboy.utils;

import android.location.Address;
import android.location.Location;

import java.util.List;

public interface LocationListener {

    public void onLocationChanged(Location lastKnownLocation, List<Address> address);
}
