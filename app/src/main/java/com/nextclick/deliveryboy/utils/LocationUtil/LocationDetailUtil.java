package com.nextclick.deliveryboy.utils.LocationUtil;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.model.LatLng;
import com.nextclick.deliveryboy.dashboard.activitys.ui.home.HomeViewModel;
import com.nextclick.deliveryboy.model.DirectionObject;
import com.nextclick.deliveryboy.utils.DirectionListener;
import com.nextclick.deliveryboy.utils.DirectionUtil;

import java.util.List;

public class LocationDetailUtil {

    public static Location CurrentLocation;

    public static void getTravelDistanceFromCurrentLocation(Context context,DirectionListener listener,
                                                            String longitude, String latitude) {
        if (CurrentLocation != null) {
            LatLng origin = new LatLng(CurrentLocation.getLatitude(), CurrentLocation.getLongitude());
            if(latitude !=null && !latitude.equals("null")) {
                try{
                    LatLng destination = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
                    GetDirectionData(context,origin,destination, listener);
                }catch (NumberFormatException e){

                    String[] chars=latitude.split(" ");
                   String latitude1 = chars[0].replace(chars[0].substring(chars[0].length()-1), "");

                   /* StringBuffer sb= new StringBuffer(chars[0]);
                    sb.deleteCharAt(sb.length()-1);

                    String[] chars1=longitude.split(" ");
                    StringBuffer sb1= new StringBuffer(chars1[0]);
                    sb.deleteCharAt(sb1.length()-1);*/
                    String[] chars1=longitude.split(" ");
                   String longitude1 = chars1[0].replace(chars1[0].substring(chars1[0].length()-1), "");

                    LatLng destination = new LatLng(Double.valueOf(latitude1), Double.valueOf(longitude1));
                    GetDirectionData(context,origin,destination, listener);
                }

            }
            else
            {
                listener.onDirectionResult(null);
            }
        }
    }

    public static void GetDirectionData(Context context, LatLng origin, LatLng destination, DirectionListener listener) {
        DirectionUtil.getDirectionData(context, origin, destination, listener);
    }


    public static void setCurrentLocation(Location location) {
        CurrentLocation = location;
    }
    public static void getCurrentLocation(Context context, LifecycleOwner owner)
    {
        HomeViewModel homeViewModel=new HomeViewModel(context);
        homeViewModel.initializeGeoLocationLib();
        final Observer<Location> locObserver = new Observer<Location>() {
            @Override
            public void onChanged(@Nullable final Location location) {
                LocationDetailUtil.CurrentLocation= location;
            }
        };
        homeViewModel.getCurrentLocation().observe(owner, locObserver);
    }
}
